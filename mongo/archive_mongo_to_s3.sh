#!/bin/bash

# ==============================================================================
# Tool that dumps all mongo databases using ``mongodump --dbpath``, compress to
# 5G chunks and then archive to S3
#
# Created on 10/02/2015
# @author: trung
# ==============================================================================


MONGO_LOCK_FILE=$1/mongod.lock

function usage {
    echo "!!! To run with root privillege and ``s3cmd`` configured !!!"
    echo "USAGE: #$0 DBPATH OUTPUT"
    echo DBPATH: PATH TO DATABASE FILES
    echo OUTPUT: OUTPUT DIRECTORY
    exit
}

if [ $# -lt 2 ]; then
    usage
fi

echo "Remove lock file if any at $MONGO_LOCK_FILE"
rm $MONGO_LOCK_FILE

echo "Dumping databases from $1 to $2..."
time mongodump --dbpath $1 --journal -o $2

echo "Compressing dump files..."
time gzip -r9 $2

#echo "Find and break files larger than 5GB into smaller parts..."
#find $2 -type f -size +5G -exec split -b 5G {} {}- \;
#find $2 -type f -size +5G -delete

echo "Sending to S3..."
time s3cmd sync --multipart-chunk-size-mb=5120 $2 s3://bk-database/archive_legacy_mongo/

echo "DONE!"
