#!/bin/bash

time mongodump -h data.ssh.sentifi.com --port 27017 -o data-27017 -d logging
time mongodump -h data.ssh.sentifi.com --port 27017 -o data-27017 -d meta
time mongodump -h data.ssh.sentifi.com --port 27017 -o data-27017 -d screen_counting
time mongodump -h data.ssh.sentifi.com --port 27017 -o data-27017 -d search_items
time mongodump -h data.ssh.sentifi.com --port 27017 -o data-27017 -d social_message

time mongorestore -h ellytran.ssh.sentifi.com --port 27017 data-27017/logging
time mongorestore -h ellytran.ssh.sentifi.com --port 27017 data-27017/meta
time mongorestore -h ellytran.ssh.sentifi.com --port 27017 data-27017/screen_counting
time mongorestore -h ellytran.ssh.sentifi.com --port 27017 data-27017/search_items
time mongorestore -h ellytran.ssh.sentifi.com --port 27017 data-27017/social_message


time mongodump -h data.ssh.sentifi.com --port 27017 -o data-27017 -d analytic -c advertisement
time mongodump -h data.ssh.sentifi.com --port 27017 -o data-27017 -d analytic -c bank_context
time mongodump -h data.ssh.sentifi.com --port 27017 -o data-27017 -d analytic -c bank_score
time mongodump -h data.ssh.sentifi.com --port 27017 -o data-27017 -d analytic -c duplication
time mongodump -h data.ssh.sentifi.com --port 27017 -o data-27017 -d analytic -c report_duplication
time mongodump -h data.ssh.sentifi.com --port 27017 -o data-27017 -d analytic -c logging_data
time mongodump -h data.ssh.sentifi.com --port 27017 -o data-27017 -d analytic -c remove_reason
time mongodump -h data.ssh.sentifi.com --port 27017 -o data-27017 -d analytic -c report_pattern
time mongodump -h data.ssh.sentifi.com --port 27017 -o data-27017 -d analytic -c sem_restore
time mongodump -h data.ssh.sentifi.com --port 27017 -o data-27017 -d analytic -c sharescreen

time mongorestore -h ellytran.ssh.sentifi.com --port 27017 -d analytic data-27017/analytic/advertisement.bson
time mongorestore -h ellytran.ssh.sentifi.com --port 27017 -d analytic data-27017/analytic/bank_context.bson
time mongorestore -h ellytran.ssh.sentifi.com --port 27017 -d analytic data-27017/analytic/bank_score.bson
time mongorestore -h ellytran.ssh.sentifi.com --port 27017 -d analytic data-27017/analytic/duplication.bson
time mongorestore -h ellytran.ssh.sentifi.com --port 27017 -d analytic data-27017/analytic/report_duplication.bson
time mongorestore -h ellytran.ssh.sentifi.com --port 27017 -d analytic data-27017/analytic/logging_data.bson
time mongorestore -h ellytran.ssh.sentifi.com --port 27017 -d analytic data-27017/analytic/remove_reason.bson
time mongorestore -h ellytran.ssh.sentifi.com --port 27017 -d analytic data-27017/analytic/report_pattern.bson
time mongorestore -h ellytran.ssh.sentifi.com --port 27017 -d analytic data-27017/analytic/sem_restore.bson
time mongorestore -h ellytran.ssh.sentifi.com --port 27017 -d analytic data-27017/analytic/sharescreen.bson
