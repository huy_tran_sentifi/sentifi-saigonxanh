#!/usr/bin/env python
# Copyright (C) 2015 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 22/07/2015
# @author: trung

from rediscluster import StrictRedisCluster

import sys


# TODO: replace the busy loop to

startup_nodes = [
    {
        "host": "redis-com-01.ireland.sentifi.internal",
        "port": 6379
    }
]
rc = StrictRedisCluster(startup_nodes=startup_nodes, decode_responses=True)


def set_1m():
    for i in xrange(100000):
        rc.set('an_average_long_key:%s' % i, i)

def get_1m():
    for i in xrange(100000):
        rc.get('an_average_long_key:%s' % i)

def main(options):
    import timeit
    print 'Time elapsed for 1M Set: %s' % timeit.timeit("set_1m()", setup="from __main__ import set_1m")
    print 'Time elapsed for 1M Get: %s' % timeit.timeit("get_1m()", setup="from __main__ import get_1m")

if __name__ == '__main__':
    main(sys.argv[1:])