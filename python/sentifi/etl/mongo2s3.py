# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Jun 30, 2014
# @author: trung

from datetime import date, datetime, timedelta

import gzip
import logging
import os
import uuid

from helpers.s3_utils import upload_from_filename_using_multipart
from helpers.utils import date2datetime


logger = logging.getLogger(__name__)


def export_by_creation_date(creation_date, dict_dao, folder_path):
    '''
    @summary: Export and compress documents of the collection managed by
        ``dict_dao``, created at ``creation_date`` (based on their ObjectId) to
        a file

    @return: Absolute file path, # of document written to the archive
    @rtype : (String, Integer)

    @param creation_date: Date where documents were created
    @type  creation_date: datetime.date

    @param mg_collection: Cursor to a MongoDB's collection
    @type  mg_collection: pymongo.collection.Collection

    @param folder_path: Absolute path to the folder storing the archive
    @type  folder_path: String
    '''
    if not isinstance(creation_date, date):
        raise ValueError('creation_date must be a date instead of %s' % type(
                creation_date))

    start_time = date2datetime(creation_date)
    end_time = start_time + timedelta(days=1)

    doc_list = dict_dao.find_by_time_range(start_time, end_time)

    file_path = os.path.join(folder_path, '%s-%s.gz' % (
        creation_date.strftime('%Y-%m-%d'),
        str(uuid.uuid1())))

    doc_count = 0
    logger.info('Openning file %s...' % file_path)
    with gzip.open(file_path, 'wb') as gzip_file:
        for doc in doc_list:
            if not doc:
                logger.warning('None detected in the list of documents')
                continue
            logger.debug('Compressing and writing document %s...' % doc.get('_id'))
            gzip_file.write(doc.to_json_string())
            gzip_file.write('\n')
            doc_count += 1
    logger.info('%s documents exported to %s' %(doc_count, file_path))
    return file_path, doc_count


def migrate_to_s3_by_creation_date(creation_date, dict_dao, s3_bucket, s3_prefix):
    '''
    @summary: Compress and migrate documents of the collection managed by
        ``dict_dao``, created at ``creation_date`` (based on their ObjectId) to S3

    @return: Absolute file path, # of document written to the archive
    @rtype : (String, Integer)

    @param creation_date: Date where documents were created
    @type  creation_date: datetime.date

    @param mg_collection: Cursor to a MongoDB's collection
    @type  mg_collection: pymongo.collection.Collection

    @param s3_bucket: Connection to a S3 bucket
    @type  s3_bucket: boto.s3.bucket.Bucket

    @param s3_prefix: Prefix (folder) where data is stored in the bucket
    @type  s3_prefix: String
    '''
    logger.info('Exporting document created on %s' %
        creation_date.strftime('%Y-%m-%d')
    )
    file_path, doc_count = export_by_creation_date(
        creation_date=creation_date,
        dict_dao=dict_dao,
        folder_path='/mnt/ephemeral/sentifi'
    )

    if doc_count > 0:
        key_name = '%s/%s/%s.gz' % (
            s3_prefix,
            creation_date.strftime('%Y/%m/%d'),
            datetime.now().strftime('%Y%m%d-%H%M%S')
        )
        bytes_count = upload_from_filename_using_multipart(s3_bucket, key_name, file_path, True)
        os.remove(file_path)
        return key_name, bytes_count
    logger.info('No document found')
    os.remove(file_path)
    return None, 0
