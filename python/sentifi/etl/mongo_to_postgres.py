# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Oct 22, 2014
# @author: trung

from bson import ObjectId

from constant import STATUS_ACTIVE, STATUS_HIDED, STATUS_INACTIVE, \
    STATUS_MACHINE_AUDITED, STATUS_NEW
from helpers.dict_utils import empty_string_to_none
from helpers.utils import oid_hex_to_datetime


def make_publisher_status(mg_publisher):
    publisher_active = mg_publisher.get('active')
    publisher_status = mg_publisher.get('status')
    if publisher_active is True:
        if publisher_status == 0:
            return STATUS_NEW
        elif publisher_status == 1:
            return STATUS_MACHINE_AUDITED
        elif publisher_status == 2:
            return STATUS_ACTIVE
        elif publisher_status == 3:
            return STATUS_HIDED
    elif publisher_active is False:
        return STATUS_INACTIVE
    return None


def publisher_json_from_mongo(mg_publisher, publisher_id=None, object_id=None):
    """Reformat the JSON of a Mongo Publisher
    """
    if not isinstance(mg_publisher, dict):
        raise TypeError('dict expected')

    extras = mg_publisher.get('extras')
    extras = extras if isinstance(extras, dict) else {}
    news = mg_publisher.get('news')
    news = news if isinstance(news, dict) else {}
    organization = mg_publisher.get('organization')
    organization = organization if isinstance(organization, dict) else {}
    tracking = mg_publisher.get('tracking')
    tracking = tracking if isinstance(tracking, dict) else {}
    # Map related fields
    id_ = mg_publisher.get('_id')
    parent_id = mg_publisher.get('parentId')
    d = dict(
        publisher_id=publisher_id,
        status=make_publisher_status(mg_publisher),
        created_at=oid_hex_to_datetime(id_),
        # updated_at=tracking.get('updatedAt'),
        publisher_mongo_id=unicode(id_) if isinstance(id_, ObjectId) else id_,
        parent_publisher_mongo_id=unicode(parent_id) if isinstance(
            parent_id, ObjectId) else parent_id,
        object_id=object_id,
        updated_by_analyst_id=tracking.get('lastUpdateAuthorId'),
        publisher_name=(
            mg_publisher.get('displayName')
            or mg_publisher.get('name')
        ),
        detail=mg_publisher.get('description') or news.get('description'),
        lang=mg_publisher.get('language').split('|') if mg_publisher.get(
            'language') else None,
        publisher_url=(
            news.get('url')
            or extras.get('company_url_blog')
            or organization.get('blogUrl')
            or extras.get('company_url')
            or organization.get('url')
        ),
        photo_url=mg_publisher.get('image'),
        priority=mg_publisher.get('priority'),
        news_aggregator=mg_publisher.get('newsAggregator'),
        is_restricted=extras.get('restricted'),
        is_verified=extras.get('verified'),
        is_virtual=mg_publisher.get('virtual'),
        # released_at=tracking.get('releasedDate'),
        source=tracking.get('source'),
        survey=tracking.get('survey'),
        update_note=tracking.get('updateNote'),
        priority_excluded=mg_publisher.get('priorityExclusive'),
        item_key=extras.get('itemKey'),
        top=extras.get('top')
    )
    empty_string_to_none(d)
    return d


# def publisher_audit_information_json_from_mongo(mg_publisher,
#                                                 publisher_id=None):
#     """Extract PublisherAuditInformation JSON from a Mongo Publisher
#     """
#     if not isinstance(mg_publisher, dict):
#         raise TypeError('dict expected')
#
#     extras = mg_publisher.get('extras')
#     extras = extras if isinstance(extras, dict) else {}
#     tracking = mg_publisher.get('tracking')
#     tracking = tracking if isinstance(tracking, dict) else {}
#     # Map related fields
#     id_ = mg_publisher.get('_id')
#     d = dict(
#         publisher_id=publisher_id,
#         created_at=oid_hex_to_datetime(id_),
#         # updated_at=tracking.get('updatedAt'),
#         analyst_id=tracking.get('auditAuthorId'),
#         last_released_analyst_id=tracking.get('lastReleaseAuthorId'),
#         last_updated_analyst_id=tracking.get('lastUpdateAuthorId'),
#         # released_at=tracking.get('releasedDate'),
#         source=tracking.get('source'),
#         survey=tracking.get('survey'),
#         update_note=tracking.get('updateNote'),
#         priority_exclusive=mg_publisher.get('priorityExclusive'),
#         item_key=extras.get('itemKey'),
#         top=extras.get('top')
#     )
#     _empty_string_to_none(d)
#     # Check for empty record
#     if (d['analyst_id'] or d['last_released_analyst_id'] or d[
#         'last_updated_analyst_id'] or d['source'] or d['survey'] or d[
#         'update_note'] or d['priority_exclusive'] or d['item_key'] or d['top']):
#         return d
#     return None


def publisher_ranking_json_from_mongo(mg_publisher, publisher_id=None):
    """Extract PublisherRanking JSON from a Mongo Publisher
    """
    if not isinstance(mg_publisher, dict):
        raise TypeError('dict expected')

    score = mg_publisher.get('score')
    score = score if isinstance(score, dict) else {}
    # tracking = mg_publisher.get('tracking')
    # tracking = tracking if isinstance(tracking, dict) else {}
    # Map related fields
    id_ = mg_publisher.get('_id')
    d = dict(
        publisher_id=publisher_id,
        created_at=oid_hex_to_datetime(id_),
        # updated_at=tracking.get('updatedAt'),
        alexa_rank_1m=score.get('alexaRank1month'),
        alexa_rank_3m=score.get('alexaRank3months'),
        company_level=score.get('companyLevel'),
        finance_dedication_level=score.get('financeDedicationLevel'),
        klout_score=score.get('klout'),
        moreover_rank=score.get('moreoverRank'),
        profile_quality_level=score.get('profileQualityLevel')
    )
    empty_string_to_none(d)
    # Check for empty record
    if (d['alexa_rank_1m'] or d['alexa_rank_3m'] or d['company_level'] or
            d['finance_dedication_level'] or d['klout_score'] or
            d['moreover_rank'] or d['profile_quality_level']):
        return d
    return None


def person_json_from_mongo(mg_publisher, person_id=None):
    """Extract Person JSON from a Mongo Publisher
    """
    if not isinstance(mg_publisher, dict):
        raise TypeError('dict expected')

    extras = mg_publisher.get('extras')
    extras = extras if isinstance(extras, dict) else {}
    person = mg_publisher.get('person')
    person = person if isinstance(person, dict) else {}
    # tracking = mg_publisher.get('tracking')
    # tracking = tracking if isinstance(tracking, dict) else {}
    # Map related fields
    id_ = mg_publisher.get('_id')
    d = dict(
        person_id=person_id,
        status=int(mg_publisher.get('active')) if mg_publisher.get(
            'active') is not None else None,
        created_at=oid_hex_to_datetime(id_),
        # updated_at=tracking.get('updatedAt'),
        person_name=mg_publisher.get('name') or mg_publisher.get('displayName'),
        title=extras.get('title'),
        first_name=person.get('firstName'),
        last_name=person.get('lastName'),
        detail=mg_publisher.get('description'),
        email=mg_publisher.get('email'),
        address=mg_publisher.get('address'),
        city=mg_publisher.get('city'),
        country_code=mg_publisher.get('countryCode'),
        photo_url=mg_publisher.get('image')
    )
    empty_string_to_none(d)
    return d


def organization_json_from_mongo(mg_publisher, organization_id=None):
    """Extract Organization JSON from a Mongo Publisher
    """
    if not isinstance(mg_publisher, dict):
        raise TypeError('dict expected')

    extras = mg_publisher.get('extras')
    extras = extras if isinstance(extras, dict) else {}
    news = mg_publisher.get('news')
    news = news if isinstance(news, dict) else {}
    organization = mg_publisher.get('organization')
    organization = organization if isinstance(organization, dict) else {}
    # tracking = mg_publisher.get('tracking')
    # tracking = tracking if isinstance(tracking, dict) else {}
    # Map related fields
    id_ = mg_publisher.get('_id')
    d = dict(
        organization_id=organization_id,
        status=int(mg_publisher.get('active')) if mg_publisher.get(
            'active') is not None else None,
        created_at=oid_hex_to_datetime(id_),
        # updated_at=tracking.get('updatedAt'),
        organization_name=(
            mg_publisher.get('name')
            or mg_publisher.get('displayName')
            or news.get('name')
        ),
        detail=mg_publisher.get('description') or news.get('description'),
        address=mg_publisher.get('address'),
        city=mg_publisher.get('city'),
        country_code=mg_publisher.get('countryCode'),
        founded=organization.get('founded'),
        organization_url=(
            news.get('url')
            or extras.get('company_url')
            or organization.get('url')
            or extras.get('company_url_blog')
            or organization.get('blogUrl')
        ),
        photo_url=mg_publisher.get('image')
    )
    empty_string_to_none(d)
    return d


def named_entity_json_from_mongo(mg_publisher, named_entity_id=None,
                                 object_id=None):
    """Extract NamedEntity JSON from a Mongo Publisher
    """
    if not isinstance(mg_publisher, dict):
        raise TypeError('dict expected')

    extras = mg_publisher.get('extras')
    extras = extras if isinstance(extras, dict) else {}
    news = mg_publisher.get('news')
    news = news if isinstance(news, dict) else {}
    organization = mg_publisher.get('organization')
    organization = organization if isinstance(organization, dict) else {}
    tracking = mg_publisher.get('tracking')
    tracking = tracking if isinstance(tracking, dict) else {}
    # Map related fields
    id_ = mg_publisher.get('_id')
    categories = mg_publisher.get('categories')
    category_id = None
    category_level_1 = None
    current_category_level = 0
    for each in categories:
        if each.get('level') > current_category_level and each.get(
                'numericId') > 0:
            category_id = each.get('numericId')
            current_category_level = each.get('level')
        if each.get('level') == 1:
            category_level_1 = each.get('numericId')
    d = dict(
        named_entity_id=named_entity_id,
        status=int(mg_publisher.get('active')) if mg_publisher.get(
            'active') is not None else None,
        created_at=oid_hex_to_datetime(id_),
        # updated_at=tracking.get('updatedAt'),
        old_id=extras.get('itemKey'),
        object_id=object_id,
        category_id=category_id,
        updated_by_analyst_id=tracking.get('lastUpdateAuthorId') or tracking.get('auditAuthorId'),
        named_entity_name=(
            mg_publisher.get('displayName')
            or mg_publisher.get('name')
            or news.get('name')
        ),
        named_entity_type=category_level_1,
        detail=mg_publisher.get('description') or news.get('description'),
        named_entity_url=(
            news.get('url')
            or extras.get('company_url')
            or organization.get('url')
            or extras.get('company_url_blog')
            or organization.get('blogUrl')
        ),
        is_topic=extras.get('itemKey') not in (None, ''),
        company=(
            extras.get('company')
            or extras.get('company_name')
            or extras.get('motherCompanyName')
        )
    )
    empty_string_to_none(d)
    return d


# def named_entity_audit_information_json_from_mongo(mg_publisher,
#                                                    named_entity_id=None):
#     """Extract NamedEntityAuditInformation JSON from a Mongo Publisher
#     """
#     if not isinstance(mg_publisher, dict):
#         raise TypeError('dict expected')
#
#     extras = mg_publisher.get('extras')
#     extras = extras if isinstance(extras, dict) else {}
#     tracking = mg_publisher.get('tracking')
#     tracking = tracking if isinstance(tracking, dict) else {}
#     # Map related fields
#     id_ = mg_publisher.get('_id')
#     d = dict(
#         named_entity_id=named_entity_id,
#         created_at=oid_hex_to_datetime(id_),
#         # updated_at=tracking.get('updatedAt'),
#         analyst_id=tracking.get('auditAuthorId'),
#         company=(
#             extras.get('company')
#             or extras.get('company_name')
#             or extras.get('motherCompanyName')
#         )
#     )
#     _empty_string_to_none(d)
#     # Check for empty record
#     if d['analyst_id'] or d['company']:
#         return d
#     return None
