# Copyright (C) 2015 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 13/08/2015
# @author: trung


def empty_string_to_none(a_dict):
    """Set empty-string to None
    """
    for key in a_dict:
        if isinstance(a_dict[key], basestring) and a_dict[key].strip() == '':
            a_dict[key] = None


def stringify_dict(a_dict):
    """Stringify all dict's values
    """
    if isinstance(a_dict, dict):
        result = {}
        for k in a_dict:
            if not isinstance(a_dict[k], (str, unicode)):
                result[k] = str(a_dict[k])
        return result
    elif a_dict is None:
        return None
    else:
        raise TypeError('Type %s is expected, not %s' % (
            dict.__name__, type(a_dict))
        )


def strip_dict_string_value(a_dict):
    if not isinstance(a_dict, dict):
        raise TypeError('record must be a dict instead of %s' % type(a_dict))

    for key in a_dict:
        if isinstance(a_dict[key], basestring):
            a_dict[key] = a_dict[key].strip()
