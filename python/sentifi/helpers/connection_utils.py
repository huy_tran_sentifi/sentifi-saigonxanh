# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Jun 28, 2014
# @author: trung

from boto.s3.connection import S3Connection
from cassandra.policies import DCAwareRoundRobinPolicy, HostDistance, \
    RetryPolicy, TokenAwarePolicy
from elasticsearch import Elasticsearch
from imp import load_source
from kombu.connection import Connection
from psycopg2 import connect
from pymongo import MongoClient
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider

import logging
import os


logger = logging.getLogger(__name__)
setting = load_source('setting', os.environ['SETTING_FILE'])


#===============================================================================
# CASSANDRA
#===============================================================================
def get_cassandra_connection():
    logger.info('Connecting to Cassandra at %s' % setting.CASSANDRA_HOSTS)
    ap = PlainTextAuthProvider(username=setting.CASSANDRA_USER, password=setting.CASSANDRA_PASSWORD)
    #return Cluster(setting.CASSANDRA_HOSTS, auth_provider=ap, cql_version='3.1.7')
    return Cluster(setting.CASSANDRA_HOSTS, auth_provider=ap)

def get_cas_pr_cluster():
    logger.info('Connecting to Cassandra at %s' % setting.CASSANDRA_HOSTS)
    cluster = Cluster(
        contact_points=setting.CASSANDRA_HOSTS,
        auth_provider=PlainTextAuthProvider(
            username=setting.CASSANDRA_USER,
            password=setting.CASSANDRA_PASSWORD
        ),
        default_retry_policy=RetryPolicy(),
        load_balancing_policy=TokenAwarePolicy(
            DCAwareRoundRobinPolicy(local_dc='Cassandra')
        ),
        protocol_version=3
    )
    return cluster


# ===============================================================================
# ELASTICSEARCH
#===============================================================================
def get_es_connection(timeout=10):
    logger.info('Connecting to Elasticsearch at %s' % setting.ES_HOSTS)
    return Elasticsearch(
        hosts=setting.ES_HOSTS,
        retry_on_timeout=True,
        sniff_on_start=True,
        sniff_on_connection_fail=True,
        sniffer_timeout=60,
        timeout=timeout
    )


#===============================================================================
# MONGO
#===============================================================================
def get_mg_client(host, port):
    logger.info('Connecting to MongoDB %s:%s...' % (host, port))
    return MongoClient(host=host, port=port, document_class=dict)

def get_mg_merged_publisher_client():
    return get_mg_client(setting.MG_MERGED_PUBLISHER_HOST,
                         setting.MG_MERGED_PUBLISHER_PORT)

def get_mg_publisher_connection():
    return get_mg_client(setting.MG_MONGO3_HOST, setting.MG_MONGO3_PORT)

def get_mg_raw_moreover_connection():
    return get_mg_client(setting.MG_RAW_MOREOVER_HOST,
                         setting.MG_RAW_MOREOVER_PORT)

def get_mg_twitter_tweet_connection():
    return get_mg_client(setting.MG_RAW_TWEET_HOST, setting.MG_RAW_TWEET_PORT)

def get_mg_relevant_document_connection():
    return get_mg_client(setting.MG_RELEVANT_DOCUMENT_HOST,
                         setting.MG_RELEVANT_DOCUMENT_PORT)


#===============================================================================
# POSTGRESQL
#===============================================================================
def get_pg_connection(database, user, password, host, port):
    logger.info('Connecting to PostgreSQL %s@%s:%s/%s...' % (user, host, port,
                                                             database))
    return connect(database=database, user=user, password=password, host=host,
                   port=port)

def get_pg_core_connection():
    '''Get a connection to Core DB
    '''
    return get_pg_connection(database=setting.PG_CORE_DATABASE,
                             user=setting.PG_CORE_USER,
                             password=setting.PG_CORE_PASSWORD,
                             host=setting.PG_CORE_HOST,
                             port=setting.PG_CORE_PORT)

def get_pg_core_slave1_connection():
    '''Get a connection to Core DB - Slave 1
    '''
    return get_pg_connection(database=setting.PG_CORE_DATABASE,
                             user=setting.PG_CORE_USER,
                             password=setting.PG_CORE_PASSWORD,
                             host=setting.PG_CORE_SLAVE1_HOST,
                             port=setting.PG_CORE_PORT)

def get_pg_da0_connection():
    '''Get a connection to Data Acquisition DB
    '''
    return get_pg_connection(database=setting.PG_DA0_DATABASE,
                             user=setting.PG_DA0_USER,
                             password=setting.PG_DA0_PASSWORD,
                             host=setting.PG_DA0_HOST,
                             port=setting.PG_DA0_PORT)


#===============================================================================
# RABBITMQ
#===============================================================================
def get_rabbitmq_connection():
    logger.info('Connecting to RabbitMQ %s@%s...' % (
        setting.AMQP_USER, setting.AMQP_HOST))
    return Connection(setting.BROKER_URL)


#===============================================================================
# S3
#===============================================================================
def get_s3_bucket(bucket_name):
    logger.info('Getting bucket %s from S3 connection %s' % (bucket_name,
                                                             setting.S3_KEY_ID))
    conn = S3Connection(aws_access_key_id=setting.S3_KEY_ID,
                        aws_secret_access_key=setting.S3_SECRET_KEY,
                        suppress_consec_slashes=False)
    return conn.get_bucket(bucket_name)
