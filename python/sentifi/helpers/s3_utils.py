# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Jun 30, 2014
# @author: trung

from boto.s3.key import Key

import logging
import os
import traceback


logger = logging.getLogger(__name__)
CHUNK_SIZE = 5 * (2 ** 30) # 5GB


def upload_from_filename(bucket, key_name, file_path, verify_size=True):
    # Get file size (and also check for its existence)
    file_size = os.path.getsize(file_path)

    key = Key(bucket=bucket, name=key_name)
    logger.info('Uploading file %s to S3(%s, %s)...' % (
        file_path, bucket.name, key_name
    ))
    bytes_uploaded = key.set_contents_from_filename(file_path)
    key.close()
    if verify_size:
        if file_size != bytes_uploaded:
            msg = 'Number of bytes written doesn\'t match file size'
            logger.warning(msg)
            raise BytesWarning(msg)
    logger.info('%s bytes uploaded' % bytes_uploaded)
    return bytes_uploaded


def upload_from_filename_using_multipart(bucket, key_name, file_path, verify_size=True):
    # Get file size (and also check for its existence)
    file_size = os.path.getsize(file_path)

    logger.info('Uploading file %s to S3(%s, %s)...' % (
        file_path, bucket.name, key_name
    ))

    mp = bucket.initiate_multipart_upload(key_name)
    bytes_uploaded = 0
    offset = 0
    part_number = 1
    try:
        while offset < file_size:
            with open(file_path, 'r') as file_to_upload:
                file_to_upload.seek(offset)
                if file_size >= CHUNK_SIZE:
                    logger.info('Uploading part %s' % part_number)
                part = mp.upload_part_from_file(fp=file_to_upload, part_num=part_number, size=CHUNK_SIZE)
                bytes_uploaded += part.size
                offset = offset + CHUNK_SIZE
                part_number += 1
        mp.complete_upload()
    except:
        mp.cancel_upload()
        logger.warning('Cancel upload due to exception %s' % traceback.format_exc())
        return -1

    if verify_size:
        if file_size != bytes_uploaded:
            msg = 'Number of bytes written doesn\'t match file size'
            logger.warning(msg)
            raise BytesWarning(msg)
    logger.info('%s bytes uploaded' % bytes_uploaded)
    return bytes_uploaded
