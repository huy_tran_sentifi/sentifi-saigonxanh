# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Jun 5, 2014
# @author: trung

from bson import ObjectId
from datetime import datetime, time


#===============================================================================
# LOGGING HELPER
#===============================================================================
def init_logger(name, log_level=None, log_file=None):
    '''Initiate the logger at ``log_level``. If there is no ``log_file``
    indicated, log to the console

    @param name: Name of module's logger
    @type  name: String

    @param log_level: Severity level of the logger
    @type  log_level: Integer (logging.DEBUG, logging.INFO,...

    @param log_file: Full path to the log file
    @type  log_file: String

    @return: Logger
    @rtype: logging.Logger
    '''
    import logging
    logger = logging.getLogger(name)
    if log_level:
        logger.setLevel(log_level)
    # Format logger records
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(name)s: %(message)s')
    # Setup file logging if ``log_file`` is specified
    if log_file:
        fh = logging.FileHandler(log_file)
        fh.setFormatter(formatter)
        logger.addHandler(fh)
    # Otherwise console logging
    else:
        ch = logging.StreamHandler()
        ch.setFormatter(formatter)
        logger.addHandler(ch)
    return logger


#===============================================================================
# DATE, TIME HELPER
#===============================================================================
def date2datetime(a_date):
    '''Convert a date object to a datetime object

    @param a_date: Date to convert
    @type  a_date: datetime.date

    @return:
    @rtype: datetime.datetime
    '''
    return datetime.combine(a_date, time()) if a_date else None


def datetime2epoch(a_datetime):
    '''Convert a datetime object to epoch time

    @param a_datetime: Datetime to convert
    @type  a_datetime: datetime.datetime

    @return:
    @rtype: Integer
    '''
    return (a_datetime - datetime(1970, 1, 1)).total_seconds() if a_datetime else None


#===============================================================================
# MISC. HELPER
#===============================================================================
def bson2json(row):
    if isinstance(row, dict):
        for k in row:
            if isinstance(row[k], ObjectId):
                row[k] = unicode(row[k])
            elif isinstance(row[k], datetime):
                row[k] = row[k].isoformat()
            elif isinstance(row[k], (dict, list)):
                bson2json(row[k])
    elif isinstance(row, list):
        for i in range(len(row)):
            if isinstance(row[i], ObjectId):
                row[i] = unicode(row[i])
            elif isinstance(row[i], datetime):
                row[i] = row[i].isoformat()
            elif isinstance(row[i], dict):
                bson2json(row[i])


def get_common_item(a, b):
    '''Returns list of common items of a and b
    '''
    return [_ for _ in a if _ in b]


def has_common_item(a, b):
    '''Verifies if list-like a and b has common item(s) using the most efficient
    way to test as stated at: http://stackoverflow.com/questions/3170055/test-if-lists-share-any-items-in-python
    '''
    return any(_ in b for _ in a)


def oid_hex_to_datetime(hex_string):
    """Get the timestamp from the hexstring representing an ObjectId

    :param hex_string: String representing the ObjectId
    :return: datetime.datetime
    """
    return ObjectId(hex_string).generation_time


def chunks(input_sequence, n):
    """ Yield successive n-sized chunks from input_sequence.
    """
    for i in xrange(0, len(input_sequence), n):
        yield input_sequence[i:i + n]
