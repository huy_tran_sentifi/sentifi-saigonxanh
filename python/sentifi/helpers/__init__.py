__author__ = 'diepdt'

from psycopg2 import connect
from elasticsearch import Elasticsearch
from pymongo import MongoClient

# elastic search config
ES_HOST = [
    {'host': 'es0.ssh.sentifi.com', 'port': 9200, 'timeout': 240},
    {'host': 'es8.ssh.sentifi.com', 'port': 9200, 'timeout': 240},
    {'host': 'es9.ssh.sentifi.com', 'port': 9200, 'timeout': 240}
]

# production pg config
PG_HOST_W = 'psql0.ssh.sentifi.com'
PG_HOST_R = 'psql1.ssh.sentifi.com'
PG_DB = 'rest'
PG_USER = 'dbo'
PG_PASS = 'sentifi'
PG_PORT = '5432'

# staging pg config
STAGING_PG_HOST = 'staging.ssh.sentifi.com'
STAGING_PG_DB = 'sentifi_staging'
STAGING_PG_USER = 'dbo'
STAGING_PG_PASS = 'sentifi'
STAGING_PG_PORT = '5432'

# mg config
MG_HOST = 'dev.ssh.sentifi.com'
MG_PORT = 27017


def get_pg_connection(mode='r'):
    pg_host = PG_HOST_R if mode == 'r' else PG_HOST_W
    print 'Connecting to PosgreSQL at %s:%s' % (pg_host, PG_PORT)
    conn = connect(database=PG_DB, user=PG_USER, password=PG_PASS, host=pg_host, port=PG_PORT)
    conn.set_client_encoding('utf-8')
    return conn


def get_staging_pg_connection(mode='w'):
    pg_host = STAGING_PG_HOST if mode == 'w' else STAGING_PG_HOST
    print 'Connecting to PosgreSQL at %s:%s' % (STAGING_PG_HOST, pg_host)
    conn = connect(database=STAGING_PG_DB, user=STAGING_PG_USER, password=STAGING_PG_PASS, host=pg_host,
                   port=STAGING_PG_PORT)
    conn.set_client_encoding('utf-8')
    return conn


def get_es_connection():
    print 'Connecting to Elasticsearch at %s' % ES_HOST
    return Elasticsearch(ES_HOST)


def get_es_local_connection():
    print 'Connecting to Elasticsearch local'
    return Elasticsearch(timeout=3600, retry_on_timeout=True, hosts=[{"host": "192.168.1.81", "port": 9200}])


def get_mg_local_connection():
    print 'Connecting to Mongo local'
    return MongoClient(host='192.168.1.81')


def get_mg_connection():
    print 'Connecting to Mongo %s:%s...' % (MG_HOST, MG_PORT)
    return MongoClient(host=MG_HOST, port=MG_PORT, document_class=dict)

