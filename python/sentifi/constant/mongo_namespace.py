# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Jun 26, 2014
# @author: trung

from constant import DEFAULT_BATCH_SIZE


#===============================================================================
# DATABASE
#===============================================================================
D_ANALYTIC = 'analytic'
D_CORE = 'core'
D_MERGED_PUBLISHER = 'mydb_merged10'
D_RAW_MOREOVER = 'moreover'
D_TWITTER_TWEET = 'sentifi'


#===============================================================================
# COLLECTION
#===============================================================================
C_CATEGORY = 'Category'
C_FACEBOOK = 'Facebook'
C_GOOGLEPLUS = 'GooglePlus'
C_LINKEDIN = 'Linkedin'
C_NEWS = 'News'
C_PUBLISHER = 'Publisher'
C_PUBLISHER_FACEBOOK = 'PublisherFacebook'
C_PUBLISHER_GOOGLEPLUS = 'PublisherGooglePlus'
C_PUBLISHER_LINKEDIN = 'PublisherLinkedin'
C_PUBLISHER_NEWS = 'PublisherNews'
C_PUBLISHER_TWITTER = 'PublisherTwitter'
C_RAW_MOREOVER_DOCUMENT = 'articles_20131127'
C_RELEVANT_DOCUMENT = 'relevant_document'
C_TWITTER = 'Twitter'
C_TWITTER_TWEET = 'tweet'

#===============================================================================
# MISC
#===============================================================================
BATCH_SIZE = DEFAULT_BATCH_SIZE
