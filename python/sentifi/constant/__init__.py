CSV_MULTI_VALUE_DELIMITER = '|'


DEFAULT_BATCH_SIZE = 1000


# List of accepted ``sns_name``
ACCEPTED_SNS_NAME = [
    'blog',
    'fb', # Facebook
    'gp', # GooglePlus
    'li', # LinkedIn
    'news',
    'tw', # Twitter
]


# STATUS
STATUS_MACHINE_AUDITED = 4
STATUS_HIDED = 3
STATUS_NEW = 2
STATUS_ACTIVE = 1  # Or ENABLED
STATUS_INACTIVE = 0  # Or DELETED, DISABLED, REMOVED -> Can't be reused
