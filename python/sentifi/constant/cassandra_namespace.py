# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Dec 18, 2014
# @author: hoan

#===============================================================================
# KEYSPACE/DATABASE
#===============================================================================
K_ANALYTIC = 'analytic'
PUBLISHER_ANALYTICS = 'publisher_ranking'


#===============================================================================
# COLUMNFAMILY/TABLE
#===============================================================================
CF_TOPIC_SNS_SCORE_BY_SNS = 'topic_sns_score_by_sns'
CF_TOPIC_SNS_SCORE_BY_TOPIC = 'topic_sns_score_by_topic'
DAILY_COUNT = 'daily_sns_count'
DAILY_COUNT_ITEM = 'daily_sns_count_by_item'
DAILY_COUNT_PATTERN = 'daily_sns_count_by_pattern'
WEEKLY_COUNT = 'weekly_sns_stats'
WEEKLY_METRIC = 'weekly_metric'
WEEKLY_SCALED_COUNT = 'weekly_sns_scaled_stats'
WEEKLY_SCALED_METRIC = 'weekly_scaled_metric'
METRIC = 'metric'
