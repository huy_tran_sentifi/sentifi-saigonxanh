# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Jul 11, 2014
# @author: trung

# ===============================================================================
# SCHEMA
#===============================================================================
S_MONGO = 'mongo'
S_PUBLIC = 'public'
S_REMOVED = 'removed'
S_STAGING = 'staging'


#===============================================================================
# CORE TABLE
#===============================================================================
# Schema ``mongo``
T_FACEBOOK = '%s.%s' % (S_MONGO, 'mg_facebook')
T_GOOGLE_PLUS = '%s.%s' % (S_MONGO, 'mg_google_plus')
T_LINKEDIN = '%s.%s' % (S_MONGO, 'mg_linkedin')
T_NEWS = '%s.%s' % (S_MONGO, 'mg_news')
T_PUBLISHER_FROM_MONGO = '%s.%s' % (S_MONGO, 'mg_publisher')
T_PUBLISHER_SNS = '%s.%s' % (S_MONGO, 'mg_publisher_sns')
T_TWITTER = '%s.%s' % (S_MONGO, 'mg_twitter')

# Schema ``public``
T_CATEGORY = '%s.%s' % (S_PUBLIC, 'category')
T_MESSAGE = '%s.%s' % (S_PUBLIC, 'message')
T_NAMED_ENTITY = '%s.%s' % (S_PUBLIC, 'named_entity')
T_NAMED_ENTITY_AUDIT_HISTORY = '%s.%s' % (
    S_PUBLIC, 'named_entity_audit_history')
T_NAMED_ENTITY_SNS_LINK = '%s.%s' % (S_PUBLIC, 'named_entity_sns_link')
T_ORGANIZATION = '%s.%s' % (S_PUBLIC, 'organization')
T_PERSON = '%s.%s' % (S_PUBLIC, 'person')
T_PUBLISHER = '%s.%s' % (S_PUBLIC, 'publisher')
T_PUBLISHER_AUDIT_HISTORY = '%s.%s' % (S_PUBLIC, 'publisher_audit_history')
T_PUBLISHER_RANKING = '%s.%s' % (S_PUBLIC, 'publisher_ranking')
T_ROLE = '%s.%s' % (S_PUBLIC, 'role')
T_ROLE_NETWORK = '%s.%s' % (S_PUBLIC, 'role_network')
T_SNS_ACCOUNT = '%s.%s' % (S_PUBLIC, 'sns_account')
T_SNS_CATEGORY = '%s.%s' % (S_PUBLIC, 'sns_category')
T_SNS_ROLE = '%s.%s' % (S_PUBLIC, 'sns_role')


#===============================================================================
# DA TABLE
#===============================================================================
T_TWITTER_TWEET = '%s.%s' % (S_PUBLIC, 'twitter_tweet')
T_TWITTER_USER = '%s.%s' % (S_PUBLIC, 'twitter_user')
