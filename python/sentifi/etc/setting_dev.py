# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Jun 23, 2014
# @author: trung

import os


ENV = 'dev'


#===============================================================================
# CASSANDRA
#==============================================================================
CASSANDRA_HOSTS = [
    'saigonu14.ssh.sentifi.com'
]
CASSANDRA_USER = 'kso'
CASSANDRA_PASSWORD = os.environ['CASSANDRA_PASSWORD']


#===============================================================================
# ELASTICSEARCH
#===============================================================================
ES_HOSTS = [
    {'host': 'es0.eu-west-1.compute.internal', 'port': 9200},
    {'host': 'es8.eu-west-1.compute.internal', 'port': 9200},
    {'host': 'es9.eu-west-1.compute.internal', 'port': 9200}
]


#===============================================================================
# MONGO
#===============================================================================
MG_RELEVANT_DOCUMENT_HOST = 'mongoctrinh.eu-west-1.compute.internal'
MG_RELEVANT_DOCUMENT_PORT = 27017

MG_RAW_MOREOVER_HOST = 'mongo6.eu-west-1.compute.internal'
MG_RAW_MOREOVER_PORT = 27017

MG_RAW_TWEET_HOST = 'mongo6.eu-west-1.compute.internal'
MG_RAW_TWEET_PORT = 27017

MG_MONGO3_HOST = 'staging.eu-west-1.compute.internal'
MG_MONGO3_PORT = 27017

MG_MERGED_PUBLISHER_HOST = 'mongo1x.eu-west-1.compute.internal'
MG_MERGED_PUBLISHER_PORT = 27027


#===============================================================================
# MYSQL
#===============================================================================
MYSQL_HOST = 'websiteinstance.cjnt6uokv0cc.eu-west-1.rds.amazonaws.com'
MYSQL_PORT = 3306
MYSQL_DATABASE = 'website'
MYSQL_USERNAME = 'website'
MYSQL_PASSWORD = os.environ['MYSQL_PASSWORD']


#===============================================================================
# POSTGRESQL
#===============================================================================
PG_CORE_HOST = 'psql-dev-1.ireland.sentifi.internal'
PG_CORE_SLAVE1_HOST = 'psql-dev-1.ireland.sentifi.internal'
PG_CORE_PORT = 5432
PG_CORE_DATABASE = 'rip_%s' % ENV
PG_CORE_USER = 'dbw'
PG_CORE_PASSWORD = os.environ['PG_CORE_PASSWORD']

PG_DA0_HOST = 'psql-dev-1.ireland.sentifi.internal'
PG_DA0_PORT = 5432
PG_DA0_DATABASE = 'da_dev'
PG_DA0_USER = 'dbw'
PG_DA0_PASSWORD = os.environ['PG_DA0_PASSWORD']


#===============================================================================
# RABBITMQ
#===============================================================================
AMQP_USER = 'worker'
AMQP_PASSWORD = os.environ['AMQP_PASSWORD']
AMQP_HOST = [
    'rabbitmq-prod1.eu-west-1.compute.internal:5672',
    'rabbitmq-prod2.eu-west-1.compute.internal:5672'
]
BROKER_LIST = []
for each in AMQP_HOST:
    BROKER_LIST.append('librabbitmq://%s:%s@%s' % (
        AMQP_USER, AMQP_PASSWORD, each))
BROKER_URL = ';'.join(BROKER_LIST)


#===============================================================================
# S3
#===============================================================================
S3_BUCKET_DATABASE_BACKUP = 'bk-database'
S3_PREFIX_RAW_MOREOVER = '%s/raw_moreover' % ENV
S3_PREFIX_RAW_TWEET = '%s/raw_tweet' % ENV

S3_KEY_ID = 'AKIAJ33MOQGKW6XEH24Q'
S3_SECRET_KEY = os.environ['S3_SECRET_KEY']

S3_READ_ONLY_KEY_ID = 'AKIAJGVU5IEDMOQ5ZQYA'
S3_READ_ONLY_SECRET_KEY = os.environ['S3_READ_ONLY_SECRET_KEY']
