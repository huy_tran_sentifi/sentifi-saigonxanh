import logging
import sys
import math
from argparse import ArgumentParser
from multiprocessing import Queue, Process

import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from cassandra.cluster import Cluster
from cassandra.policies import TokenAwarePolicy, DCAwareRoundRobinPolicy, RetryPolicy

from constant.cassandra_namespace import PUBLISHER_ANALYTICS
from constant.cassandra_namespace import WEEKLY_METRIC
from constant.cassandra_namespace import WEEKLY_SCALED_COUNT, WEEKLY_SCALED_METRIC
from sentifi_ranking.utils.dateutils import get_week_date_range
from model.metric import Metric
from model.metric_dao import MetricDao, chunks
from helpers.utils import init_logger

from model.aggregate_cas import Aggregation
from model.aggregate_cas_dao import AggregationDao

CASSANDRA_HOSTS = [
    '10.0.0.251',
    '10.0.0.250',
    '10.0.0.249'
]

NUMBER_OF_PROCESSES = 3
BATCH_SIZE = 3000
WEEK = 'WEEK'

cluster = Cluster(
    CASSANDRA_HOSTS,
    load_balancing_policy=TokenAwarePolicy(DCAwareRoundRobinPolicy()),
    default_retry_policy=RetryPolicy()
    #cql_version='3.1.7'
)

# logger = logging.getLogger('scripts.cassandra.ranking_metrics_scaling')
logger = init_logger(__name__, log_level=logging.INFO)


class Worker(Process):

    def __init__(self, queue, table_name):
        Process.__init__(self)
        logger.info("Initializing connection")
        self._table_name = table_name
        self._cluster = Cluster(
            CASSANDRA_HOSTS,
            load_balancing_policy=TokenAwarePolicy(DCAwareRoundRobinPolicy()),
            default_retry_policy=RetryPolicy()
            #cql_version='3.1.7'
        )
        self._queue = queue
        self._count = 0

    def run(self):
        logger.info("Worker %s started" % self.name)
        # retry 20 times if not working
        for i in range(1, 20):
            try:
                with MetricDao(self._cluster, PUBLISHER_ANALYTICS) as dao:
                    while True:
                        metrics = self._queue.get()

                        if metrics is None:
                            break

                        dao.insert(metrics, table_name=self._table_name)

                        self._count += len(metrics)
                        if self._count % 500000 == 0:
                            logger.info("[Worker %s][[%s] inserts %d metrics" % (self.name, WEEK, self._count))
            except Exception, e:
                logger.error(e)
                logger.info('Retry %d times' % i)
                continue
            break


def insert_aggregation(aggregations):
    logger.info('Start inserting aggregation')
    with AggregationDao(cluster, PUBLISHER_ANALYTICS) as dao:
        dao.insert(aggregations, table_name='weekly_metric_stats')


def scale_column(df, column_name, feature_range=(1, 99)):
    """
    Scaling a column with column_name in a DataFrame df

    Args:
        df (DataFrame): a DataFrame which has columns to be scaled
        column_name (string): the name of the column to be scaled
        feature_range (tuple): the tuple of (min, max) range to scale the column to

    Returns:
        DataFrame: a DataFrame that has the scaled column_name
    """
    # scaling positive and negative sets separately
    pos = df[df[column_name] >= 0][column_name].apply(lambda x: math.log10(math.fabs(x + 1)))
    neg = df[df[column_name] < 0][column_name].apply(lambda x: math.log10(math.fabs(x)))

    # scaling the positive set to the range [1, 100]
    scaler = MinMaxScaler(feature_range=feature_range)
    pos = pd.Series(scaler.fit_transform(pos.tolist()), index=pos.index)

    if len(neg) == 0:
        df['%s_scaled' % column_name] = pos
    else:
        # scaling the negative set to the range [-1, 0)
        scaler = MinMaxScaler(feature_range=(0, 1))
        neg = pd.Series(scaler.fit_transform(neg.tolist()), index=neg.index)
        neg = -neg

        df['%s_scaled' % column_name] = pd.concat([pos, neg])

    return df


def scale_all(metrics_ids, weeks, year):
    global WEEK
    for week in weeks:
        WEEK = week
        start, end = get_week_date_range(week=week, year=year)
        logger.info('Week #%d from [%s] to [%s]' % (week, str(start), str(end)))
        sys.stdout.flush()

        queue_weekly_count = Queue()
        queue_weekly_metric = Queue()

        count = 0
        metrics = []

        for metric_id in metrics_ids:

            with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
                logger.info('Scaling metric #%d' % metric_id)
                logger.info('Querying data')
                df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, metric_id, start=start))

                aggregations = []
                aggregations.append(
                    Aggregation(stat_date=start, metric_id=metric_id, stat_name='min', stat_value=df['stat_value'].min()))
                aggregations.append(
                    Aggregation(stat_date=start, metric_id=metric_id, stat_name='max', stat_value=df['stat_value'].max()))
                insert_aggregation(aggregations)

                logger.info('Scaling data')
                df = scale_column(df, 'stat_value')

                for index, row in df.iterrows():
                    sns_id = str(int(row.sns_id))
                    metrics.append(
                        Metric(sns_name='tw', sns_id=sns_id, metric_id=metric_id,
                               count_date=start, count_value=row['stat_value_scaled']))

                    count += 1
                    if index % 50000 == 0:
                        logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))
                        sys.stdout.flush()

        for chunk in chunks(metrics, NUMBER_OF_PROCESSES):
            queue_weekly_count.put(chunk)
            queue_weekly_metric.put(chunk)

        logger.info('Inserting SCALED VALUE to Cassandra in parallel')
        workers_weekly_count = [Worker(queue_weekly_count, WEEKLY_SCALED_COUNT) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_count]

        workers_weekly_metric = [Worker(queue_weekly_metric, WEEKLY_SCALED_METRIC)
                                 for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_metric]

        for i in range(NUMBER_OF_PROCESSES):
            queue_weekly_count.put(None)
            queue_weekly_metric.put(None)

        c = [each.join() for each in workers_weekly_count]
        c = [each.join() for each in workers_weekly_metric]


def scale_all_with_snsId(metrics_ids, weeks, year, snsIds):
    global WEEK
    for week in weeks:
        WEEK = week
        start, end = get_week_date_range(week=week, year=year)
        logger.info('Week #%d from [%s] to [%s]' % (week, str(start), str(end)))
        sys.stdout.flush()

        queue_weekly_count = Queue()
        queue_weekly_metric = Queue()

        count = 0
        metrics = []

        # for metric_id in metrics_ids:

            # with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
            # logger.info('Scaling metric #%d' % metric_id)
            #     logger.info('Querying data')
            #     df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, metric_id, start=start))
            #
            #     aggregations = []
            #     aggregations.append(
            #         Aggregation(stat_date=start, metric_id=metric_id, stat_name='min', stat_value=df['stat_value'].min()))
            #     aggregations.append(
            #         Aggregation(stat_date=start, metric_id=metric_id, stat_name='max', stat_value=df['stat_value'].max()))
            #     insert_aggregation(aggregations)
            #
            #     logger.info('Scaling data')
            #     df = scale_column(df, 'stat_value')
            #
            #     for index, row in df.iterrows():
            #         sns_id = str(int(row.sns_id))
            #         metrics.append(
            #             Metric(sns_name='tw', sns_id=sns_id, metric_id=metric_id,
            #                    count_date=start, count_value=row['stat_value_scaled']))
            #
            #         count += 1
            #         if index % 50000 == 0:
            #             logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))
            #             sys.stdout.flush()

        with AggregationDao(cluster, PUBLISHER_ANALYTICS) as aggDao:
            df_MinMax = pd.DataFrame(aggDao.get_stat_by_date('weekly_metric_stats', start))
        df = pd.DataFrame()
        with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
            for idx, sns_id in enumerate(snsIds):
                cur_df = pd.DataFrame(dao.get_metrics_by_sns_id(WEEKLY_METRIC, sns_id, metrics_ids, start))
                df = pd.concat([cur_df, df])
            df = df.fillna(0)
            logger.info('Scaling data')
            for metric_id in metrics_ids:
                df_metric = df[df['metric_id'] == metric_id]
                df_metric = df_metric.reset_index()
                df_metric.loc[len(df_metric), 'stat_value'] = float(
                    df_MinMax.loc[(df_MinMax['metric_id'] == metric_id) & (df_MinMax['stat_name'] == 'min'), 'stat_value'])
                df_metric.loc[len(df_metric), 'stat_value'] = float(
                    df_MinMax.loc[(df_MinMax['metric_id'] == metric_id) & (df_MinMax['stat_name'] == 'max'), 'stat_value'])
                df_metric = scale_column(df_metric, 'stat_value')
                df_metric = df_metric[0:len(df_metric) - 3]
                for index, row in df_metric.iterrows():
                    sns_id = str(int(row.sns_id))
                    metrics.append(
                        Metric(sns_name='tw', sns_id=sns_id, metric_id=metric_id,
                               count_date=start, count_value=row['stat_value_scaled']))

                    count += 1
                    if index % 50000 == 0:
                        logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))
                        sys.stdout.flush()
        # print metrics

        for chunk in chunks(metrics, NUMBER_OF_PROCESSES):
            queue_weekly_count.put(chunk)
            queue_weekly_metric.put(chunk)

        logger.info('Inserting SCALED VALUE to Cassandra in parallel')
        workers_weekly_count = [Worker(queue_weekly_count, WEEKLY_SCALED_COUNT) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_count]

        workers_weekly_metric = [Worker(queue_weekly_metric, WEEKLY_SCALED_METRIC)
                                 for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_metric]

        for i in range(NUMBER_OF_PROCESSES):
            queue_weekly_count.put(None)
            queue_weekly_metric.put(None)

        c = [each.join() for each in workers_weekly_count]
        c = [each.join() for each in workers_weekly_metric]


def main(options):
    global WEEK
    global BATCH_SIZE
    global NUMBER_OF_PROCESSES

    parser = ArgumentParser(prog='MetricImporter', description='Tool to scale metrics in Cassandra')
    parser.add_argument('-m', '--metrics', dest='metrics', required=True,
                        nargs='+', type=int,
                        help='A list of metric ids to scale')
    parser.add_argument('-w', '--week', dest='week', required=True,
                        nargs='+', type=int,
                        help='A list of week number to retrieve and scale data')
    parser.add_argument('-y', '--year', dest='year', type=int, required=True,
                        help='The year number to retrieve and scale data')
    parser.add_argument('-b', '--batch', dest='batch_size', type=int,
                        help='Batch size to insert')
    parser.add_argument('-p', '--processes', dest='processes', type=int,
                        help='Number of threads to insert')

    args = parser.parse_args()

    weeks = args.week
    year = args.year
    metrics_ids = args.metrics
    batch_size = args.batch_size
    processes = args.processes

    if batch_size is not None and batch_size > 0:
        BATCH_SIZE = batch_size
    if processes is not None and processes > 0:
        NUMBER_OF_PROCESSES = processes

    print('Scaling [%d metrics] of weeks %s in year %d' % (len(metrics_ids), str(weeks), year))
    sys.stdout.flush()
    scale_all(metrics_ids, weeks, year)


if __name__ == '__main__':
    main(sys.argv[1:])
    # snsIds = ['2743641378', '36102165']
    # scale_all_with_snsId([7,8], [27], 2015, snsIds=snsIds)