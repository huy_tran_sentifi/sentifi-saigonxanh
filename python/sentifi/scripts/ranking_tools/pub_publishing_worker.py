import sys
from logging import INFO, DEBUG
from argparse import ArgumentParser

from pymongo import MongoClient
from kombu import connections, producers, Exchange, BrokerConnection

from helpers.utils import init_logger

logger = init_logger(__name__, log_level=INFO)

logger.info('Initializing params and constants')
BROKER_URL = [
    'amqp://worker:123cayngaycaydem@rabbitmq-prod1.ssh.sentifi.com:5672',
    'amqp://worker:123cayngaycaydem@rabbitmq-prod2.ssh.sentifi.com:5672'
]

verbosity = 1

logger.info('Initializing connections')
client = MongoClient('mongo3.ssh.sentifi.com')
pub_twitter_coll = client['core']['PublisherTwitter']
pub_coll = client['core']['Publisher']
twitter_coll = client['core']['Twitter']


def publish_pub_data(exchange_name):
    count = 0
    exchange = Exchange(name=exchange_name, type='fanout', durable=True)

    logger.info('Publishing data...')
    with producers[connections[BrokerConnection(BROKER_URL)].acquire(block=True)].acquire(block=True) as producer:
        for pub_twitter in pub_twitter_coll.find(timeout=False):
            count += 1
            if count % 2000 == 0:
                logger.debug('%d processed' % count)
            if count % verbosity == 0:
                logger.info('%d processed' % count)

            pub_id = str(pub_twitter['publisherId'])
            twitter_id = str(pub_twitter['twitterId'])
            producer.publish(body=[pub_id, twitter_id],
                             retry=True,
                             exchange=exchange,
                             declare=[exchange])


def main(options):
    global verbosity

    parser = ArgumentParser(prog='PubDataPublishing', description='Indexing publisher scores and meta data to ES')
    parser.add_argument('-e', '--exchange', dest='exchange', type=str, required=True,
                        help='the exchange to publish to')
    parser.add_argument('-v', '--verbose', dest='verbosity', type=int,
                        help='the level of verbosity')
    args = parser.parse_args()

    if args.verbosity:
        verbosity = int(1000000.0 / args.verbosity) if args.verbosity > 2 else 100000
        logger.setLevel(DEBUG)
    else:
        verbosity = 100000
        logger.setLevel(INFO)

    publish_pub_data(args.exchange)


if __name__ == '__main__':
    main(sys.argv[1:])
