from argparse import ArgumentParser
import logging
import sys
from multiprocessing import Queue, Process

from cassandra.cluster import Cluster
from cassandra.policies import TokenAwarePolicy, DCAwareRoundRobinPolicy, RetryPolicy

from constant.cassandra_namespace import PUBLISHER_ANALYTICS, WEEKLY_COUNT, METRIC, WEEKLY_METRIC
from sentifi_ranking.utils.dateutils import get_week_date_range
from sentifi_ranking.collect_data.weekly_klout import get_klout
from sentifi_ranking.collect_data.weekly_twitter_stats import get_reach_stats, get_reach_stats_with_snsId
from sentifi_ranking.collect_data.weekly_interaction_score import get_interaction_score, get_interaction_score_with_snsId
from sentifi_ranking.collect_data.weekly_content_stats import get_content_stats, get_content_stats_with_snsId, get_count_relevant_message, get_count_relevant_message_with_snsId
from model.metric import Metric
from model.metric_dao import MetricDao, chunks

# TODO: use environment variables
CASSANDRA_HOSTS = [
    '10.0.0.251',
    '10.0.0.250',
    '10.0.0.249'
]

NUMBER_OF_PROCESSES = 6
BATCH_SIZE = 3000
COMPONENT = 'COMPONENT'
WEEK = 'WEEK'

logger = logging.getLogger('scripts.cassandra.ranking_metrics_importer')


class Worker(Process):

    def __init__(self, queue, table_name):
        Process.__init__(self)
        logger.info("Initializing connection")
        self._table_name = table_name
        self._cluster = Cluster(
            CASSANDRA_HOSTS,
            load_balancing_policy=TokenAwarePolicy(DCAwareRoundRobinPolicy()),
            default_retry_policy=RetryPolicy()
            #cql_version='3.1.7'
        )
        self._queue = queue
        self._count = 0

    def run(self):
        logger.info("Worker %s started" % self.name)
        with MetricDao(self._cluster, PUBLISHER_ANALYTICS) as dao:
            while True:
                metrics = self._queue.get()

                if metrics is None:
                    break

                dao.insert(metrics, table_name=self._table_name)
                # dao.insert_batch(metrics, table_name=self._table_name, batch_size=BATCH_SIZE, timeout=36000)

                self._count += len(metrics)
                if self._count % 500000 == 0:
                    logger.info("[Worker %s][%s][%s] inserts %d metrics" % (self.name, COMPONENT, WEEK, self._count))


def import_reach(weeks=[43, 44, 45, 46, 47, 48, 49], year=2014):
    global WEEK
    for week in weeks:
        WEEK = str(object=week)
        start, end = get_week_date_range(week=week, year=year)
        logger.info('Week #%d from [%s] to [%s]' % (week, str(start), str(end)))

        df = get_reach_stats(week, year)

        queue_weekly_count = Queue()
        queue_weekly_metric = Queue()

        count = 0
        metrics = []
        for index, row in df.iterrows():
            sns_id = str(int(row.sns_author_id))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=1, count_date=start, count_value=row.favourites))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=2, count_date=start, count_value=row.followers))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=3, count_date=start, count_value=row.friends))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=4, count_date=start, count_value=row.listed))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=5, count_date=start, count_value=row.statuses))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=6, count_date=start, count_value=row.mentions))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=7, count_date=start, count_value=row.cum_mentions))

            count += 7
            if index % 50000 == 0:
                logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))

        for chunk in chunks(metrics, NUMBER_OF_PROCESSES):
            queue_weekly_count.put(chunk)
            queue_weekly_metric.put(chunk)

        logger.info('Inserting REACH to Cassandra in parallel')
        workers_weekly_count = [Worker(queue_weekly_count, WEEKLY_COUNT) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_count]

        workers_weekly_metric = [Worker(queue_weekly_metric, WEEKLY_METRIC) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_metric]

        for i in range(NUMBER_OF_PROCESSES):
            queue_weekly_count.put(None)
            queue_weekly_metric.put(None)

        c = [each.join() for each in workers_weekly_count]
        c = [each.join() for each in workers_weekly_metric]

def import_reach_with_snsId(weeks=[43, 44, 45, 46, 47, 48, 49], year=2014, snsIds=[]):
    global WEEK
    for week in weeks:
        WEEK = str(object=week)
        start, end = get_week_date_range(week=week, year=year)
        logger.info('Week #%d from [%s] to [%s]' % (week, str(start), str(end)))

        df = get_reach_stats_with_snsId(week, year, snsIds=snsIds)

        queue_weekly_count = Queue()
        queue_weekly_metric = Queue()

        count = 0
        metrics = []
        for index, row in df.iterrows():
            sns_id = str(int(row.sns_author_id))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=1, count_date=start, count_value=row.favourites))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=2, count_date=start, count_value=row.followers))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=3, count_date=start, count_value=row.friends))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=4, count_date=start, count_value=row.listed))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=5, count_date=start, count_value=row.statuses))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=6, count_date=start, count_value=row.mentions))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=7, count_date=start, count_value=row.cum_mentions))

            count += 7
            if index % 50000 == 0:
                logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))
        # print metrics
        for chunk in chunks(metrics, NUMBER_OF_PROCESSES):
            queue_weekly_count.put(chunk)
            queue_weekly_metric.put(chunk)

        logger.info('Inserting REACH to Cassandra in parallel')
        workers_weekly_count = [Worker(queue_weekly_count, WEEKLY_COUNT) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_count]

        workers_weekly_metric = [Worker(queue_weekly_metric, WEEKLY_METRIC) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_metric]

        for i in range(NUMBER_OF_PROCESSES):
            queue_weekly_count.put(None)
            queue_weekly_metric.put(None)

        c = [each.join() for each in workers_weekly_count]
        c = [each.join() for each in workers_weekly_metric]

def import_klout(weeks, year):
    global WEEK
    for week in weeks:
        WEEK = str(object=week)
        start_cur_week, end_cur_week = get_week_date_range(week=week, year=year)
        # start, end = get_week_date_range(week=(week + 1), year=year)
        start, end = get_week_date_range(week=week, year=year)
        # end = start + timedelta(days=1) - timedelta(microseconds=1)
        logger.info('Week #%d from [%s] to [%s]' % (week, str(start_cur_week), str(end_cur_week)))

        df = get_klout(start, end)[['id_str', 'value']]

        queue_weekly_count = Queue()
        queue_weekly_metric = Queue()

        count = 0
        metrics = []
        for index, row in df.iterrows():
            sns_id = str(int(row.id_str))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=29, count_date=start_cur_week, count_value=row['value']))
            count += 1
            if index % 50000 == 0:
                logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))

        for chunk in chunks(metrics, NUMBER_OF_PROCESSES):
            queue_weekly_count.put(chunk)
            queue_weekly_metric.put(chunk)

        logger.info('Inserting KLOUT to Cassandra in parallel')
        workers_weekly_count = [Worker(queue_weekly_count, WEEKLY_COUNT) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_count]

        workers_weekly_metric = [Worker(queue_weekly_metric, WEEKLY_METRIC) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_metric]

        for i in range(NUMBER_OF_PROCESSES):
            queue_weekly_count.put(None)
            queue_weekly_metric.put(None)

        c = [each.join() for each in workers_weekly_count]
        c = [each.join() for each in workers_weekly_metric]


def import_interaction(weeks, year=2014, is_fix=False):
    global WEEK
    for week in weeks:
        WEEK = str(object=week)
        start, end = get_week_date_range(week=week, year=year)
        logger.info('Week #%d from [%s] to [%s]' % (week, str(start), str(end)))
        df = get_interaction_score(week, year, is_fix)

        queue_weekly_count = Queue()
        queue_weekly_metric = Queue()

        logger.info('Creating metrics')
        count = 0
        metrics = []
        for index, row in df.iterrows():
            sns_id = str(int(row.sns_author_id))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=8, count_date=start, count_value=row.interaction_score))
            count += 1
            if index % 50000 == 0:
                logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))

        for chunk in chunks(metrics, NUMBER_OF_PROCESSES):
            queue_weekly_count.put(chunk)
            queue_weekly_metric.put(chunk)

        logger.info('Inserting INTERACTION to Cassandra in parallel')
        workers_weekly_count = [Worker(queue_weekly_count, WEEKLY_COUNT) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_count]

        workers_weekly_metric = [Worker(queue_weekly_metric, WEEKLY_METRIC) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_metric]

        for i in range(NUMBER_OF_PROCESSES):
            queue_weekly_count.put(None)
            queue_weekly_metric.put(None)

        c = [each.join() for each in workers_weekly_count]
        c = [each.join() for each in workers_weekly_metric]

def import_interaction_with_snsId(weeks, year=2014, is_fix=False, snsIds=[]):
    global WEEK
    for week in weeks:
        WEEK = str(object=week)
        start, end = get_week_date_range(week=week, year=year)
        logger.info('Week #%d from [%s] to [%s]' % (week, str(start), str(end)))
        df = get_interaction_score_with_snsId(week, year, is_fix, snsIds)

        queue_weekly_count = Queue()
        queue_weekly_metric = Queue()

        logger.info('Creating metrics')
        count = 0
        metrics = []
        for index, row in df.iterrows():
            sns_id = str(int(row.sns_author_id))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=8, count_date=start, count_value=row.interaction_score))
            count += 1
            if index % 50000 == 0:
                logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))

        for chunk in chunks(metrics, NUMBER_OF_PROCESSES):
            queue_weekly_count.put(chunk)
            queue_weekly_metric.put(chunk)

        logger.info('Inserting INTERACTION to Cassandra in parallel')
        workers_weekly_count = [Worker(queue_weekly_count, WEEKLY_COUNT) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_count]

        workers_weekly_metric = [Worker(queue_weekly_metric, WEEKLY_METRIC) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_metric]

        for i in range(NUMBER_OF_PROCESSES):
            queue_weekly_count.put(None)
            queue_weekly_metric.put(None)

        c = [each.join() for each in workers_weekly_count]
        c = [each.join() for each in workers_weekly_metric]

def import_content(weeks=[43, 44, 45, 46, 47, 48, 49], year=2014):
    global WEEK
    for week in weeks:
        WEEK = str(object=week)
        start, end = get_week_date_range(week=week, year=year)
        logger.info('Week #%d from [%s] to [%s]' % (week, str(start), str(end)))
        df = get_content_stats(week, year)

        queue_weekly_count = Queue()
        queue_weekly_metric = Queue()

        logger.info('Creating metrics')
        count = 0
        metrics = []
        for index, row in df.iterrows():
            sns_id = str(int(row.sns_author_id))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=10, count_date=start, count_value=row.original))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=11, count_date=start, count_value=row.reply))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=12, count_date=start, count_value=row.retweet))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=13, count_date=start, count_value=row.retweet2))

            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=15, count_date=start, count_value=row.original_avg))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=16, count_date=start, count_value=row.reply_avg))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=17, count_date=start, count_value=row.retweet_avg))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=18, count_date=start, count_value=row.retweet2_avg))

            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=20, count_date=start, count_value=row.original_cum))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=21, count_date=start, count_value=row.reply_cum))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=22, count_date=start, count_value=row.retweet_cum))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=23, count_date=start, count_value=row.retweet2_cum))

            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=25, count_date=start, count_value=row.original_avg_cum))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=26, count_date=start, count_value=row.reply_avg_cum))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=27, count_date=start, count_value=row.retweet_avg_cum))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=28, count_date=start, count_value=row.retweet2_avg_cum))

            count += 16
            if index % 50000 == 0:
                logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))

        for chunk in chunks(metrics, NUMBER_OF_PROCESSES):
            queue_weekly_count.put(chunk)
            queue_weekly_metric.put(chunk)

        logger.info('Inserting CONTENT to Cassandra in parallel')
        workers_weekly_count = [Worker(queue_weekly_count, WEEKLY_COUNT) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_count]

        workers_weekly_metric = [Worker(queue_weekly_metric, WEEKLY_METRIC) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_metric]

        for i in range(NUMBER_OF_PROCESSES):
            queue_weekly_count.put(None)
            queue_weekly_metric.put(None)

        c = [each.join() for each in workers_weekly_count]
        c = [each.join() for each in workers_weekly_metric]

def import_content_with_snsId(weeks=[43, 44, 45, 46, 47, 48, 49], year=2014, snsIds=[]):
    global WEEK
    for week in weeks:
        WEEK = str(object=week)
        start, end = get_week_date_range(week=week, year=year)
        logger.info('Week #%d from [%s] to [%s]' % (week, str(start), str(end)))
        df = get_content_stats_with_snsId(week, year, snsIds=snsIds)

        queue_weekly_count = Queue()
        queue_weekly_metric = Queue()

        logger.info('Creating metrics')
        count = 0
        metrics = []
        for index, row in df.iterrows():
            sns_id = str(int(row.sns_author_id))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=10, count_date=start, count_value=row.original))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=11, count_date=start, count_value=row.reply))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=12, count_date=start, count_value=row.retweet))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=13, count_date=start, count_value=row.retweet2))

            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=15, count_date=start, count_value=row.original_avg))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=16, count_date=start, count_value=row.reply_avg))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=17, count_date=start, count_value=row.retweet_avg))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=18, count_date=start, count_value=row.retweet2_avg))

            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=20, count_date=start, count_value=row.original_cum))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=21, count_date=start, count_value=row.reply_cum))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=22, count_date=start, count_value=row.retweet_cum))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=23, count_date=start, count_value=row.retweet2_cum))

            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=25, count_date=start, count_value=row.original_avg_cum))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=26, count_date=start, count_value=row.reply_avg_cum))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=27, count_date=start, count_value=row.retweet_avg_cum))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=28, count_date=start, count_value=row.retweet2_avg_cum))

            count += 16
            if index % 50000 == 0:
                logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))
        # print metrics
        for chunk in chunks(metrics, NUMBER_OF_PROCESSES):
            queue_weekly_count.put(chunk)
            queue_weekly_metric.put(chunk)

        logger.info('Inserting CONTENT to Cassandra in parallel')
        workers_weekly_count = [Worker(queue_weekly_count, WEEKLY_COUNT) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_count]

        workers_weekly_metric = [Worker(queue_weekly_metric, WEEKLY_METRIC) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_metric]

        for i in range(NUMBER_OF_PROCESSES):
            queue_weekly_count.put(None)
            queue_weekly_metric.put(None)

        c = [each.join() for each in workers_weekly_count]
        c = [each.join() for each in workers_weekly_metric]


def import_rel_msg(weeks, year=2014):
    global WEEK
    for week in weeks:
        WEEK = str(object=week)
        start, end = get_week_date_range(week=week, year=year)
        logger.info('Week #%d from [%s] to [%s]' % (week, str(start), str(end)))
        df = get_count_relevant_message(start, end)

        queue_weekly_count = Queue()
        queue_weekly_metric = Queue()

        logger.info('Creating metrics')
        count = 0
        metrics = []
        for index, row in df.iterrows():
            sns_id = str(int(row.sns_author_id))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=9, count_date=start, count_value=row.rel_msg))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=14, count_date=start, count_value=row.original))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=19, count_date=start, count_value=row.retweet))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=42, count_date=start, count_value=row.retweet2))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=24, count_date=start, count_value=row.reply))
            count += 5
            if index % 50000 == 0:
                logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))

        for chunk in chunks(metrics, NUMBER_OF_PROCESSES):
            queue_weekly_count.put(chunk)
            queue_weekly_metric.put(chunk)

        logger.info('Inserting REL MSG COUNT to Cassandra in parallel')
        workers_weekly_count = [Worker(queue_weekly_count, WEEKLY_COUNT) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_count]

        workers_weekly_metric = [Worker(queue_weekly_metric, WEEKLY_METRIC) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_metric]

        for i in range(NUMBER_OF_PROCESSES):
            queue_weekly_count.put(None)
            queue_weekly_metric.put(None)

        c = [each.join() for each in workers_weekly_count]
        c = [each.join() for each in workers_weekly_metric]

def import_rel_msg_with_snsId(weeks, year=2014, snsIds=[]):
    global WEEK
    for week in weeks:
        WEEK = str(object=week)
        start, end = get_week_date_range(week=week, year=year)
        logger.info('Week #%d from [%s] to [%s]' % (week, str(start), str(end)))
        df = get_count_relevant_message_with_snsId(start, end, snsIds=snsIds)

        queue_weekly_count = Queue()
        queue_weekly_metric = Queue()

        logger.info('Creating metrics')
        count = 0
        metrics = []
        for index, row in df.iterrows():
            sns_id = str(int(row.sns_author_id))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=9, count_date=start, count_value=row.rel_msg))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=14, count_date=start, count_value=row.original))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=19, count_date=start, count_value=row.retweet))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=42, count_date=start, count_value=row.retweet2))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=24, count_date=start, count_value=row.reply))
            count += 5
            if index % 50000 == 0:
                logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))
        # print metrics
        for chunk in chunks(metrics, NUMBER_OF_PROCESSES):
            queue_weekly_count.put(chunk)
            queue_weekly_metric.put(chunk)

        logger.info('Inserting REL MSG COUNT to Cassandra in parallel')
        workers_weekly_count = [Worker(queue_weekly_count, WEEKLY_COUNT) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_count]

        workers_weekly_metric = [Worker(queue_weekly_metric, WEEKLY_METRIC) for _id in range(NUMBER_OF_PROCESSES)]
        c = [each.start() for each in workers_weekly_metric]

        for i in range(NUMBER_OF_PROCESSES):
            queue_weekly_count.put(None)
            queue_weekly_metric.put(None)

        c = [each.join() for each in workers_weekly_count]
        c = [each.join() for each in workers_weekly_metric]


def main(options):
    global BATCH_SIZE
    global NUMBER_OF_PROCESSES
    global COMPONENT
    global WEEK

    parser = ArgumentParser(prog='MetricImporter', description='Tool to import metrics to Cassandra')
    parser.add_argument('-c', '--component', dest='component', required=True,
                        choices=['interaction', 'reach', 'klout', 'content', 'rel_msg'],
                        help='The component in the publisher ranking model to insert data')
    parser.add_argument('-w', '--week', dest='week', required=True,
                        nargs='+', type=int,
                        help='A list of week number to retrieve and insert data')
    parser.add_argument('-y', '--year', dest='year', type=int, required=True,
                        help='The year number to retrieve and insert data')
    parser.add_argument('-b', '--batch', dest='batch_size', type=int,
                        help='Batch size to insert')
    parser.add_argument('-p', '--processes', dest='processes', type=int,
                        help='Number of threads to insert')

    args = parser.parse_args()

    weeks = args.week
    year = args.year
    component = args.component
    COMPONENT = str(object=component)
    batch_size = args.batch_size
    processes = args.processes

    if batch_size is not None and batch_size > 0:
        BATCH_SIZE = batch_size
    if processes is not None and processes > 0:
        NUMBER_OF_PROCESSES = processes

    logger.info('Inserting [%s] using [%d threads] with batch of [%d metrics] of weeks %s in year %d' % (
        component, NUMBER_OF_PROCESSES, BATCH_SIZE, str(weeks), year))

    if component == 'reach':
        import_reach(weeks, year)
    elif component == 'klout':
        import_klout(weeks, year)
    elif component == 'interaction':
        import_interaction(weeks, year)
    elif component == 'interaction_newpub':
        import_interaction_with_snsId(weeks, year, snsIds)
    elif component == 'content':
        import_content(weeks, year)
    elif component == 'rel_msg':
        import_rel_msg(weeks, year)


if __name__ == '__main__':
    main(sys.argv[1:])
    # snsIds = ['2743641378', '36102165']
    # import_rel_msg_with_snsId([27], 2015, snsIds)
