from datetime import datetime
import schedule
import shlex
import time
import sys
from argparse import ArgumentParser
import subprocess


def wait_parallel_tasks(tasks):
    flags = [0 for i in range(len(tasks))]
    while sum(flags) < len(tasks):
        print 'MAIN sleeping...'
        time.sleep(60 * 5)
        for i, each in enumerate(tasks):
            if each.poll() is not None:
                print 'Finish %d' % i
                flags[i] = 1


def calc_score_pipeline(weeks, year, weeks_list):

    #print "\nINSERTING KLOUT\n"
    #p_klout = subprocess.Popen(shlex.split(
    #    "python sentifi-saigonxanh/python/sentifi/scripts/ranking_tools/ranking_metrics_importer.py -c klout -w %s -y %d" % (weeks, year)))

    print "\nINSERTING RELEVANT MESSAGES\n"
    p_imp_rel_content = subprocess.Popen(shlex.split(
        "python sentifi-saigonxanh/python/sentifi/scripts/ranking_tools/ranking_metrics_importer.py -c rel_msg -w %s -y %d" % (weeks, year)))

    print "\nCALCULATING INTERACTION\n"
    for week in weeks_list:
        p_calc_interaction = subprocess.Popen(shlex.split(
            "python sentifi-ranking/sentifi_ranking/collect_data/weekly_interaction_score.py %d %d" % (week, year)))
        print 'Exit code: ', p_calc_interaction.wait()

    print "\nINSERTING INTERACTION\n"
    p_imp_interaction = subprocess.Popen(shlex.split(
        "python sentifi-saigonxanh/python/sentifi/scripts/ranking_tools/ranking_metrics_importer.py -c interaction -w %s -y %d" % (weeks, year)))
    p_imp_interaction.wait()

    print "\nSCALE INTERACTION\n"
    p_scale_interaction = subprocess.Popen(shlex.split(
        "python sentifi-saigonxanh/python/sentifi/scripts/ranking_tools/ranking_metrics_scaling.py -w %s -y %d -m 8" % (weeks, year)))

    time.sleep(60 * 5)

    print "\nINSERTING REACH\n"
    p_imp_reach = subprocess.Popen(shlex.split(
        "python sentifi-saigonxanh/python/sentifi/scripts/ranking_tools/ranking_metrics_importer.py -c reach -w %s -y %d" % (weeks, year)))
    p_imp_reach.wait()

    print "\nINSERTING CONTENT\n"
    p_imp_content = subprocess.Popen(shlex.split(
        "python sentifi-saigonxanh/python/sentifi/scripts/ranking_tools/ranking_metrics_importer.py -c content -w %s -y %d" % (weeks, year)))
    p_imp_content.wait()

    print "\nSCALE REACH\n"
    p_scale_reach = [None for i in range(7)]
    for i, metric in enumerate([1, 2, 3, 4, 5, 6, 7]):
        p_scale_reach[i] = subprocess.Popen(shlex.split(
            "python sentifi-saigonxanh/python/sentifi/scripts/ranking_tools/ranking_metrics_scaling.py -w %s -y %d -m %d" % (weeks, year, metric)))
        time.sleep(60 * 2)
    wait_parallel_tasks(p_scale_reach)

    print "\nSCALE CONTENT\n"
    p_scale_content = [None for i in range(16)]
    for i, metric in enumerate([10, 11, 12, 13, 15, 16, 17, 18, 20, 21, 22, 23, 25, 26, 27, 28]):
        p_scale_content[i] = subprocess.Popen(shlex.split(
            "python sentifi-saigonxanh/python/sentifi/scripts/ranking_tools/ranking_metrics_scaling.py -w %s -y %d -m %d" % (weeks, year, metric)))
        time.sleep(60 * 2)
    wait_parallel_tasks(p_scale_content)

    print "\nCALC CONTENT\n"
    p_calc_content = subprocess.Popen(shlex.split(
        "python sentifi-saigonxanh/python/sentifi/scripts/ranking_tools/ranking_metrics_calculating.py -c content -w %s -y %d -m 0 -o 0" % (weeks, year)))

    time.sleep(60 * 2)

    print "\nCALC VEL REACH\n"
    p_calc_vel_reach = [None for i in range(6)]
    for i, (metric, out_metric) in enumerate([(1, 30), (2, 31), (3, 32), (4, 33), (5, 34), (6, 35)]):
        p_calc_vel_reach[i] = subprocess.Popen(shlex.split(
            "python sentifi-saigonxanh/python/sentifi/scripts/ranking_tools/ranking_metrics_calculating.py -c vel_reach -w %s -y %d -m %d -o %d" % (weeks, year, metric, out_metric)))
        time.sleep(30)
    wait_parallel_tasks(p_calc_vel_reach)

    print "\nSCALE VEL REACH\n"
    p_scale_vel_reach = [None for i in range(6)]
    for i, metric in enumerate([30, 31, 32, 33, 34, 35]):
        p_scale_vel_reach[i] = subprocess.Popen(shlex.split(
            "python sentifi-saigonxanh/python/sentifi/scripts/ranking_tools/ranking_metrics_scaling.py -w %s -y %d -m %d" % (weeks, year, metric)))
        time.sleep(60)
    wait_parallel_tasks(p_scale_vel_reach)

    print "\nCALC REACH\n"
    p_calc_reach = subprocess.Popen(shlex.split(
        "python sentifi-saigonxanh/python/sentifi/scripts/ranking_tools/ranking_metrics_calculating.py -c reach -w %s -y %d -m 0 -o 0" % (weeks, year)))
    p_calc_reach.wait()

    print "\nCALC SCORE\n"
    p_calc_score = subprocess.Popen(shlex.split(
        "python sentifi-saigonxanh/python/sentifi/scripts/ranking_tools/ranking_metrics_calculating.py -c score -w %s -y %d -m 0 -o 0" % (weeks, year)))
    p_calc_score.wait()

    print "\nSCALE SCORE\n"
    p_scale_score = subprocess.Popen(shlex.split(
        "python sentifi-saigonxanh/python/sentifi/scripts/ranking_tools/ranking_metrics_scaling.py -w %s -y %d -m 38" % (weeks, year)))
    p_scale_score.wait()

    print "\nCALC DELAY SCORE\n"
    p_calc_decay_score = subprocess.Popen(shlex.split(
        "python sentifi-saigonxanh/python/sentifi/scripts/ranking_tools/ranking_metrics_calculating.py -c decay_score -w %s -y %d -m 0 -o 0" % (weeks, year)))
    p_calc_decay_score.wait()

    print "\nCALC PERCENTILE\n"
    p_calc_percentile = subprocess.Popen(shlex.split(
        "python sentifi-saigonxanh/python/sentifi/scripts/ranking_tools/ranking_metrics_calculating.py -c percentile -w %s -y %d -m 39 8 36 37 48 -o 46 45 44 43 47" % (weeks, year)))
    print "\nCALC REPORT\n"
    p_calc_report = subprocess.Popen(shlex.split(
        "python sentifi-saigonxanh/python/sentifi/scripts/ranking_tools/ranking_metrics_calculating.py -c report -w %s -y %d -m 0 -o 0" % (weeks, year)))
    wait_parallel_tasks([p_calc_percentile, p_calc_report])

    print "\nINDEXING\n"
    p_index = subprocess.Popen(shlex.split(
        "python sentifi-saigonxanh/python/sentifi/scripts/ranking_tools/pub_indexor.py -w %s -y %d -v 5 -e 1" % (weeks, year)))
    p_index.wait()

    print "DONE"


def job():
    week = datetime.now().isocalendar()[1] - 1
    weeks = str(week)
    year = datetime.now().isocalendar()[0]
    calc_score_pipeline(weeks, year, [week])


def main(options):
    parser = ArgumentParser(prog='main_prog')
    parser.add_argument('-w', '--week', dest='week',
                        nargs='+', type=int,
                        help='A list of week number to retrieve and scale data')
    parser.add_argument('-y', '--year', dest='year', type=int,
                        help='The year number to retrieve and scale data')
    parser.add_argument('-a', '--auto', dest='auto', type=int,
                        help='Automatic mode, run the current week at the beginning of Monday')
    args = parser.parse_args()

    if args.auto > 0:
        print "\n\n\nSCHEDULING TO RUN EVERY MONDAY\n\n\n"
        schedule.every().monday.at('00:01').do(job)
        while True:
            schedule.run_pending()
            time.sleep(1)
    else:
        weeks = ' '.join([str(week) for week in args.week])
        year = args.year
        calc_score_pipeline(weeks, year, args.week)

if __name__ == '__main__':
    main(sys.argv[1:])
