import gc
# from gc import DEBUG_LEAK
# gc.set_debug(DEBUG_LEAK)

import traceback
from time import sleep
import sys
import math
from datetime import timedelta
from logging import INFO, DEBUG
from argparse import ArgumentParser

from helpers.utils import init_logger
from sentifi_ranking.utils.dateutils import get_week_date_range
from constant.cassandra_namespace import PUBLISHER_ANALYTICS
from constant.cassandra_namespace import WEEKLY_METRIC, WEEKLY_SCALED_METRIC
from model.es_dao import ModelDao
from model.metric_dao import MetricDao, chunks
from model.cassandra_dao.topic_sns_score import TopicSnsScoreDao
from model.topic_sns_score import TopicSnsScore

from cassandra.cluster import Cluster
from cassandra.policies import TokenAwarePolicy, DCAwareRoundRobinPolicy, RetryPolicy
# from pymongo import MongoClient
from elasticsearch import Elasticsearch
from elasticsearch.client import IndicesClient
import pandas as pd

from psycopg2.extras import RealDictCursor
from psycopg2 import connect

logger = init_logger(__name__, log_level=DEBUG)

logger.info('Initializing params and constants')

OCCUPATION_ID = 9
FUNCROLE_ID = 10

LISTS = 'lists'
FAVORITES = 'favorites'
FOLLOWERS = 'followers'
MENTIONS = 'mentions'
REACH_VOLUME = 'reach_volume'
REACH_VELOCITY = 'reach_velocity'
ORIGINAL = 'original'
RETWEET = 'retweet'
REPLY = 'reply'
FIN_EVENT = 'fin_event'
FIN_TOPIC = 'fin_topic'
CONTENT_VOLUME = 'content_volume'
CONTENT_VELOCITY = 'content_velocity'
CONTENT_AVG = 'content_avg'
CONTENT_MESSAGE_SCORE = 'content_message_score'
CONTENT = 'content'
REACH = 'reach'
INTERACTION = 'interaction'
BONUS = 'bonus'
DEFAULT_SCORE = 'default_score'
DECAY_INTERVAL = 'decay_interval'
DECAY_VALUE = 'decay_value'
DECAY_RATE = 'decay_rate'
DECAY_RATE_GURU = 'decay_rate_guru'
NUMBER_OF_PROCESSES = 6

MESSAGE_SCORE_PARAMS = {
    'DEFAULT_MN': 10,
    'DEFAULT_MP': 1,
    'MIN_MNP': 0.0,
    'SUB_MNP': 33340.0,
    'MIN_CR': 0.0,
    'SUB_CR': 100.0,
    'MIN_LANG': 0.0,
    'SUB_LANG': 0.263
}

WEIGHTS = {
    'original': 3.0 / 6,
    'retweet': 1.0 / 6,
    'retweet2': 1.0 / 6,
    'reply': 2.0 / 6,
    'content_avg': 0.6,
    'content_message_score': 0.4,
}

TWEET_TYPE = {
    3: 'retweet2',
    2: 'retweet',
    1: 'reply',
    0: 'original'
}

CASSANDRA_HOSTS = [
    '10.0.0.251',
    '10.0.0.250',
    '10.0.0.249'
]

ES_HOST = [
    {'host': 'es0.ssh.sentifi.com', 'port': 9200, 'retry_on_timeout': True},
    {'host': 'es8.ssh.sentifi.com', 'port': 9200, 'retry_on_timeout': True},
    {'host': 'es9.ssh.sentifi.com', 'port': 9200, 'retry_on_timeout': True}
]

BROKER_URL = [
    'amqp://worker:123cayngaycaydem@rabbitmq-prod1.ssh.sentifi.com:5672',
    'amqp://worker:123cayngaycaydem@rabbitmq-prod2.ssh.sentifi.com:5672'
]

fields = {
    50: 's_vel',
    51: 's_vol',
    52: 's_topic_avg',
    53: 'c_topic_hits',
    54: 's_score'
}

verbosity = 1
error_output = 0
save_path = '/mnt/ranking-data/ranking/'

logger.info('Initializing connections')
cluster = Cluster(
    CASSANDRA_HOSTS,
    load_balancing_policy=TokenAwarePolicy(DCAwareRoundRobinPolicy(local_dc='Cassandra')),
    default_retry_policy=RetryPolicy()
    # cql_version='3.1.7'
)

client_ES = Elasticsearch(ES_HOST, timeout=3600)
tss_dao = TopicSnsScoreDao(cluster, PUBLISHER_ANALYTICS)

# client = MongoClient('mongo3.ssh.sentifi.com')
# pub_coll = client['core']['Publisher']
# twitter_coll = client['core']['Twitter']
# pub_twitter_coll = client['core']['PublisherTwitter']

starting_week, _ = get_week_date_range(6, 2015)

from multiprocessing import Queue, Process


class Worker(Process):

    def __init__(self, queue, table_name):
        Process.__init__(self)
        logger.info("Initializing connection")
        self._table_name = table_name
        self._cluster = Cluster(
            CASSANDRA_HOSTS,
            load_balancing_policy=TokenAwarePolicy(DCAwareRoundRobinPolicy()),
            default_retry_policy=RetryPolicy()
            #cql_version='3.1.7'
        )
        self._queue = queue
        self._count = 0

    def run(self):
        logger.info("Worker %s started" % self.name)
        with MetricDao(self._cluster, PUBLISHER_ANALYTICS) as dao:
            while True:
                metrics = self._queue.get()

                if metrics is None:
                    break

                dao.insert(metrics, table_name=self._table_name)
                # dao.insert_batch(metrics, table_name=self._table_name, batch_size=BATCH_SIZE, timeout=36000)

                self._count += len(metrics)
                if self._count % 500000 == 0:
                    logger.info("[Worker %s][%s][%s] inserts %d metrics" %
                                (self.name, 'COMPONENT', 'WEEK', self._count))


def get_weights():
    """
    Get weighting parameters for scoring model

    Returns:
        dictionary: a dictionary of weights for the scoring model
    """
    decay_interval_guru = 8

    decay_interval = 4
    decay_value = 0.8

    decay_rate = math.pow(decay_value, 1.0 / decay_interval)
    decay_rate_guru = math.pow(decay_value, 1.0 / decay_interval_guru)

    return {
        LISTS: 4.0 / 15,
        FAVORITES: 2.0 / 15,
        FOLLOWERS: 3.0 / 15,
        MENTIONS: 6.0 / 15,
        REACH_VOLUME: 0.4,
        REACH_VELOCITY: 0.6,
        ORIGINAL: 3.0 / 6,
        RETWEET: 1.0 / 6,
        REPLY: 2.0 / 6,
        CONTENT_VOLUME: 0.4,
        CONTENT_VELOCITY: 0.6,
        CONTENT_AVG: 0.6,
        CONTENT_MESSAGE_SCORE: 0.4,
        FIN_EVENT: 0.6,
        FIN_TOPIC: 0.4,
        CONTENT: 5.0 / 19,
        REACH: 8.0 / 19,
        INTERACTION: 4.0 / 19,
        BONUS: 2.0 / 19,
        DEFAULT_SCORE: 20.0,
        DECAY_INTERVAL: decay_interval,
        DECAY_VALUE: decay_value,
        DECAY_RATE: decay_rate,
        DECAY_RATE_GURU: decay_rate_guru
    }

weights = get_weights()

s_scores = {}
s_scores_prev = {}

inserting_topic_scores = {
    50: [],
    51: [],
    52: [],
    53: [],
    54: [],
    'length': 0
}

topic_columns = [
    'sns_name',
    'sns_id',
    'metric_id',
    'scored_at',
    'id',
    'score'
]

topic_final_columns = [
    'id',
    's_score',
    's_score_delta',
    's_vol',
    's_vel',
    's_topic_avg',
    'c_topic_hits'
]


def reset_inserting_topic_scores():
    inserting_topic_scores.clear()
    inserting_topic_scores[50] = []
    inserting_topic_scores[51] = []
    inserting_topic_scores[52] = []
    inserting_topic_scores[53] = []
    inserting_topic_scores[54] = []
    inserting_topic_scores['length'] = 0


def get_pg_connection(database, user, password, host, port):
    logger.info('Connecting to PostgreSQL %s@%s:%s/%s...' % (user, host, port,
                                                             database))
    return connect(database=database, user=user, password=password, host=host,
                   port=port)


def get_topic_scores(start, sns_ids):
    """
    Get the topic scores from a list of publishers' ids of a given time

    Args:
        start (datetime): The starting datetime
        sns_ids (list of str): A list of publisher ids
    Returns:

    """
    results = tss_dao.get_topic_scores_by_sns_ids('topic_sns_score_by_sns', sns_ids, 51, start)
    results2 = tss_dao.get_topic_scores_by_sns_ids('topic_sns_score_by_sns', sns_ids, 54, start)
    scores = {}
    firstPrint = True
    for sns_id, result, result2 in zip(sns_ids, results, results2):
        if type(result[1]) == list and type(result2[1]) == list:
            if len(result2[1]) > 0:
                df2 = pd.DataFrame(result2[1])[['topic_id', 'score']].rename_axis({'topic_id':'id'}, axis=1)
                if len(result[1]) > 0:
                    df1 = pd.DataFrame(result[1])[['topic_id', 'score']].rename_axis({'topic_id':'id'}, axis=1)
                else:
                    df1 = pd.DataFrame(columns=['id', 'score'])
                scores[sns_id] = df1.merge(df2, on='id', how='outer', suffixes=['_vol', '']).fillna(0.0)
        else:
            list1 = []
            list2 = []
            for row in result[1]:
                list1.append(row)
            for row2 in result2[1]:
                list2.append(row2)
            if firstPrint:
                print sns_id, len(list1), len(list2), list1[0:2], list2[0:2]
                firstPrint = False
            # logger.info('Processing sns_id=' + str(sns_id))
            if len(list2) > 0:
                df2 = pd.DataFrame(list2)[['topic_id', 'score']].rename_axis({'topic_id':'id'}, axis=1)
                if len(list1) > 0:
                    df1 = pd.DataFrame(list1)[['topic_id', 'score']].rename_axis({'topic_id':'id'}, axis=1)
                else:
                    df1 = pd.DataFrame(columns=['id', 'score'])
                scores[sns_id] = df1.merge(df2, on='id', how='outer', suffixes=['_vol', '']).fillna(0.0)
                # return df1, df2, scores
    return scores


def cast_params(sns_id, topics, field_name):
    return [(int(topic['id']), sns_id, float(topic[field_name])) for topic in topics]


def add_topics(twitter_id, topics):
    for topic_id, field_name in fields.iteritems():
        inserting_topic_scores[topic_id] += cast_params(twitter_id, topics, field_name)
    inserting_topic_scores['length'] += len(topics)


def calc_score(df, pub_id):
    if 's_vol' in df:
        df['s_vol'] = df['s_vel'] + df['s_vol']
        df['s_score_prev'] = df['s_score']
    else:
        df['s_vol'] = df['s_vel'] + df['score_vol']
        df['s_score_prev'] = df['score']

    df['s_score'] = (weights[CONTENT_MESSAGE_SCORE] *
                     (weights[CONTENT_VOLUME] * df.s_vol +
                         weights[CONTENT_VELOCITY] * df.s_vel) +
                     weights[CONTENT_AVG] * df.s_topic_avg)
    # logger.info('before apply decay, length df ' + str(len(df)))
    df.s_score = df.apply(
        lambda x: x['s_score'] if x['s_score'] > (x['s_score_prev'] * weights[DECAY_RATE]) else x['s_score_prev'] * weights[DECAY_RATE], axis=1)
    # logger.info('after apply decay')
    df['s_score_delta'] = df.s_score - df.s_score_prev

    df = df[((df['s_score']>1) & (df['s_score']<100)) | (df['c_topic_hits']!=0)]
    # logger.info('after filter, length df: ' + str(len(df)))
    return df[topic_final_columns].to_dict('records')


def calc_topic_scores(start, end, prev_topic_scores):
    """
    Get the calculated topic scores for publishers in a given period of time.

    Args:
        start (datetime): The stcalcarting datetime
        end (datetime): The ending datetime
        prev_topic_scores (dict of pandas.DataFrame): A dictionary of (pub_id -> topic_scores DataFrame)
    Returns:

    """

    query = {
        "size": 0,
        "query": {
            "filtered": {
                "query": {
                    "match_all": {}
                },
                "filter": {
                    "and": {
                        "filters": [
                            {
                                "exists": {
                                    "field": "tag"
                                }
                            },
                            {
                                "range": {
                                    "published_at": {
                                        "from": start,
                                        "to": end
                                    }
                                }
                            },
                            {
                                "term": {
                                    "channel": "twitter"
                                }
                            }
                        ]
                    }
                }
            }
        },
        "aggs": {
            "by_publisher": {
                "terms": {
                    "field": "sns_author_id",
                    "size": 0
                },
                "aggs": {
                    "by_topic": {
                        "terms": {
                            "field": "tag",
                            "size": 0
                        },
                        "aggs": {
                            "by_tweet_type": {
                                "terms": {
                                    "field": "channel_meta.tweet_type",
                                    "size": 0
                                },
                                "aggs": {
                                    "content_score": {
                                        "sum": {
                                            "script": ("mn = doc['score_mn'].value; "
                                                       "mp = doc['score_mp'].value; "
                                                       "if (doc['score_mn'].empty) "
                                                       "{mn = %(DEFAULT_MN)d;}; "
                                                       "if (doc['score_mp'].empty) "
                                                       "{mp = %(DEFAULT_MP)d;}; "
                                                       "return 60 * doc['score_cr'].value + "
                                                       "10 * (((mp * mn) - %(MIN_MNP)f) / %(SUB_MNP)f) + "
                                                       "5 * doc['score_long'].value + "
                                                       "20 * doc['score_mimp'].value + "
                                                       "5 * doc['score_lang'].value;") % MESSAGE_SCORE_PARAMS
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    res = client_ES.search(index='message', doc_type='message', body=query, request_timeout=36000, search_type='count')
    result = {}
    count = 0
    for pub in res['aggregations']['by_publisher']['buckets']:
        count += 1
        if count % 100000 == 0:
            logger.debug('%d processed' % count)
        if count % verbosity == 0:
            logger.info('%d processed' % count)
        pub_id = pub['key']
        topic_scores = []
        for topic in pub['by_topic']['buckets']:
            s = 0.0
            a = 0
            for tweet_type in topic['by_tweet_type']['buckets']:
                s += WEIGHTS[TWEET_TYPE[tweet_type['key']]] * tweet_type['content_score']['value']
                a += tweet_type['doc_count']
            avg = s / a
            s_vel = s if s <= 2.0 else math.log(s, 2)
            topic_scores.append(
                {'id': topic['key'], 's_vel': s_vel, 's_topic_avg': avg, 'c_topic_hits': a})
        df = pd.DataFrame(topic_scores)
        df_prev = pd.DataFrame(prev_topic_scores.get(pub_id))
        df_prev = df_prev if len(df_prev) > 0 else None
        if type(df_prev) is pd.core.frame.DataFrame:
            df = df.merge(df_prev, on='id', how='outer', suffixes=['', '_prev'])
            df = df.fillna(0.0)
        else:
            df['score'] = 0.0
            df['score_vol'] = 0.0
            # df = df.rename_axis({'sns_author_id': 'sns_id'}, axis=1)

        topics = calc_score(df, pub_id)
        result[pub_id] = topics
        add_topics(pub_id, topics)

    if len(prev_topic_scores) > 0:
        has_score_ids = set(result.keys())
        logger.info('Processing topic scores from those (%d) who inactive this week' %
                    len(set(prev_topic_scores.keys()) - has_score_ids))
        count = 0
        for pub_id in (set(prev_topic_scores.keys()) - has_score_ids):
            count += 1
            if count % 100000 == 0:
                logger.debug('%d processed' % count)
            if count % verbosity == 0:
                logger.info('%d processed' % count)
            df = pd.DataFrame(prev_topic_scores.get(pub_id))
            if len(df) > 0:
                df = df.fillna(0.0)
                df['s_vel'] = 0.0
                df['s_topic_avg'] = 0.0
                df['c_topic_hits'] = 0

                topics = calc_score(df, pub_id)
                result[pub_id] = topics
                add_topics(pub_id, topics)

    return result


def calc_index_insert_topic_scores(week, year, start, end, all_sns_ids, publishers):
    """
    Get the calculated topic scores for publishers in a given period of time.

    Args:
        start (datetime): The starting datetime
        end (datetime): The ending datetime
        prev_topic_scores (dict of pandas.DataFrame): A dictionary of (pub_id -> topic_scores DataFrame)
    Returns:

    """

    query = {
        "size": 0,
        "query": {
            "filtered": {
                "query": {
                    "match_all": {}
                },
                "filter": {
                    "and": {
                        "filters": [
                            {
                                "exists": {
                                    "field": "tag"
                                }
                            },
                            {
                                "range": {
                                    "published_at": {
                                        "from": start,
                                        "to": end
                                    }
                                }
                            },
                            {
                                "term": {
                                    "channel": "twitter"
                                }
                            }
                        ]
                    }
                }
            }
        },
        "aggs": {
            "by_publisher": {
                "terms": {
                    "field": "sns_author_id",
                    "size": 0
                },
                "aggs": {
                    "by_topic": {
                        "terms": {
                            "field": "tag",
                            "size": 0
                        },
                        "aggs": {
                            "by_tweet_type": {
                                "terms": {
                                    "field": "channel_meta.tweet_type",
                                    "size": 0
                                },
                                "aggs": {
                                    "content_score": {
                                        "sum": {
                                            "script": ("mn = doc['score_mn'].value; "
                                                       "mp = doc['score_mp'].value; "
                                                       "if (doc['score_mn'].empty) "
                                                       "{mn = %(DEFAULT_MN)d;}; "
                                                       "if (doc['score_mp'].empty) "
                                                       "{mp = %(DEFAULT_MP)d;}; "
                                                       "return 60 * doc['score_cr'].value + "
                                                       "10 * (((mp * mn) - %(MIN_MNP)f) / %(SUB_MNP)f) + "
                                                       "5 * doc['score_long'].value + "
                                                       "20 * doc['score_mimp'].value + "
                                                       "5 * doc['score_lang'].value;") % MESSAGE_SCORE_PARAMS
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    res = client_ES.search(index='message', doc_type='message', body=query, request_timeout=36000, search_type='count')
    result = {}
    sns_ids = []
    count = 0
    has_score_ids = set()
    time_str = '[w%d][y%d] ' % (week, year)
    start_prev = start - timedelta(days=7)
    prev_topic_scores = {}
    # client_ES.index()
    PUB_NUM = 50000
    for idx, pub in enumerate(res['aggregations']['by_publisher']['buckets']):

        if idx % PUB_NUM == 0:
            del sns_ids[:]
            pubList = res['aggregations']['by_publisher']['buckets'][idx:idx + PUB_NUM]
            for p in pubList:
                sns_ids.append(p['key'])
            prev_topic_scores.clear()
            # logger.info('before get topic score with ' + str(idx))
            prev_topic_scores = get_topic_scores(start_prev, sns_ids)
            # logger.info('after get topic score with ' + str(idx))
        count += 1
        pub_id = pub['key']
        has_score_ids.add(pub_id)
        topic_scores = []
        for topic in pub['by_topic']['buckets']:
            s = 0.0
            a = 0
            for tweet_type in topic['by_tweet_type']['buckets']:
                s += WEIGHTS[TWEET_TYPE[tweet_type['key']]] * tweet_type['content_score']['value']
                a += tweet_type['doc_count']
            avg = s / a
            s_vel = s if s <= 2.0 else math.log(s, 2)
            topic_scores.append(
                {'id': topic['key'], 's_vel': s_vel, 's_topic_avg': avg, 'c_topic_hits': a})
        df = pd.DataFrame(topic_scores)
        # logger.info('length df after ES: ' + str(len(df)))
        df_prev = pd.DataFrame(prev_topic_scores.get(pub_id))
        # logger.info('length dfpre after ES ' + str(len(df_prev)))
        df_prev = df_prev if len(df_prev) > 0 else None
        if type(df_prev) is pd.core.frame.DataFrame:
            df = df.merge(df_prev, on='id', how='outer', suffixes=['', '_prev'])
            df = df.fillna(0.0)
        else:
            df['score'] = 0.0
            df['score_vol'] = 0.0
            # df = df.rename_axis({'sns_author_id': 'sns_id'}, axis=1)
        # logger.info('processing : ' + str(pub_id))
        # logger.info('**** before calculating score ' + str(len(df)))
        topics = calc_score(df, pub_id)
        # logger.info('**** after calculating score' + str(len(df)))
        result[pub_id] = topics
        add_topics(pub_id, topics)
    #####################################Start indexing and inserting#########
        if count % PUB_NUM == 0:
            logger.debug('%d processed' % count)
            logger.info(time_str + 'Indexing publishers and topics to ES')
            index_publishers_in_topic_scores(week, year, publishers, result)
            result.clear()
            gc.collect()
            logger.info(time_str + 'DONE')
            logger.info(time_str + 'Inserting topics to Cassandra')
            insert_topic_scores(start)
            reset_inserting_topic_scores()
            gc.collect()
            logger.info(time_str + 'DONE')
        if count % verbosity == 0:
            logger.info('%d processed' % count)

    # for pub in res['aggregations']['by_publisher']['buckets']:
    #     has_score_ids.add(pub['key'])
    # logger.info('Total Number of pubs that have topic score this week: %d' % len(has_score_ids))
    # count = len(has_score_ids)

    if len(all_sns_ids) > 0:
        remainList = list(set(all_sns_ids) - has_score_ids)
        # has_score_ids = set(result.keys())
        logger.info('Processing topic scores from those (%d) who inactive this week' %
                    len(remainList))
        # count = 0
        noscore = 0
        for idxx, pub_id in enumerate(remainList):
            if idxx % PUB_NUM == 0:
                del sns_ids[:]
                pubList = remainList[idxx:idxx + PUB_NUM]
                for sns_id in pubList:
                    sns_ids.append(sns_id)
                prev_topic_scores.clear()
                # logger.info('before get topic score with ' + str(idxx))
                prev_topic_scores = get_topic_scores(start_prev, sns_ids)
                # logger.info('after get topic score with ' + str(idxx))
            count += 1
            if count % PUB_NUM == 0:
                logger.debug('%d processed' % count)
                logger.info(time_str + 'Indexing publishers and topics to ES: %d' % len(result))
                index_publishers_in_topic_scores(week, year, publishers, result)
                result.clear()
                gc.collect()
                logger.info(time_str + 'DONE')
                logger.info(time_str + 'Inserting topics to Cassandra')
                insert_topic_scores(start)
                reset_inserting_topic_scores()
                gc.collect()
                logger.info(time_str + 'DONE')
            if count % verbosity == 0:
                logger.info('%d processed' % count)

            df = pd.DataFrame(prev_topic_scores.get(pub_id))
            if len(df) > 0:
                df = df.fillna(0.0)
                df['s_vel'] = 0.0
                df['s_topic_avg'] = 0.0
                df['c_topic_hits'] = 0

                topics = calc_score(df, pub_id)
                result[pub_id] = topics
                add_topics(pub_id, topics)
            else:
                result[pub_id] = {}
                noscore += 1
                if noscore < 10:
                    logger.info('********* warning: ****** no score ')
                elif noscore % 10000 == 0:
                    logger.info('********** number of no score********* %d' % noscore)
        logger.info('*********total number of no score ******* %d' % noscore)

    logger.debug('%d LAST processed' % count)
    logger.info(time_str + 'Indexing publishers and topics to ES')
    index_publishers_in_topic_scores(week, year, publishers, result)
    result.clear()
    gc.collect()
    logger.info(time_str + 'DONE')
    logger.info(time_str + 'Inserting topics to Cassandra')
    insert_topic_scores(start)
    reset_inserting_topic_scores()
    gc.collect()
    logger.info(time_str + 'DONE')
    return result


def load_sentifi_scores(start, start_prev):
    _scores = {
        'sentifi': {},
        'klout': {},
        'followers': {},
        'sentifi_prev': {},
        'sentifi_raw': {},
        'sentifi_scaled': {},
        'reach': {},
        'content': {},
        'interaction': {}
    }
    with MetricDao(cluster, PUBLISHER_ANALYTICS) as metric_dao:
        for i in range(0, 50):
            try:
                logger.info('Loading s_sentifi from Cassandra')
                _scores['sentifi'] = {each['sns_id']: each['stat_value']
                                      for each in metric_dao.get_metrics_by_date(WEEKLY_METRIC, 39, start=start)}
                logger.info('Loading s_klout from Cassandra')
                _scores['klout'] = {each['sns_id']: each['stat_value']
                                    for each in metric_dao.get_metrics_by_date(WEEKLY_METRIC, 29, start=start)}
                logger.info('Loading c_followers from Cassandra')
                _scores['followers'] = {each['sns_id']: each['stat_value']
                                        for each in metric_dao.get_metrics_by_date(WEEKLY_METRIC, 2, start=start)}
                logger.info('Loading s_sentifi_prev from Cassandra')
                _scores['sentifi_prev'] = {each['sns_id']: each['stat_value']
                                           for each in metric_dao.get_metrics_by_date(WEEKLY_METRIC, 39, start=start_prev)}
                logger.info('Loading s_sentifi_raw from Cassandra')
                _scores['sentifi_raw'] = {each['sns_id']: each['stat_value']
                                          for each in metric_dao.get_metrics_by_date(WEEKLY_METRIC, 38, start=start)}
                logger.info('Loading s_sentifi_scaled from Cassandra')
                _scores['sentifi_scaled'] = {each['sns_id']: each['stat_value']
                                             for each in metric_dao.get_metrics_by_date(WEEKLY_SCALED_METRIC, 38, start=start)}
                logger.info('Loading reach from Cassandra')
                _scores['reach'] = {each['sns_id']: each['stat_value']
                                    for each in metric_dao.get_metrics_by_date(WEEKLY_METRIC, 36, start=start)}
                logger.info('Loading content from Cassandra')
                _scores['content'] = {each['sns_id']: each['stat_value']
                                      for each in metric_dao.get_metrics_by_date(WEEKLY_METRIC, 37, start=start)}
                logger.info('Loading interaction from Cassandra')
                _scores['interaction'] = {each['sns_id']: each['stat_value']
                                          for each in metric_dao.get_metrics_by_date(WEEKLY_SCALED_METRIC, 8, start=start)}
                logger.info('DONE Loading from Cassandra')
                return _scores
            except Exception, e:
                logger.info(str(e))
                sleep(2)
                continue
    return None


def read_scores_by_sns_id(twitter_id):
    s_sentifi = s_scores['sentifi'][twitter_id] if twitter_id in s_scores['sentifi'] else None
    s_klout = s_scores['klout'][twitter_id] if twitter_id in s_scores['klout'] else None
    c_followers = s_scores['followers'][twitter_id] if twitter_id in s_scores['followers'] else None
    s_sentifi_prev = s_scores['sentifi_prev'][twitter_id] \
        if twitter_id in s_scores['sentifi_prev'] else None
    s_sentifi_raw = s_scores['sentifi_raw'][twitter_id] if twitter_id in s_scores['sentifi_raw'] else None
    s_sentifi_scaled = s_scores['sentifi_scaled'][twitter_id] if twitter_id in s_scores['sentifi_scaled'] else None
    s_reach = s_scores['reach'][twitter_id] if twitter_id in s_scores['reach'] else None
    s_content = s_scores['content'][twitter_id] if twitter_id in s_scores['content'] else None
    s_interaction = s_scores['interaction'][twitter_id] if twitter_id in s_scores['interaction'] else None
    return s_sentifi, s_sentifi_prev, s_klout, c_followers, s_sentifi_raw, s_sentifi_scaled, s_reach, s_content, s_interaction


country_fix = {
    u' ': '',
    u'': '',
    u'0': '',
    u'AU\n': 'AU',
    u'C': '',
    u'CA/ US': 'US',
    u'CO\n': 'CO',
    u'FOUNDLAND': '',
    u'FR\n': 'FR',
    u'GB.': 'GB',
    u'GB0': 'GB',
    u'GB\n': 'GB',
    u'I N': 'IN',
    u'INDIA/US': 'US',
    u'L': '',
    u'NONE': '',
    u'NZ\n': 'NZ',
    u'RADOR': '',
    u'S': '',
    u'SE\n': 'SE',
    u'SGP': 'SG',
    u'TH\n': 'TH',
    u'TTT': 'TT',
    u'UAE': 'AE',
    u'UK': 'GB',
    u'US ': 'US',
    u'USA': 'US',
    u'US\n': 'US',
    u'XNE': ''}


def fix_isocode(isocode):
    code = isocode.upper() if isinstance(isocode, basestring) else ''
    code = country_fix[code] if code in country_fix else code
    return code.lower()


def get_pub_roles():
    conn_dev = get_pg_connection('sentifi_dev', 'dbw', 'sentifi', 'psql-dev-1.ireland.sentifi.internal', 5432)

    with conn_dev.cursor(cursor_factory=RealDictCursor) as cur:
        query = """
            SELECT  publisher_mongo_id,
                    json_agg(r.role_id_path) as role_paths,
                    json_agg(r.role_name) as role_names
            FROM    publisher_role pr JOIN
                    role r ON pr.role_id=r.role_id
            GROUP BY pr.publisher_mongo_id;
        """
        cur.execute(query)
        conn_dev.commit()
        pub_roles = {pub_role['publisher_mongo_id']: {
            'names': pub_role['role_names'], 'paths': pub_role['role_paths']} for pub_role in cur}

    conn_dev.close()
    return pub_roles


def get_sns_roles():
    conn = get_pg_connection('rest', 'dbr', 'sentifi', 'psql1.ssh.sentifi.com', 5432)

    with conn.cursor(cursor_factory=RealDictCursor) as cur:
        query = """
            SELECT  sns_id,
                    json_agg(r.role_id_path) as role_paths,
                    json_agg(r.role_name) as role_names
            FROM    sns_role sr JOIN
                    role r ON sr.role_id=r.role_id
            GROUP BY sr.sns_id;
        """
        cur.execute(query)
        conn.commit()
        sns_roles = {sns_role['sns_id']: {
            'names': sns_role['role_names'], 'paths': sns_role['role_paths']} for sns_role in cur}

    conn.close()
    return sns_roles


BATCH_SIZE = 30000


def get_category(conn):
    query = """
        SELECT  category_mongo_id AS category_mongo_id,
                parent_category_mongo_id AS parent_category_mongo_id,
                category_name AS name,
                category_level AS level,
                category_id AS category_id,
                parent_category_id AS parent_category_id,
                get_category_id_path_by_id(category_id) as cat_path
        FROM    category
    """
    with conn.cursor(cursor_factory=RealDictCursor) as cursor:
        cursor.execute(query)
        conn.commit()
        return {each['category_id']: each for each in cursor}


def get_twitter_data_with_cat(conn):
    result = None
    from_object_id = ''
    logger.info('Executing in batch')
    with conn.cursor(cursor_factory=RealDictCursor) as cursor:
        statement = '''
        SELECT      s.sns_id as sns_id,
                    s.display_name AS name,
                    s.country_code AS countrycode,
                    s.sns_tag AS tags,
                    tw.object_payload->>'id_str' AS twitterid,
                    tw.object_payload->>'screen_name' AS screen_name,
                    tw.object_payload->>'profile_image_url' AS profile_image_url,
                    tw.object_payload->>'description' AS description,
                    scat.category_id as category
        FROM        mg_twitter tw JOIN sns_account s
                        ON tw.object_payload->>'id_str' = s.sns_id
                        LEFT JOIN sns_category scat
                            ON (s.sns_name = scat.sns_name AND s.sns_id = scat.sns_id)
        WHERE       s.sns_name = 'tw' AND s.sns_id > %s
        ORDER BY    s.sns_id
        LIMIT       %s
        '''
        query = cursor.mogrify(statement, (from_object_id, BATCH_SIZE))
        # logger.info('Executing %s' % query)
        cursor.execute(query)
        conn.commit()
        counter = 0
        logger.info('Executing batch #%s' % counter)
        while cursor and cursor.rowcount > 0:
            for result in cursor:
                from_object_id = result.get('sns_id')
                yield result
            query = cursor.mogrify(statement, (from_object_id, BATCH_SIZE))
            logger.info('Executing batch #%s' % counter)
            cursor.execute(query)
            conn.commit()
            counter += 1
            logger.info('Processed %s Publishers' % (counter * BATCH_SIZE))


def get_all_publishers_data(week, year, start):
    logger.info("Get profile roles from postgres")
    # pub_roles = get_pub_roles()
    sns_roles = get_sns_roles()

    twitter_errors = []
    pub_errors = []
    sns_ids = []
    all_publishers = []
    count = 0

    logger.info("Get profile from postgres")
    conn = get_pg_connection('rest', 'dbr', 'sentifi', 'psql1.ssh.sentifi.com', 5432)
    # conn = get_pg_connection('sentifi_dev', 'dbw', 'sentifi', 'psql-dev-1.ireland.sentifi.internal', 5432)
    logger.info("Get categories")
    categories = get_category(conn)

    for pub_twitter in get_twitter_data_with_cat(conn):
        count += 1
        if count % 100000 == 0:
            logger.debug('%d publishers processed' % count)
        if count % verbosity == 0:
            logger.info('%d publishers processed' % count)
        # pub_id = pub_twitter['publisherid']
        twitter_id = str(pub_twitter['twitterid'])
        tags = pub_twitter['tags']
        tags = tags if tags else []
        tags.append('fmi')
        if 'fip' in tags:
            tags.append('fmp')
        tags = list(set(tags))
        # roles = pub_roles.get(pub_id, None)
        roles = sns_roles.get(twitter_id, None)
        func_roles = []
        occupations = []
        if roles:
            for each in zip(*roles.values()):
                if FUNCROLE_ID in each[0]:
                    func_roles.append({'name': each[1], 'role_ids': each[0]})
                if OCCUPATION_ID in each[0]:
                    occupations.append({'name': each[1], 'role_ids': each[0]})

        name = pub_twitter['name'] if 'name' in pub_twitter else ''
        isocode = pub_twitter['countrycode'] if 'countrycode' in pub_twitter else ''
        try:
            cat_paths = [categories[cat_id] for cat_id in categories[pub_twitter['category']]['cat_path']]

            cats = [{'id': cat['category_id'],
                     'parent_id': cat['parent_category_id'],
                     'mongo_id': cat['category_mongo_id'],
                     'parent_mongo_id': cat['parent_category_mongo_id'],
                     'name': cat['name'],
                     'level': cat['level']} for cat in cat_paths]
            # cats = [{'id': str(cat['_id']),
            #          'parent_id': str(cat['parentid']),
            #          'name': cat['name'],
            #          'level': cat['level']} for cat in categories[pub_twitter['category']] if pub_twitter['category'] in categories]
        except:
            cats = None

        s_sentifi, s_sentifi_prev, s_klout, c_followers, s_sentifi_raw, s_sentifi_scaled, s_reach, s_content, s_interaction = read_scores_by_sns_id(
            twitter_id)

        s_sentifi_delta = (s_sentifi - s_sentifi_prev
                           if s_sentifi is not None and
                           s_sentifi_prev is not None and
                           s_sentifi_prev is not None else None)
        s_klout_delta = s_sentifi - s_klout if s_klout is not None and s_sentifi is not None else None

        try:
            all_publishers.append({
                'week': week,
                'year': year,
                'start_date': start,
                'channel_meta': {
                    'sns_name': 'tw',
                    'sns_id': twitter_id,
                    'screen_name': pub_twitter['screen_name'],
                    'name': name,
                    'photo_url': pub_twitter['profile_image_url'] if 'profile_image_url' in pub_twitter else '',
                    'description': pub_twitter['description'] if 'description' in pub_twitter else '',
                },
                'score': {
                    's_sentifi': s_sentifi,
                    's_sentifi_prev': s_sentifi_prev,
                    's_sentifi_delta': s_sentifi_delta,
                    's_klout': s_klout,
                    's_klout_delta': s_klout_delta,
                    'c_followers': c_followers,
                    's_sentifi_raw': s_sentifi_raw,
                    's_sentifi_scaled': s_sentifi_scaled,
                    's_reach': s_reach,
                    's_content': s_content,
                    's_interaction': s_interaction
                },
                'filters': {
                    'isocode': fix_isocode(isocode),
                    'categories': cats,
                    'tags': tags,
                    'occupations': occupations,
                    'func_roles': func_roles
                }
            })
            sns_ids.append(twitter_id)
        except:
            if error_output > 0:
                if 'profile_image_url' in pub_twitter:
                    logger.error(traceback.format_exc())
                    logger.error(pub_twitter)
            continue

    if twitter_errors:
        logger.error('Cannot find [%d] twitter accounts' % len(twitter_errors))
        # logger.error('Cannot find twitter account [%s]' % ','.join(twitter_errors))
    if pub_errors:
        logger.error('Cannot find [%d] publisher data' % len(pub_errors))
        # logger.error('Cannot find publisher data [%s]' % ','.join(pub_errors))

    # client.close()
    conn.close()
    return all_publishers, sns_ids


def get_all_publishers_data_OLD(week, year, start):
    logger.info("Get profile roles from postgres")
    # pub_roles = get_pub_roles()
    sns_roles = get_sns_roles()
    twitter_errors = []
    pub_errors = []
    sns_ids = []
    all_publishers = []
    count = 0

    logger.info("Get profile from postgres")
    conn = get_pg_connection('rest', 'dbr', 'sentifi', 'psql0.ssh.sentifi.com', 5432)
    with conn.cursor(cursor_factory=RealDictCursor) as cur:
        query = """
            SELECT  pub.object_payload->>'name' as name,
                    pub.object_payload->>'countryCode' as countrycode,
                    pub.object_payload->'categories' as categories,
                    pubsns.sns_id as twitterid,
                    tw.object_payload->>'screen_name' as screen_name,
                    tw.object_payload->>'profile_image_url' as profile_image_url,
                    tw.object_payload->>'description' as description,
                    pub.object_id as publisherid
            FROM    (mg_publisher_sns_clean pubsns JOIN
                    mg_publisher pub ON pub.object_id = pubsns.publisher_mongo_id) JOIN
                    mg_twitter tw ON pubsns.sns_id = tw.object_payload->>'id_str'
            WHERE pubsns.sns_name = 'tw';
        """
        cur.execute(query)
        conn.commit()
        for pub_twitter in cur:
            count += 1
            if count % 100000 == 0:
                logger.debug('%d publishers processed' % count)
            if count % verbosity == 0:
                logger.info('%d publishers processed' % count)
            # pub_id = pub_twitter['publisherid']
            twitter_id = str(pub_twitter['twitterid'])

            # roles = pub_roles.get(pub_id, None)
            roles = sns_roles.get(twitter_id, None)
            func_roles = []
            occupations = []
            if roles:
                for each in zip(*roles.values()):
                    if FUNCROLE_ID in each[0]:
                        func_roles.append({'name': each[1], 'role_ids': each[0]})
                    if OCCUPATION_ID in each[0]:
                        occupations.append({'name': each[1], 'role_ids': each[0]})

            name = pub_twitter['name'] if 'name' in pub_twitter else ''
            isocode = pub_twitter['countrycode'] if 'countrycode' in pub_twitter else ''
            try:
                cats = [{'id': str(cat['_id']),
                         'parent_id': str(cat['parentId']),
                         'name': cat['name'],
                         'level': cat['level']} for cat in pub_twitter['categories'] if 'categories' in pub_twitter]
            except:
                cats = None

            s_sentifi, s_sentifi_prev, s_klout, c_followers, s_sentifi_raw, s_sentifi_scaled, s_reach, s_content, s_interaction = read_scores_by_sns_id(
                twitter_id)

            s_sentifi_delta = (s_sentifi - s_sentifi_prev
                               if s_sentifi is not None and
                               s_sentifi_prev is not None and
                               s_sentifi_prev is not None else None)
            s_klout_delta = s_sentifi - s_klout if s_klout is not None and s_sentifi is not None else None

            try:
                all_publishers.append({
                    'week': week,
                    'year': year,
                    'start_date': start,
                    'channel_meta': {
                        'sns_id': twitter_id,
                        'screen_name': pub_twitter['screen_name'],
                        'name': name,
                        'photo_url': pub_twitter['profile_image_url'] if 'profile_image_url' in pub_twitter else '',
                        'description': pub_twitter['description'] if 'description' in pub_twitter else '',
                    },
                    'score': {
                        's_sentifi': s_sentifi,
                        's_sentifi_prev': s_sentifi_prev,
                        's_sentifi_delta': s_sentifi_delta,
                        's_klout': s_klout,
                        's_klout_delta': s_klout_delta,
                        'c_followers': c_followers,
                        's_sentifi_raw': s_sentifi_raw,
                        's_sentifi_scaled': s_sentifi_scaled,
                        's_reach': s_reach,
                        's_content': s_content,
                        's_interaction': s_interaction
                    },
                    'filters': {
                        'isocode': fix_isocode(isocode),
                        'categories': cats,
                        'occupations': occupations,
                        'func_roles': func_roles
                    }
                })
                sns_ids.append(twitter_id)
            except:
                if error_output > 0:
                    if 'profile_image_url' in pub_twitter:
                        logger.error(traceback.format_exc())
                        logger.error(pub_twitter)
                continue

    if twitter_errors:
        logger.error('Cannot find [%d] twitter accounts' % len(twitter_errors))
        # logger.error('Cannot find twitter account [%s]' % ','.join(twitter_errors))
    if pub_errors:
        logger.error('Cannot find [%d] publisher data' % len(pub_errors))
        # logger.error('Cannot find publisher data [%s]' % ','.join(pub_errors))

    # client.close()
    conn.close()
    return all_publishers, sns_ids


def insert_topic_scores(start):
    for metric_id, topics in inserting_topic_scores.iteritems():
        if metric_id in [50, 51, 52, 53, 54]:
            logger.info('Insert into metric_id [%d]' % metric_id)
            if type(metric_id) is int:
                tss = TopicSnsScore(
                    scored_at=start,
                    metric_id=metric_id,
                    sns_name='tw',
                    triplets_list=topics
                )
                tss_dao.insert(tss)


# def insert_topic_scores(start):
#     for metric_id, topics in inserting_topic_scores.iteritems():
#         if metric_id in [50, 51, 52, 53, 54]:
#             logger.info('Insert into metric_id [%d]' % metric_id)
#             if type(metric_id) is int:
#                 tss = TopicSnsScore(
#                     scored_at=start,
#                     metric_id=metric_id,
#                     sns_name='tw',
#                     triplets_list=topics
#                 )
#                 tss_dao.insert(tss)
#

def insert_topic_scores_with_Workers(start):

    queue_weekly_metric = Queue()

    count = 0
    metrics = []

    for metric_id, topics in inserting_topic_scores.iteritems():
        if metric_id in [50, 51, 52, 53, 54]:
            logger.info('Insert into metric_id [%d]' % metric_id)
            if type(metric_id) is int:
                tss = TopicSnsScore(
                    scored_at=start,
                    metric_id=metric_id,
                    sns_name='tw',
                    triplets_list=topics
                )
                tss_dao.insert(tss)

    for index, row in df.iterrows():
        sns_id = str(int(row.sns_author_id))
        metrics.append(
            Metric(sns_name='tw', sns_id=sns_id, metric_id=1, count_date=start, count_value=row.favourites))
        metrics.append(
            Metric(sns_name='tw', sns_id=sns_id, metric_id=2, count_date=start, count_value=row.followers))
        metrics.append(
            Metric(sns_name='tw', sns_id=sns_id, metric_id=3, count_date=start, count_value=row.friends))
        metrics.append(
            Metric(sns_name='tw', sns_id=sns_id, metric_id=4, count_date=start, count_value=row.listed))
        metrics.append(
            Metric(sns_name='tw', sns_id=sns_id, metric_id=5, count_date=start, count_value=row.statuses))
        metrics.append(
            Metric(sns_name='tw', sns_id=sns_id, metric_id=6, count_date=start, count_value=row.mentions))
        metrics.append(
            Metric(sns_name='tw', sns_id=sns_id, metric_id=7, count_date=start, count_value=row.cum_mentions))

        count += 7
        if index % 50000 == 0:
            logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))

    for chunk in chunks(metrics, NUMBER_OF_PROCESSES):
        queue_weekly_metric.put(chunk)

    logger.info('Inserting REACH to Cassandra in parallel')

    workers_weekly_metric = [Worker(queue_weekly_metric, WEEKLY_METRIC) for _id in range(NUMBER_OF_PROCESSES)]
    c = [each.start() for each in workers_weekly_metric]

    for i in range(NUMBER_OF_PROCESSES):
        queue_weekly_metric.put(None)

    c = [each.join() for each in workers_weekly_metric]


def index_publishers_in_topic_scores(week, year, publishers, topic_scores):
    from copy import deepcopy

    time_str = '[w%d][y%d] ' % (week, year)
    es_ts_index = 'ts_w%d_y%d' % (week, year)
    es_ts_doc_type = 'ts'
    es_index = 'ps_w%d_y%d' % (week, year)
    es_doc_type = 'ps'
    logger.info('Indexing publishers to %s/%s' % (es_index, es_doc_type))
    with ModelDao(client_ES, chunk_size=500) as es_dao:
        for index, pub in enumerate(publishers):
            pub_id = pub['channel_meta']['sns_id']
            if pub_id in topic_scores:
                topics = topic_scores.get(pub_id, [])
                for topic in topics:
                    ts_id = '%s_%s' % (str(pub_id), str(int(topic['id'])))
                    pub_topic = deepcopy(pub)
                    pub_topic['filters']['topic'] = topic
                    es_dao.index(es_ts_index, es_ts_doc_type, pub_topic, obj_id=ts_id, synchronously=False)
                pub['filters']['topics'] = topics
                es_dao.index(es_index, es_doc_type, pub, obj_id=pub_id, synchronously=False)
                if index % 50000 == 0:
                    logger.debug(time_str + '%d processed' % index)
                if index % verbosity == 0:
                    logger.info(time_str + '%d processed' % index)


def index_publishers(week, year, publishers, topic_scores):
    from copy import deepcopy

    time_str = '[w%d][y%d] ' % (week, year)
    es_ts_index = 'ts_w%d_y%d' % (week, year)
    es_ts_doc_type = 'ts'
    es_index = 'ps_w%d_y%d' % (week, year)
    es_doc_type = 'ps'
    logger.info('Indexing publishers to %s/%s' % (es_index, es_doc_type))
    with ModelDao(client_ES, chunk_size=500) as es_dao:
        for index, pub in enumerate(publishers):
            pub_id = pub['channel_meta']['sns_id']
            topics = topic_scores.get(pub_id, [])
            for topic in topics:
                ts_id = '%s_%s' % (str(pub_id), str(int(topic['id'])))
                pub_topic = deepcopy(pub)
                pub_topic['filters']['topic'] = topic
                es_dao.index(es_ts_index, es_ts_doc_type, pub_topic, obj_id=ts_id, synchronously=False)
            pub['filters']['topics'] = topics
            es_dao.index(es_index, es_doc_type, pub, obj_id=pub_id, synchronously=False)
            if index % 50000 == 0:
                logger.debug(time_str + '%d processed' % index)
            if index % verbosity == 0:
                logger.info(time_str + '%d processed' % index)

# @profile


def indexing(week, year):
    #
    # INITIALIZING
    #
    import cPickle as pickle
    import os

    global s_scores
    global s_scores_prev

    time_str = '[w%d][y%d] ' % (week, year)
    start, end = get_week_date_range(week=week, year=year)
    start_prev = start - timedelta(days=7)

    #
    # LOADING SENTIFI SCORES
    #

    if os.path.isfile(save_path + 'score/topic_publishers_w%d_y%d.pkl' % (week, year)):
        logger.info('No need to load sentifi scores')
    else:
        logger.info(time_str + 'Loading sentifi scores from Cassandra')
        if os.path.isfile(save_path + 'score/topic_sscore_w%d_y%d.pkl' % (week, year)):
            logger.info(time_str + 'Loading from file')
            with open(save_path + 'score/topic_sscore_w%d_y%d.pkl' % (week, year), 'rb') as f:
                s_scores = pickle.load(f)
        else:
            s_scores = load_sentifi_scores(start, start_prev)
            with open(save_path + 'score/topic_sscore_w%d_y%d.pkl' % (week, year), 'wb') as f:
                pickle.dump(s_scores, f)
        logger.info(time_str + 'DONE ... size = %d bytes, %d elements' % (sys.getsizeof(s_scores), len(s_scores)))

        logger.info(time_str + 'Loading 2 weeks ago sentifi scores')
        if os.path.isfile(save_path + 'score/topic_sscore_w%d_y%d.pkl' % (week - 1, year)):
            logger.info(time_str + 'Loading from file')
            with open(save_path + 'score/topic_sscore_w%d_y%d.pkl' % (week - 1, year), 'rb') as f:
                s_scores_prev = pickle.load(f)

    #
    # LOADING PUBLISHER DATA
    #
    logger.info(time_str + 'Loading publishers from Mongo')
    if os.path.isfile(save_path + 'score/topic_publishers_w%d_y%d.pkl' % (week, year)):
        logger.info(time_str + 'Loading from file')
        with open(save_path + 'score/topic_publishers_w%d_y%d.pkl' % (week, year), 'rb') as f:
            publishers, sns_ids = pickle.load(f)
    else:
        publishers, sns_ids = get_all_publishers_data(week, year, start)
        with open(save_path + 'score/topic_publishers_w%d_y%d.pkl' % (week, year), 'wb') as f:
            pickle.dump([publishers, sns_ids], f)
    logger.info(time_str + 'DONE ... size = %d bytes, %d elements' % (sys.getsizeof(publishers), len(publishers)))

    logger.info('[GC] before collecting %d' % len(gc.garbage))
    gc.collect()
    logger.info('[GC] after collecting %d' % len(gc.garbage))

    #
    # TOPIC SCORES
    #
    # logger.info(time_str + 'Loading previous topic scores')
    # if start != starting_week:
    #     if os.path.isfile(save_path + 'hoan.nguyen/data/topic_tscores_w%d_y%d.pkl' % (week - 1, year)):
    #         logger.info(time_str + 'Loading from file')
    #         with open(save_path + 'hoan.nguyen/data/topic_tscores_w%d_y%d.pkl' % (week - 1, year), 'rb') as f:
    #             prev_topic_scores = pickle.load(f)
    #     else:
    #         prev_topic_scores = get_topic_scores(start_prev, sns_ids)
    #         with open(save_path + 'hoan.nguyen/data/topic_tscores_w%d_y%d.pkl' % (week - 1, year), 'wb') as f:
    #             pickle.dump(prev_topic_scores, f)
    # else:
    #     prev_topic_scores = {}
    # logger.info(time_str + 'DONE ... size = %d bytes, %d elements' %
    #             (sys.getsizeof(prev_topic_scores), len(prev_topic_scores)))
    #
    # logger.info('[GC] before collecting %d' % len(gc.garbage))
    # gc.collect()
    # logger.info('[GC] after collecting %d' % len(gc.garbage))

    logger.info(time_str + 'Calculating topic scores from ES')
    # if os.path.isfile(save_path + 'hoan.nguyen/data/topic_tscores_w%d_y%d.pkl' % (week, year)):
    #     logger.info(time_str + 'Loading from file')
    #     with open(save_path + 'hoan.nguyen/data/topic_tscores_w%d_y%d.pkl' % (week, year), 'rb') as f:
    #         topic_scores = pickle.load(f)
    #         for twitter_id, topics in topic_scores.iteritems():
    #             add_topics(twitter_id, topics)
    # else:

    calc_index_insert_topic_scores(week, year, start, end, sns_ids, publishers)
        # topic_scores = calc_topic_scores(start, end, prev_topic_scores)
        # with open(save_path + 'hoan.nguyen/data/topic_tscores_w%d_y%d.pkl' % (week, year), 'wb') as f:
        #     pickle.dump(topic_scores, f)

    # logger.info(time_str + 'DONE ... size = %d bytes, %d elements' % (sys.getsizeof(topic_scores), len(topic_scores)))

    logger.info('[GC] before collecting %d' % len(gc.garbage))
    gc.collect()
    logger.info('[GC] after collecting %d' % len(gc.garbage))

    #
    # INDEXING PUBLISHERS AND TOPICS
    #
    # logger.info(time_str + 'Indexing publishers and topics to ES')
    # index_publishers(week, year, publishers, topic_scores)
    # logger.info(time_str + 'DONE')
    #
    # logger.info(time_str + 'Inserting topics to Cassandra')
    # insert_topic_scores(start)
    # logger.info(time_str + 'DONE')
    tss_dao.close()

    logger.info('[GC] before collecting %d' % len(gc.garbage))
    gc.collect()
    logger.info('[GC] after collecting %d' % len(gc.garbage))

    #
    # UPDATING ALIASES
    #
    logger.info(time_str + 'Updating index alias')
    actions = {
        "actions": [
            {
                "remove": {
                    "alias": "ps_cur_week",
                    "index": "ps_w*"
                }
            },
            {
                "remove": {
                    "alias": "ts_cur_week",
                    "index": "ts_w*"
                }
            },
            {
                "add": {
                    "alias": "ps_cur_week",
                    "index": "ps_w%d_y%d" % (week, year)
                }
            },
            {
                "add": {
                    "alias": "ts_cur_week",
                    "index": "ts_w%d_y%d" % (week, year)
                }
            }
        ]
    }
    indices_client_ES = IndicesClient(client_ES)
    indices_client_ES.update_aliases(body=actions)
    cluster.shutdown()

    logger.info('[GC] before collecting %d' % len(gc.garbage))
    gc.collect()
    logger.info('[GC] after collecting %d' % len(gc.garbage))


def main(options):
    global verbosity
    global error_output
    global save_path

    parser = ArgumentParser(prog='PubDataIndexing', description='Indexing publisher scores and meta data to ES')
    parser.add_argument('-w', '--week', dest='week', type=int, required=True,
                        help='The week number to retrieve and index data')
    parser.add_argument('-y', '--year', dest='year', type=int, required=True,
                        help='The year number to retrieve and index data')
    parser.add_argument('-v', '--verbose', dest='verbosity', type=int,
                        help='the level of verbosity')
    parser.add_argument('-e', '--error', dest='error_output', type=int, default=0,
                        help='output the error to screen or not')
    parser.add_argument('-s', '--save_path', dest='save_path', type=str, default='/mnt/ranking-data/ranking/',
                        help='the output path for temporary files')

    args = parser.parse_args()

    save_path = args.save_path

    if args.verbosity:
        verbosity = int(500000.0 / args.verbosity) if args.verbosity > 2 else 100000
        logger.setLevel(DEBUG)
    else:
        verbosity = 100000
        logger.setLevel(INFO)

    if args.error_output > 0:
        error_output = 1
    else:
        error_output = 0

    try:
        return indexing(args.week, args.year)

    except Exception, e:
        logger.exception(e)
        logger.error('The program is terminated.')
        return False


if __name__ == '__main__':
    a = main(sys.argv[1:])

    # start, end = get_week_date_range(week=28, year=2015)
    # start_prev = start - timedelta(days=7)
    # df1, df2, scores = get_topic_scores(start_prev, ['2796938558'])
    # print '29', get_topic_scores(start_prev, ['36102165'])
    #
    # start, end = get_week_date_range(week=28, year=2015)
    # start_prev = start - timedelta(days=7)
    # print 's8', get_topic_scores(start_prev, ['36102165'])
