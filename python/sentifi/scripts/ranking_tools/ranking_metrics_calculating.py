import sys
import logging
import json
import math
from datetime import datetime
from numpy import percentile
import numpy as np
from argparse import ArgumentParser
from multiprocessing import Queue, Process
import pickle

import pandas as pd
from sklearn.preprocessing import MinMaxScaler
# from pymongo import MongoClient
from psycopg2.extras import RealDictCursor
from psycopg2 import connect
from cassandra.cluster import Cluster
from cassandra.policies import TokenAwarePolicy, DCAwareRoundRobinPolicy, RetryPolicy

from constant.cassandra_namespace import PUBLISHER_ANALYTICS
from constant.cassandra_namespace import WEEKLY_COUNT, WEEKLY_METRIC
from constant.cassandra_namespace import WEEKLY_SCALED_METRIC


from model.metric import Metric
from model.metric_dao import MetricDao, chunks

from sentifi_ranking.utils.dateutils import get_week_date_range
from helpers.utils import init_logger

__author__ = 'hoan'

LISTS = 'lists'
FAVORITES = 'favorites'
FOLLOWERS = 'followers'
MENTIONS = 'mentions'
REACH_VOLUME = 'reach_volume'
REACH_VELOCITY = 'reach_velocity'
ORIGINAL = 'original'
RETWEET = 'retweet'
REPLY = 'reply'
FIN_EVENT = 'fin_event'
FIN_TOPIC = 'fin_topic'
CONTENT_VOLUME = 'content_volume'
CONTENT_VELOCITY = 'content_velocity'
CONTENT_AVG = 'content_avg'
CONTENT_MESSAGE_SCORE = 'content_message_score'
CONTENT = 'content'
REACH = 'reach'
INTERACTION = 'interaction'
BONUS = 'bonus'
DEFAULT_SCORE = 'default_score'
DECAY_INTERVAL = 'decay_interval'
DECAY_VALUE = 'decay_value'
DECAY_RATE = 'decay_rate'
DECAY_RATE_GURU = 'decay_rate_guru'

# TODO: use environment variables
CASSANDRA_HOSTS = [
    '10.0.0.251',
    '10.0.0.250',
    '10.0.0.249'
]

NUMBER_OF_PROCESSES = 6
BATCH_SIZE = 1500
COMPONENT = 'COMPONENT'
WEEK = 'WEEK'

cluster = Cluster(
    CASSANDRA_HOSTS,
    load_balancing_policy=TokenAwarePolicy(DCAwareRoundRobinPolicy()),
    default_retry_policy=RetryPolicy()
    #cql_version='3.2.0'
)

logger = init_logger(__name__, log_level=logging.INFO)


def get_pg_connection(database, user, password, host, port):
    logger.info('Connecting to PostgreSQL %s@%s:%s/%s...' % (user, host, port,
                                                             database))
    return connect(database=database, user=user, password=password, host=host,
                   port=port)

# conn = get_pg_connection('rest', 'dbr', 'sentifi', 'psql0.ssh.sentifi.com', 5432)

# client = MongoClient('mongo1x.ssh.sentifi.com:27027')
# coll = client['mydb_merged10']['Publisher']

def getflags():
    FMPSet = set()
    FIPSet = set()
    prioritySet = set()
    FJSet = set()
    guruSet = set()
    celebSet = set()
    conn_dev = get_pg_connection('sentifi_dev', 'dbw', 'sentifi', 'psql-dev-1.ireland.sentifi.internal', 5432)
    with conn_dev.cursor(cursor_factory=RealDictCursor) as cur:
        query = """
            SELECT  sns_id, flag
            FROM    sns_flag
            WHERE flag IN ('FIP', 'FMP', 'priority', 'guru', 'celeb') and sns_name = 'tw';
        """
        cur.execute(query)
        conn_dev.commit()
        for each in cur:
            if each['flag']== 'priority':
                prioritySet.add(each['sns_id'])
            elif each['flag'] == 'FMP':
                FMPSet.add(each['sns_id'])
            elif each['flag'] == 'FIP':
                FIPSet.add(each['sns_id'])
            elif each['flag'] == 'guru':
                guruSet.add(each['sns_id'])
            elif each['flag'] == 'celeb':
                celebSet.add(each['sns_id'])
    with conn_dev.cursor(cursor_factory=RealDictCursor) as cur:
        query = """
            SELECT  sns_id
            FROM    sns_role
            WHERE role_id = 226;
        """
        cur.execute(query)
        conn_dev.commit()
        for each in cur:
            FJSet.add(each['sns_id'])
    conn_dev.close()
    return FIPSet, FMPSet, prioritySet, FJSet, guruSet, celebSet

# gurus = set(['1534167900', '23125257', '40471897', '2293387561', '174385219', '332617373', '1364930179', '140149665', '25913531',
#              '20525971', '264370502', '20700725', '37002777', '78563470', '22088714', '21460063', '330361451', '19811134',
#              '20936135', '16399949', '33576180', '52299954', '38566790', '250206398', '14897785', '191177057', '242893113',
#              '1060578475', '142351603', '42632530', '264996175', '342829141', '67203194', '381289719', '19224439', '56562803',
#              '10793052', '28858121', '246045169', '30271982', '129079450', '14798101', '20345218'])
#
# celebs = set(['35094637', '31927467', '90420314', '53153263', '3004231', '259379883', '533085085', '15439395', '113419517',
#               '20015311', '6427882', '58528137', '24741517', '135421739', '16827333', '200913899', '15253147', '140750163', '18825961',
#               '19637934', '54641845', '21879024', '47786101', '86254626', '57998991', '71876190', '24086418', '41265813', '20978103',
#               '46919513', '31353077', '69396376', '26642006', '221122127', '287834630', '18159470', '1269425228', '376506636', '21414413',
#               '358996059', '330446733', '108936259', '612792060', '38127255', '15227791', '149590738', '33868638', '24083587', '23148645',
#               '19249062', '46973152', '29930037', '38403110', '32309662', '207923746', '40368195', '53414786', '156791186', '155642429',
#               '56481539', '127431090', '47285504', '80655102', '136402916', '23061619', '38708573', '87239242', '30913899', '20641995',
#               '123774113', '815537202', '18132032', '21761627', '268059956', '184779135', '24776235', '165511377', '14444926', '24579796',
#               '25382170', '92685993', '103614732', '62764427', '94816809', '146099195', '47571031', '278550447', '14562221', '82319018',
#               '33452466', '113560280', '111392659', '8443752', '22745779', '34959923', '90379747', '35469661', '25465356', '257246857',
#               '149103331', '135039839', '48466183', '784680583', '164596794', '50774064', '331382144', '63112528', '166173854',
#               '102230123', '83055581', '228794007', '1450087165', '46042404', '273933548', '335495725', '222881714', '75444699',
#               '179559238', '197588356', '21184930', '110195330', '64049044', '132267828', '246515294', '31483387', '69771400', '30478560',
#               '253016329', '42170658', '43534925', '631926517', '15596316', '27750964', '18279004', '252818096', '128220786', '170998795',
#               '21758143', '298195904', '116947296', '987131196', '119516512', '237177177', '17427779', '2226467971', '29281193', '943542662',
#               '33900336', '79722019', '50612214', '26527734', '105210222', '135984962', '897435294', '176557682', '62491943', '69326018',
#               '103061572', '531707563', '20261843', '29951362', '153009922', '79341370', '27894342', '332971617', '19688173', '219240315',
#               '178782325', '1520018400', '21390378'])

FIPs, FMPs, PRIOs, FJs, gurus, celebs = getflags()

# def get_bonus_OLD():
#     logger.info("Get context")
#     all_twitters = []
#     index = 0
#     for each in coll.find():
#         if 'twitterIds' not in each:
#             continue
#         prio = 1 if 'priority' in each and each['priority'] == 1 else 0
#         name = each['name'] if 'name' in each else ''
#         pub_type = ''
#         if 'categories' in each and each['categories'] is not None:
#             for cat in each['categories']:
#                 if 'level' in cat and 'name' in cat and cat['level'] == 1:
#                     pub_type = cat['name']
#                     break
#             for _id in each['twitterIds']:
#                 if _id is None:
#                     continue
#                 context = 25 if _id in gurus else prio * 35
#                 start = weights[DEFAULT_SCORE] + weights[BONUS] * context
#                 all_twitters.append([_id.strip(), prio, context, pub_type, name, start])
#         index += 1
#         if index % 50000 == 0:
#             logger.info(index)
#     return pd.DataFrame(all_twitters, columns=['sns_id', 'prio', 'context', 'pub_type', 'name', 'start'])


def get_bonus():
    logger.info("Get profile from postgres")

    conn = get_pg_connection('rest', 'dbr', 'sentifi', 'psql0.ssh.sentifi.com', 5432)
    index = 0
    all_twitters = []
    with conn.cursor(cursor_factory=RealDictCursor) as cur:
        query = """
            SELECT  pub.object_payload->>'name' as name,
                    pub.object_payload->>'priority' as priority,
                    pub.object_payload->'categories' as categories,
                    pubsns.sns_id as sns_id
            FROM    mg_publisher_sns_clean pubsns JOIN
                    mg_publisher pub
                        ON pub.object_id = pubsns.publisher_mongo_id
            WHERE pubsns.sns_name = 'tw';
        """
        cur.execute(query)
        conn.commit()
        for each in cur:
            # prio = 1 if 'priority' in each and each['priority'] == 1 else 0
            name = each['name'] if 'name' in each else ''
            pub_type = ''
            if 'categories' in each and each['categories'] is not None:
                for cat in each['categories']:
                    if 'level' in cat and 'name' in cat and cat['level'] == 1:
                        pub_type = cat['name']
                        break
                _id = str(each['sns_id'])
                prio = 1 if _id in PRIOs else 0
                context = 15
                if _id in gurus or _id in FJs or _id in FMPs:
                    context = 25
                if _id in FIPs or _id in PRIOs:
                    context = 35
                start = weights[DEFAULT_SCORE] + weights[BONUS] * context
                all_twitters.append([_id.strip(), prio, context, pub_type, name, start])
            index += 1
            if index % 50000 == 0:
                logger.info(index)
    conn.close()

    return pd.DataFrame(all_twitters, columns=['sns_id', 'prio', 'context', 'pub_type', 'name', 'start'])

def get_bonus_with_snsId(snsIds=[]):
    logger.info("Get profile from postgres")

    conn = get_pg_connection('rest', 'dbr', 'sentifi', 'psql0.ssh.sentifi.com', 5432)
    index = 0
    all_twitters = []
    snsIdsList = '(' + ','.join('\''+str(each)+'\'' for each in snsIds) + ')'
    with conn.cursor(cursor_factory=RealDictCursor) as cur:
        query = """
            SELECT  pub.object_payload->>'name' as name,
                    pub.object_payload->>'priority' as priority,
                    pub.object_payload->'categories' as categories,
                    pubsns.sns_id as sns_id
            FROM    mg_publisher_sns_clean pubsns JOIN
                    mg_publisher pub
                        ON pub.object_id = pubsns.publisher_mongo_id
            WHERE pubsns.sns_name = 'tw' AND pubsns.sns_id in %s ;
        """%(snsIdsList)
        # print query
        cur.execute(query)
        conn.commit()
        for each in cur:
            # prio = 1 if 'priority' in each and each['priority'] == 1 else 0
            name = each['name'] if 'name' in each else ''
            pub_type = ''
            if 'categories' in each and each['categories'] is not None:
                for cat in each['categories']:
                    if 'level' in cat and 'name' in cat and cat['level'] == 1:
                        pub_type = cat['name']
                        break
                _id = str(each['sns_id'])
                prio = 1 if _id in PRIOs else 0
                context = 15
                if _id in gurus or _id in FJs:
                    context = 25
                if _id in FMPs or _id in PRIOs:
                    context = 35
                start = weights[DEFAULT_SCORE] + weights[BONUS] * context
                all_twitters.append([_id.strip(), prio, context, pub_type, name, start])
            index += 1
            if index % 50000 == 0:
                logger.info(index)
    conn.close()

    return pd.DataFrame(all_twitters, columns=['sns_id', 'prio', 'context', 'pub_type', 'name', 'start'])


class Worker(Process):

    def __init__(self, queue, table_name):
        Process.__init__(self)
        logger.info("Initializing connection")
        self._table_name = table_name
        self._cluster = Cluster(
            CASSANDRA_HOSTS,
            load_balancing_policy=TokenAwarePolicy(DCAwareRoundRobinPolicy()),
            default_retry_policy=RetryPolicy()
            #cql_version='3.2.0'
        )
        self._queue = queue
        self._count = 0

    def run(self):
        logger.info("Worker %s started" % self.name)
        with MetricDao(self._cluster, PUBLISHER_ANALYTICS) as dao:
            while True:
                metrics = self._queue.get()

                if metrics is None:
                    break

                dao.insert(metrics, table_name=self._table_name)

                self._count += len(metrics)
                if self._count % 500000 == 0:
                    logger.info("[Worker %s][%s][%s] inserts %d metrics" % (self.name, COMPONENT, WEEK, self._count))


def insert_parallelly(metrics):
    queue_weekly_count = Queue()
    queue_weekly_metric = Queue()

    for chunk in chunks(metrics, NUMBER_OF_PROCESSES):
        queue_weekly_count.put(chunk)
        queue_weekly_metric.put(chunk)

    logger.info('Inserting to Cassandra in parallel')
    workers_weekly_count = [Worker(queue_weekly_count, WEEKLY_COUNT) for _id in range(NUMBER_OF_PROCESSES)]
    c = [each.start() for each in workers_weekly_count]

    workers_weekly_metric = [Worker(queue_weekly_metric, WEEKLY_METRIC) for _id in range(NUMBER_OF_PROCESSES)]
    c = [each.start() for each in workers_weekly_metric]

    for i in range(NUMBER_OF_PROCESSES):
        queue_weekly_count.put(None)
        queue_weekly_metric.put(None)

    c = [each.join() for each in workers_weekly_count]
    c = [each.join() for each in workers_weekly_metric]


def scale_column(df, column_name, feature_range=(1, 99)):
    """
    Scaling a column with column_name in a DataFrame df

    Args:
        df (DataFrame): a DataFrame which has columns to be scaled
        column_name (string): the name of the column to be scaled
        feature_range (tuple): the tuple of (min, max) range to scale the column to

    Returns:
        DataFrame: a DataFrame that has the scaled column_name
    """
    # scaling positive and negative sets separately
    pos = df[df[column_name] >= 0][column_name].apply(lambda x: math.log10(math.fabs(x + 1)))
    neg = df[df[column_name] < 0][column_name].apply(lambda x: math.log10(math.fabs(x)))

    # scaling the positive set to the range [1, 100]
    scaler = MinMaxScaler(feature_range=feature_range)
    pos = pd.Series(scaler.fit_transform(pos.tolist()), index=pos.index)

    if len(neg) == 0:
        df['%s_scaled' % column_name] = pos
    else:
        # scaling the negative set to the range [-1, 0)
        scaler = MinMaxScaler(feature_range=(0, 1))
        neg = pd.Series(scaler.fit_transform(neg.tolist()), index=neg.index)
        neg = -neg

        df['%s_scaled' % column_name] = pd.concat([pos, neg])

    return df


def get_weights():
    """
    Get weighting parameters for scoring model

    Returns:
        dictionary: a dictionary of weights for the scoring model
    """
    decay_interval_guru = 8

    decay_interval = 4
    decay_value = 0.8

    decay_rate = math.pow(decay_value, 1.0 / decay_interval)
    decay_rate_guru = math.pow(decay_value, 1.0 / decay_interval_guru)

    return {
        LISTS: 4.0 / 15,
        FAVORITES: 2.0 / 15,
        FOLLOWERS: 3.0 / 15,
        MENTIONS: 6.0 / 15,
        REACH_VOLUME: 0.4,
        REACH_VELOCITY: 0.6,
        ORIGINAL: 3.0 / 6,
        RETWEET: 1.0 / 6,
        REPLY: 2.0 / 6,
        CONTENT_VOLUME: 0.4,
        CONTENT_VELOCITY: 0.6,
        CONTENT_AVG: 0.6,
        CONTENT_MESSAGE_SCORE: 0.4,
        FIN_EVENT: 0.6,
        FIN_TOPIC: 0.4,
        CONTENT: 5.0 / 19,
        REACH: 8.0 / 19,
        INTERACTION: 4.0 / 19,
        BONUS: 2.0 / 19,
        DEFAULT_SCORE: 20.0,
        DECAY_INTERVAL: decay_interval,
        DECAY_VALUE: decay_value,
        DECAY_RATE: decay_rate,
        DECAY_RATE_GURU: decay_rate_guru
    }

weights = get_weights()


def calculating_velocity_reach(metrics_ids, inserted_metrics_ids, weeks=[44, 45, 46, 47, 48, 49], year=2014):
    """Calculating the velocity of metrics in REACH

    Args:
        metrics_ids (list of int): a list in metric id
        weeks (list of int): a list of week number
        year (int): the current year of those weeks

    Returns:

    """
    for week in weeks:
        logger.info(week)
        start, end = get_week_date_range(week=week, year=year)
        start_prev, end_prev = get_week_date_range(week=(week - 1), year=year)
        for metric_id in metrics_ids:
            logger.info(metric_id)
            with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
                logger.info('Querying data')
                df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, metric_id, start=start))
                df_prev = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, metric_id, start=start_prev))
                if week == 43 and year == 2014:
                    df['vel'] = df.stat_value
                else:
                    df = df.merge(df_prev, how='left', on='sns_id', suffixes=('', '_prev'))
                    df = df.fillna(0)
                    df['vel'] = df.stat_value - df.stat_value_prev

                count = 0
                metrics = []
                for index, row in df.iterrows():
                    sns_id = str(int(row.sns_id))
                    metrics.append(
                        Metric(sns_name='tw', sns_id=sns_id, metric_id=inserted_metrics_ids[metric_id],
                               count_date=start, count_value=row['vel']))
                    count += 1
                    if index % 250000 == 0:
                        logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))

            logger.info('Inserting VEL REACH')
            insert_parallelly(metrics)

def calculating_velocity_reach_with_snsId(metrics_ids, inserted_metrics_ids, weeks=[44, 45, 46, 47, 48, 49], year=2014, snsIds=[]):
    """Calculating the velocity of metrics in REACH

    Args:
        metrics_ids (list of int): a list in metric id
        weeks (list of int): a list of week number
        year (int): the current year of those weeks

    Returns:

    """
    for week in weeks:
        logger.info(week)
        start, end = get_week_date_range(week=week, year=year)
        start_prev, end_prev = get_week_date_range(week=(week - 1), year=year)
        for metric_id in metrics_ids:
            logger.info(metric_id)
            with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
                count = 0
                metrics = []
                logger.info('Querying data')
                for snsid in snsIds:
                    df = pd.DataFrame(dao.get_metrics_by_sns_id(WEEKLY_METRIC, snsid, [metric_id], start=start))
                    df_prev = pd.DataFrame(dao.get_metrics_by_sns_id(WEEKLY_METRIC, snsid, [metric_id], start=start_prev))
                    if week == 43 and year == 2014:
                        df['vel'] = df.stat_value
                    else:
                        df = df.merge(df_prev, how='left', on='sns_id', suffixes=('', '_prev'))
                        df = df.fillna(0)
                        df['vel'] = df.stat_value - df.stat_value_prev
                    for index, row in df.iterrows():
                        sns_id = str(int(row.sns_id))
                        metrics.append(
                            Metric(sns_name='tw', sns_id=sns_id, metric_id=inserted_metrics_ids[metric_id],
                                   count_date=start, count_value=row['vel']))
                        count += 1
                        if count % 1000 == 0:
                            logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))
            # print metrics
            logger.info('Inserting VEL REACH')
            insert_parallelly(metrics)

def calculating_reach(weeks=[44, 45, 46, 47, 48], year=2014):
    """Calculating the REACH

    Args:
        weeks (list of int): a list of week number
        year (int): the current year of those weeks

    Returns:

    """
    metric_ids = [1, 2, 4, 7, 30, 31, 33, 35]

    for week in weeks:
        logger.info(week)
        start, end = get_week_date_range(week=week, year=year)
        df = pd.DataFrame(columns=['sns_id'])
        for metric_id in metric_ids:
            # logger.info(metric_id)
            with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
                logger.info('Querying data for metric: %d' % metric_id)
                cur_df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_SCALED_METRIC, metric_id, start=start))
                cur_df = cur_df[['sns_id', 'stat_value']]
                cur_df = cur_df.rename_axis({'stat_value': '%d' % metric_id}, axis=1)
                df = df.merge(cur_df, how='outer', on='sns_id')
        df = df.fillna(0)

        logger.info('Calculating volume')
        df['total_volume'] = (
            weights[LISTS] * df['4'] +
            weights[FAVORITES] * df['1'] +
            weights[FOLLOWERS] * df['2'] +
            weights[MENTIONS] * df['7'])

        logger.info('Calculating velocity')
        df['total_velocity'] = (
            weights[LISTS] * df['33'] +
            weights[FAVORITES] * df['30'] +
            weights[FOLLOWERS] * df['31'] +
            weights[MENTIONS] * df['35'])

        logger.info('Calculating REACH')
        df['reach'] = weights[REACH_VOLUME] * df.total_volume + weights[REACH_VELOCITY] * df.total_velocity

        count = 0
        metrics = []
        for index, row in df.iterrows():
            sns_id = str(int(row.sns_id))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=36, count_date=start, count_value=row['reach']))

            count += 1
            if index % 350000 == 0:
                logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))

        logger.info('Inserting REACH')
        insert_parallelly(metrics)

def calculating_reach_with_snsId(weeks=[44, 45, 46, 47, 48], year=2014, snsIds=[]):
    """Calculating the REACH

    Args:
        weeks (list of int): a list of week number
        year (int): the current year of those weeks

    Returns:

    """
    metric_ids = [1, 2, 4, 7, 30, 31, 33, 35]

    for week in weeks:
        logger.info(week)
        start, end = get_week_date_range(week=week, year=year)
        df = pd.DataFrame(columns=['sns_id','1', '2', '4', '7', '30', '31', '33', '35'])
        # for metric_id in metric_ids:
        #     # logger.info(metric_id)
        #     with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
        #         logger.info('Querying data for metric: %d' % metric_id)
        #         cur_df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_SCALED_METRIC, metric_id, start=start))
        #         cur_df = cur_df[['sns_id', 'stat_value']]
        #         cur_df = cur_df.rename_axis({'stat_value': '%d' % metric_id}, axis=1)
        #         df = df.merge(cur_df, how='outer', on='sns_id')
        with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
            for idx, sns_id in enumerate(snsIds):
                df.loc[idx,'sns_id']=sns_id
                cur_df = pd.DataFrame(dao.get_metrics_by_sns_id(WEEKLY_SCALED_METRIC, sns_id, metric_ids, start))
                for _,r in cur_df.iterrows():
                    df.loc[idx, str(r['metric_id'])] = r['stat_value']
        df = df.fillna(0)

        logger.info('Calculating volume')
        df['total_volume'] = (
            weights[LISTS] * df['4'] +
            weights[FAVORITES] * df['1'] +
            weights[FOLLOWERS] * df['2'] +
            weights[MENTIONS] * df['7'])

        logger.info('Calculating velocity')
        df['total_velocity'] = (
            weights[LISTS] * df['33'] +
            weights[FAVORITES] * df['30'] +
            weights[FOLLOWERS] * df['31'] +
            weights[MENTIONS] * df['35'])

        logger.info('Calculating REACH')
        df['reach'] = weights[REACH_VOLUME] * df.total_volume + weights[REACH_VELOCITY] * df.total_velocity

        count = 0
        metrics = []
        for index, row in df.iterrows():
            sns_id = str(int(row.sns_id))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=36, count_date=start, count_value=row['reach']))

            count += 1
            if index % 350000 == 0:
                logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))
        # print metrics
        logger.info('Inserting REACH')
        insert_parallelly(metrics)

def calculating_content(weeks=[44, 45, 46, 47, 48], year=2014):
    """Calculating the CONTENT

    Args:
        weeks (list of int): a list of week number
        year (int): the current year of those weeks

    Returns:

    """
    t = 1.0 / 2 ** 64
    metric_ids = [10, 11, 12, 13, 20, 21, 22, 23, 9]

    for week in weeks:
        logger.info(week)
        start, end = get_week_date_range(week=week, year=year)
        df = pd.DataFrame(columns=['sns_id'])
        with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
            for metric_id in metric_ids:
                logger.info('Querying data for metric: %d' % metric_id)
                if metric_id == 9:
                    cur_df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, metric_id, start=start))
                else:
                    cur_df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_SCALED_METRIC, metric_id, start=start))
                cur_df = cur_df[['sns_id', 'stat_value']]
                cur_df = cur_df.rename_axis({'stat_value': '%d' % metric_id}, axis=1)
                df = df.merge(cur_df, how='outer', on='sns_id')
        df = df.fillna(0)

        df['total_content_msg_score'] = (
            weights[CONTENT_VOLUME] * (weights[ORIGINAL] * df['20'] +
                                       weights[RETWEET] * (df['22'] + df['23']) +
                                       weights[REPLY] * df['21']) +

            weights[CONTENT_VELOCITY] * (weights[ORIGINAL] * df['10'] +
                                         weights[RETWEET] * (df['12'] + df['13']) +
                                         weights[REPLY] * df['11'])
        )

        df['total_content_avg'] = (df['10'] + df['11'] + df['12'] + df['13']) * 1.0 / (df['9'] + t)
        df.total_content_avg = df.apply(lambda x: x['total_content_avg'] if x['9'] >= 1 else 0.0, axis=1)

        df['content'] = weights[CONTENT_AVG] * df.total_content_avg + \
            weights[CONTENT_MESSAGE_SCORE] * df.total_content_msg_score

        count = 0
        metrics = []
        for index, row in df.iterrows():
            sns_id = str(int(row.sns_id))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=37, count_date=start, count_value=row['content']))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=48, count_date=start, count_value=row['total_content_avg']))

            count += 2
            if index % 350000 == 0:
                logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))

        logger.info('Inserting CONTENT')
        insert_parallelly(metrics)

def calculating_content_with_snsId(weeks=[44, 45, 46, 47, 48], year=2014, snsIds=[]):
    """Calculating the CONTENT

    Args:
        weeks (list of int): a list of week number
        year (int): the current year of those weeks

    Returns:

    """
    t = 1.0 / 2 ** 64
    metric_ids = [10, 11, 12, 13, 20, 21, 22, 23, 9]
    metric_9 = [9]
    metric_content = [10, 11, 12, 13, 20, 21, 22, 23]
    for week in weeks:
        logger.info(week)
        start, end = get_week_date_range(week=week, year=year)
        df = pd.DataFrame(columns=['sns_id','9','10','11','12','13','20','21','22','23'])
        with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
            # for metric_id in metric_ids:
            #     logger.info('Querying data for metric: %d' % metric_id)
            #     if metric_id == 9:
            #         cur_df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, metric_id, start=start))
            #     else:
            #         cur_df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_SCALED_METRIC, metric_id, start=start))
            #     cur_df = cur_df[['sns_id', 'stat_value']]
            #     cur_df = cur_df.rename_axis({'stat_value': '%d' % metric_id}, axis=1)
            #     df = df.merge(cur_df, how='outer', on='sns_id')
            for idx, sns_id in enumerate(snsIds):
                df.loc[idx,'sns_id']=sns_id
                cur_df = pd.DataFrame(dao.get_metrics_by_sns_id(WEEKLY_SCALED_METRIC, sns_id, metric_content, start))
                for _,r in cur_df.iterrows():
                    df.loc[idx, str(r['metric_id'])] = r['stat_value']
                cur_df = pd.DataFrame(dao.get_metrics_by_sns_id(WEEKLY_METRIC, sns_id, metric_9, start))
                for _,r in cur_df.iterrows():
                    df.loc[idx, str(r['metric_id'])] = r['stat_value']

        df = df.fillna(0)

        df['total_content_msg_score'] = (
            weights[CONTENT_VOLUME] * (weights[ORIGINAL] * df['20'] +
                                       weights[RETWEET] * (df['22'] + df['23']) +
                                       weights[REPLY] * df['21']) +

            weights[CONTENT_VELOCITY] * (weights[ORIGINAL] * df['10'] +
                                         weights[RETWEET] * (df['12'] + df['13']) +
                                         weights[REPLY] * df['11'])
        )

        df['total_content_avg'] = (df['10'] + df['11'] + df['12'] + df['13']) * 1.0 / (df['9'] + t)
        df.total_content_avg = df.apply(lambda x: x['total_content_avg'] if x['9'] >= 1 else 0.0, axis=1)

        df['content'] = weights[CONTENT_AVG] * df.total_content_avg + \
            weights[CONTENT_MESSAGE_SCORE] * df.total_content_msg_score

        count = 0
        metrics = []
        for index, row in df.iterrows():
            sns_id = str(int(row.sns_id))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=37, count_date=start, count_value=row['content']))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=48, count_date=start, count_value=row['total_content_avg']))

            count += 2
            if index % 350000 == 0:
                logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))
        # print metrics
        logger.info('Inserting CONTENT')
        insert_parallelly(metrics)

def calculating_sentifi_score(weeks=[44, 45, 46, 47, 48], year=2014):
    """Calculating the SENTIFI SCORE

    Args:
        weeks (list of int): a list of week number
        year (int): the current year of those weeks

    Returns:

    """
    metric_ids = [36, 37]
    df_bonus = get_bonus()

    for week in weeks:
        logger.info(week)
        start, end = get_week_date_range(week=week, year=year)
        df = pd.DataFrame(columns=['sns_id'])
        for metric_id in metric_ids:
            # logger.info(metric_id)
            with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
                logger.info('Querying data for metric: %d' % metric_id)
                cur_df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, metric_id, start=start))
                cur_df = cur_df[['sns_id', 'stat_value']]
                cur_df = cur_df.rename_axis({'stat_value': '%d' % metric_id}, axis=1)
                df = df.merge(cur_df, how='outer', on='sns_id')

        with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
            logger.info('Querying data for scaled interaction')
            cur_df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_SCALED_METRIC, 8, start=start))
            cur_df = cur_df[['sns_id', 'stat_value']]
            cur_df = cur_df.rename_axis({'stat_value': '%d' % 8}, axis=1)
            df = df.merge(cur_df, how='outer', on='sns_id')

        df = df.merge(df_bonus, on='sns_id', how='left')
        df = df.fillna(0)

        logger.info('Calculating final raw score')
        df['raw_score'] = df.apply(lambda x:
                                   weights[CONTENT] * x['37'] + weights[REACH] * x['36'] / 3.0 +
                                   weights[INTERACTION] * x['8'] + weights[BONUS] * x['context']
                                   if (x['37'] <= 0.1) and (x['sns_id'] in celebs) else
                                   weights[CONTENT] * x['37'] + weights[REACH] * x['36'] +
                                   weights[INTERACTION] * x['8'] + weights[BONUS] * x['context'], axis=1)
        count = 0
        metrics = []
        for index, row in df.iterrows():
            sns_id = str(int(row.sns_id))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=38, count_date=start, count_value=row['raw_score']))

            count += 1
            if index % 350000 == 0:
                logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))

        logger.info('Inserting S-SCORE')
        insert_parallelly(metrics)

def calculating_sentifi_score_with_snsId(weeks=[44, 45, 46, 47, 48], year=2014, snsIds=[]):
    """Calculating the SENTIFI SCORE

    Args:
        weeks (list of int): a list of week number
        year (int): the current year of those weeks
        36:reach, 37:content, 38:s_score_raw, 39:s_core, 8:interaction
    Returns:

    """
    metric_ids = [36, 37]
    df_bonus = get_bonus_with_snsId(snsIds=snsIds)

    for week in weeks:
        logger.info(week)
        start, end = get_week_date_range(week=week, year=year)
        df = pd.DataFrame(columns=['sns_id'])
        # for metric_id in metric_ids:
        #     # logger.info(metric_id)
        #     with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
        #         logger.info('Querying data for metric: %d' % metric_id)
        #         cur_df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, metric_id, start=start))
        #         cur_df = cur_df[['sns_id', 'stat_value']]
        #         cur_df = cur_df.rename_axis({'stat_value': '%d' % metric_id}, axis=1)
        #         df = df.merge(cur_df, how='outer', on='sns_id')

        for idx, sns_id in enumerate(snsIds):
            with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
                df.loc[idx,'sns_id']=sns_id
                cur_df = pd.DataFrame(dao.get_metrics_by_sns_id(WEEKLY_METRIC, sns_id, metric_ids, start))
                for _,r in cur_df.iterrows():
                    df.loc[idx, str(r['metric_id'])] = r['stat_value']
                cur_df = pd.DataFrame(dao.get_metrics_by_sns_id(WEEKLY_SCALED_METRIC, sns_id, [8], start))
                for _,r in cur_df.iterrows():
                    df.loc[idx, str(r['metric_id'])] = r['stat_value']

        # with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
        #     logger.info('Querying data for scaled interaction')
        #     cur_df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_SCALED_METRIC, 8, start=start))
        #     cur_df = cur_df[['sns_id', 'stat_value']]
        #     cur_df = cur_df.rename_axis({'stat_value': '%d' % 8}, axis=1)
        #     df = df.merge(cur_df, how='outer', on='sns_id')

        df = df.merge(df_bonus, on='sns_id', how='left')
        df = df.fillna(0)

        logger.info('Calculating final raw score')
        df['raw_score'] = df.apply(lambda x:
                                   weights[CONTENT] * x['37'] + weights[REACH] * x['36'] / 3.0 +
                                   weights[INTERACTION] * x['8'] + weights[BONUS] * x['context']
                                   if (x['37'] <= 0.1) and (x['sns_id'] in celebs) else
                                   weights[CONTENT] * x['37'] + weights[REACH] * x['36'] +
                                   weights[INTERACTION] * x['8'] + weights[BONUS] * x['context'], axis=1)
        count = 0
        metrics = []
        for index, row in df.iterrows():
            sns_id = str(int(row.sns_id))
            metrics.append(
                Metric(sns_name='tw', sns_id=sns_id, metric_id=38, count_date=start, count_value=row['raw_score']))

            count += 1
            if index % 350000 == 0:
                logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))
        # print metrics
        logger.info('Inserting S-SCORE')
        insert_parallelly(metrics)

def calculating_final_decay_score(weeks=[44, 45, 46, 47, 48], year=2014, starting_week=43, starting_year=2014):
    """Calculating the final DECAY SENTIFI SCORE

    Args:
        weeks (list of int): a list of week number
        year (int): the current year of those weeks

    Returns:

    """
    for week in weeks:
        logger.info(week)
        start, end = get_week_date_range(week=week, year=year)
        start_prev, end_prev = get_week_date_range(week=(week - 1), year=year)
        with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
            logger.info('Querying data')
            df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_SCALED_METRIC, 38, start=start))
            if week == starting_week and year == starting_year:
                df_prev = get_bonus()
                df_prev = df_prev.rename_axis({'start': 'stat_value'}, axis=1)
            else:
                df_prev = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, 39, start=start_prev))

            df = df.merge(df_prev, how='left', on='sns_id', suffixes=('', '_prev'))
            df = df.fillna(0)
            df['decay_only'] = df.apply(lambda x: x['stat_value_prev'] * weights[DECAY_RATE_GURU]
                                        if x['sns_id'] in gurus else x['stat_value_prev'] * weights[DECAY_RATE], axis=1)
            # df.loc[, 'decay_only']

            df['final'] = df.apply(
                lambda x: x['stat_value'] if x['stat_value'] > x['decay_only'] else x['decay_only'], axis=1)
            # df['final'] = (df.stat_value + df.stat_value_prev * weights[DECAY_RATE]) / (1 + weights[DECAY_RATE])
            df = scale_column(df, 'final')

            count = 0
            metrics = []
            for index, row in df.iterrows():
                sns_id = str(int(row.sns_id))
                metrics.append(
                    Metric(sns_name='tw', sns_id=sns_id, metric_id=39, count_date=start, count_value=row['final']))

            count += 1
            if index % 350000 == 0:
                logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))

        logger.info('Inserting DECAY S-SCORE')
        insert_parallelly(metrics)

def calculating_final_decay_score_with_snsId(weeks=[44, 45, 46, 47, 48], year=2014, starting_week=43, starting_year=2014, snsIds=[]):
    """Calculating the final DECAY SENTIFI SCORE

    Args:
        weeks (list of int): a list of week number
        year (int): the current year of those weeks

    Returns:

    """
    for week in weeks:
        logger.info(week)
        start, end = get_week_date_range(week=week, year=year)
        start_prev, end_prev = get_week_date_range(week=(week - 1), year=year)
        with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
            logger.info('Querying data')
            ls = []
            for _, sns_id in enumerate(snsIds):
                ls = ls + dao.get_metrics_by_sns_id(WEEKLY_SCALED_METRIC, sns_id, [38], start)
            df = pd.DataFrame(ls)
            # df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_SCALED_METRIC, 38, start=start))
            if week == starting_week and year == starting_year:
                df_prev = get_bonus()
                df_prev = df_prev.rename_axis({'start': 'stat_value'}, axis=1)
            else:
                ls_prev = []
                for _, sns_id in enumerate(snsIds):
                    ls_prev = ls_prev + dao.get_metrics_by_sns_id(WEEKLY_METRIC, sns_id, [39], start_prev)
                df_prev = pd.DataFrame(ls_prev)
                # df_prev = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, 39, start=start_prev))

            df = df.merge(df_prev, how='left', on='sns_id', suffixes=('', '_prev'))
            df = df.fillna(0)
            df['decay_only'] = df.apply(lambda x: x['stat_value_prev'] * weights[DECAY_RATE_GURU]
                                        if x['sns_id'] in gurus else x['stat_value_prev'] * weights[DECAY_RATE], axis=1)
            # df.loc[, 'decay_only']

            df['final'] = df.apply(
                lambda x: x['stat_value'] if x['stat_value'] > x['decay_only'] else x['decay_only'], axis=1)
            # df['final'] = (df.stat_value + df.stat_value_prev * weights[DECAY_RATE]) / (1 + weights[DECAY_RATE])
            df = scale_column(df, 'final')

            count = 0
            metrics = []
            for index, row in df.iterrows():
                sns_id = str(int(row.sns_id))
                metrics.append(
                    Metric(sns_name='tw', sns_id=sns_id, metric_id=39, count_date=start, count_value=row['final']))

            count += 1
            if index % 350000 == 0:
                logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))
        # print metrics
        logger.info('Inserting DECAY S-SCORE')
        insert_parallelly(metrics)

def calculating_score_percentile(metrics_ids, inserted_metrics_ids, weeks, year):
    for week in weeks:
        logger.info('Week: %d' % week)
        start, end = get_week_date_range(week=week, year=year)
        for metric in metrics_ids:
            logger.info('Metric: %d' % metric)
            with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
                logger.info('Querying data')
                if metric == 8:
                    df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_SCALED_METRIC, metric, start=start))
                else:
                    df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, metric, start=start))
                p = percentile(df.stat_value, range(101))
                pickle.dump(p, open('/mnt/ranking-data/ranking/percentile/%d_%d_%d_percentile.pkl' % (metric, week, year), 'wb'))
                logger.info('Calculating percentile')
                df['percentile'] = df.stat_value.apply(lambda x: next((i for i, val in enumerate(p) if x < val), -1))

                count = 0
                metrics = []
                for index, row in df.iterrows():
                    sns_id = str(int(row.sns_id))
                    metrics.append(
                        Metric(sns_name='tw', sns_id=sns_id, metric_id=inserted_metrics_ids[metric], count_date=start, count_value=row['percentile']))

                count += 1
                if index % 350000 == 0:
                    logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))

            logger.info('Inserting PERCENTILE')
            insert_parallelly(metrics)

def calculating_score_percentile_with_snsId(metrics_ids, inserted_metrics_ids, weeks, year, snsIds):
    for week in weeks:
        logger.info('Week: %d' % week)
        start, end = get_week_date_range(week=week, year=year)
        for metric in metrics_ids:
            logger.info('Metric: %d' % metric)
            with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
                logger.info('Querying data')
                ls = []
                if metric == 8:
                    for _, sns_id in enumerate(snsIds):
                        ls = ls + dao.get_metrics_by_sns_id(WEEKLY_SCALED_METRIC, sns_id, [metric], start)
                    df = pd.DataFrame(ls)
                    # df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_SCALED_METRIC, metric, start=start))
                else:
                    for _, sns_id in enumerate(snsIds):
                        ls = ls + dao.get_metrics_by_sns_id(WEEKLY_METRIC, sns_id, [metric], start)
                    df = pd.DataFrame(ls)
                    # df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, metric, start=start))
                # p = percentile(df.stat_value, range(101))
                try:
                    p = pickle.load(open('/mnt/ranking-data/ranking/percentile/%d_%d_%d_percentile.pkl' % (metric, week, year), 'rb'))
                except:
                    logger.info('fail to get percentile file %d_%d_%d_percentile.pkl' % (metric, week, year))
                    continue
                logger.info('Calculating percentile')
                # df = df[df['sns_id'].isin(snsIds)]
                df['percentile'] = df.stat_value.apply(lambda x: next((i for i, val in enumerate(p) if x < val), -1))

                count = 0
                metrics = []
                for index, row in df.iterrows():
                    sns_id = str(int(row.sns_id))
                    metrics.append(
                        Metric(sns_name='tw', sns_id=sns_id, metric_id=inserted_metrics_ids[metric], count_date=start, count_value=row['percentile']))

                count += 1
                if index % 350000 == 0:
                    logger.info('Processed [%d] rows, [%d] metrics created' % (index, count))
            # print metrics
            logger.info('Inserting PERCENTILE')
            insert_parallelly(metrics)

def gen_histogram(weeks, year):
    save_path = '/mnt/ranking-data/ranking/'
    save_path_d = save_path + 'dashboards/'
    df_pub = get_bonus()
    df_pub.sns_id = df_pub.sns_id.apply(str)
    for week in weeks:
        logger.info('Week %d' % week)
        start, end = get_week_date_range(week=week, year=year)

        with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
            df_raw = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, 38, start=start))
        df_raw = df_raw.merge(df_pub, on='sns_id', how='left')
        histogram_raw_P = np.histogram(df_raw[df_raw.pub_type == 'P'].stat_value, bins=100)[0]
        histogram_raw_O = np.histogram(df_raw[df_raw.pub_type == 'O'].stat_value, bins=100)[0]

        with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
            df_scaled = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_SCALED_METRIC, 38, start=start))
        df_scaled = df_scaled.merge(df_pub, on='sns_id', how='left')
        histogram_scaled_P = np.histogram(df_scaled[df_scaled.pub_type == 'P'].stat_value, bins=100)[0]
        histogram_scaled_O = np.histogram(df_scaled[df_scaled.pub_type == 'O'].stat_value, bins=100)[0]

        with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
            df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, 39, start=start))
        histogram_all, bins_all = np.histogram(df.stat_value, bins=100)

        df = df.merge(df_pub, on='sns_id', how='inner')

        histogram_P = np.histogram(df[df.pub_type == 'P'].stat_value, bins=100)[0]
        histogram_O = np.histogram(df[df.pub_type == 'O'].stat_value, bins=100)[0]
        logger.info("export histogram")
        with open(save_path_d + 'histogram_%d_%d_P.txt' % (week, year), 'wb') as f:
            f.write(json.dumps([histogram_raw_P.tolist(), histogram_scaled_P.tolist(), histogram_P.tolist()]))
        with open(save_path_d + 'histogram_%d_%d_O.txt' % (week, year), 'wb') as f:
            f.write(json.dumps([histogram_raw_O.tolist(), histogram_scaled_O.tolist(), histogram_O.tolist()]))

        logger.info("get data for component histogram")
        with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
            df_content = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, 37, start=start))
            df_reach = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, 36, start=start))
            df_interaction = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_SCALED_METRIC, 8, start=start))
            df_avg_content = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, 48, start=start))

        histogram_content, bins_content = np.histogram(df_content.stat_value, bins=100)
        histogram_reach, bins_reach = np.histogram(df_reach.stat_value, bins=100)
        histogram_interaction, bins_interaction = np.histogram(df_interaction.stat_value, bins=100)
        histogram_avg_content, bins_avg_content = np.histogram(df_avg_content.stat_value, bins=100)

        logger.info("export histogram")
        with open(save_path_d + 'histogram_%d_%d_community.txt' % (week, year), 'wb') as f:
            f.write(json.dumps([histogram_all.tolist(), histogram_content.tolist(),
                                histogram_reach.tolist(), histogram_interaction.tolist(), histogram_avg_content.tolist()]))

        logger.info("export bins")
        with open(save_path_d + 'bins_%d_%d_community.txt' % (week, year), 'wb') as f:
            f.write(json.dumps(
                [bins_all.tolist(), bins_content.tolist(), bins_reach.tolist(), bins_interaction.tolist(), bins_avg_content.tolist()]))

# def gen_histogram_with_snsId(weeks, year, snsIds):
#     save_path = '/mnt/ranking-data/ranking/'
#     save_path_d = save_path + 'dashboards/'
#     df_pub = get_bonus()
#     df_pub.sns_id = df_pub.sns_id.apply(str)
#     for week in weeks:
#         logger.info('Week %d' % week)
#         start, end = get_week_date_range(week=week, year=year)
#
#         with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
#             ls = []
#             for _, sns_id in enumerate(snsIds):
#                 ls = ls + dao.get_metrics_by_sns_id(WEEKLY_METRIC, sns_id, [38], start)
#             df_raw = pd.DataFrame(ls)
#             # df_raw = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, 38, start=start))
#         df_raw = df_raw.merge(df_pub, on='sns_id', how='left')
#         histogram_raw_P = np.histogram(df_raw[df_raw.pub_type == 'P'].stat_value, bins=100)[0]
#         histogram_raw_O = np.histogram(df_raw[df_raw.pub_type == 'O'].stat_value, bins=100)[0]
#
#         with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
#             ls = []
#             for _, sns_id in enumerate(snsIds):
#                 ls = ls + dao.get_metrics_by_sns_id(WEEKLY_SCALED_METRIC, sns_id, [38], start)
#             df_scaled = pd.DataFrame(ls)
#             # df_scaled = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_SCALED_METRIC, 38, start=start))
#         df_scaled = df_scaled.merge(df_pub, on='sns_id', how='left')
#         histogram_scaled_P = np.histogram(df_scaled[df_scaled.pub_type == 'P'].stat_value, bins=100)[0]
#         histogram_scaled_O = np.histogram(df_scaled[df_scaled.pub_type == 'O'].stat_value, bins=100)[0]
#
#         with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
#             ls = []
#             for _, sns_id in enumerate(snsIds):
#                 ls = ls + dao.get_metrics_by_sns_id(WEEKLY_METRIC, sns_id, [39], start)
#             df = pd.DataFrame(ls)
#             # df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, 39, start=start))
#         histogram_all, bins_all = np.histogram(df.stat_value, bins=100)
#
#         df = df.merge(df_pub, on='sns_id', how='inner')
#
#         histogram_P = np.histogram(df[df.pub_type == 'P'].stat_value, bins=100)[0]
#         histogram_O = np.histogram(df[df.pub_type == 'O'].stat_value, bins=100)[0]
#         logger.info("export histogram")
#         with open(save_path_d + 'Nhistogram_%d_%d_P.txt' % (week, year), 'wb') as f:
#             f.write(json.dumps([histogram_raw_P.tolist(), histogram_scaled_P.tolist(), histogram_P.tolist()]))
#         with open(save_path_d + 'Nhistogram_%d_%d_O.txt' % (week, year), 'wb') as f:
#             f.write(json.dumps([histogram_raw_O.tolist(), histogram_scaled_O.tolist(), histogram_O.tolist()]))
#
#         logger.info("get data for component histogram")
#         with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
#             ls_content = []
#             ls_reach = []
#             ls_interaction = []
#             ls_avg_content = []
#             for _, sns_id in enumerate(snsIds):
#                 ls_content = ls_content + dao.get_metrics_by_sns_id(WEEKLY_METRIC, sns_id, [37], start)
#                 ls_reach = ls_reach + dao.get_metrics_by_sns_id(WEEKLY_METRIC, sns_id, [36], start)
#                 ls_interaction = ls_interaction + dao.get_metrics_by_sns_id(WEEKLY_SCALED_METRIC, sns_id, [8], start)
#                 ls_avg_content = ls_avg_content + dao.get_metrics_by_sns_id(WEEKLY_METRIC, sns_id, [48], start)
#             df_content = pd.DataFrame(ls_content)
#             df_reach = pd.DataFrame(ls_reach)
#             df_interaction = pd.DataFrame(ls_interaction)
#             df_avg_content = pd.DataFrame(ls_avg_content)
#             # df_content = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, 37, start=start))
#             # df_reach = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, 36, start=start))
#             # df_interaction = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_SCALED_METRIC, 8, start=start))
#             # df_avg_content = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, 48, start=start))
#
#         histogram_content, bins_content = np.histogram(df_content.stat_value, bins=100)
#         histogram_reach, bins_reach = np.histogram(df_reach.stat_value, bins=100)
#         histogram_interaction, bins_interaction = np.histogram(df_interaction.stat_value, bins=100)
#         histogram_avg_content, bins_avg_content = np.histogram(df_avg_content.stat_value, bins=100)
#
#         logger.info("export histogram")
#         with open(save_path_d + 'Nhistogram_%d_%d_community.txt' % (week, year), 'wb') as f:
#             f.write(json.dumps([histogram_all.tolist(), histogram_content.tolist(),
#                                 histogram_reach.tolist(), histogram_interaction.tolist(), histogram_avg_content.tolist()]))
#
#         logger.info("export bins")
#         with open(save_path_d + 'Nbins_%d_%d_community.txt' % (week, year), 'wb') as f:
#             f.write(json.dumps(
#                 [bins_all.tolist(), bins_content.tolist(), bins_reach.tolist(), bins_interaction.tolist(), bins_avg_content.tolist()]))

def main(options):
    global BATCH_SIZE
    global NUMBER_OF_PROCESSES
    global COMPONENT

    BATCH_SIZE = 300

    parser = ArgumentParser(prog='MetricImporter', description='Tool to calculate metrics in Cassandra')
    parser.add_argument('-m', '--metrics', dest='metrics', required=True,
                        nargs='+', type=int,
                        help='A list of metric ids to calculate')
    parser.add_argument('-o', '--out_metrics', dest='out_metrics',
                        nargs='+', type=int,
                        help='A list of corresponding metric ids to calculate and save in')
    parser.add_argument('-w', '--week', dest='week', required=True,
                        nargs='+', type=int,
                        help='A list of week number to retrieve and calculate data')
    parser.add_argument('-y', '--year', dest='year', type=int, required=True,
                        help='The year number to retrieve and calculate data')
    parser.add_argument('-b', '--batch', dest='batch_size', type=int,
                        help='Batch size to insert')
    parser.add_argument('-p', '--processes', dest='processes', type=int,
                        help='Number of threads to insert')
    parser.add_argument('-c', '--component', dest='component', required=True,
                        choices=['vel_reach', 'reach', 'content', 'score',
                                 'decay_score', 'percentile', 'report'],
                        help='The component in the publisher ranking model to calculate data')

    args = parser.parse_args()
    weeks = args.week
    year = args.year
    metrics_ids = args.metrics
    out_metrics_ids = args.out_metrics
    batch_size = args.batch_size
    processes = args.processes
    component = args.component
    COMPONENT = str(object=component)

    if batch_size is not None and batch_size > 0:
        BATCH_SIZE = batch_size
    if processes is not None and processes > 0:
        NUMBER_OF_PROCESSES = processes

    logger.info('Calculating component[%s] of [%d metrics] of weeks %s in year %d' %
          (component, len(metrics_ids), str(weeks), year))
    mapping = {a: b for a, b in zip(metrics_ids, out_metrics_ids)}
    # scale_all(metrics_ids, weeks, year)

    if component == 'reach':
        calculating_reach(weeks, year)
    elif component == 'vel_reach':
        calculating_velocity_reach(metrics_ids=metrics_ids, inserted_metrics_ids=mapping, weeks=weeks, year=year)
    elif component == 'content':
        calculating_content(weeks, year)
    elif component == 'score':
        calculating_sentifi_score(weeks, year)
    elif component == 'decay_score':
        calculating_final_decay_score(weeks, year)
    elif component == 'percentile':
        calculating_score_percentile(metrics_ids=metrics_ids, inserted_metrics_ids=mapping, weeks=weeks, year=year)
    elif component == 'report':
        gen_histogram(weeks=weeks, year=year)

    # conn.close()

if __name__ == '__main__':
    # print 'FMPs', FMPs, 'PRIOs', PRIOs, 'FJs', FJs, 'gurus', gurus, 'celebs', celebs
    # snsIds = ['2743641378', '36102165']
    # calculating_content_with_snsId([27], 2015, snsIds)
    # calculating_velocity_reach_with_snsId([1, 2], {1:30, 2:31}, [27], 2015, snsIds=snsIds)
    # calculating_reach_with_snsId([27], 2015, snsIds)
    # calculating_sentifi_score_with_snsId([26], 2015, snsIds)
    # calculating_final_decay_score_with_snsId([27], 2015, snsIds=snsIds)
    # calculating_score_percentile_with_snsId([39, 8], {39: 46, 8: 45}, [27], 2015, snsIds=snsIds)
    main(sys.argv[1:])
    # calculating_velocity_reach_with_snsId()
    '''
    snsIds = ['2743641378', '36102165']
    metric_9 = [9]
    metric_content = [9, 10, 11, 12, 13, 20, 21, 22, 23]
    start, end = get_week_date_range(week=23, year=2015)
    df = pd.DataFrame(columns=['sns_id','9','10','11','12','13','20','21','22','23'])
    with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
        for metric_id in metric_content:
            logger.info('Querying data for metric: %d' % metric_id)
            if metric_id == 9:
                cur_df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, metric_id, start=start))
            else:
                cur_df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_SCALED_METRIC, metric_id, start=start))
                print 'df 1', cur_df
                cur_df = cur_df[['sns_id', 'stat_value']]
                cur_df = cur_df.rename_axis({'stat_value': '%d' % metric_id}, axis=1)
                df = df.merge(cur_df, how='outer', on='sns_id')
        for idx, sns_id in enumerate(snsIds):
            df.loc[idx,'sns_id']=sns_id
            cur_df = pd.DataFrame(dao.get_metrics_by_sns_id(WEEKLY_SCALED_METRIC, sns_id, metric_content, start))
            for _,r in cur_df.iterrows():
                df.loc[idx, str(r['metric_id'])] = r['stat_value']
                cur_df = pd.DataFrame(dao.get_metrics_by_sns_id(WEEKLY_METRIC, sns_id, metric_9, start))
    df = df.fillna(0)
    print df
    '''
    # conn.close()

    # aggregations = []
    # aggregations.append(Aggregation(stat_date=start, metric_id=36, stat_name='min', stat_value=0))
    # aggregations.append(Aggregation(stat_date=start, metric_id=36, stat_name='max', stat_value=100))
    # insert_aggregation(aggregations)
    # with AggregationDao(cluster, PUBLISHER_ANALYTICS) as dao:
    #     cur_df = pd.DataFrame(dao.get_stat_by_name('weekly_metric_stats', 36, 'min', start ))
    #     print 'min', cur_df
    #     cur_df = pd.DataFrame(dao.get_stat_by_name('weekly_metric_stats', 36, 'max', start ))
    #     print 'max', cur_df
    #     cur_df = pd.DataFrame(dao.get_stat_by_metric('weekly_metric_stats', 36, start ))
    #     print 'all', cur_df
    # conn.close()
