import traceback
from time import sleep
import sys
import math
from datetime import timedelta
from logging import INFO, DEBUG
from argparse import ArgumentParser

from helpers.utils import init_logger
from sentifi_ranking.utils.dateutils import get_week_date_range
from constant.cassandra_namespace import PUBLISHER_ANALYTICS
from constant.cassandra_namespace import WEEKLY_METRIC
from model.es_dao import ModelDao
from model.metric_dao import MetricDao
from model.cassandra_dao.topic_sns_score import TopicSnsScoreDao
from model.topic_sns_score import TopicSnsScore

from cassandra.cluster import Cluster
from cassandra.policies import TokenAwarePolicy, DCAwareRoundRobinPolicy, RetryPolicy
from pymongo import MongoClient
from elasticsearch import Elasticsearch
from kombu import connections, BrokerConnection, Queue
from kombu.mixins import ConsumerMixin
from bson.objectid import ObjectId
import pandas as pd

logger = init_logger(__name__, log_level=DEBUG)

logger.info('Initializing params and constants')

LISTS = 'lists'
FAVORITES = 'favorites'
FOLLOWERS = 'followers'
MENTIONS = 'mentions'
REACH_VOLUME = 'reach_volume'
REACH_VELOCITY = 'reach_velocity'
ORIGINAL = 'original'
RETWEET = 'retweet'
REPLY = 'reply'
FIN_EVENT = 'fin_event'
FIN_TOPIC = 'fin_topic'
CONTENT_VOLUME = 'content_volume'
CONTENT_VELOCITY = 'content_velocity'
CONTENT_AVG = 'content_avg'
CONTENT_MESSAGE_SCORE = 'content_message_score'
CONTENT = 'content'
REACH = 'reach'
INTERACTION = 'interaction'
BONUS = 'bonus'
DEFAULT_SCORE = 'default_score'
DECAY_INTERVAL = 'decay_interval'
DECAY_VALUE = 'decay_value'
DECAY_RATE = 'decay_rate'
DECAY_RATE_GURU = 'decay_rate_guru'

MESSAGE_SCORE_PARAMS = {
    'DEFAULT_MN': 10,
    'DEFAULT_MP': 1,
    'MIN_MNP': 0.0,
    'SUB_MNP': 33340.0,
    'MIN_CR': 0.0,
    'SUB_CR': 100.0,
    'MIN_LANG': 0.0,
    'SUB_LANG': 0.263
}

WEIGHTS = {
    'original': 3.0 / 6,
    'retweet': 1.0 / 6,
    'retweet2': 1.0 / 6,
    'reply': 2.0 / 6,
    'content_avg': 0.6,
    'content_message_score': 0.4,
}

TWEET_TYPE = {
    3: 'retweet2',
    2: 'retweet',
    1: 'reply',
    0: 'original'
}

CASSANDRA_HOSTS = [
    '10.0.0.251',
    '10.0.0.250',
    '10.0.0.249'
]

ES_HOST = [
    {'host': 'es0.ssh.sentifi.com', 'port': 9200, 'retry_on_timeout': True},
    {'host': 'es8.ssh.sentifi.com', 'port': 9200, 'retry_on_timeout': True},
    {'host': 'es9.ssh.sentifi.com', 'port': 9200, 'retry_on_timeout': True}
]

BROKER_URL = [
    'amqp://worker:123cayngaycaydem@rabbitmq-prod1.ssh.sentifi.com:5672',
    'amqp://worker:123cayngaycaydem@rabbitmq-prod2.ssh.sentifi.com:5672'
]

verbosity = 1
error_output = 0

logger.info('Initializing connections')
cluster = Cluster(
    CASSANDRA_HOSTS,
    load_balancing_policy=TokenAwarePolicy(DCAwareRoundRobinPolicy(local_dc='Cassandra')),
    default_retry_policy=RetryPolicy()
    #cql_version='3.1.7'
)

client_ES = Elasticsearch(ES_HOST, timeout=3600)
es_dao = ModelDao(client_ES, chunk_size=1000)
tss_dao = TopicSnsScoreDao(cluster, PUBLISHER_ANALYTICS)

client = MongoClient('mongo3.ssh.sentifi.com')
pub_coll = client['core']['Publisher']
twitter_coll = client['core']['Twitter']


def get_weights():
    """
    Get weighting parameters for scoring model

    Returns:
        dictionary: a dictionary of weights for the scoring model
    """
    decay_interval_guru = 8

    decay_interval = 4
    decay_value = 0.8

    decay_rate = math.pow(decay_value, 1.0 / decay_interval)
    decay_rate_guru = math.pow(decay_value, 1.0 / decay_interval_guru)

    return {
        LISTS: 4.0 / 15,
        FAVORITES: 2.0 / 15,
        FOLLOWERS: 3.0 / 15,
        MENTIONS: 6.0 / 15,
        REACH_VOLUME: 0.4,
        REACH_VELOCITY: 0.6,
        ORIGINAL: 3.0 / 6,
        RETWEET: 1.0 / 6,
        REPLY: 2.0 / 6,
        CONTENT_VOLUME: 0.4,
        CONTENT_VELOCITY: 0.6,
        CONTENT_AVG: 0.6,
        CONTENT_MESSAGE_SCORE: 0.4,
        FIN_EVENT: 0.6,
        FIN_TOPIC: 0.4,
        CONTENT: 5.0 / 19,
        REACH: 10.0 / 19,
        INTERACTION: 4.0 / 19,
        BONUS: 3.0 / 19,
        DEFAULT_SCORE: 20.0,
        DECAY_INTERVAL: decay_interval,
        DECAY_VALUE: decay_value,
        DECAY_RATE: decay_rate,
        DECAY_RATE_GURU: decay_rate_guru
    }

weights = get_weights()


def get_specialist(sns_id, start, end, start_prev):
    """
    Get the calculated topic scores for each publisher in a given period of time.

    Args:
        sns_id (string): The id of a channel of a publisher in string format
        start (datetime): The starting datetime
        end (datetime): The ending datetime

    Returns:

    """
    # debug_count_str = '%d messages of (sns_id=' + str(sns_id) + ') processed'
    query = {
        "size": 0,
        "query": {
            "filtered": {
                "query": {
                    "match_all": {}
                },
                "filter": {
                    "and": {
                        "filters": [
                            {
                                "exists": {
                                    "field": "tag"
                                }
                            },
                            {
                                "term": {
                                    "sns_author_id": sns_id
                                }
                            },
                            {
                                "range": {
                                    "published_at": {
                                        "from": start,
                                        "to": end
                                    }
                                }
                            },
                            {
                                "term": {
                                    "channel": "twitter"
                                }
                            }
                        ]
                    }
                }
            }
        },
        "aggs": {
            "by_topic": {
                "terms": {
                    "field": "tag",
                    "size": 0
                },
                "aggs": {
                    "by_tweet_type": {
                        "terms": {
                            "field": "channel_meta.tweet_type",
                            "size": 0
                        },
                        "aggs": {
                            "content_score": {
                                "sum": {
                                    "script": ("mn = doc['score_mn'].value; "
                                               "mp = doc['score_mp'].value; "
                                               "if (doc['score_mn'].empty) "
                                               "{mn = %(DEFAULT_MN)d;}; "
                                               "if (doc['score_mp'].empty) "
                                               "{mp = %(DEFAULT_MP)d;}; "
                                               "return 60 * doc['score_cr'].value + "
                                               "10 * (((mp * mn) - %(MIN_MNP)f) / %(SUB_MNP)f) + "
                                               "5 * doc['score_long'].value + "
                                               "20 * doc['score_mimp'].value + "
                                               "5 * doc['score_lang'].value;") % MESSAGE_SCORE_PARAMS
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    res = client_ES.search(index='message', doc_type='message', body=query, request_timeout=36000, search_type='count')

    topic_scores = []
    for topic in res['aggregations']['by_topic']['buckets']:
        s = 0.0
        a = 0
        for tweet_type in topic['by_tweet_type']['buckets']:
            s += WEIGHTS[TWEET_TYPE[tweet_type['key']]] * tweet_type['content_score']['value']
            a += tweet_type['doc_count']
        avg = s / a
        topic_scores.append(
            {'id': topic['key'], 's_topic': s, 's_topic_avg': avg, 'c_topic_hits': a, 'sns_id': sns_id})

    # df = pd.DataFrame(topic_scores)

    # df_prev = pd.DataFrame(tss_dao.get_topic_score_by_sns_id('topic_sns_score_by_sns', sns_id, 51, scored_at=start_prev),
    #                        columns=['sns_name', 'sns_id', 'metric_id', 'start', 'id', 'score'])

    # df = df.merge(df_prev, on='id', how='outer', suffixes=['', '_prev'])
    # df = df.fillna(0.0)
    # df['vol'] = df['s_topic'] + df['score']
    # df['final_score'] = weights[CONTENT_MESSAGE_SCORE] * (weights[CONTENT_VOLUME] * df.vol + weights[CONTENT_VELOCITY] * df.s_topic) +
    #     weights[CONTENT_AVG] * df.s_topic_avg

    return topic_scores


class PublisherIndexing(ConsumerMixin):

    def __init__(self, broker_url, queue, week, year):
        self.count = 0
        self._fields = {
            50: 's_topic',
            51: 's_topic',
            52: 's_topic_avg',
            53: 'c_topic_hits'
        }

        if broker_url:
            self.broker_url = broker_url
        else:
            raise ValueError
        self.connection = connections[BrokerConnection(broker_url)].acquire(block=True)
        self.queue = queue
        self.week = week
        self.year = year
        self.es_index = 'ps_w%d_y%d' % (week, year)
        self.es_doc_type = 'ps'

        logger.info('Indexing publishers to %s/%s' % (self.es_index, self.es_doc_type))

        self.start, self.end = get_week_date_range(week=week, year=year)
        self.start_prev = self.start - timedelta(days=7)

        self.metric_dao = MetricDao(cluster, PUBLISHER_ANALYTICS).connect()
        self.es_dict_dao = ModelDao(client_ES)
        self.tss_dao = TopicSnsScoreDao(cluster, PUBLISHER_ANALYTICS)

        self._topics = self._init_topics_dict()

        if not self._load_scores():
            raise Exception('Cannot load scores from Cassandra')

    def __del__(self):
        self.metric_dao.close()
        self.tss_dao.close()

    def _load_scores(self):
        self._scores = {
            'sentifi': {},
            'klout': {},
            'followers': {},
            'sentifi_prev': {}
        }
        for i in range(0, 50):
            try:
                logger.info('Loading s_sentifi from Cassandra')
                self._scores['sentifi'] = {each['sns_id']: each['stat_value']
                                           for each in self.metric_dao.get_metrics_by_date(WEEKLY_METRIC, 39, start=self.start)}
                logger.info('Loading s_klout from Cassandra')
                self._scores['klout'] = {each['sns_id']: each['stat_value']
                                         for each in self.metric_dao.get_metrics_by_date(WEEKLY_METRIC, 29, start=self.start)}
                logger.info('Loading c_followers from Cassandra')
                self._scores['followers'] = {each['sns_id']: each['stat_value']
                                             for each in self.metric_dao.get_metrics_by_date(WEEKLY_METRIC, 2, start=self.start)}
                logger.info('Loading s_sentifi_prev from Cassandra')
                self._scores['sentifi_prev'] = {each['sns_id']: each['stat_value']
                                                for each in self.metric_dao.get_metrics_by_date(WEEKLY_METRIC, 39, start=self.start_prev)}
                logger.info('DONE Loading from Cassandra')
                return True
            except Exception, e:
                logger.info(str(e))
                sleep(2)
                continue
        return False

    def _read_scores_by_sns_id(self, twitter_id):
        s_sentifi = self._scores['sentifi'][twitter_id] if twitter_id in self._scores['sentifi'] else None
        s_klout = self._scores['klout'][twitter_id] if twitter_id in self._scores['klout'] else None
        c_followers = self._scores['followers'][twitter_id] if twitter_id in self._scores['followers'] else None
        s_sentifi_prev = self._scores['sentifi_prev'][
            twitter_id] if twitter_id in self._scores['sentifi_prev'] else None
        return s_sentifi, s_klout, c_followers, s_sentifi_prev

    def _cast_params(self, sns_id, topics, field_name):
        return [(int(topic['id']), topic['sns_id'], float(topic[field_name])) for topic in topics]

    def _init_topics_dict(self):
        return {
            50: [],
            51: [],
            52: [],
            53: [],
            'length': 0
        }

    def _add_topics(self, twitter_id, topics):
        for topic_id, field_name in self._fields.iteritems():
            self._topics[topic_id] += self._cast_params(twitter_id, topics, field_name)
        self._topics['length'] += len(topics)

    def _insert_topic_scores(self):
        logger.info('Started ')
        for topic_id, topics in self._topics.iteritems():
            if type(topic_id) is int:
                tss = TopicSnsScore(
                    scored_at=self.start,
                    metric_id=topic_id,
                    sns_name='tw',
                    triplets_list=topics
                )
                self.tss_dao.insert(tss)

        self._topics = self._init_topics_dict()

    def get_consumers(self, Consumer, channel):
        return [Consumer(
            queues=self.queue,
            callbacks=[self.on_message_received])]

    def on_message_received(self, payload, message):
        self.count += 1
        if self.count % 10000 == 0:
            logger.debug('%d processed' % self.count)
        if self.count % verbosity == 0:
            logger.info('%d processed' % self.count)

        pub_id = ObjectId(payload[0])
        twitter_id = str(payload[1]).strip()
        twitter_id = '' if twitter_id is None else twitter_id
        if not twitter_id:
            logger.error('Cannot find twitter id')
            return False

        pub = pub_coll.find_one({'_id': pub_id})
        twitter_user = twitter_coll.find_one({'id_str': twitter_id})

        s_sentifi, s_sentifi_prev, s_klout, c_followers = self._read_scores_by_sns_id(twitter_id)

        s_sentifi_delta = (s_sentifi - s_sentifi_prev
                           if s_sentifi is not None and s_sentifi_prev is not None else None)
        s_klout_delta = s_sentifi - s_klout if s_klout is not None and s_sentifi is not None else None

        name = pub['name'] if 'name' in pub else ''
        isocode = pub['countryCode'] if 'countryCode' in pub else ''
        try:
            cats = [{'id': str(cat['_id']),
                     'parent_id': str(cat['parentId']),
                     'name': cat['name'],
                     'level': cat['level']} for cat in pub['categories'] if 'categories' in pub]
        except:
            cats = None

        try:
            topics = get_specialist(twitter_id, self.start, self.end, self.start_prev)
            publisher = {
                'week': self.week,
                'year': self.year,
                'start_date': self.start,
                'channel_meta': {
                    'sns_id': twitter_id,
                    'screen_name': twitter_user['screen_name'],
                    'name': name,
                    'photo_url': twitter_user['profile_image_url'],
                    'description': twitter_user['description'],
                },
                'score': {
                    's_sentifi': s_sentifi,
                    's_sentifi_prev': s_sentifi_prev,
                    's_sentifi_delta': s_sentifi_delta,
                    's_klout': s_klout,
                    's_klout_delta': s_klout_delta,
                    'c_followers': c_followers
                },
                'filters': {
                    'isocode': isocode,
                    'categories': cats,
                    'topics': topics
                }
            }

            self._add_topics(twitter_id, topics)
            if self._topics['length'] > 50000:
                self._insert_topic_scores()
            self.es_dict_dao.index(self.es_index, self.es_doc_type, publisher, synchronously=False)
            # message.ack()
        except:
            if error_output > 0:
                logger.error(traceback.format_exc())
                logger.error(twitter_user)
            # message.reject()
            pass


def main(options):
    global verbosity
    global error_output

    parser = ArgumentParser(prog='PubDataIndexing', description='Indexing publisher scores and meta data to ES')
    parser.add_argument('-q', '--queue', dest='queue', type=str, required=True,
                        help='the queue name to get pub id')
    parser.add_argument('-w', '--week', dest='week', type=int, required=True,
                        help='The week number to retrieve and index data')
    parser.add_argument('-y', '--year', dest='year', type=int, required=True,
                        help='The year number to retrieve and index data')
    parser.add_argument('-v', '--verbose', dest='verbosity', type=int,
                        help='the level of verbosity')
    parser.add_argument('-e', '--error', dest='error_output', type=int, default=0,
                        help='output the error to screen or not')
    args = parser.parse_args()

    if args.verbosity:
        verbosity = int(100000.0 / args.verbosity) if args.verbosity > 2 else 100000
        logger.setLevel(DEBUG)
    else:
        verbosity = 100000
        logger.setLevel(INFO)

    if args.error_output > 0:
        error_output = 1
    else:
        error_output = 0

    classifier = PublisherIndexing(
        broker_url=BROKER_URL,
        queue=Queue(args.queue),
        week=args.week,
        year=args.year
    )

    try:
        logger.info('Indexing publisher of week %d, year %d' % (args.week, args.year))
        classifier.run()

    except Exception, e:
        logger.exception(e)
        logger.error('The program is terminated.')

    if classifier._topics['length'] > 0:
            classifier._insert_topic_scores()
    classifier.es_dict_dao.commit()

if __name__ == '__main__':
    main(sys.argv[1:])


# coll = client['Candidates']['TwitterSFollower']
# exchange = Exchange(name='Hoan_DescriptionAnalysis_SFollower_Exchange', type='fanout', durable=True)count = 0
# with producers[connections[BrokerConnection(BROKER_URL)].acquire(block=True)].acquire(block=True) as producer:                                                                                                    for twitter in coll.find({'cat':'NoRule'}, timeout=False):
#         count += 1
#         if count % 2000 == 0:
#             print count
#         producer.publish(body=twitter['id_str'], retry=True, exchange=exchange, declare=[exchange])
#    ....:
