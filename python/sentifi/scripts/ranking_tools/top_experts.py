import sys
from logging import INFO, DEBUG
from argparse import ArgumentParser
from multiprocessing import Queue, Process

from helpers.utils import init_logger
from sentifi_ranking.utils.dateutils import get_week_date_range
from constant.cassandra_namespace import PUBLISHER_ANALYTICS
from constant.cassandra_namespace import WEEKLY_METRIC
from model.metric_dao import MetricDao
from model.cassandra_dao.topic_sns_score import TopicSnsScoreDao
from sentifi.common import utils
from sentifi import make_redis

from cassandra.cluster import Cluster
from cassandra.policies import TokenAwarePolicy, DCAwareRoundRobinPolicy, RetryPolicy
from psycopg2 import connect
from scipy import stats

logger = init_logger(__name__, log_level=DEBUG)

logger.info('Initializing params and constants')

CASSANDRA_HOSTS = [
    '10.0.0.251',
    '10.0.0.250',
    '10.0.0.249'
]

NUMBER_OF_PROCESSES = 16
EXPERT_PERCENTILE = 90

verbosity = 1
error_output = 0
save_path = ''

logger.info('Initializing connections')


def get_pg_connection(database, user, password, host, port):
    logger.info('Connecting to PostgreSQL %s@%s:%s/%s...' % (user, host, port,
                                                             database))
    return connect(database=database, user=user, password=password, host=host,
                   port=port)

conn = get_pg_connection('rest', 'dbr', 'sentifi', 'psql0.ssh.sentifi.com', 5432)


class Worker(Process):

    def __init__(self, queue, start):
        Process.__init__(self)
        logger.info("Initializing connection")
        self._cluster = Cluster(
            CASSANDRA_HOSTS,
            load_balancing_policy=TokenAwarePolicy(DCAwareRoundRobinPolicy()),
            default_retry_policy=RetryPolicy()
            #cql_version='3.1.7'
        )
        self._queue = queue
        self._count = 0
        self._start = start

    def run(self):
        logger.info("Worker %s started with %d topic ids" % (self.name, self._queue.qsize()))
        tss_dao = TopicSnsScoreDao(self._cluster, PUBLISHER_ANALYTICS)
        redis_main_client = make_redis(db=0)
        redis_score_client = make_redis(db=1)
        for topic_id in self._queue.get():
            # Retrieve scores from Cassandra
            scores = {score['sns_id']: score['score']
                      for score in tss_dao.get_topic_score_by_topic_id('topic_sns_score_by_topic',
                                                                       topic_id,
                                                                       54,
                                                                       self._start)}
            # Calculating percentiles
            total_topics = len(scores)
            percentiles = stats.rankdata(scores.values(), 'average') * 100 / total_topics

            # Insert to redis
            redis_topic_key = 'topic_experts:%d' % topic_id
            redis_sns_key = 'sns_id_topics:%s'
            pairs = []
            try:
                for sns_id, score, percentile in zip(scores.keys(), scores.values(), percentiles):
                    if percentile >= EXPERT_PERCENTILE and redis_score_client.get(sns_id):
                        pairs.append(redis_score_client.get(sns_id))
                        pairs.append(sns_id)
                    redis_main_client.zadd(redis_sns_key % sns_id, percentile, topic_id)
                if pairs:
                    redis_main_client.zadd(redis_topic_key, *pairs)
                logger.info("[Worker %s][topic: %d] has %d sns_ids" % (self.name, topic_id, total_topics))
            except Exception, e:
                logger.info("[Worker %s][topic: %d] ERRORs" % (self.name, topic_id))
                logger.info(str(object=pairs))
                logger.error(str(e))


def get_topic_ids():
    logger.info('Get all topic ids')
    query = """SELECT DISTINCT(i.id) FROM item i WHERE i.profileready=0;"""
    with conn.cursor() as cur:
        cur.execute(query)
        conn.commit()
        topic_ids = [row[0] for row in cur]
    logger.info('There are %d topics' % len(topic_ids))
    return topic_ids


def get_sentifi_scores(start):
    logger.info('Get all sentifi scores')
    cluster = Cluster(
        CASSANDRA_HOSTS,
        load_balancing_policy=TokenAwarePolicy(DCAwareRoundRobinPolicy()),
        default_retry_policy=RetryPolicy()
        #cql_version='3.1.7'
    )
    # sentifi scores will be expired after 6 hours
    EXPIRES = 60 * 60 * 6

    redis_scores_client = make_redis(db=1)
    # Retrieve sentifi score
    with MetricDao(cluster, PUBLISHER_ANALYTICS) as metric_dao:
        for each in metric_dao.get_metrics_by_date(WEEKLY_METRIC, 39, start=start):
            redis_scores_client.set(each['sns_id'], each['stat_value'], ex=EXPIRES)


def calculating(week, year):
    logger.info('Calculating experts of week %d, in year %d' % (week, year))
    start, end = get_week_date_range(week=week, year=year)
    logger.info('From %s to %s' % (str(start), str(end)))

    queue = Queue()

    topic_ids = get_topic_ids()
    # get_sentifi_scores(start)

    logger.info('Chunking topic ids to workers')
    for topic_chunks in utils.chunking(topic_ids, n=2000):
        queue.put(topic_chunks)
    logger.info('Running workers')
    workers = [Worker(queue, start) for _id in range(NUMBER_OF_PROCESSES)]
    _ = [each.start() for each in workers]
    _ = [each.join() for each in workers]
    return True


def main(options):
    global verbosity
    global error_output
    global save_path

    parser = ArgumentParser(prog='TopExperts', description='Extracting top experts by topic and caching')
    parser.add_argument('-w', '--week', dest='week', type=int, required=True,
                        help='The week number to retrieve and get top experts')
    parser.add_argument('-y', '--year', dest='year', type=int, required=True,
                        help='The year number to retrieve and get top experts')
    parser.add_argument('-v', '--verbose', dest='verbosity', type=int,
                        help='the level of verbosity')
    parser.add_argument('-e', '--error', dest='error_output', type=int, default=0,
                        help='output the error to screen or not')

    args = parser.parse_args()

    if args.verbosity:
        verbosity = int(500000.0 / args.verbosity) if args.verbosity > 2 else 100000
        logger.setLevel(DEBUG)
    else:
        verbosity = 100000
        logger.setLevel(INFO)

    if args.error_output > 0:
        error_output = 1
    else:
        error_output = 0

    try:
        return calculating(args.week, args.year)
    except Exception, e:
        logger.exception(e)
        logger.error('The program is terminated.')
        return False
    conn.close()


if __name__ == '__main__':
    a = main(sys.argv[1:])
