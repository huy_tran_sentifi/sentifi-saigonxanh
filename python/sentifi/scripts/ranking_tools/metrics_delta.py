__author__ = 'hoan'
from datetime import datetime
import logging
import math


import pandas as pd
# from sklearn.preprocessing import MinMaxScaler
from pymongo import MongoClient
from cassandra.cluster import Cluster
# from cassandra.auth import PlainTextAuthProvider

from constant.cassandra_namespace import PUBLISHER_ANALYTICS
from constant.cassandra_namespace import WEEKLY_COUNT, WEEKLY_METRIC
# from constant.cassandra_namespace import WEEKLY_SCALED_COUNT, WEEKLY_SCALED_METRIC
from model.metric import Metric
from model.metric_dao import MetricDao

from sentifi_ranking.utils.dateutils import get_week_date_range

CASSANDRA_HOSTS = [
    '10.0.0.251',
    '10.0.0.250',
    '10.0.0.249'
]

NUMBER_OF_PROCESSES = 16
BATCH_SIZE = 1500
COMPONENT = 'COMPONENT'
WEEK = 'WEEK'

cluster = Cluster(CASSANDRA_HOSTS)


logger = logging.getLogger('scripts.cassandra.metrics_delta')
client = MongoClient('mongo1x.ssh.sentifi.com:27027')
coll = client['mydb_merged10']['Publisher']



gurus = set(['1534167900', '23125257', '40471897', '2293387561', '174385219', '332617373', '1364930179', '140149665', '25913531',
             '20525971', '264370502', '20700725', '37002777', '78563470', '22088714', '21460063', '330361451', '19811134',
             '20936135', '16399949', '33576180', '52299954', '38566790', '250206398', '14897785', '191177057', '242893113',
             '1060578475', '142351603', '42632530', '264996175', '342829141', '67203194', '381289719', '19224439', '56562803',
             '10793052', '28858121', '246045169', '30271982', '129079450', '14798101', '20345218'])

celebs = set(['35094637', '31927467', '90420314', '53153263', '3004231', '259379883', '533085085', '15439395', '113419517',
              '20015311', '6427882', '58528137', '24741517', '135421739', '16827333', '200913899', '15253147', '140750163', '18825961',
              '19637934', '54641845', '21879024', '47786101', '86254626', '57998991', '71876190', '24086418', '41265813', '20978103',
              '46919513', '31353077', '69396376', '26642006', '221122127', '287834630', '18159470', '1269425228', '376506636', '21414413',
              '358996059', '330446733', '108936259', '612792060', '38127255', '15227791', '149590738', '33868638', '24083587', '23148645',
              '19249062', '46973152', '29930037', '38403110', '32309662', '207923746', '40368195', '53414786', '156791186', '155642429',
              '56481539', '127431090', '47285504', '80655102', '136402916', '23061619', '38708573', '87239242', '30913899', '20641995',
              '123774113', '815537202', '18132032', '21761627', '268059956', '184779135', '24776235', '165511377', '14444926', '24579796',
              '25382170', '92685993', '103614732', '62764427', '94816809', '146099195', '47571031', '278550447', '14562221', '82319018',
              '33452466', '113560280', '111392659', '8443752', '22745779', '34959923', '90379747', '35469661', '25465356', '257246857',
              '149103331', '135039839', '48466183', '784680583', '164596794', '50774064', '331382144', '63112528', '166173854',
              '102230123', '83055581', '228794007', '1450087165', '46042404', '273933548', '335495725', '222881714', '75444699',
              '179559238', '197588356', '21184930', '110195330', '64049044', '132267828', '246515294', '31483387', '69771400', '30478560',
              '253016329', '42170658', '43534925', '631926517', '15596316', '27750964', '18279004', '252818096', '128220786', '170998795',
              '21758143', '298195904', '116947296', '987131196', '119516512', '237177177', '17427779', '2226467971', '29281193', '943542662',
              '33900336', '79722019', '50612214', '26527734', '105210222', '135984962', '897435294', '176557682', '62491943', '69326018',
              '103061572', '531707563', '20261843', '29951362', '153009922', '79341370', '27894342', '332971617', '19688173', '219240315',
              '178782325', '1520018400', '21390378'])


def get_bonus():
    print "Get context"
    all_twitters = []
    index = 0
    for each in coll.find():
        if 'twitterIds' not in each:
            continue
        prio = 1 if 'priority' in each and each['priority'] == 1 else 0
        # itemid = each['itemId'] if 'itemId' in each else ''
        # _id = each['_id']
        name = each['name'] if 'name' in each else ''
        pub_type = ''
        if 'categories' in each and each['categories'] is not None:
            for cat in each['categories']:
                if 'level' in cat and 'name' in cat and cat['level'] == 1:
                    pub_type = cat['name']
                    break
            for _id in each['twitterIds']:
                if _id is None:
                    continue
                context = 25 if _id in gurus else prio * 35
                start = 20
                all_twitters.append([_id.strip(), prio, context, pub_type, name, start])
        index += 1
        if index % 50000 == 0:
            print index
    return pd.DataFrame(all_twitters, columns=['sns_id', 'prio', 'context', 'pub_type', 'name', 'start'])

def calculating_delta(df_pub, metric_left, metric_right, metric_table_left, metric_table_right, inserted_metric_id, weeks_years):
    """Calculating the velocity of metrics in REACH

    Args:
        metrics_ids (list of int): a list in metric id
        weeks (list of int): a list of week number
        year (int): the current year of those weeks

    Returns:

    """
    for week_left, week_right, year_left, year_right in weeks_years:
        print week_left
        start_left, end_left = get_week_date_range(week=week_left, year=year_left)
        start_right, end_right = get_week_date_range(week=week_right, year=year_right)

        with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
            print 'Querying data'
            df_left = pd.DataFrame(dao.get_metrics_by_date(metric_table_left, metric_left, start=start_left))
            df_right = pd.DataFrame(dao.get_metrics_by_date(metric_table_right, metric_right, start=start_right))

            df = df_left.merge(df_right, how='outer', on='sns_id', suffixes=('_left', '_right'))
            df = df.merge(df_pub, how='inner', on='sns_id', suffixes=('', '_pub'))
            df = df.dropna().fillna(0)
            df['delta'] = df.stat_value_left - df.stat_value_right

            metrics = []
            for index, row in df.iterrows():
                sns_id = str(int(row.sns_id))
                metrics.append(
                    Metric(sns_name='tw', sns_id=sns_id, metric_id=inserted_metric_id,
                           count_date=start_left, count_value=row['delta']))

                if index % 100000 == 0:
                    print index

            print 'Inserting DELTA to Cassandra, table weekly_sns_stats'
            begin = datetime.now()
            dao.insert_batch(metrics, table_name=WEEKLY_COUNT, batch_size=1000, timeout=36000)
            stop_time = datetime.now()
            print week_left, (stop_time - begin).total_seconds(), len(metrics)

            print 'Inserting DELTA to Cassandra, table weekly_metric'
            begin = datetime.now()
            dao.insert_batch(metrics, table_name=WEEKLY_METRIC, batch_size=1000, timeout=36000)
            stop_time = datetime.now()
            print week_left, (stop_time - begin).total_seconds(), len(metrics)


if __name__ == '__main__':
    df_pub = get_bonus()
    df_pub.sns_id = df_pub.sns_id.apply(str)
    # # Calculated delta of 2 consecutive weeks S-score
    # calculating_delta(df_pub, 39, 39, WEEKLY_METRIC, WEEKLY_METRIC, 40, [(44, 43, 2014, 2014), (45, 44, 2014, 2014), (46, 45, 2014, 2014),
    #                                                                      (47, 46, 2014, 2014), (48, 47, 2014, 2014), (49, 48, 2014, 2014),
    #                                                                      (50, 49, 2014, 2014), (51, 50, 2014, 2014), (52, 51, 2014, 2014),
    #                                                                      (1, 52, 2015, 2014), (2, 1, 2015, 2015), (3, 2, 2015, 2015),
    #                                                                      (4, 3, 2015, 2015), (5, 4, 2015, 2015)])
    # # Calculated delta of S-score and klout
    # calculating_delta(df_pub, 39, 29, WEEKLY_METRIC, WEEKLY_METRIC, 41, [(43, 43, 2014, 2014), (44, 44, 2014, 2014), (45, 45, 2014, 2014),
    #                                                                      (46, 46, 2014, 2014), (47, 47, 2014, 2014), (48, 48, 2014, 2014),
    #                                                                      (49, 49, 2014, 2014), (50, 50, 2014, 2014), (51, 51, 2014, 2014),
    #                                                                      (52, 52, 2014, 2014), (1, 1, 2015, 2015), (2, 2, 2015, 2015),
    #                                                                      (3, 3, 2015, 2015), (4, 4, 2015, 2015), (5, 5, 2015, 2015)])

    calculating_delta(df_pub, 39, 39, WEEKLY_METRIC, WEEKLY_METRIC, 40, [(7, 6, 2015, 2015)])
    # Calculated delta of S-score and klout
    calculating_delta(df_pub, 39, 29, WEEKLY_METRIC, WEEKLY_METRIC, 41, [(7, 7, 2015, 2015)])

    calculating_delta(df_pub, 39, 39, WEEKLY_METRIC, WEEKLY_METRIC, 40, [(8, 7, 2015, 2015)])
    # Calculated delta of S-score and klout
    calculating_delta(df_pub, 39, 29, WEEKLY_METRIC, WEEKLY_METRIC, 41, [(8, 8, 2015, 2015)])
