__author__ = 'hoan'
from datetime import datetime
import logging
import math

import pandas as pd
# from sklearn.preprocessing import MinMaxScaler
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider

from constant.cassandra_namespace import PUBLISHER_ANALYTICS, METRIC
from constant.cassandra_namespace import WEEKLY_COUNT, WEEKLY_METRIC
# from constant.cassandra_namespace import WEEKLY_SCALED_COUNT, WEEKLY_SCALED_METRIC
# from helper.connection_util import get_cassandra_connection
from sentifi_ranking.utils.dateutils import get_week_date_range
# from model.metric import Metric
from model.metric_dao import MetricDao


# TODO: use environment variables
CASSANDRA_HOSTS = [
    '10.0.0.126'
]
CASSANDRA_USER = 'kso'
CASSANDRA_PASSWORD = 'sentifi'

ap = PlainTextAuthProvider(username=CASSANDRA_USER, password=CASSANDRA_PASSWORD)
cluster = Cluster(CASSANDRA_HOSTS, auth_provider=ap)

logger = logging.getLogger('scripts.cassandra.get_scores')


def get_scores(metric_id, week, year):
    """
    Get the scores of all publishers for a given week

    Args:
        week (int):
        year (int):

    Returns:
        DataFrame: a DataFrame that has the score
    """

    start, end = get_week_date_range(week=week, year=year)
    with MetricDao(cluster, PUBLISHER_ANALYTICS) as dao:
        df = pd.DataFrame(dao.get_metrics_by_date(WEEKLY_METRIC, metric_id, start=start))
        return df

if __name__ == '__main__':
    metric_id = 39
    df = get_scores(metric_id, 48, 2014)
