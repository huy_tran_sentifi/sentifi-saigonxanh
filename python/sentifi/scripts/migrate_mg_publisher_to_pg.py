#!/usr/bin/env python
#  Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 31/03/2015
# @author: trung

from argparse import ArgumentParser
from bson import ObjectId

import logging
import sys

from constant.mongo_namespace import C_PUBLISHER, D_MERGED_PUBLISHER
from constant.postgres_namespace import T_PUBLISHER_FROM_MONGO
from helpers.connection_utils import get_mg_merged_publisher_client, \
    get_pg_core_connection
from helpers.utils import init_logger, oid_hex_to_datetime
from model.mongo_dao import DictionaryDao as MgDictionary
from model.postgres_dao import DictionaryDao as PgDictionary


def main(options):
    parser = ArgumentParser(description='tools that replicates Publisher from Mongo to Postgres')
    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    # Setup logger
    global logger
    log_file = args.log_file
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=log_file)
    logger = logging.getLogger(__name__)

    with get_mg_merged_publisher_client() as mg_client, \
            get_pg_core_connection() as pg_conn:
        mg_publisher_dao = MgDictionary(mg_client, D_MERGED_PUBLISHER, C_PUBLISHER)
        pg_publisher_dao = PgDictionary(pg_conn, T_PUBLISHER_FROM_MONGO)

        # Get timestamp of the newest Publisher in Postgres database
        last_timestamp = pg_publisher_dao.max('created_at')
        # Get recent data after that timestamp
        if last_timestamp:
            recent_data_cursor = mg_publisher_dao.find_recent(
                time_field='_id',
                a_timestamp=ObjectId.from_datetime(last_timestamp),
                limit=0
            )
        else:
            recent_data_cursor = mg_publisher_dao.find_recent(
                time_field='_id',
                limit=0
            )
        # Iterate over the cursor and upsert into Postgres
        for mg_publisher in recent_data_cursor:
            logger.debug('Publisher fetched from mongo: %s' % mg_publisher)
            oid = mg_publisher.get('_id')
            logger.info('Upserting Publisher %s to Postgres' % oid)
            pg_publisher_dao.upsert(
                mg_publisher,
                object_id=oid,
                created_at=oid_hex_to_datetime(oid)
            )

if __name__ == '__main__':
    main(sys.argv[1:])
