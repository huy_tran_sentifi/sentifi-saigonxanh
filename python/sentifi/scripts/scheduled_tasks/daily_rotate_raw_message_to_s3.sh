#!/bin/bash

export WORKON_HOME="/home/sentifi/virtualenvs"
source `which virtualenvwrapper.sh`
workon sentifi-saigonxanh

working_date=$(date --date "-1 day" +%Y-%m-%d)

# Backup raw Moreover
cmd="$VIRTUAL_ENV/python/sentifi/scripts/migration_tools/backup_mongo_to_s3.py -H mongo-8.eu-west-1.compute.internal -p 27017 -d moreover -c articles -s $working_date -e $working_date -f prod/raw_moreover -v"
echo "Executing: $cmd"
$cmd >> $VIRTUAL_ENV/log/backup_raw_moreover_to_s3.log 2>&1

# Backup raw RSS
cmd="$VIRTUAL_ENV/python/sentifi/scripts/migration_tools/backup_mongo_to_s3.py -H mongo-8.eu-west-1.compute.internal -p 27017 -d rss -c rss_entries -s $working_date -e $working_date -f prod/raw_rss_entries -v"
echo "Executing: $cmd"
$cmd >> $VIRTUAL_ENV/log/backup_raw_rss_entries_to_s3.log 2>&1

# Backup raw Tweets
cmd="$VIRTUAL_ENV/python/sentifi/scripts/migration_tools/backup_mongo_to_s3.py -H mongo-8.eu-west-1.compute.internal -p 27017 -d sentifi -c tweet -s $working_date -e $working_date -f prod/raw_tweet -v"
echo "Executing: $cmd"
$cmd >> $VIRTUAL_ENV/log/backup_raw_tweet_to_s3.log 2>&1

# Backup raw GNIP
cmd="$VIRTUAL_ENV/python/sentifi/scripts/migration_tools/backup_mongo_to_s3.py -H mongo-8.eu-west-1.compute.internal -p 27017 -d sentifi -c gnip -s $working_date -e $working_date -f prod/raw_gnip -v"
echo "Executing: $cmd"
$cmd >> $VIRTUAL_ENV/log/backup_raw_gnip_to_s3.log 2>&1

working_date=$(date --date "-31 days" +%Y-%m-%d)

# Remove raw Moreover
cmd="$VIRTUAL_ENV/python/sentifi/scripts/tools/remove_mongo_by_date.py -H mongo-8.eu-west-1.compute.internal -p 27017 -d moreover -c articles -s $working_date -e $working_date -v -f"
echo "Executing: $cmd"
$cmd >> $VIRTUAL_ENV/log/remove_raw_moreover.log 2>&1

# Remove raw RSS
cmd="$VIRTUAL_ENV/python/sentifi/scripts/tools/remove_mongo_by_date.py -H mongo-8.eu-west-1.compute.internal -p 27017 -d rss -c rss_entries -s $working_date -e $working_date -v -f"
echo "Executing: $cmd"
$cmd >> $VIRTUAL_ENV/log/remove_raw_rss_entries.log 2>&1

# Remove raw Tweets
cmd="$VIRTUAL_ENV/python/sentifi/scripts/tools/remove_mongo_by_date.py -H mongo-8.eu-west-1.compute.internal -p 27017 -d sentifi -c tweet -s $working_date -e $working_date -v -f"
echo "Executing: $cmd"
$cmd >> $VIRTUAL_ENV/log/remove_raw_tweet.log 2>&1

deactivate
