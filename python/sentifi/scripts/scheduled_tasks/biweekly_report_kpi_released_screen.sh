#!/bin/bash

export WORKON_HOME="/home/sentifi/virtualenvs"
source `which virtualenvwrapper.sh`
workon sentifi-saigonxanh

cmd="$VIRTUAL_ENV/python/sentifi/scripts/analytic_tools/kpi_released_screens.py -v"
mkdir -p $VIRTUAL_ENV/log
touch $VIRTUAL_ENV/log/kpi_released_screens.log
echo "Executing: $cmd"
$cmd >> $VIRTUAL_ENV/log/kpi_released_screens.log 2>&1

deactivate
