#!/bin/bash

export WORKON_HOME="/home/sentifi/virtualenvs"
source `which virtualenvwrapper.sh`
workon sentifi-saigonxanh

cmd="$VIRTUAL_ENV/python/sentifi/scripts/analytic_tools/kpi_statistics.py -v"
mkdir -p $VIRTUAL_ENV/log
touch $VIRTUAL_ENV/log/kpi_statistics.log
echo "Executing: $cmd"
$cmd >> $VIRTUAL_ENV/log/kpi_statistics.log 2>&1

deactivate
