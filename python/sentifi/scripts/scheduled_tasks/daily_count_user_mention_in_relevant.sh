#!/bin/bash

export WORKON_HOME="/home/sentifi/virtualenvs"
source `which virtualenvwrapper.sh`
workon sentifi-saigonxanh

working_date=$(date --date "-1 day" +%Y-%m-%d)

cmd="$VIRTUAL_ENV/python/sentifi/scripts/analytic_tools/count_id_mentions_by_date.py -s $working_date -v"
echo "Executing: $cmd"
$cmd >> $VIRTUAL_ENV/log/count_id_mentions_by_date.log 2>&1

deactivate
