#!/bin/bash

export WORKON_HOME="/home/sentifi/virtualenvs"
source `which virtualenvwrapper.sh`
workon sentifi-saigonxanh

working_date=$(date --date "-1 day" +%Y-%m-%d)

# Backup new RelevantMessage
cmd="$VIRTUAL_ENV/python/sentifi/scripts/migration_tools/backup_mongo_to_s3.py -H mongo-rank-dev-2.ireland.sentifi.internal -p 27017 -d Twitter -c User -s $working_date -e $working_date -f prod/twitter_profile -v"
echo "Executing: $cmd"
$cmd >> $VIRTUAL_ENV/log/backup_twitter_profile_to_s3.log 2>&1

deactivate
