#!/bin/bash

export WORKON_HOME="/home/sentifi/virtualenvs"
source `which virtualenvwrapper.sh`
workon sentifi-saigonxanh

working_date=$(date --date "-1 day" +%Y-%m-%d)

# Backup Orange Flow's raw Tweets
cmd="$VIRTUAL_ENV/python/sentifi/scripts/migration_tools/backup_mongo_to_s3.py -H mongo7.ireland.sentifi.internal -p 27017 -d sentifi -c tweet_potential_publisher -s $working_date -e $working_date -f prod/orange_flow_raw_tweet -v"
echo "Executing: $cmd"
$cmd >> $VIRTUAL_ENV/log/backup_orange_flow_raw_tweet_to_s3.log 2>&1

# Backup Orange Flow's RelevantMessage
cmd="$VIRTUAL_ENV/python/sentifi/scripts/migration_tools/backup_mongo_to_s3.py -H mongo7.ireland.sentifi.internal -p 27017 -d analytic_tmp -c relevant_document -s $working_date -e $working_date -f prod/orange_flow_relevant_document -v"
echo "Executing: $cmd"
$cmd >> $VIRTUAL_ENV/log/backup_orange_flow_relevant_document_to_s3.log 2>&1

working_date=$(date --date "-8 days" +%Y-%m-%d)

# Remove Orange Flow's raw Tweets
cmd="$VIRTUAL_ENV/python/sentifi/scripts/tools/remove_mongo_by_date.py -H mongo7.ireland.sentifi.internal -p 27017 -d sentifi -c tweet_potential_publisher -s $working_date -e $working_date -v -f"
echo "Executing: $cmd"
$cmd >> $VIRTUAL_ENV/log/remove_orange_flow_raw_tweet.log 2>&1

# Remove raw RSS
cmd="$VIRTUAL_ENV/python/sentifi/scripts/tools/remove_mongo_by_date.py -H mongo7.ireland.sentifi.internal -p 27017 -d analytic_tmp -c relevant_document -s $working_date -e $working_date -v -f"
echo "Executing: $cmd"
$cmd >> $VIRTUAL_ENV/log/remove_orange_flow_relevant_document.log 2>&1

deactivate
