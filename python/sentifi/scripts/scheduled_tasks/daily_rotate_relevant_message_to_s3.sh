#!/bin/bash

export WORKON_HOME="/home/sentifi/virtualenvs"
source `which virtualenvwrapper.sh`
workon sentifi-saigonxanh

working_date=$(date --date "-31 days" +%Y-%m-%d)

# Backup new RelevantMessage
cmd="$VIRTUAL_ENV/python/sentifi/scripts/migration_tools/backup_mongo_to_s3.py -H mongoctrinh.ssh.sentifi.com -p 27017 -d analytic2 -c relevant -s $working_date -e $working_date -f prod/relevant -v"
echo "Executing: $cmd"
$cmd >> $VIRTUAL_ENV/log/backup_relevant_to_s3.log 2>&1

deactivate
