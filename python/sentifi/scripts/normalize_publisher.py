#!/usr/bin/env python
#  Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 04/04/2015
# @author: trung

from argparse import ArgumentParser
from bson import ObjectId
from psycopg2.extensions import register_type, UNICODE, UNICODEARRAY
from psycopg2.extras import RealDictCursor
from unidecode import unidecode

import logging
import re
import sys
import ujson as json

from constant.postgres_namespace import T_PUBLISHER_FROM_MONGO, S_MONGO
from helpers.connection_utils import get_pg_core_connection
from helpers.utils import init_logger, oid_hex_to_datetime
from model.postgres_dao import DictionaryDao, ModelDao


T_NORMALIZED_PUBLISHER = '%s.%s' %(S_MONGO, 'mg_normalized_publisher')
NAME_PATTERN = re.compile('[^a-zA-Z0-9 ]', re.UNICODE)
URL_PATTERN = re.compile('[^a-zA-Z0-9:/@\-\._?=&]', re.UNICODE)
PHONE_PATTERN = re.compile('[^a-zA-Z0-9]', re.UNICODE)


def normalize_name(name):
    # if name:
    #     logger.info('In: %s - %s' % (type(name), name))
    #     s = NAME_PATTERN.sub('', unidecode(name).lower())
    #     logger.info('Out: %s - %s' % (type(s), s))
    #     return s
    # else:
    #     return None
    result = NAME_PATTERN.sub('', unidecode(name).lower()) if name else None
    return result if result else None

def normalize_url(url):
    result = URL_PATTERN.sub('', unidecode(url).lower()) if url else None
    return result if result else None

def normalize_phone(phone):
    result = PHONE_PATTERN.sub('', unidecode(phone).lower()) if phone else None
    return result if result else None


class NormalizedPublisher():
    '''Class representing a normalized Publisher
    '''

    def __init__(self, publisher_mongo_id=None, status=None, created_at=None,
                 updated_at=None, name=None, display_name=None, first_name=None,
                 last_name=None, address=None, description=None, url=None,
                 phone=None, fb_url=None, gp_url=None, li_url=None,
                 news_name=None, news_url=None, tw_name=None,
                 tw_screen_name=None, tw_description=None):
        self.publisher_mongo_id = publisher_mongo_id
        self.status = status
        self.created_at = created_at
        self.updated_at = updated_at
        self.name = normalize_name(name)
        self.display_name = normalize_name(display_name)
        self.first_name = normalize_name(first_name)
        self.last_name = normalize_name(last_name)
        self.address = normalize_name(address)
        self.description = normalize_name(description)
        self.url = normalize_url(url)
        self.phone = normalize_phone(phone)

        self.fb_url = []
        self.gp_url = []
        self.li_url = []
        self.news_name = []
        self.news_url = []
        self.tw_name = []
        self.tw_screen_name = []
        self.tw_description = []

        if fb_url:
            self.fb_url = [normalize_url(_) for _ in fb_url]
            self.fb_url = [_ for _ in self.fb_url if _]

        if gp_url:
            self.gp_url = [normalize_url(_) for _ in gp_url]
            self.gp_url = [_ for _ in self.gp_url if _]

        if li_url:
            self.li_url = [normalize_url(_) for _ in li_url]
            self.li_url = [_ for _ in self.li_url if _]

        if news_name:
            self.news_name = [normalize_name(_) for _ in news_name]
            self.news_name = [_ for _ in self.news_name if _]
        if news_url:
            self.news_url = [normalize_url(_) for _ in news_url]
            self.news_url = [_ for _ in self.news_url if _]

        if tw_name:
            self.tw_name = [normalize_name(_) for _ in tw_name]
            self.tw_name = [_ for _ in self.tw_name if _]
        if tw_screen_name:
            self.tw_screen_name = [normalize_name(_) for _ in tw_screen_name]
            self.tw_screen_name = [_ for _ in self.tw_screen_name if _]
        if tw_description:
            self.tw_description = [normalize_name(_) for _ in tw_description]
            self.tw_description = [_ for _ in self.tw_description if _ ]

    @staticmethod
    def from_json(payload):
        return NormalizedPublisher(
            publisher_mongo_id=payload.get('publisher_mongo_id'),
            created_at=payload.get('created_at'),
            updated_at=payload.get('updated_at'),
            name=payload.get('name'),
            display_name=payload.get('display_name'),
            first_name=payload.get('first_name'),
            last_name=payload.get('last_name'),
            address=payload.get('address'),
            description=payload.get('description'),
            url=payload.get('url'),
            phone=payload.get('phone'),
            fb_url=payload.get('fb_url'),
            gp_url=payload.get('gp_url'),
            li_url=payload.get('li_url'),
            news_name=payload.get('news_name'),
            news_url=payload.get('news_url'),
            tw_name=payload.get('tw_name'),
            tw_screen_name=payload.get('tw_screen_name'),
            tw_description=payload.get('tw_description')
        ) if payload else None

    @property
    def mongo_id(self):
        return self.publisher_mongo_id

    def to_json_string(self):
        return json.dumps(self.__dict__)


class NormalizedPublisherDao(ModelDao):
    def __init__(self, connection, normalized_publisher_table_name):
        ModelDao.__init__(self, connection, normalized_publisher_table_name,
                          NormalizedPublisher)

    # Override
    def insert(self, normalized_publisher):
        self._validate_model(normalized_publisher)

        with self._conn.cursor() as cursor:
            query = '''
INSERT INTO {} (
    publisher_mongo_id,
    status,
    created_at,
    updated_at,
    name,
    display_name,
    first_name,
    last_name,
    address,
    description,
    url,
    phone,
    fb_url,
    gp_url,
    li_url,
    news_name,
    news_url,
    tw_name,
    tw_screen_name,
    tw_description
) VALUES (
    %(publisher_mongo_id)s,
    %(status)s,
    %(created_at)s,
    %(updated_at)s,
    %(name)s,
    %(display_name)s,
    %(first_name)s,
    %(last_name)s,
    %(address)s,
    %(description)s,
    %(url)s,
    %(phone)s,
    %(fb_url)s,
    %(gp_url)s,
    %(li_url)s,
    %(news_name)s,
    %(news_url)s,
    %(tw_name)s,
    %(tw_screen_name)s,
    %(tw_description)s
) RETURNING publisher_mongo_id;'''.format(self._table)
            query = cursor.mogrify(query, normalized_publisher.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result

    # Override
    def update(self, normalized_publisher):
        self._validate_model(normalized_publisher)

        with self._conn.cursor() as cursor:
            query = '''
UPDATE {} SET
    publisher_mongo_id = %(publisher_mongo_id)s,
    status = %(status)s,
    created_at = %(created_at)s,
    updated_at = %(updated_at)s,
    name = %(name)s,
    display_name = %(display_name)s,
    first_name = %(first_name)s,
    last_name = %(last_name)s,
    address = %(address)s,
    description = %(description)s,
    url = %(url)s,
    phone = %(phone)s,
    fb_url = %(fb_url)s,
    gp_url = %(gp_url)s,
    li_url = %(li_url)s,
    news_name = %(news_name)s,
    news_url = %(news_url)s,
    tw_name = %(tw_name)s,
    tw_screen_name = %(tw_screen_name)s,
    tw_description = %(tw_description)s
  WHERE publisher_mongo_id = %(publisher_mongo_id)s
  RETURNING publisher_mongo_id;'''.format(self._table)
            query = cursor.mogrify(query, normalized_publisher.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result

    # Override
    def upsert(self, normalized_publisher):
        self._validate_model(normalized_publisher)

        with self._conn.cursor() as cursor:
            update_sql = '''
UPDATE {} SET
    publisher_mongo_id = %(publisher_mongo_id)s,
    status = %(status)s,
    created_at = %(created_at)s,
    updated_at = %(updated_at)s,
    name = %(name)s,
    display_name = %(display_name)s,
    first_name = %(first_name)s,
    last_name = %(last_name)s,
    address = %(address)s,
    description = %(description)s,
    url = %(url)s,
    phone = %(phone)s,
    fb_url = %(fb_url)s,
    gp_url = %(gp_url)s,
    li_url = %(li_url)s,
    news_name = %(news_name)s,
    news_url = %(news_url)s,
    tw_name = %(tw_name)s,
    tw_screen_name = %(tw_screen_name)s,
    tw_description = %(tw_description)s
  WHERE publisher_mongo_id = %(publisher_mongo_id)s
  RETURNING publisher_mongo_id;'''.format(self._table)
            update_sql = cursor.mogrify(update_sql, normalized_publisher.__dict__)
            insert_sql = '''
INSERT INTO {} (
    publisher_mongo_id,
    status,
    created_at,
    updated_at,
    name,
    display_name,
    first_name,
    last_name,
    address,
    description,
    url,
    phone,
    fb_url,
    gp_url,
    li_url,
    news_name,
    news_url,
    tw_name,
    tw_screen_name,
    tw_description
) VALUES (
    %(publisher_mongo_id)s,
    %(status)s,
    %(created_at)s,
    %(updated_at)s,
    %(name)s,
    %(display_name)s,
    %(first_name)s,
    %(last_name)s,
    %(address)s,
    %(description)s,
    %(url)s,
    %(phone)s,
    %(fb_url)s,
    %(gp_url)s,
    %(li_url)s,
    %(news_name)s,
    %(news_url)s,
    %(tw_name)s,
    %(tw_screen_name)s,
    %(tw_description)s
) RETURNING publisher_mongo_id;'''.format(self._table)
            insert_sql = cursor.mogrify(insert_sql, normalized_publisher.__dict__)
            query = '''
SELECT upsert(%s, %s);'''
            query = cursor.mogrify(query, (update_sql, insert_sql))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result


def main(options):
    parser = ArgumentParser(description='tools that normalize Publisher from Postgres')
    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    # Setup logger
    global logger
    log_file = args.log_file
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=log_file)
    logger = logging.getLogger(__name__)

    with get_pg_core_connection() as pg_conn:
        register_type(UNICODE)
        register_type(UNICODEARRAY)
        publisher_dao = DictionaryDao(pg_conn, T_PUBLISHER_FROM_MONGO)
        normalized_publisher_dao = NormalizedPublisherDao(pg_conn, T_NORMALIZED_PUBLISHER)

        p_cur = pg_conn.cursor(cursor_factory=RealDictCursor)
        query = '''
SELECT
    object_id AS publisher_mongo_id,
    (object_payload->>'status')::float::int,
    created_at,
    updated_at,
    object_payload->>'name' AS name,
    object_payload->>'displayName' AS display_name,
    object_payload->'person' AS person,
    object_payload->>'address' AS address,
    object_payload->>'description' AS description,
    object_payload->>'url' AS url,
    object_payload->>'phone' AS phone,
    object_payload->'facebooks' AS facebook,
    object_payload->'googlepluss' AS googleplus,
    object_payload->'linkedins' AS linkedin,
    object_payload->'news' AS news,
    object_payload->'newss' AS newss,
    object_payload->'twitters' AS twitter
  FROM mongo.mg_publisher p
  WHERE NOT EXISTS (
    SELECT NULL
      FROM mongo.mg_normalized_publisher np
      WHERE np.publisher_mongo_id = p.object_id
  );'''
        logger.debug('Executing %s' % query)
        p_cur.execute(query)
        pg_conn.commit()
        for row in p_cur:
            if isinstance(row['person'], dict):
                row['first_name'] = row['person'].get('firstName', '')
                row['last_name'] = row['person'].get('lastName', '')
            if isinstance(row['facebook'], list):
                row['fb_url'] = [_.get('url') for _ in row['facebook'] if isinstance(_, dict) and _]
            if isinstance(row['googleplus'], list):
                row['gp_url'] = [_.get('url') for _ in row['googleplus'] if isinstance(_, dict) and _]
            if isinstance(row['linkedin'], list):
                row['li_url'] = [_.get('url') for _ in row['linkedin'] if isinstance(_, dict) and _]

            row['news_name'] = []
            row['news_url'] = []
            if isinstance(row['news'], list):
                for n in row['news']:
                    if isinstance(n, dict) and n:
                        if n.get('name'):
                            row['news_name'].append(n.get('name'))
                        if n.get('url'):
                            row['news_url'].append(n.get('url'))
            if isinstance(row['newss'], list):
                for n in row['newss']:
                    if isinstance(n, dict) and n:
                        if n.get('name'):
                            row['news_name'].append(n.get('name'))
                        if n.get('url'):
                            row['news_url'].append(n.get('url'))

            row['tw_name'] = []
            row['tw_screen_name'] = []
            row['tw_description'] = []
            if isinstance(row['twitter'], list):
                for t in row['twitter']:
                    if isinstance(t, dict) and t:
                        if t.get('name'):
                            row['tw_name'].append(t.get('name'))
                        if t.get('screen_name'):
                            row['tw_screen_name'].append(t.get('screen_name'))
                        if t.get('description'):
                            row['tw_description'].append(t.get('description'))

            np = NormalizedPublisher.from_json(row)
            normalized_publisher_dao.upsert(np)


if __name__ == '__main__':
    main(sys.argv[1:])
