#!/usr/bin/env python
#  Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 14/02/2015
# @author: trung

from argparse import ArgumentParser
from elasticsearch.client import Elasticsearch
from getpass import getpass
from imp import load_source
from psycopg2.extras import RealDictCursor

import logging
import os
import sys

from helpers.utils import init_logger
from helpers.connection_utils import get_pg_connection
from model.es_dao import DictionaryDao


setting = load_source('setting', os.environ['SETTING_FILE'])


def main(options):
    parser = ArgumentParser(description='index Postgres query\'s results')
    parser.add_argument('-H', '--host', dest='host', default='localhost', help='Postgres host to connect to (default: \'localhost\')' )
    parser.add_argument('-p', '--port', dest='port', type=int, default=5432, help='server port (default: 5432)')
    parser.add_argument('-d', '--db-name', dest='db_name', required=True, help='database name')
    parser.add_argument('-u', '--user', dest='user', required=True, help='database user')
    parser.add_argument('-x', '--password', dest='password', help='database password')
    parser.add_argument('-q', '--query', dest='query', help='SQL query, i.e: \'SELECT * FROM foo\'')

    parser.add_argument('-e', '--elasticsearch-hosts', dest='es_hosts', required=True, help='list of ElasticSearch hosts, i.e: \'host1.example.com:9200,host2.example.com:9200\'')
    parser.add_argument('-i', '--es-index', dest='es_index', required=True, help='ElasticSearch index name')
    parser.add_argument('-t', '--es-type', dest='es_type', required=True, help='ElasticSearch index type')

    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    # Setup logger
    global logger
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=args.log_file)
    logger = logging.getLogger(__name__)

    password = args.password if args.password else getpass('Password for user %s: ' % args.user)
    es_conn = Elasticsearch(hosts=args.es_hosts.split(','), sniff_on_start=True, sniff_on_connection_fail=True, sniffer_timeout=60)
    with get_pg_connection(args.db_name, args.user, password, args.host, args.port) as pg_conn, \
            DictionaryDao(es_conn) as es_dict_dao:
        cursor = pg_conn.cursor(cursor_factory=RealDictCursor)
        cursor.execute(args.query)
        for row in cursor:
            es_dict_dao.index(args.es_index, args.es_type, row)


if __name__ == '__main__':
    main(sys.argv[1:])
