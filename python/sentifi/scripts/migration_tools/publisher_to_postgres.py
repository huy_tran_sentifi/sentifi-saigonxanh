#!/usr/bin/env python
# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Oct 3, 2014
# @author: trung

from argparse import ArgumentParser
from bson import ObjectId
from psycopg2 import ProgrammingError
from threading import Event, Thread

import logging
import signal
import sys
import time
import traceback

from constant.mongo_namespace import C_FACEBOOK, C_GOOGLEPLUS, C_LINKEDIN, \
    C_NEWS, C_PUBLISHER, C_PUBLISHER_FACEBOOK, C_PUBLISHER_GOOGLEPLUS, \
    C_PUBLISHER_LINKEDIN, C_PUBLISHER_NEWS, C_PUBLISHER_TWITTER, C_TWITTER, \
    D_CORE
from constant.postgres_namespace import T_FACEBOOK, T_GOOGLE_PLUS, T_LINKEDIN, \
    T_NEWS, T_PUBLISHER_FROM_MONGO, T_TWITTER
from helpers.connection_utils import get_mg_publisher_connection, \
    get_pg_core_connection, get_rabbitmq_connection
from helpers.utils import init_logger
from model.mongo_dao import DictionaryDao as MgDictionaryDao
from model.postgres_dao import DictionaryDao as PgDictionaryDao
from workers.mg_publisher_to_pg_worker import publisher_to_postgres
from workers.mg_publisher_to_pg_worker import PublisherToPostgresWorker
from workers.queue import publisher_to_postgres_queues


SLEEP_DURATION = 5
threads = []


def handler(signum, frame):
    global logger, threads
    logger.warning('Signal %s received. Stopping all workers...' % signum)
    for thread in threads:
        thread.stop()


def migrate_from_mongo():
    """Import all Publishers directly from Mongo to Postgres by ObjectId order
    """
    with get_mg_publisher_connection() as mg_client, get_pg_core_connection() as pg_conn:
        # Initiate DAO objects on top of ``mg_client`` and ``pg_conn``
        mg_publisher_facebook = MgDictionaryDao(mg_client, D_CORE, C_PUBLISHER_FACEBOOK)
        mg_publisher_googleplus = MgDictionaryDao(mg_client, D_CORE, C_PUBLISHER_GOOGLEPLUS)
        mg_publisher_linkedin = MgDictionaryDao(mg_client, D_CORE, C_PUBLISHER_LINKEDIN)
        mg_publisher_news = MgDictionaryDao(mg_client, D_CORE, C_PUBLISHER_NEWS)
        mg_publisher_twitter = MgDictionaryDao(mg_client, D_CORE, C_PUBLISHER_TWITTER)
        mg_publisher = MgDictionaryDao(mg_client, D_CORE, C_PUBLISHER)
        mg_facebook = MgDictionaryDao(mg_client, D_CORE, C_FACEBOOK)
        mg_googleplus = MgDictionaryDao(mg_client, D_CORE, C_GOOGLEPLUS)
        mg_linkedin = MgDictionaryDao(mg_client, D_CORE, C_LINKEDIN)
        mg_news = MgDictionaryDao(mg_client, D_CORE, C_NEWS)
        mg_twitter = MgDictionaryDao(mg_client, D_CORE, C_TWITTER)
        pg_publisher = PgDictionaryDao(pg_conn, T_PUBLISHER_FROM_MONGO)
        pg_facebook = PgDictionaryDao(pg_conn, T_FACEBOOK)
        pg_googleplus = PgDictionaryDao(pg_conn, T_GOOGLE_PLUS)
        pg_linkedin = PgDictionaryDao(pg_conn, T_LINKEDIN)
        pg_news = PgDictionaryDao(pg_conn, T_NEWS)
        pg_twitter = PgDictionaryDao(pg_conn, T_TWITTER)
        # Start importing
        logger.info('Fetching Publisher\'s IDs from Mongo...')
        publisher_cursor = mg_publisher.find_recent(time_field='_id', fields={'_id': True})
        logger.info('Importing Publishers to Postgres...')
        while publisher_cursor:
            id_ = None
            for each in publisher_cursor:
                id_ = each.get('_id')
                logger.info('Migrating Publisher %s...' % id_)
                try:
                    publisher_to_postgres(
                        publisher_mongo_id=id_,
                        mg_publisher=mg_publisher,
                        mg_publisher_facebook=mg_publisher_facebook,
                        mg_publisher_google_plus=mg_publisher_googleplus,
                        mg_publisher_linkedin=mg_publisher_linkedin,
                        mg_publisher_news=mg_publisher_news,
                        mg_publisher_twitter=mg_publisher_twitter,
                        mg_facebook=mg_facebook,
                        mg_google_plus=mg_googleplus,
                        mg_linkedin=mg_linkedin,
                        mg_news=mg_news,
                        mg_twitter=mg_twitter,
                        pg_publisher=pg_publisher,
                        pg_facebook=pg_facebook,
                        pg_google_plus=pg_googleplus,
                        pg_linkedin=pg_linkedin,
                        pg_news=pg_news,
                        pg_twitter=pg_twitter
                    )
                except ProgrammingError:
                    logger.error('Exception found when migrate Publisher %s: %s' % (id_, traceback.format_exc()))
                    exit(1)
                except:
                    logger.error('Exception found when migrate Publisher %s: %s' % (id_, traceback.format_exc()))
                    pg_conn.rollback()
            logger.info('Fetching Publisher\'s IDs from Mongo...')
            publisher_cursor = mg_publisher.find_recent(time_field='_id', a_timestamp=ObjectId(id_), fields={'_id': True})


class QueueWorkerThread(Thread):
    def __init__(self, name):
        Thread.__init__(self)
        self.name = name
        self._stop = Event()
        self._worker = None

    def stop(self):
        self._stop.set()
        if self._worker:
            self._worker.should_stop = True

    def run(self):
        logger.info('Start Thread %s' % self.name)
        while not self._stop.is_set():
            with get_rabbitmq_connection()as amqp_conn, \
                    get_mg_publisher_connection() as mg_client, \
                    get_pg_core_connection() as pg_conn:

                self._worker = PublisherToPostgresWorker(
                    amqp_connection=amqp_conn,
                    input_queues=publisher_to_postgres_queues,
                    mg_client=mg_client,
                    pg_connection=pg_conn
                )
                try:
                    self._worker.run()
                except:
                    logger.error(
                        'Exception detected %s' % traceback.format_exc())
                    logger.warning('Restart worker in %s seconds' % SLEEP_DURATION)
                    time.sleep(SLEEP_DURATION)


def main(options):
    parser = ArgumentParser(description='Tool that migrate Publisher from Mongo to Postgres')
    parser.add_argument('-a', '--all', dest='all', action='store_true',
                        help='migrate all at once and exit. Without this flag, this tools will watch the queues for changes update.')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count',
                        help='set severity level for the logger')
    parser.add_argument('-w', '--number-of-workers', dest='number_of_workers',
                        type=int, help='number of workers')
    args = parser.parse_args()

    global logger, threads
    # Setup logger
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    # Root logger
    root_logger = init_logger(name='', log_level=log_level)
    # Module logger
    logger = logging.getLogger(__name__)

    # setting = load_source('setting', os.environ['SETTING_FILE'])

    if args.all:
        logger.info('Start migrating all Publishers directly from Mongo...')
        migrate_from_mongo()
        logger.info('Done')
        return

    # Bind handler for signals
    signal.signal(signal.SIGHUP, handler)
    signal.signal(signal.SIGINT, handler)
    signal.signal(signal.SIGTERM, handler)

    logger.info('Start migrating Publishers from Queue...')
    number_of_workers = args.number_of_workers or 1
    logger.info('Creating a pool with %s worker' % number_of_workers)
    threads = [QueueWorkerThread('Thread %2s' % i) for i in
               xrange(number_of_workers)]
    logger.info('All threads are created. Starting them...')
    for thread in threads:
        thread.start()
    logger.info('All threads are started, waiting for them to terminate')

    has_alive_thread = True
    while has_alive_thread:
        has_alive_thread = False
        for thread in threads:
            if thread.is_alive():
                has_alive_thread = True
                break
        time.sleep(1)
    logger.info('All threads has been stopped')


if __name__ == '__main__':
    main(sys.argv[1:])
