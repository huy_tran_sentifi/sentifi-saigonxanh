#!/usr/bin/env python
# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Aug 14, 2014
# @author: trung

from psycopg2 import IntegrityError

import sys

from constant.mongo_namespace import D_CORE, C_CATEGORY
from constant.postgres_namespace import T_CATEGORY
from helpers.connection_utils import get_mg_publisher_connection, get_pg_core_connection
from model.mongo_dao import DictionaryDao as MgDictionary
from model.named_entity import Category
from model.postgres_dao import CategoryDao as PgCategoryDao


def main(options):
    with get_mg_publisher_connection() as mg_client, \
            get_pg_core_connection() as pg_conn:
        mg_category = MgDictionary(mg_client, D_CORE, C_CATEGORY)
        pg_category = PgCategoryDao(pg_conn, T_CATEGORY)
        for cat in mg_category.find_recent(time_field='_id', limit=0):
            category = Category.from_json(dict(
                category_id=int(cat.get('newNumericId')) if cat.get('newNumericId') is not None else cat.get('numericId'),
                status=1,
                parent_category_id=cat.get('parentNumericId'),
                category_mongo_id=cat.get('_id'),
                parent_category_mongo_id=cat.get('parentId'),
                category_name=cat.get('name'),
                category_level=cat.get('level')
            ))
            print 'Trying updating category %s - %s...' % (category.id, category.category_name)
            result = pg_category.update(category)
            if not result:
                print 'Category %s - %s not exists, inserting...' % (category.id, category.category_name)
                pg_category.insert(category)
        print 'Fixing parent_category_id...'
        print '%s rows fixed!' % pg_category.fix_parent_category_id()

if __name__ == '__main__':
    main(sys.argv[1:])
