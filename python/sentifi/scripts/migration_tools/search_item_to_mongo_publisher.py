#!/usr/bin/env python
# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 05/12/2014
# @author: trung

from argparse import ArgumentParser
from threading import Event, Thread

import logging
import signal
import sys
import time
import traceback

from helpers.connection_utils import get_mg_publisher_connection, \
    get_pg_core_connection, get_rabbitmq_connection
from helpers.utils import init_logger
from workers.publisher_worker import SearchItemToMongoPublisherWorker
from workers.queue import search_item_to_mongo_publisher_queues


SLEEP_DURATION = 5
threads = []


def handler(signum, frame):
    global logger, threads
    logger.warning('Signal %s received. Stopping all workers...' % signum)
    for thread in threads:
        thread.stop()


class QueueWorkerThread(Thread):
    def __init__(self, name):
        Thread.__init__(self)
        self.name = name
        self._stop = Event()
        self._worker = None

    def stop(self):
        self._stop.set()
        if self._worker:
            self._worker.should_stop = True

    def run(self):
        logger.info('Start Thread %s' % self.name)
        while not self._stop.is_set():
            with get_rabbitmq_connection()as amqp_conn, \
                    get_mg_publisher_connection() as mg_client, \
                    get_pg_core_connection() as pg_conn:

                self._worker = SearchItemToMongoPublisherWorker(
                    amqp_connection=amqp_conn,
                    input_queues=search_item_to_mongo_publisher_queues,
                    mg_client=mg_client,
                    pg_connection=pg_conn
                )
                try:
                    self._worker.run()
                except:
                    logger.error(
                        'Exception detected\n%s' % traceback.format_exc())
                    logger.warning('Restart workers in %s seconds' % SLEEP_DURATION)
                    time.sleep(SLEEP_DURATION)


def main(options):
    parser = ArgumentParser(description='Worker that syncs from Postgres SearchItem to Mongo Publisher ')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count',
                        help='set severity level for the logger')
    parser.add_argument('-w', '--number-of-workers', dest='number_of_workers',
                        type=int, help='number of workers')
    args = parser.parse_args()

    global logger, threads
    # Setup logger
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    # Root logger
    root_logger = init_logger(name='', log_level=log_level)
    # Module logger
    logger = logging.getLogger(__name__)


    # Bind handler for signals
    signal.signal(signal.SIGHUP, handler)
    signal.signal(signal.SIGINT, handler)
    signal.signal(signal.SIGTERM, handler)

    logger.info('Start migrating Publishers from Queue...')
    number_of_workers = args.number_of_workers or 1
    logger.info('Creating a pool with %s workers' % number_of_workers)
    threads = [QueueWorkerThread('Thread %2s' % i) for i in
               xrange(number_of_workers)]
    logger.info('All threads are created. Starting them...')
    for thread in threads:
        thread.start()
    logger.info('All threads are started, waiting for them to terminate')

    has_alive_thread = True
    while has_alive_thread:
        has_alive_thread = False
        for thread in threads:
            if thread.is_alive():
                has_alive_thread = True
                break
        time.sleep(1)
    logger.info('All threads has been stopped')


if __name__ == '__main__':
   main(sys.argv[1:])
    # main2()
