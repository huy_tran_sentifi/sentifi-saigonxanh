#!/usr/bin/env python
# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 02/02/2015
# @author: trung

from argparse import ArgumentParser
from datetime import date, datetime, timedelta
from imp import load_source

import logging
import os
import sys

from helpers.utils import init_logger
from model.mongo_dao import DictionaryDao
from helpers.connection_utils import get_mg_client, get_s3_bucket
from etl.mongo2s3 import migrate_to_s3_by_creation_date


setting = load_source('setting', os.environ['SETTING_FILE'])


def main(options):
    parser = ArgumentParser(description='tools that archive Mongo Collection to Amazon S3')
    parser.add_argument('-H', '--host', dest='host', default='localhost', help='Mongo host to connect to (default: \'localhost\')' )
    parser.add_argument('-p', '--port', dest='port', type=int, default=27017, help='server port (default: 27017)')
    parser.add_argument('-d', '--db-name', dest='db_name', required=True, help='database to archive')
    parser.add_argument('-c', '--collections', dest='collections', help='comma separated list of collections to archive (default: all non-system-collections)')

    parser.add_argument('-f', '--s3-prefix', dest='s3_prefix', required=True, help='prefix (folder) where data is stored')

    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-s', '--start-date', dest='start_date', help='`format: YYYY-MM-DD (default: \'1970-01-01\')')
    parser.add_argument('-e', '--end-date', dest='end_date', help='format: YYYY-MM-DD (default: tomorrow)')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    start_date = datetime.strptime(args.start_date, '%Y-%m-%d').date() if args.start_date else date(1970, 1, 1)
    end_date = datetime.strptime(args.end_date, '%Y-%m-%d').date() if args.end_date else date.today() + timedelta(days=1)

    # Setup logger
    global logger
    log_file = args.log_file
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=log_file)
    logger = logging.getLogger(__name__)

    if start_date > end_date:
        logger.warning('start_date > end_date, nothing to do')
        return 0

    mg_client = get_mg_client(args.host, args.port)
    s3_bucket = get_s3_bucket(setting.S3_BUCKET_DATABASE_BACKUP)

    # Archive all Collections if there is no ``collections`` parameter provided
    db = mg_client[args.db_name]
    collections_list = args.collections.split(',') if args.collections else db.collection_names(include_system_collections=False)
    for collection in collections_list:
        with DictionaryDao(mg_client, args.db_name, collection) as dict_dao:
            working_date = start_date
            while working_date <= end_date:
                migrate_to_s3_by_creation_date(
                    working_date,
                    dict_dao,
                    s3_bucket,
                    args.s3_prefix
                )
                working_date += timedelta(days=1)


if __name__ == '__main__':
    main(sys.argv[1:])
