#!/bin/bash

function usage {
    echo "USAGE: $0 COMMAND"
    echo COMMAND: command to execute
    exit
}

function _term () {
    kill -TERM $child
}
trap _term SIGHUP SIGINT SIGTERM

if [ $# -lt 1 ]; then
    usage
fi

export WORKON_HOME='/home/sentifi/virtualenvs'
source `which virtualenvwrapper.sh`
workon sentifi-saigonxanh

cmd=$VIRTUAL_ENV/python/sentifi/scripts/${@:1:$#}
echo "Executing: $cmd"
exec $cmd &
child=$!
wait $child

deactivate
