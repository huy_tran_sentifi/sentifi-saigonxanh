#!/usr/bin/env python
#  Copyright (C) 2015 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 08/06/2015
# @author: trung

from argparse import ArgumentParser
from csv import DictReader, DictWriter
from imp import load_source
from pprint import pprint

import logging
import os
import sys
from psycopg2.extras import RealDictCursor

from constant import STATUS_INACTIVE
from constant.postgres_namespace import T_ROLE, T_ROLE_NETWORK, T_CATEGORY, \
    T_PUBLISHER_FROM_MONGO
from helpers.dict_utils import empty_string_to_none
from helpers.utils import init_logger
from helpers.connection_utils import get_pg_core_connection
from model.postgres_dao import CategoryDao, DictionaryDao, RoleDao, \
    RoleNetworkDao
from model.role import Role, RoleNetwork
from workers.search_item_to_publisher_worker import make_publisher_mg_status

setting = load_source('setting', os.environ['SETTING_FILE'])


ACCEPTED_HEADER_LIST = [
    'name',
    'aboutUs',
    'itemType',
    'itemGroup',
    'firstCat',
    'secondCat',
    'thirdCat',
    'forthCat',
    'fifthCat',
    'sixthCat',
    'photo',
    'tw-general',
    'publisherStatus'
]


def is_valid_header(header):
    if all(_ in ACCEPTED_HEADER_LIST for _ in header):
        return True

    logger.error('Unknown headers %s' % [_ for _ in header if _ not in ACCEPTED_HEADER_LIST])
    return False


def is_valid_record(record):
    if isinstance(record, dict):
        return True

    return False


def get_publisher_by_twitter_id(conn, twitter_id):
    with conn.cursor(cursor_factory=RealDictCursor) as cursor:
        query = '''
SELECT
    object_id,
    created_at,
    updated_at,
    object_payload
  FROM mongo.mg_publisher p
  WHERE EXISTS (
    SELECT NULL
      FROM mongo.mg_publisher_sns ps
      WHERE ps.publisher_mongo_id = p.object_id
        AND ps.sns_name = 'tw'
        AND ps.sns_id = %s
  )'''
        query = cursor.mogrify(query, (str(twitter_id), ))
        logger.debug('Executing %s' % query)
        cursor.execute(query)
        return cursor.fetchall()


def make_publisher_category(item_dict, category_dict):
    categories = []

    if item_dict.get('itemType'):
        itemtype = category_dict.get('1|' + item_dict.get('itemType').lower().strip())
        if itemtype:
            categories.append(dict(
                _id=itemtype.get('category_mongo_id'),
                level=itemtype.get('category_level'),
                name=itemtype.get('category_name'),
                numericId=itemtype.get('category_id'),
                parentNumericId=itemtype.get('parent_category_id'),
                parentId=itemtype.get('parent_category_mongo_id')
            ))

    if item_dict.get('itemGroup'):
        itemgroup = category_dict.get('2|' + item_dict.get('itemGroup').lower().strip())
        if itemgroup:
            categories.append(dict(
                _id=itemgroup.get('category_mongo_id'),
                level=itemgroup.get('category_level'),
                name=itemgroup.get('category_name'),
                numericId=itemgroup.get('category_id'),
                parentNumericId=itemgroup.get('parent_category_id'),
                parentId=itemgroup.get('parent_category_mongo_id')
            ))

    if item_dict.get('firstCat'):
        cat1 = category_dict.get('3|' + item_dict.get('firstCat').lower().strip())
        if cat1:
            categories.append(dict(
                _id=cat1.get('category_mongo_id'),
                level=cat1.get('category_level'),
                name=cat1.get('category_name'),
                numericId=cat1.get('category_id'),
                parentNumericId=cat1.get('parent_category_id'),
                parentId=cat1.get('parent_category_mongo_id')
            ))

    if item_dict.get('secondCat'):
        cat2 = category_dict.get('4|' + item_dict.get('secondCat').lower().strip())
        if cat2:
            categories.append(dict(
                _id=cat2.get('category_mongo_id'),
                level=cat2.get('category_level'),
                name=cat2.get('category_name'),
                numericId=cat2.get('category_id'),
                parentNumericId=cat2.get('parent_category_id'),
                parentId=cat2.get('parent_category_mongo_id')
            ))

    if item_dict.get('thirdCat'):
        cat3 = category_dict.get('5|' + item_dict.get('thirdCat').lower().strip())
        if cat3:
            categories.append(dict(
                _id=cat3.get('category_mongo_id'),
                level=cat3.get('category_level'),
                name=cat3.get('category_name'),
                numericId=cat3.get('category_id'),
                parentNumericId=cat3.get('parent_category_id'),
                parentId=cat3.get('parent_category_mongo_id')
            ))

    if item_dict.get('forthCat'):
        cat4 = category_dict.get('6|' + item_dict.get('forthCat').lower().strip())
        if cat4:
            categories.append(dict(
                _id=cat4.get('category_mongo_id'),
                level=cat4.get('category_level'),
                name=cat4.get('category_name'),
                numericId=cat4.get('category_id'),
                parentNumericId=cat4.get('parent_category_id'),
                parentId=cat4.get('parent_category_mongo_id')
            ))

    if item_dict.get('fifthCat'):
        cat5 = category_dict.get('7|' + item_dict.get('fifthCat').lower().strip())
        if cat5:
            categories.append(dict(
                _id=cat5.get('category_mongo_id'),
                level=cat5.get('category_level'),
                name=cat5.get('category_name'),
                numericId=cat5.get('category_id'),
                parentNumericId=cat5.get('parent_category_id'),
                parentId=cat5.get('parent_category_mongo_id')
            ))

    if item_dict.get('sixthCat'):
        cat6 = category_dict.get('8|' + item_dict.get('sixthCat').lower().strip())
        if cat6:
            categories.append(dict(
                _id=cat6.get('category_mongo_id'),
                level=cat6.get('category_level'),
                name=cat6.get('category_name'),
                numericId=cat6.get('category_id'),
                parentNumericId=cat6.get('parent_category_id'),
                parentId=cat6.get('parent_category_mongo_id')
            ))

    return categories


def make_publisher_from_csv(csv_dict, category_dict):
    publisher = dict(
        active=csv_dict.get('publisherStatus') > STATUS_INACTIVE,
        categories=make_publisher_category(csv_dict, category_dict),
        description=csv_dict.get('aboutUs'),
        image=csv_dict.get('photo'),
        name=csv_dict.get('name'),
        status=make_publisher_mg_status(int(csv_dict.get('publisherStatus')))
    )
    empty_string_to_none(publisher)
    return publisher


def parse_twitter_column(twitter_cell_content):
    result  = []
    if not twitter_cell_content:
        return result

    twitter_info_list = [_.strip() for _ in twitter_cell_content.split(',') if _]
    for twitter_info in twitter_info_list:
        twitter_info_component_list = [_.strip() for _ in twitter_info.split() if _]
        result.append(dict(
            screen_name=twitter_info_component_list[0],
            id_str=twitter_info_component_list[1].replace('{', '').replace('}', '')
        ))

    return result


def preprocess_record(record):
    if not isinstance(record, dict):
        raise TypeError('record must be a dict instead of %s' % type(record))

    for key in record:
        record[key] = record[key].strip()


def main(options):
    parser = ArgumentParser(description='Publisher import tool')
    # TODO: implement dry-run mode
    # parser.add_argument('-d', '--dry-run', help='perform a trial run with no changes made')
    parser.add_argument('-i', '--input-file', dest='input_file', required=True, help='path to input CSV file defining changes')
    parser.add_argument('-o', '--output-file', dest='output_file', required=True, help='path to the result CSV file')
    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    # Setup logger
    global logger
    log_file = args.log_file
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=log_file)
    logger = logging.getLogger(__name__)

    with get_pg_core_connection() as conn, \
            open(args.input_file, 'r') as input, \
            open(args.output_file, 'w') as output:
        publisher_dao = DictionaryDao(conn, T_PUBLISHER_FROM_MONGO)
        category_dao = CategoryDao(conn, T_CATEGORY)
        logger.info('Caching the category tree...')
        category_dict = category_dao.get_category_dict_with_level_and_name_key()

        dict_reader = DictReader(input, delimiter=',', quotechar='"')
        dict_writer = DictWriter(output, ['old', 'new'], delimiter=',', quotechar='"')
        header = dict_reader.fieldnames
        if not is_valid_header(header):
            exit(1)

        dict_writer.writeheader()
        line_number = 1
        for row in dict_reader:
            line_number += 1
            preprocess_record(row)
            if not is_valid_record(row):
                logger.error('Line %s containing %s which is not valid' % (line_number, row))
                exit(1)

            publisher = make_publisher_from_csv(row, category_dict)
            # pprint(publisher)
            linked_twitter_info_list = parse_twitter_column(row.get('tw-general'))
            for linked_twitter_info in linked_twitter_info_list:
                existed_publisher_list = get_publisher_by_twitter_id(conn, linked_twitter_info['id_str'])
                dict_writer.writerow({'old': existed_publisher_list, 'new': ''})
                for existed_publisher in existed_publisher_list:
                    for k in publisher:
                        if publisher[k] is not None:
                            if isinstance(publisher[k], dict):
                                existed_publisher['object_payload'][k].update(publisher[k])
                            else:
                                existed_publisher['object_payload'][k] = publisher[k]
                    publisher_dao.upsert(
                        existed_publisher['object_payload'],
                        object_id=existed_publisher['object_id'],
                        created_at=existed_publisher['created_at']
                    )
                dict_writer.writerow({'old': '', 'new': existed_publisher_list})


if __name__ == '__main__':
    main(sys.argv[1:])
