#!/usr/bin/env python
# Copyright (C) 2015 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Mar 04, 2015
# @author: diep.dao

from argparse import ArgumentParser
from helpers.connection_utils import get_pg_core_connection
from psycopg2.extras import RealDictCursor
import sys


def get_category_dict_with_full_path():
    """
    Get category dict (full_path, category_properties)
    :return: category dict
    """
    conn = get_pg_core_connection()
    category_dict = {}
    with conn.cursor(cursor_factory=RealDictCursor) as cursor:
        cursor.execute("SELECT lower(trim(get_category_name_path(category_id, '~'))) as full_path, * FROM category")
        conn.commit()
        for row in cursor:
            category_dict[row['full_path']] = row
    return category_dict


def category_checker(cat_path, category_dict):
    """
    Check a category path that has existed or not
    :param cat_path: category path
    :param category_dict: category dict
    :return: return True if existed, else return False\
    """
    if cat_path in category_dict.keys():
        print 'Exist: ' + cat_path
        return True
    print 'New: ' + cat_path
    return False


def process_category_file(file_name):
    """
    Check all category path in file
    :param file_name: file location
    :return:
    """
    # get category dic
    category_dict = get_category_dict_with_full_path()
    # read file line by line to check each category path
    file_contents = ''
    with open(file_name, 'r') as fp:
        for i, line in enumerate(fp):
            if i == 0:
                file_contents += 'action,id,' + line
                continue
            categories = line[:-1].split(',')
            cat_path = categories[0].strip()
            for cat in categories[1:]:
                if cat.strip() == '':
                    break
                cat_path += '~' + cat.strip()
            new_line = ''
            cat_path = cat_path.lower()
            if category_checker(cat_path, category_dict):
                new_line += ',' + category_dict[cat_path]['category_mongo_id'] + ',' + line
            else:
                new_line += ',,' + line
            file_contents += new_line
    # Write checked result to file
    checked_file = open(file_name[:-4] + '_checked.csv', 'w+')
    checked_file.write(file_contents)
    checked_file.close()
    return


def main(options):
    """
    Main program for checking category path
    :param options: Application start-up params
    :return:
    """
    parser = ArgumentParser(
        description='Tool that use for checking category path')
    parser.add_argument('-f', '--file', dest='file', help='Category file',
                        required=True)
    args = parser.parse_args()
    process_category_file(args.file)
    return


if __name__ == '__main__':
    main(sys.argv[1:])
