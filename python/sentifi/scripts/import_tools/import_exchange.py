# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 18/03/2015
# @author: trung

from argparse import ArgumentParser
from csv import DictReader, DictWriter
from datetime import date, datetime, timedelta
from imp import load_source

import logging
import os
import sys
from constant.mongo_namespace import D_CORE, C_CATEGORY

from helpers.utils import init_logger
from model.mongo_dao import DictionaryDao
from helpers.connection_utils import get_mg_publisher_connection, \
    get_pg_core_connection
from etl.mongo2s3 import migrate_to_s3_by_creation_date


setting = load_source('setting', os.environ['SETTING_FILE'])


COLUMNS_LIST = [
    'exchange_id', 'status', 'created_at', 'updated_at', 'exchange_name', \
    'acronym', 'alternative_name', 'url', 'isocode', 'bloomberg_name', \
    'google_finance_name', 'local_name', 'reuters_name', 'bloomberg_exchange', \
    'google_finance_exchange', 'reuters_exchange'
]


def main(options):
    parser = ArgumentParser(description='Exchange import tool')
    parser.add_argument('-f', '--input-file', dest='input_file', required=True, help='path to input CSV file defining changes')
    parser.add_argument('-f', '--output-file', dest='output_file', required=True, help='path to the result CSV file')
    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    # Setup logger
    global logger
    log_file = args.log_file
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=log_file)
    logger = logging.getLogger(__name__)

    with get_pg_core_connection() as conn, \
            open(args.input_file, 'r') as input, \
            open(args.output_file, 'w') as output:
        cursor.conn.cursor()
        dict_reader = DictReader(input, delimiter=',', quotechar='"')
        dict_writer = DictWriter(output, delimiter=',', quotechar='"')
        headers = dict_reader.fieldnames
        if not all(_ in COLUMNS_LIST for _ in headers):
            logger.error('Unknown headers %s' % [_ for _ in headers if _ not in COLUMNS_LIST])
            return

        for row in dict_reader:
            if row.get(dict_id):













if __name__ == '__main__':
    main(sys.argv[1:])
