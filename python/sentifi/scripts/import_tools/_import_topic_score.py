#!/usr/bin/env python
# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 26/03/2015
# @author: trung

from argparse import ArgumentParser
from gevent.monkey import patch_all
from datetime import datetime
from unicodecsv import UnicodeReader

import logging
import sys

from helpers.connection_utils import get_cas_pr_cluster
from helpers.utils import init_logger
from model.cassandra_dao.topic_sns_score import TopicSnsScoreDao
from model.topic_sns_score import TopicSnsScore


patch_all()


def cast_params(csv_reader):
    for row in csv_reader:
        yield (int(row[0]), row[1], float(row[2]))

def main(options):
    parser = ArgumentParser(description='import TopicSnsScore into the cassandra')
    parser.add_argument('-i', '--input-file', dest='input_file', help='path to the CSV file')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    global logger, setting
    # Setup logger
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    # Root logger
    root_logger = init_logger(name='', log_level=log_level)
    # Module logger
    logger = logging.getLogger(__name__)

    with open(args.input_file, 'r') as input_file:
        cluster = get_cas_pr_cluster()
        csv_reader = UnicodeReader(input_file, delimiter=',', quotechar='"')
        tss_dao = TopicSnsScoreDao(cluster, 'tr_dev')

        tss = TopicSnsScore(
            scored_at=datetime(2015, 03, 26),
            metric_id=100,
            sns_name='tw',
            triplets_list=cast_params(csv_reader)
        )

        tss_dao.insert(tss)
        cluster.shutdown()


if __name__ == '__main__':
    main(sys.argv[1:])
