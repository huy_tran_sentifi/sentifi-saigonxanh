#!/usr/bin/env python
# Copyright (C) 2015 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Mar 04, 2015
# @author: diep.dao

from argparse import ArgumentParser
from helpers.connection_utils import get_mg_publisher_connection, get_pg_core_connection

from bson.objectid import ObjectId

import sys


def get_max_pg_category_id():
    """
    Get max category numeric id in postgresql
    :return: category numeric id
    """
    conn = get_pg_core_connection()
    with conn.cursor() as cur:
        cur.execute('SELECT max(category_id) as max_id FROM category')
        max_cat_id = cur.fetchone()[0]
    conn.close
    return max_cat_id

# global max numeric category id
max_pg_category_id = get_max_pg_category_id()


def convert_cat_level_to_type(level):
    """
    Convert category level to category type
    :param level: category level
    :return: category type
    """
    if level == 1:
        cat_type = 'Profile Type'
    elif level == 2:
        cat_type = 'Publisher Group'
    else:
        cat_type = 'Category %d' % (level - 2)
    return cat_type


def find_category_by_name(cat_name):
    """
    Find category by name
    :param cat_name: category name
    :return:
    """
    conn = get_mg_publisher_connection()
    category_cols = conn.core.Category
    match_cat = category_cols.find_one({'name': cat_name})
    conn.close()
    return match_cat


def update_category_parent_id(cur_cat, pre_cat):
    """
    Update category parent id
    :param cur_cat: child category
    :param pre_cat: parent category
    :return:
    """
    conn = get_mg_publisher_connection()
    category_cols = conn.core.Category
    category_cols.update({'_id': cur_cat['_id']}, {"$set": {'parentId': pre_cat['_id'],
                                                            'parentNumericId': pre_cat['numericId']}})
    conn.close()
    return


def add_category(category):
    """
    Add new category
    :param category: category dict
    :return: new category id
    """
    global max_pg_category_id
    max_pg_category_id += 1
    category['numericId'] = max_pg_category_id
    conn = get_mg_publisher_connection()
    category_cols = conn.core.Category
    print 'Insert new category: ' + str(category)
    new_cat_id = category_cols.insert(category)
    conn.close()
    return new_cat_id


def delete_category(cat_id):
    """
    Delete a category
    :param cat_id: category id
    :return:
    """
    conn = get_mg_publisher_connection()
    category_cols = conn.core.Category
    result = category_cols.remove({'_id': ObjectId(cat_id)})
    conn.close()
    return result


def update_category_path(cat_path):
    """
    Update category path
    :param cat_path: category path
    :return:
    """
    # check each category in category path
    for idx, cat_name in enumerate(cat_path):
        cat_name = cat_name.strip()
        if cat_name == '':
            break
        cur_cat = find_category_by_name(cat_name)
        if idx == 0:
            # process first category, is root
            if cur_cat is None:
                # if root don't existed, create new
                new_cat = dict()
                new_cat['level'] = idx + 1
                new_cat['name'] = cat_name
                new_cat['parentNumericId'] = None
                new_cat['parentId'] = None
                new_cat['type'] = convert_cat_level_to_type(idx + 1)
                add_category(new_cat)
        else:
            # process remain category, is subs of root
            # get previous category
            pre_cat = find_category_by_name(cat_path[idx - 1])
            if pre_cat is None:
                print 'Parent category (%s) not found for category (%s): ' % (cat_path[idx - 1], cat_name)
                return
            if cur_cat is None:
                # if current category is not existed, create new
                new_cat = dict()
                new_cat['level'] = idx + 1
                new_cat['name'] = cat_name
                new_cat['parentNumericId'] = pre_cat['numericId']
                new_cat['parentId'] = pre_cat['_id']
                new_cat['type'] = convert_cat_level_to_type(idx + 1)
                add_category(new_cat)
            else:
                # if current category is existed, check its parent
                if str(cur_cat['parentId']) != str(pre_cat['_id']):
                    # category parent is wrong, so update this
                    print 'Update category parent id: category_name=%s, category_parent_name=%s' \
                          % (cat_name, cat_path[idx - 1])
                    update_category_parent_id(cur_cat, pre_cat)
    return


def add_new_category_path(cat_path):
    """
    Add new category path
    :param cat_path: category path
    :return:
    """
    update_category_path(cat_path)
    return


def process_category_file(file_name):
    """
    Updating all category path in file
    :param file_name: file location
    :return:
    """
    # read file to update category path line by line
    with open(file_name, 'r') as fp:
        for i, line in enumerate(fp):
            line = line.rstrip('\n')
            if i == 0:
                continue
            cat_arr = line.split(',')
            cat_path = cat_arr[2:]
            cat_id = cat_arr[1].strip()
            options = cat_arr[0].strip().lower()
            if options == 'a':
                print 'Line %s: Add new category path - %s' % (str(i + 1), line)
                add_new_category_path(cat_path)
            elif options == 'u':
                print 'Line %s: Update category path - %s' % (str(i + 1), line)
                update_category_path(cat_path)
            elif options == 'x':
                print 'Line %s: Delete category - %s' % (str(i + 1), cat_id)
                delete_category(cat_id)
            else:
                print 'Line %s: No operation - %s' % (str(i + 1), line)
    return


def main(options):
    """
    Main application for update category path
    :param options: Application start-up params
    :return:
    """
    parser = ArgumentParser(
        description='Tool that use for updating category path')
    parser.add_argument('-f', '--file', dest='file', help='Updated Category file',
                        required=True)
    args = parser.parse_args()
    process_category_file(args.file)
    return


if __name__ == '__main__':
    main(sys.argv[1:])