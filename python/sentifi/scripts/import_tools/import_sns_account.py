#!/usr/bin/env python
# Copyright (C) 2015 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 12/08/2015
# @author: trung


from argparse import ArgumentParser
from datetime import datetime
from imp import load_source
from unicodecsv import DictReader, DictWriter

import logging
import os
import sys
import traceback

from constant import CSV_MULTI_VALUE_DELIMITER
from constant.postgres_namespace import T_CATEGORY, T_ROLE, T_SNS_ACCOUNT, \
    T_SNS_CATEGORY, T_SNS_ROLE
from helpers.utils import init_logger
from helpers.connection_utils import get_pg_core_connection
from model.postgres_dao import CategoryDao, RoleDao, SnsAccountDao, \
    SnsCategoryDao, SnsRoleDao
from model.sns import SnsAccount, SnsCategory, SnsRole

setting = load_source('setting', os.environ['SETTING_FILE'])


ACCEPTED_HEADER_LIST = [
    'sns_name',
    'sns_id',
    'status',
    'named_entity_id',
    'geo_level',
    'display_name',
    'avatar_url',
    'country_code',
    'sns_tag',
    'named_entity_id_excluded',
    'itemtype',
    'itemgroup',
    'cat1',
    'cat2',
    'cat3',
    'cat4',
    'cat5',
    'cat6',
    'occupation',
    'function'
]
category_order = ['cat6', 'cat5', 'cat4', 'cat3', 'cat2', 'cat1', 'itemgroup', 'itemtype']
category_dao = None
role_dao = None
sns_account_dao = None
sns_category_dao = None
sns_role_dao = None


def _is_valid_header(header):
    if all(_ in ACCEPTED_HEADER_LIST for _ in header):
        return True

    return False


def _is_valid_record(dict_record):
    if isinstance(dict_record, dict) \
            and dict_record.get('sns_name') \
            and dict_record.get('sns_id'):
        return True

    return False


def _convert_to_category_id(dict_record):
    category_level = 8
    for cat_key in category_order:
        cat_name = dict_record.get(cat_key, '').strip().lower()
        if cat_name:
            category = category_dao.qfind_by_level_and_name(category_level, cat_name)
            if not category:
                raise ValueError("Category [ %s ] level [ %s ] is invalid" % (dict_record.get(cat_key, ''), category_level))
            return category.get('category_id')
        category_level -= 1

    return None


def _make_sns_account(dict_record):
    sns_tag = named_entity_id = named_entity_id_excluded = None
    if dict_record.get('sns_tag'):
        sns_tag = [_.strip().lower() for _ in dict_record.get('sns_tag').split(CSV_MULTI_VALUE_DELIMITER)]
    if dict_record.get('named_entity_id') and dict_record.get('named_entity_id').strip():
        named_entity_id = int(dict_record.get('named_entity_id').strip())
    if dict_record.get('named_entity_id_excluded'):
        named_entity_id_excluded = [int(_.strip()) for _ in dict_record.get('sns_tag').split(CSV_MULTI_VALUE_DELIMITER)]
    return SnsAccount(
        sns_name=dict_record.get('sns_name', '').strip().lower(),
        sns_id=dict_record.get('sns_id', '').strip(),
        status=dict_record.get('status'),
        updated_at=datetime.utcnow(),
        # human_updated_at=datetime.utcnow(),
        named_entity_id=named_entity_id,
        geo_level=dict_record.get('geo_level').strip().lower() if dict_record.get('geo_level', '').strip() else None,
        display_name=dict_record.get('display_name', '').strip(),
        avatar_url=dict_record.get('avatar_url', '').strip(),
        country_code=dict_record.get('country_code', '').strip().upper(),
        sns_tag=sns_tag,
        named_entity_id_excluded=named_entity_id_excluded
    ) if dict_record else None


def _make_sns_category(dict_record):
    global category_dao

    category_level = 8
    for cat_key in category_order:
        cat_name = dict_record.get(cat_key, '').strip().lower()
        if cat_name:
            category = category_dao.qfind_by_level_and_name(category_level, cat_name)
            if not category:
                raise ValueError("Category [ %s ] level [ %s ] is invalid" % (dict_record.get(cat_key, ''), category_level))
            return SnsCategory(
                sns_name=dict_record.get('sns_name').strip().lower(),
                sns_id=dict_record.get('sns_id').strip(),
                human_updated_at=datetime.utcnow(),
                category_id=category.category_id
            )
        category_level -= 1

    return None


def _make_sns_role_list(dict_record):
    global role_dao

    role_key_set = set()
    for each in dict_record.get('occupation', '').split(CSV_MULTI_VALUE_DELIMITER):
        role_key_set.add(each.strip().lower())
    for each in dict_record.get('function', '').split(CSV_MULTI_VALUE_DELIMITER):
        role_key_set.add(each.strip().lower())

    if '' in role_key_set:
        role_key_set.remove('')
    sns_role_list = []
    for role_key in role_key_set:
        role = role_dao.qfind_by_key(role_key)
        if not role:
            raise ValueError("Role [ %s ] is invalid" % role_key)
        sns_role_list.append(SnsRole(
            sns_name=dict_record.get('sns_name').strip().lower(),
            sns_id=dict_record.get('sns_id').strip(),
            human_updated_at=datetime.utcnow(),
            role_id=role.role_id
        ))

    return sns_role_list if sns_role_list else None


def _import_data(input_csv_file, output_csv_file, dry_run_only=False):
    global sns_account_dao, sns_category_dao
    dict_reader = DictReader(input_csv_file, delimiter=',', quotechar='"')

    header = dict_reader.fieldnames

    # Verify header
    if not _is_valid_header(header):
        logger.warning('Unknown headers %s will be ignored' % [_ for _ in header if _ not in ACCEPTED_HEADER_LIST])

    header.append('result')
    dict_writer = DictWriter(output_csv_file, fieldnames=header, delimiter=',', quotechar='"')
    dict_writer.writeheader()

    line_number = 1
    while True:
        line_number += 1

        try:
            row = dict_reader.next()
        except StopIteration:
            break
        except:
            logger.error('Line %s: %s' % (line_number, traceback.format_exc(0)))
            row = {}
            row['result'] = traceback.format_exception_only(sys.exc_type, sys.exc_value)[0].strip()
            dict_writer.writerow(row)
            continue

        logger.debug('Processing line %s: %s' % (line_number, row))

        # Check for valid record
        if not _is_valid_record(row):
            logger.error('Line %s: sns_name or sns_id is invalid' % line_number)
            row['result'] = 'sns_name or sns_id is invalid'
            dict_writer.writerow(row)
            continue

        # Make SnsAccount
        sns_account = _make_sns_account(row)
        # Make SnsCategory
        try:
            sns_category = _make_sns_category(row)
        except:
            logger.error('Line %s: %s' % (line_number, traceback.format_exc(0)))
            row['result'] = traceback.format_exception_only(sys.exc_type, sys.exc_value)[0].strip()
            dict_writer.writerow(row)
            continue
        # Make SnsRole
        try:
            sns_role_list = _make_sns_role_list(row)
        except ValueError:
            logger.error('Line %s: %s' % (line_number, traceback.format_exc(0)))
            row['result'] = traceback.format_exception_only(sys.exc_type, sys.exc_value)[0].strip()
            dict_writer.writerow(row)
            continue

        if not dry_run_only:
            # Import SnsAccount
            sns_account_dao.merge(sns_account)
            # Import SnsCategory
            if sns_category:
                sns_category_dao.update_by_sns_id(sns_category_list=[sns_category], clean_machine_entries=True)
            # Import SnsRole
            if sns_role_list:
                sns_role_dao.update_by_sns_id(sns_role_list=sns_role_list, clean_machine_entries=True)

        # Write result to output CSV
        row['result'] = 'OK'
        dict_writer.writerow(row)


def _init_dao(pg_conn):
    global category_dao, role_dao, sns_account_dao, sns_category_dao, sns_role_dao
    category_dao = CategoryDao(pg_conn, T_CATEGORY)
    role_dao = RoleDao(pg_conn, T_ROLE)
    sns_account_dao = SnsAccountDao(pg_conn, T_SNS_ACCOUNT)
    sns_category_dao = SnsCategoryDao(pg_conn, T_SNS_CATEGORY)
    sns_role_dao = SnsRoleDao(pg_conn, T_SNS_ROLE)


def _preprocess_record(dict_record):
    if not isinstance(dict_record, dict):
        raise TypeError('dict_record must be a dict instead of %s' % type(dict_record))

    for key in dict_record:
        if isinstance(dict_record[key], basestring):
            dict_record[key] = dict_record[key].strip()


def main(options):
    parser = ArgumentParser(description='SNS account import tool')
    parser.add_argument('-d', '--dry-run', dest='dry_run', action='store_true', help='perform a trial run with no changes made')
    parser.add_argument('-i', '--input-file', dest='input_file', required=True, help='path to input CSV file defining changes')
    parser.add_argument('-o', '--output-file', dest='output_file', required=True, help='path to the result CSV file')
    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    # Setup logger
    global logger
    log_file = args.log_file
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=log_file)
    logger = logging.getLogger(__name__)

    with get_pg_core_connection() as pg_conn, \
            open(args.input_file, 'r') as input, \
            open(args.output_file, 'w') as output:
        _init_dao(pg_conn)
        _import_data(input, output, args.dry_run)


if __name__ == '__main__':
    main(sys.argv[1:])
