#!/usr/bin/env python
#  Copyright (C) 2015 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 01/06/2015
# @author: trung

from argparse import ArgumentParser
from csv import DictReader, DictWriter
from imp import load_source

import logging
import os
import sys
from constant import CSV_MULTI_VALUE_DELIMITER

from constant.postgres_namespace import T_ROLE, T_ROLE_NETWORK
from helpers.utils import init_logger
from helpers.connection_utils import get_pg_core_connection
from model.postgres_dao.role import RoleDao, RoleNetworkDao
from model.role import Role, RoleNetwork

setting = load_source('setting', os.environ['SETTING_FILE'])


ACCEPTED_HEADER_LIST = [
    'role_id',
    'role_key',
    'parent_role_key',
    'role_name',
    'detail',
    'is_hidden'
]


def preprocess_record(record):
    if not isinstance(record, dict):
        raise TypeError('record must be a dict instead of %s' % type(record))

    for key in record:
        record[key] = record[key].strip()


def is_valid_header(header):
    if all(_ in ACCEPTED_HEADER_LIST for _ in header):
        return True

    return False


def is_valid_record(record):
    if isinstance(record, dict) \
            and record.get('role_key') \
            and record.get('parent_role_key') \
            and record.get('role_name')\
            and record.get('is_hidden') is not None:
        return True

    return False


def main(options):
    parser = ArgumentParser(description='Role import tool')
    # TODO: implement dry-run mode
    # parser.add_argument('-d', '--dry-run', help='perform a trial run with no changes made')
    parser.add_argument('-i', '--input-file', dest='input_file', required=True, help='path to input CSV file defining changes')
    # TODO: push the result to an output file
    # parser.add_argument('-o', '--output-file', dest='output_file', required=True, help='path to the result CSV file')
    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    # Setup logger
    global logger
    log_file = args.log_file
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=log_file)
    logger = logging.getLogger(__name__)

    with get_pg_core_connection() as conn, \
            open(args.input_file, 'r') as input:
            # open(args.output_file, 'w') as output:
        role_dao = RoleDao(conn, T_ROLE)
        role_network_dao = RoleNetworkDao(conn, T_ROLE_NETWORK)
        dict_reader = DictReader(input, delimiter=',', quotechar='"')
        # dict_writer = DictWriter(output, delimiter=',', quotechar='"')
        header = dict_reader.fieldnames
        if not is_valid_header(header):
            logger.error('Unknown headers %s' % [_ for _ in header if _ not in ACCEPTED_HEADER_LIST])
            exit(1)

        line_number = 1
        parent_role_key_to_id_map = {}
        for row in dict_reader:
            line_number += 1
            preprocess_record(row)
            if not is_valid_record(row):
                logger.error('Line %s containing %s which is not valid' % (line_number, row))
                exit(1)

            # Update/Insert Role's information
            role = Role.from_json(row)
            role_id_returned = role_dao.update(role)
            if not role_id_returned:
                logger.info('Role %s does not exist => adding' % role.role_key)
                role_id_returned = role_dao.insert(role)

            # Resolve parent_role_key to parent_role_id
            parent_role_key_list = row.get('parent_role_key', []).split(CSV_MULTI_VALUE_DELIMITER)
            parent_role_id_set = set()
            for each in parent_role_key_list:
                if each not in parent_role_key_to_id_map:
                    tmp_role = role_dao.find_by_key(each)
                    if not tmp_role:
                        logger.error('Line %s, parent_role_key %s not exist. It should be defined first!' % (line_number, each))
                        exit(1)
                    parent_role_key_to_id_map[each] = tmp_role.role_id
                parent_role_id_set.add(parent_role_key_to_id_map[each])

            # Update/Insert RoleNetwork's information
            if len(parent_role_id_set) > 1 and 0 in parent_role_id_set:
                logger.error('Invalid argument list %s, parent_role_id cannot be Root and something else at a same time')
                exit(1)

            if parent_role_id_set == {0}:
                role_network_dao.set_root(role_id_returned)
            else:

                existed_parent_role_id_set = set(role_network_dao.get_parent_role_id_list(role_id_returned) or [])
                parent_role_id_to_delete = existed_parent_role_id_set - parent_role_id_set
                parent_role_id_to_insert = parent_role_id_set - existed_parent_role_id_set

                role_network_list_tmp = [RoleNetwork(role_id_returned, _) for _ in parent_role_id_to_delete]
                logger.info('role %s => parent_role_id to remove: %s' % (role_id_returned, parent_role_id_to_delete))
                for each in role_network_list_tmp:
                    role_network_dao.delete(each)

                role_network_list_tmp = [RoleNetwork(role_id_returned, _) for _ in parent_role_id_to_insert]
                logger.info('role %s => parent_role_id to insert: %s' % (role_id_returned, parent_role_id_to_insert))
                for each in role_network_list_tmp:
                    role_network_dao.insert(each)


if __name__ == '__main__':
    main(sys.argv[1:])
