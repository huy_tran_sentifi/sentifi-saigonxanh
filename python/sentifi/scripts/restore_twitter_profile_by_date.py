#!/usr/bin/env python
# Copyright (C) 2015 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 17/11/2015

import logging
import os
import sys

from argparse import ArgumentParser
from datetime import datetime, timedelta

from psycopg2.extensions import register_adapter
from psycopg2.extras import Json, RealDictCursor, register_default_json

from helpers.connection_utils import get_mg_twitter_tweet_connection, \
    get_pg_connection
from helpers.utils import init_logger
from model.mongo_dao import DictionaryDao


def main(options):
    parser = ArgumentParser(description='restores Twitter profile payload from a daily snapshot')
    parser.add_argument('-d', '--date', dest='date', required=True, help='`format: YYYY-MM-DD')

    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    start_time = datetime.strptime(args.date, '%Y-%m-%d')
    end_time = start_time + timedelta(days=1)

    # Setup logger
    global logger
    log_file = args.log_file
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=log_file)
    logger = logging.getLogger(__name__)

    mg_client = get_mg_twitter_tweet_connection()
    dict_dao = DictionaryDao(mg_client, 'Twitter', 'User')
    with get_pg_connection(host='da0.eu-west-1.compute.internal', port=5432, database='rip_sns_account', user='dbw', password=os.environ['PG_DA0_PASSWORD']) as pg_conn:
        with pg_conn.cursor(cursor_factory=RealDictCursor) as pg_cur:
            register_default_json(pg_cur)
            register_adapter(dict, Json)
            for each in dict_dao.find_by_time_range(start_time, end_time):
                logger.info('Updating Twitter %s...' % each['id_str'])
                query = '''
UPDATE public.sns_account SET
    payload = %s,
    updated_at = %s
  WHERE sns_name = 'tw'
    AND sns_id = %s'''
                query = pg_cur.mogrify(query, (each, each['_crawl_date'], each['id_str']))
                logger.debug('Executing query %s' % query)
                pg_cur.execute(query)
                pg_conn.commit()

if __name__ == '__main__':
    main(sys.argv[1:])