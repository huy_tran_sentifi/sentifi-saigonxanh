#!/usr/bin/env python
# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 09/05/2015
# @author: trung

from argparse import ArgumentParser
from psycopg2.extras import RealDictCursor
from unicodecsv import UnicodeWriter

import logging
import sys
import ujson as json

from helpers.connection_utils import get_es_connection, get_pg_core_slave1_connection
from helpers.utils import init_logger


READ_ES_INDEX = 'relevant_messages'
READ_ES_DOC_TYPE = 'relevant_document'


def get_relevant_document_count_by_priority_publisher(es_conn):
    query = {
      "query": {
        "filtered": {
          "query": {
            "match": {
              "publisher.priority": 1
            }
          },
          "filter": {
            "range": {
              "timestamp": {
                "gte": "2015-03-01",
                "lt": "2015-04-01"
              }
            }
          }
        }
      },
      "aggs": {
        "group_by_publisher": {
          "terms": {
            "field": "publisher._id",
            "size": 0
          }
        }
      }
    }
    return es_conn.search(
        body=query,
        index=READ_ES_INDEX,
        doc_type=READ_ES_DOC_TYPE,
        search_type='count'
    )


def get_publisher_detail_by_mongo_id(pg_conn, publisher_mongo_id):
    result = None
    with pg_conn.cursor(cursor_factory=RealDictCursor) as cursor:
        query = '''
SELECT
    p.object_id,
    p.object_payload->>'name' AS name,
    p.object_payload->>'description' AS description,
    p.object_payload->>'categories' AS categories,
    (
        SELECT string_agg(ps1.sns_id, ' | ')
          FROM mongo.mg_publisher_sns ps1
          WHERE ps1.publisher_mongo_id = p.object_id
            AND sns_name = 'tw'
    ) AS twitter_id_list,
    (
        SELECT string_agg(n.object_payload->>'url', ' | ')
          FROM mongo.mg_publisher_sns ps2
          INNER JOIN mongo.mg_news n ON (n.object_id = ps2.sns_id)
          WHERE ps2.publisher_mongo_id = p.object_id
            AND sns_name = 'news'
    ) AS url_list
  FROM mongo.mg_publisher p
  WHERE p.object_id = %s
  GROUP BY p.object_id
'''
        query = cursor.mogrify(query, (publisher_mongo_id, ))
        logger.debug('Executing %s' % query)
        cursor.execute(query)
        result = cursor.fetchone()
        pg_conn.commit()
    if not result:
        return None

    # process the categories
    cat_list = result.get('categories')
    cat_name_list = [None] * 8
    if cat_list:
        for each in json.loads(cat_list):
            if each and each.get('level') and each.get('name'):
                cat_name_list[each.get('level') - 1] = each.get('name')

    cat_name_list = [_ for _ in cat_name_list if _]
    result['categories'] = '>'.join(cat_name_list)
    return result


def main(options):
    parser = ArgumentParser(description='count tweets by priority Publisher')
    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-o', '--output-file', dest='output_file', help='path to output file', required=True)
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    # Setup logger
    global logger
    log_file = args.log_file
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=log_file)
    logger = logging.getLogger(__name__)

    es_conn = get_es_connection()
    reldoc_by_prio_pub = get_relevant_document_count_by_priority_publisher(es_conn)
    logger.info('Hits: %s' % reldoc_by_prio_pub.get('hits').get('total'))
    logger.info('Took: %sms' % reldoc_by_prio_pub.get('took'))
    logger.info('Timedout: %s' % reldoc_by_prio_pub.get('timed_out'))

    buckets = reldoc_by_prio_pub.get('aggregations').get('group_by_publisher').get('buckets')
    result = {}
    with get_pg_core_slave1_connection() as pg_conn, \
            open(args.output_file, 'w') as of:
        csv_writer = UnicodeWriter(of, delimiter=',', quotechar='"')
        csv_writer.writerow(['publisher_mongo_id', 'name', 'description', 'url', 'categories', 'twitter_id_list', 'url_list', 'doc_count'])
        for each in buckets:
            pub = get_publisher_detail_by_mongo_id(pg_conn, each.get('key')) or {}
            csv_writer.writerow([
                each.get('key'),
                pub.get('name'),
                pub.get('description'),
                pub.get('url'),
                pub.get('categories'),
                pub.get('twitter_id_list'),
                pub.get('url_list'),
                each.get('doc_count')
            ])


if __name__ == '__main__':
    main(sys.argv[1:])
