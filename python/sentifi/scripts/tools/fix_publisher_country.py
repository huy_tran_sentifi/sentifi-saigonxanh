#!/usr/bin/env python
# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 07/05/2015
# @author: trung

from argparse import ArgumentParser
from unicodecsv import UnicodeReader

import logging
import pprint
import sys
from constant.mongo_namespace import C_PUBLISHER, D_CORE

from helpers.connection_utils import get_mg_publisher_connection
from helpers.utils import init_logger


SRC_IDX = 0
DEST_IDX = 2


def read_map(file, has_header=False):
    map = {}
    with open(file, 'r') as f:
        csv_reader = UnicodeReader(f, delimiter=',', quotechar='"')
        if has_header:
            csv_reader.next()
        for row in csv_reader:
            # print row[SRC_IDX], row[DEST_IDX]
            map[row[SRC_IDX]] = row[DEST_IDX]
    for k in map:
        if not map[k] or not map[k].strip():
            map[k] = None
    return map

def main(options):
    parser = ArgumentParser(description='tools that replicates Publisher from Mongo to Postgres')
    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-i', '--input-file', dest='input_file', help='path to input file', required=True)
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    # Setup logger
    global logger
    log_file = args.log_file
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=log_file)
    logger = logging.getLogger(__name__)

    with get_mg_publisher_connection() as mg_client:
        map = read_map(args.input_file, True)

        pub_coll = mg_client[D_CORE][C_PUBLISHER]
        # Fix using map
        for k in map:
            cursor = pub_coll.update(
                spec={
                    'countryCode': k
                },
                document={
                    '$set': {
                        'countryCode': map[k]
                    }
                },
                upsert=True,
                multi=True,
            )
            logger.info('update country %s to %s : %s' % (k, map[k], cursor))


if __name__ == '__main__':
    main(sys.argv[1:])
