#!/usr/bin/env python
# Copyright (C) 2015 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 29/07/2015
# @author: trung

from argparse import ArgumentParser
from psycopg2.extras import RealDictCursor
from threading import Event, Thread
from unicodecsv import UnicodeWriter

import logging
import signal
import Queue
import sys
import time
import traceback

from helpers.connection_utils import get_es_connection, get_pg_core_slave1_connection
from helpers.utils import init_logger


BATCH_SIZE = 10000

READ_ES_INDEX = 'analytic'
READ_ES_DOC_TYPE = 'relevant_document'
COMMA_SEPARATED_INDEXES = '%s,%s' % (READ_ES_INDEX, READ_ES_INDEX)
COMMA_SEPARATED_DOC_TYPE = '%s,%s' % (READ_ES_DOC_TYPE, READ_ES_DOC_TYPE)

SLEEP_DURATION = 5

SCREEN_NAME_WHITELIST = {'job', 'jobs', 'career', 'careers', 'deal', 'deals',
    'buzz', 'marketing', 'trend', 'trends', 'fashion', 'resume'}
NAME_WHITELIST = {'deals', 'deal', 'offer', 'offers', 'jobs', '#fashion',
    'fashion', 'job centre', 'job center', 'vacancies', 'internship'}
DESCRIPTION_WHITELIST = {'deal', 'deals', 'offer', 'offers', 'job', 'jobs'}
TEXT_WHITELIST = '\\"hot deals\\" OR \\"hot #deals\\" OR (enter AND \\"to #win\\") OR \\"giveaway\\" OR \\"#giveaway\\" OR \\"tablet deal\\" OR \\"offer deal\\" OR (#deals AND #offers) OR \\"deals & offers\\" OR \\"#deal #729 (#number)\\" OR \\"#offers #432 (#number)\\" OR \\"#forsale\\" OR \\"#onsale\\" OR \\"#celebrity\\" OR \\"#entertainment #buzz\\" OR \\"#fashion #buzz\\" OR \\"#buzz #entertainment\\" OR \\"#knicks\\" OR \\"#giftcard\\" OR \\"#coupon\\" OR \\"free shipping\\" OR \\"#photography #camera\\" OR \\"#camera #photography\\"'


count_threads = []
write_thread = None
producer_done = False


def handler(signum, frame):
    global logger, count_threads, write_thread, producer_done
    logger.warning('Signal %s received. Stopping all workers...' % signum)
    for thread in count_threads:
        thread.stop()
    write_thread.stop()
    if not producer_done:
        time.sleep(SLEEP_DURATION)
        exit()


def check_name(name):
    """Returns true if name contains word in the whitelist
    """
    return any(_ in name for _ in NAME_WHITELIST)


def check_description(description):
    """Returns true if description contains word in the whitelist
    """
    return any(_ in description for _ in DESCRIPTION_WHITELIST)


def check_screen_name(screen_name):
    """Returns true if screen_name contains word in the whitelist
    """
    return any(_ in screen_name for _ in SCREEN_NAME_WHITELIST)


def get_hit_counts(es_conn, sns_id):
    query = '''
{}
{"query":{"filtered":{"filter":{"bool":{"must":[{"term":{"channel":"twitter"}},{"term":{"publisher.twitterID":"%(sns_id)s"}},{"range":{"date":{"from":"now-1M"}}}]}}}}}
{}
{"query":{"filtered":{"filter":{"bool":{"must":[{"term":{"channel":"twitter"}},{"term":{"publisher.twitterID":"%(sns_id)s"}},{"range":{"date":{"from":"now-1M"}}}]}},"query":{"query_string":{"query":"%(text_whitelist)s","default_field":"text"}}}}}
''' % ({
        'sns_id': sns_id,
        'text_whitelist': TEXT_WHITELIST
    })
    result = es_conn.msearch(
        body=query,
        index=COMMA_SEPARATED_INDEXES,
        doc_type=COMMA_SEPARATED_DOC_TYPE,
        search_type='count'
    )

    logger.info('Took: %sms' % (result['responses'][0]['took'] + result['responses'][1]['took']))
    logger.info('Took: %s' % (result['responses'][0]['timed_out'] and result['responses'][1]['timed_out']))

    total_hit = result['responses'][0]['hits']['total']
    keyword_hit = result['responses'][1]['hits']['total']
    return keyword_hit, total_hit


def get_potential_polluting_twitter_publisher(pg_conn):
    result = None
    from_object_id = '000000000000000000000000' # Very first ObjectId of the universe!

    with pg_conn.cursor(cursor_factory=RealDictCursor) as cursor:
        statement = '''
SELECT
    s.publisher_mongo_id,
    lower(t.object_payload->>'screen_name') AS screen_name,
    t.object_payload->>'id_str' AS sns_id,
    lower(t.object_payload->>'name') AS name,
    lower(t.object_payload->>'description') AS description
  FROM mongo.mg_publisher_sns_clean s
  INNER JOIN mongo.mg_twitter t ON (s.sns_name = 'tw' AND s.sns_id = (t.object_payload->>'id_str'))
  WHERE s.publisher_mongo_id > %s
  ORDER BY s.publisher_mongo_id
  LIMIT %s'''
        query = cursor.mogrify(statement, (from_object_id, BATCH_SIZE))
        logger.debug('Executing %s' % query)
        cursor.execute(query)
        pg_conn.commit()
        counter = 0
        while cursor and cursor.rowcount > 0:
            for result in cursor:
                from_object_id = result.get('publisher_mongo_id')
                yield result
            query = cursor.mogrify(statement, (from_object_id, BATCH_SIZE))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            pg_conn.commit()
            counter += 1
            logger.info('Processed %s Twitter Publishers' % (counter * BATCH_SIZE))


class CountMessageThread(Thread):
    def __init__(self, name, count_queue, write_queue):
        Thread.__init__(self)
        self.name = name
        self._iq = count_queue
        self._oq = write_queue
        self._stop = Event()

    def stop(self):
        self._stop.set()

    def run(self):
        logger.info('Start Thread %s' % self.name)
        self._es = get_es_connection(timeout=300) # 5 minutes
        while not (producer_done or self._stop.is_set()):
            doc = self._iq.get()
            try:
                keyword_hit, total_hit = get_hit_counts(self._es, doc.get('sns_id'))
                self._oq.put([
                    doc.get('publisher_mongo_id'),
                    doc.get('screen_name'),
                    doc.get('sns_id'),
                    doc.get('name'),
                    doc.get('description'),
                    keyword_hit,
                    total_hit,
                    keyword_hit*100./total_hit if total_hit else 'N/A'
                ])
                self._iq.task_done()
            except:
                logger.error('Exception detected\n%s' % traceback.format_exc())
                logger.warning('Restart workers in %s seconds' % SLEEP_DURATION)
                time.sleep(SLEEP_DURATION)
        global write_thread
        write_thread.stop()


class WriteCsvThread(Thread):

    def __init__(self, queue, output_file):
        Thread.__init__(self)
        self._q = queue
        self._of = output_file
        self._stop = Event()

    def stop(self):
        self._stop.set()

    def run(self):
        logger.info('Starting WriteCsvThread to %s' % self._of)
        with open(self._of, 'w') as of:
            csv_writer = UnicodeWriter(of, delimiter=',', quotechar='"')
            csv_writer.writerow(['publisher_mongo_id', 'screen_name', 'sns_id', 'name', 'description', 'keyword_hit', 'total_hit', '%'])
            while not (self._q.empty() and self._stop.is_set()):
                row = self._q.get()
                try:
                    csv_writer.writerow(row)
                    self._q.task_done()
                except:
                    logger.error('Exception detected\n%s' % traceback.format_exc())
                    logger.warning('Restart workers in %s seconds' % SLEEP_DURATION)
                    time.sleep(SLEEP_DURATION)


def main(options):
    parser = ArgumentParser(description='count tweets by priority Publisher')
    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-o', '--output-file', dest='output_file', help='path to output file', required=True)
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    parser.add_argument('-w', '--number-of-workers', dest='number_of_workers', type=int, help='number of workers')
    args = parser.parse_args()

    # Setup logger
    global logger, count_threads, write_thread, producer_done
    log_file = args.log_file
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=log_file)
    logger = logging.getLogger(__name__)

    # Bind handler for signals
    signal.signal(signal.SIGHUP, handler)
    signal.signal(signal.SIGINT, handler)
    signal.signal(signal.SIGTERM, handler)

    logger.info('Start exporting potential polluting Twitter Publisher...')
    number_of_workers = args.number_of_workers or 1

    # Create queues
    count_queue = Queue.Queue()
    write_queue = Queue.Queue()

    # Create worker to write result to csv
    logger.info('Creating %s...' % WriteCsvThread.__name__)
    write_thread = WriteCsvThread(write_queue, args.output_file)
    write_thread.start()
    logger.info('Thread %s created and started' % WriteCsvThread.__name__)

    # Create pool of count message worker
    logger.info('Creating a pool with %s %s...'% (number_of_workers, CountMessageThread.__name__))
    count_threads = [CountMessageThread('Thread %2s' % i, count_queue, write_queue) for i in xrange(number_of_workers)]
    logger.info('All %s are created. Starting them...' % CountMessageThread.__name__)
    for thread in count_threads:
        thread.start()
    logger.info('All %s are started' % CountMessageThread.__name__)

    # Populate potential polluting Twitter Publisher
    logger.info('Querying Postgres for potential polluting Twitter Publisher and populate to %s' % CountMessageThread)
    with get_pg_core_slave1_connection() as pg_conn:
        for each in get_potential_polluting_twitter_publisher(pg_conn):
            name = each.get('name')
            description = each.get('description')
            screen_name = each.get('screen_name')
            if (name and check_name(name)) or \
                    (description and check_description(description)) or \
                    (screen_name and check_screen_name(screen_name)):
                count_queue.put(each)
    producer_done = True

    # Wait for all thread finish
    has_alive_thread = True
    while has_alive_thread:
        has_alive_thread = False
        for thread in count_threads:
            if thread.is_alive():
                has_alive_thread = True
        if has_alive_thread:
            break
        else:
            has_alive_thread = write_thread.is_alive()
        print has_alive_thread
        time.sleep(1)
    logger.info('All threads has been stopped')


if __name__ == '__main__':
    main(sys.argv[1:])
