#!/usr/bin/env python
#  Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 29/12/2014
# @author: trung

from argparse import ArgumentParser
from kombu import Exchange
from kombu.connection import Connection
from kombu.pools import producers

import logging
import os
import sys

from helpers.utils import init_logger


def main(options):
    parser = ArgumentParser(description='Tool that populate data from file, line-by-line to a RabbitMQ\'s Exchange')
    parser.add_argument('-i', '--input-file', dest='input_file', help='path to the source file containing data to populate')
    parser.add_argument('-e', '--exchange-name', dest='exchange_name', help='name of the target RabbitMQ\'s Exchange')
    parser.add_argument('-H', '--hosts', dest='hosts', help='RabbitMQ\'s host, i.e: host1.com:5672,host2.com:5672')
    parser.add_argument('-u', '--user', dest='user', help='RabbitMQ\'s worker user')
    parser.add_argument('-w', '--password', dest='password', help='RabbitMQ\'s worker password')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    global logger, setting
    # Setup logger
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    # Root logger
    root_logger = init_logger(name='', log_level=log_level)
    # Module logger
    logger = logging.getLogger(__name__)

    broker_list = []
    for each in args.hosts.split(','):
        broker_list.append('librabbitmq://%s:%s@%s' % (args.user, args.password, each))
    broker_url = ';'.join(broker_list)

    with open(args.input_file, 'r') as input_file, \
            Connection(broker_url) as rabbitmq_conn:
        producer = producers[rabbitmq_conn].acquire(block=True)
        exchange = Exchange(args.exchange_name)
        line_count = 0
        for line in input_file:
            line = line.strip()
            producer.publish(line, serializer='json', exchange=exchange, retry=True)
            logger.debug('Published message %s' % line)
            line_count += 1
        logger.info('%s messages posted' % line_count)


if __name__ == '__main__':
    main(sys.argv[1:])
