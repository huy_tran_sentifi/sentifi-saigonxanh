#!/usr/bin/env python
#  Copyright (C) 2015 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 09/06/2015
# @author: trung

from argparse import ArgumentParser
from datetime import date, datetime, timedelta
from imp import load_source

import logging
import os
import sys
from bson.objectid import ObjectId

from helpers.utils import init_logger, date2datetime
from helpers.connection_utils import get_mg_client


setting = load_source('setting', os.environ['SETTING_FILE'])
SAFE_DAYS_THRESHOLD = 0 # Restrict removal of data in the last 0 days


def count_documents_by_date(collection, a_date):
    start_time = date2datetime(a_date)
    end_time = date2datetime(a_date + timedelta(days=1))
    logger.info('Counting documents in collection %s that was created in %s...' % (
        collection,
        a_date.isoformat()))
    cursor = collection.find(
        spec={
            '_id': {
                '$gte': ObjectId.from_datetime(start_time),
                '$lt': ObjectId.from_datetime(end_time),
            }
        }
    )
    return cursor.count()


def remove_documents_by_date(collection, a_date):
    start_time = date2datetime(a_date)
    end_time = date2datetime(a_date + timedelta(days=1))
    logger.info('Removing documents in collection %s that was created in %s...' % (
        collection,
        a_date.isoformat()))
    cursor = collection.remove(
        spec_or_id={
            '_id': {
                '$gte': ObjectId.from_datetime(start_time),
                '$lt': ObjectId.from_datetime(end_time),
            }
        }
    )
    return cursor.get('n')


def main(options):
    parser = ArgumentParser(description='tools that remove data in a Mongo Collection by date in ObjectId')
    parser.add_argument('-H', '--host', dest='host', default='localhost', help='Mongo host to connect to (default: \'localhost\')' )
    parser.add_argument('-p', '--port', dest='port', type=int, default=27017, help='server port (default: 27017)')
    parser.add_argument('-d', '--db-name', dest='db_name', required=True, help='database to archive')
    parser.add_argument('-c', '--collections', dest='collections', help='comma separated list of collections to archive (default: all non-system-collections)')

    parser.add_argument('-f', '--force', dest='force', action='store_true', help='force removal without prompting for confirmation')
    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-s', '--start-date', dest='start_date', required=True, help='`format: YYYY-MM-DD')
    parser.add_argument('-e', '--end-date', dest='end_date', required=True, help='format: YYYY-MM-DD')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    start_date = datetime.strptime(args.start_date, '%Y-%m-%d').date()
    end_date = datetime.strptime(args.end_date, '%Y-%m-%d').date()

    # Setup logger
    global logger
    log_file = args.log_file
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=log_file)
    logger = logging.getLogger(__name__)

    if start_date > end_date:
        logger.warning('start_date > end_date, nothing to do')
        return 0

    if max(start_date, end_date) >= date.today() - timedelta(days=SAFE_DAYS_THRESHOLD):
        logger.error('You are not allowed to remove data in the last %s days' % SAFE_DAYS_THRESHOLD)
        exit(1)

    mg_client = get_mg_client(args.host, args.port)

    # Archive all Collections if there is no ``collections`` parameter provided
    db = mg_client[args.db_name]
    collections_list = args.collections.split(',') if args.collections else db.collection_names(include_system_collections=False)
    for collection_name in collections_list:
            working_date = start_date
            collection = db[collection_name]
            while working_date <= end_date:
                doc_count = count_documents_by_date(collection, working_date)
                logger.info('There are %s documents' % doc_count)
                if not args.force and doc_count > 0:
                    prompt = raw_input('Are you sure to remove %s documents in collection %s that was created in %s? y/n [n]: ' % (
                        doc_count,
                        collection,
                        working_date.isoformat()))
                    if prompt.lower().strip() != 'y':
                        break

                doc_count = remove_documents_by_date(collection, working_date)
                logger.info('Removed %s documents' % doc_count)

                working_date += timedelta(days=1)


if __name__ == '__main__':
    main(sys.argv[1:])
