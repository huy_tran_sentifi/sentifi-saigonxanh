
# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 12/05/2015
# @author: trung

from __future__ import print_function

import os
import sys

from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.types import Row, StructField, StructType, StringType, IntegerType


'''
export AWS_ACCESS_KEY_ID=AKIAJ33MOQGKW6XEH24Q
export AWS_SECRET_ACCESS_KEY=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
'''
def main(options):
    conf = SparkConf()
    conf.set('fs.s3n.awsAccessKeyId', 'AKIAJGVU5IEDMOQ5ZQYA')
    conf.set('fs.s3n.awsSecretAccessKey', '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')
    conf.setAppName('Extract Twitter mentioned users from raw tweets')

    sc = SparkContext(conf=conf)
    # sc.setLocalProperty("fs.s3n.awsAccessKeyId", 'AKIAJGVU5IEDMOQ5ZQYA')
    # sc.setLocalProperty("fs.s3n.awsSecretAccessKey", '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')
    sqlContext = SQLContext(sc)

    # path = 's3n://bk-database/prod/raw_tweet/2013/11/18/20150214-075826.gz'
    path = '/mnt/ranking-data/trung/data/150513-1000.json.gz'
    raw_tweet = sqlContext.jsonFile(path)

    raw_tweet.printSchema()
    count = raw_tweet.count()
    user_mentions_rdd = raw_tweet.selectExpr('user.id as t_id', 'entities.user_mentions.id as m_id').collect()
    raw_tweet.registerTable('raw_tweet_131118')
    with open('/tmp/out.txt', 'w') as of:
        of.writelines([count])

    raw_tweet.registerTempTable('raw_tweet')
    id_mentions = sqlContext.sql('SELECT entities.user_mentions.id from raw_tweet')
    tmp = id_mentions.flatMap(lambda i: i.id).collect()

    tmp = sqlContext.sql('SELECT entities.user_mentions.id, count(*) from raw_tweet group by entities.user_mentions.id')
    sc.stop()


if __name__ == "__main__":
    main(sys.argv[1:])
