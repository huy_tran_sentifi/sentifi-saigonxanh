# Copyright (C) 2015 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 28/10/2015
# @author: trung

try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO
from xml.dom.minidom import parse

xml_file = StringIO('''
<series currency="EUR" currency_id="2" boerse_id="6">
    <s d="2015-09-25 00:00:00" o="123" h="123.5" l="112.4" c="115.55" v="103300968" V="880779" />
    <s d="2015-09-28 00:00:00" o="114.2" h="114.5" l="105" c="107.1" v="93869448" V="865427" />
    <s d="2015-09-29 00:00:00" o="105" h="108.6" l="102" c="103.3" v="53788996" V="513701" />
    <s d="2015-09-30 00:00:00" o="108.3" h="109.5" l="104" c="104.95" v="44015880" V="416515" />
    <s d="2015-10-01 00:00:00" o="108.65" h="110.2" l="102.75" c="105.05" v="50804120" V="477764" />
    <s d="2015-10-02 00:00:00" o="105.85" h="106.3" l="100" c="101.15" v="60004916" V="588727" />
    <s d="2015-10-05 00:00:00" o="101.35" h="104.25" l="95" c="102.8" v="74950728" V="754773" />
    <s d="2015-10-06 00:00:00" o="103.5" h="107.55" l="99.2" c="106.9" v="44104332" V="422398" />
    <s d="2015-10-07 00:00:00" o="109.05" h="117.7" l="108.9" c="114.9" v="88624160" V="777374" />
    <s d="2015-10-08 00:00:00" o="116" h="121.2" l="114.35" c="116.2" v="84215632" V="718094" />
    <s d="2015-10-09 00:00:00" o="118.5" h="133.75" l="115.8" c="125.9" v="193917488" V="1540333" />
    <s d="2015-10-12 00:00:00" o="129.3" h="135" l="127.35" c="132.45" v="115455456" V="873447" />
    <s d="2015-10-13 00:00:00" o="133.65" h="137.7" l="125.35" c="130.6" v="92445232" V="697957" />
    <s d="2015-10-14 00:00:00" o="128.2" h="135.6" l="125.15" c="128.6" v="76704776" V="586262" />
    <s d="2015-10-15 00:00:00" o="130.7" h="134.25" l="122.95" c="123.8" v="46826224" V="366474" />
    <s d="2015-10-16 00:00:00" o="125" h="125.2" l="119" c="121.2" v="45404568" V="376013" />
    <s d="2015-10-19 00:00:00" o="119.2" h="121.5" l="116.65" c="118.6" v="21946852" V="184634" />
    <s d="2015-10-20 00:00:00" o="118" h="118.65" l="115.3" c="116.25" v="19748838" V="169602" />
    <s d="2015-10-21 00:00:00" o="117.5" h="119.95" l="116.55" c="119.95" v="17190830" V="144825" />
    <s d="2015-10-22 00:00:00" o="121.9" h="124.5" l="120" c="123.9" v="20011694" V="163884" />
    <s d="2015-10-23 00:00:00" o="126" h="126.3" l="121.85" c="121.85" v="36745812" V="297384" />
    <s d="2015-10-26 00:00:00" o="123.1" h="125.4" l="122.5" c="125" v="15915705" V="127997" />
    <s d="2015-10-27 00:00:00" o="125.85" h="126.45" l="121.65" c="121.8" v="18054846" V="146572" />
</series>
''')

SERIES_PROPERTIES = ['currency', 'currency_id', 'boerse_id']
dom_tree = parse(xml_file)
series = dom_tree.documentElement
series_attributes = {}
for property in SERIES_PROPERTIES:
    series_attributes[property] = series.getAttribute(property)

data_sets = series.getElementsByTagName('s')

print '%s,%s,%s,%s,%s,%s,%s' % ('date', 'o', 'h', 'l', 'c', 'v', 'V')
for each in data_sets:
    print '%s,%s,%s,%s,%s,%s,%s' % (
        each.getAttribute('d'),
        each.getAttribute('o'),
        each.getAttribute('h'),
        each.getAttribute('l'),
        each.getAttribute('c'),
        each.getAttribute('v'),
        each.getAttribute('V')
    )


