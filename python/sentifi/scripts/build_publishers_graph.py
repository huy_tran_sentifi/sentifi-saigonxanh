#!/usr/bin/env python
#  Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 04/04/2015
# @author: trung

from argparse import ArgumentParser
from bson import ObjectId
from elasticsearch.helpers import scan
from psycopg2.extensions import register_type, UNICODE, UNICODEARRAY
from psycopg2.extras import RealDictCursor
from unidecode import unidecode

import logging
import re
import sys
import ujson as json

from constant.postgres_namespace import T_PUBLISHER_FROM_MONGO, S_MONGO
from helpers.connection_utils import get_pg_core_connection, get_es_connection
from helpers.utils import init_logger, oid_hex_to_datetime
from model.dictionary import Dictionary
from model.es_dao import DictionaryDao as EsDictionaryDao
from model.postgres_dao import DictionaryDao, ModelDao


# PUT publisher?ignore_conflicts=true
es_mapping = {
  "settings": {
    "analysis": {
      "analyzer": {
        "my_analyzer": {
          "type": "custom",
          "tokenizer": "my_tokenizer",
          "filter": [
            "my_stop_filter",
            "my_english_filter",
            "my_german_filter",
            "my_lowercase_filter"
          ]
        }
      },
      "tokenizer": {
        "my_tokenizer": {
          "type": "standard"
        }
      },
      "filter": {
        "my_stop_filter": {
          "type": "stop",
          "stopwords": [
            "news",
            "inc",
            "ltd",
            "group",
            "limited",
            "capital",
            "co",
            "business",
            "financial",
            "blog",
            "dr",
            "daily",
            "corporation",
            "ali",
            "llc",
            "holdings",
            "media",
            "corp",
            "times",
            "finance",
            "plc",
            "company",
            "estate",
            "trader",
            "international",
            "trading",
            "ag"
          ]
        },
        "my_english_filter": {
          "type": "stop",
          "stopwords": "_english_"
        },
        "my_german_filter": {
          "type": "stop",
          "stopwords": "_german_"
        },
        "my_lowercase_filter": {
          "type": "lowercase"
        }
      }
    },
    "index": {
      "number_of_shards": 5,
      "number_of_replicas": 1
    }
  },
  "mappings": {
    "normalized_publisher": {
      "properties": {
        "publisher_mongo_id": {
          "type": "string",
          "analyzer": "my_analyzer"
        },
        "status": {
          "type": "string",
          "index": "not_analyzed"
        },
        "created_at": {
          "type": "date",
          "format": "dateOptionalTime"
        },
        "updated_at": {
          "type": "date",
          "format": "dateOptionalTime"
        },
        "name": {
          "type": "string",
          "analyzer": "my_analyzer"
        },
        "display_name": {
          "type": "string",
          "analyzer": "my_analyzer"
        },
        "first_name": {
          "type": "string",
          "analyzer": "my_analyzer"
        },
        "last_name": {
          "type": "string",
          "analyzer": "my_analyzer"
        },
        "address": {
          "type": "string",
          "analyzer": "my_analyzer"
        },
        "description": {
          "type": "string",
          "analyzer": "my_analyzer"
        },
        "url": {
          "type": "string",
          "analyzer": "my_analyzer"
        },
        "phone": {
          "type": "string",
          "analyzer": "my_analyzer"
        },
        "fb_url": {
          "type": "string",
          "analyzer": "my_analyzer"
        },
        "gp_url": {
          "type": "string",
          "analyzer": "my_analyzer"
        },
        "li_url": {
          "type": "string",
          "analyzer": "my_analyzer"
        },
        "news_name": {
          "type": "string",
          "analyzer": "my_analyzer"
        },
        "news_url": {
          "type": "string",
          "analyzer": "my_analyzer"
        },
        "tw_name": {
          "type": "string",
          "analyzer": "my_analyzer"
        },
        "tw_screen_name": {
          "type": "string",
          "analyzer": "my_analyzer"
        },
        "tw_description": {
          "type": "string",
          "analyzer": "my_analyzer"
        }
      }
    }
  }
}


def fetch_potential_match_by_name(es_conn, name):
    logger.info('Searching for \'%s\' ...' % name)
    query = {
        "query": {
            "multi_match": {
                "fields": [
                    "name",
                    "display_name",
                    "first_name",
                    "last_name",
                    "url",
                    "description",
                    "news_name"
                    "tw_name",
                    "tw_screen_name",
                    "tw_description"
                ],
                "query": name
            }
        }
    }
    hits = scan(es_conn, query=query, index='publisher', doc_type='normalized_publisher')
    for hit in hits:
        yield hit


def main(options):
    parser = ArgumentParser(description='tools that normalize Publisher from Postgres')
    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    # Setup logger
    global logger
    log_file = args.log_file
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=log_file)
    logger = logging.getLogger(__name__)

    es_conn = get_es_connection()
    with get_pg_core_connection() as pg_conn:
        register_type(UNICODE)
        register_type(UNICODEARRAY)

        normalized_publisher_cursor = pg_conn.cursor(cursor_factory=RealDictCursor)
        query = '''
SELECT *
  FROM mongo.mg_normalized_publisher
  WHERE name IS NOT NULL
  LIMIT 2;'''
        logger.debug('Executing %s' % query)
        normalized_publisher_cursor.execute(query)
        pg_conn.commit()

        publisher_graph_cursor = pg_conn.cursor(cursor_factory=RealDictCursor)
        query = '''
SELECT upsert(\'
UPDATE mongo.upsert
);'''
        publisher_graph_cursor.prepare()

        for row in normalized_publisher_cursor:
            # Find potential match
            potential_matches = fetch_potential_match_by_name(es_conn, row['name'])
            for each in potential_matches:
                if
                each = each.get('_source')
                print 80 * '='
                print each.get('name')
                print each.get('display_name')
                print each.get('first_name')
                print each.get('last_name')
                print each.get('url')
                print each.get('description')
                print each.get('news_name')
                print each.get('tw_name')
                print each.get('tw_screen_name')
                print each.get('tw_description')


if __name__ == '__main__':
    main(sys.argv[1:])
