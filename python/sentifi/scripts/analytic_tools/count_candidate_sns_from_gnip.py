#!/usr/bin/env python
# Copyright (C) 2015 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 23/10/2015
# @author: trung

import sys

from argparse import ArgumentParser
from bson import ObjectId
from datetime import datetime, timedelta
from logging import DEBUG, INFO, WARNING, getLogger
from pymongo import ASCENDING
from unicodecsv.py2 import UnicodeWriter

from constant.mongo_namespace import C_TWITTER_TWEET, D_TWITTER_TWEET
from helpers.connection_utils import get_mg_twitter_tweet_connection
from helpers.utils import init_logger


def make_set_from_id_file(filename):
    s = set()
    with open(filename, 'r') as file:
        for row in file:
            s.add(int(row))
    return s


def main(options):
    parser = ArgumentParser(description='count candidate Twitter\'s accounts')

    parser.add_argument('-s', '--start-date', dest='start_date', help='`format: YYYY-MM-DD', required=True)
    parser.add_argument('-e', '--end-date', dest='end_date', help='format: YYYY-MM-DD (default: ``start_date``)')
    parser.add_argument('-o', '--output-csv', dest='output', required=True, help='path to output CSV file')
    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    # Setup logger
    global logger
    log_file = args.log_file
    log_level = WARNING
    if args.verbose >= 2:
        log_level = DEBUG
    if args.verbose == 1:
        log_level = INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=log_file)
    logger = getLogger(__name__)

    start_date = datetime.strptime(args.start_date, '%Y-%m-%d')
    end_date = datetime.strptime(args.end_date, '%Y-%m-%d') if args.end_date else start_date
    end_date += timedelta(days=1)

    mg_conn = get_mg_twitter_tweet_connection()
    tweet_collection = mg_conn[D_TWITTER_TWEET][C_TWITTER_TWEET]

    logger.info('Loading SNS IDs...')
    sns_id_set = make_set_from_id_file('/tmp/tw_id.csv')
    logger.info('Loaded %s items' % len(sns_id_set))

    logger.info('Loading Candidate Twitter IDs...')
    candidate_tw_id_set = make_set_from_id_file('/tmp/candidate_tw_id.csv')
    logger.info('Loaded %s items' % len(candidate_tw_id_set))

    user_id_set = set()
    doc_count = 0

    with open(args.output, 'w') as of:
        csv_writer = UnicodeWriter(of, delimiter=',', quotechar='"')
        # csv_writer.writerow(['user_id'])
        csv_writer.writerow(['message_id', 'created_at', 'tweet_id', 'user_id'])
        current_oid = ObjectId.from_datetime(start_date)
        end_oid = ObjectId.from_datetime(end_date)
        while current_oid < end_oid:
            logger.info('Processing raw tweet from %s' % current_oid.generation_time)
            cursor = tweet_collection.find(
                spec={
                    '_id': {'$gte': current_oid},
                    '_source': 'gnip'
                },
                fields=['id', 'user.id'],
                sort=[('_id', ASCENDING)],
                limit=100000
            )
            for doc in cursor:
                current_oid = doc['_id']
                doc_count += 1
                user_id = doc['user']['id']
                if user_id not in user_id_set:
                    row = [
                        str(current_oid),
                        current_oid.generation_time.isoformat(),
                        doc['id'],
                        user_id
                    ]
                    csv_writer.writerow(row)
                    user_id_set.add(user_id)
            logger.info('GNIP doc processed: %s => Number of distinct Twitter users: %s' % (doc_count, len(user_id_set)))

    logger.info('Distinct Twitter users: %s' % len(user_id_set))
    logger.info('Distinct Twitter users existed in P: %s' % len(user_id_set.intersection(sns_id_set)))
    logger.info('Distinct Twitter users existed in the candidates pool: %s' % len(user_id_set.intersection(candidate_tw_id_set)))
    logger.info('Distinct Twitter users that are not existed in P and the candidates pool: %s' % len(user_id_set.difference(sns_id_set).difference(candidate_tw_id_set)))


if __name__ == '__main__':
    main(sys.argv[1:])
