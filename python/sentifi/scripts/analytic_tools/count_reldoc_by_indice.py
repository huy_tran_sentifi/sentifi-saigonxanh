#!/usr/bin/env python
# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 04/05/2015
# @author: trung

from argparse import ArgumentParser
from datetime import datetime, timedelta
from psycopg2.extras import RealDictCursor
from unicodecsv import UnicodeWriter

import logging
import sys

from helpers.connection_utils import get_es_connection, get_pg_core_slave1_connection
from helpers.utils import init_logger


READ_ES_INDEX = 'analytic_search'
READ_ES_DOC_TYPE = 'relevant_document'


def get_relevant_document_count_by_index(es_conn):
    query = {
      "query": {
        "filtered": {
          "query": {
            "terms": {
              "tag": [
                396,
                397,
                399,
                400,
                401,
                402,
                403,
                404,
                405,
                406,
                407,
                408,
                409,
                410,
                411,
                413,
                414,
                415,
                557,
                558
              ]
            }
          },
          "filter": {
            "range": {
              "date": {
                "from": "now-1y"
              }
            }
          }
        }
      },
      "aggs": {
        "0": {
          "terms": {
            "field": "tag",
            "size": 0
          },
          "aggs": {
            "1": {
              "terms": {
                "field": "channel",
                "size": 0
              }
            }
          }
        }
      }
    }

    return es_conn.search(
        body=query,
        index=READ_ES_INDEX,
        doc_type=READ_ES_DOC_TYPE,
        search_type='count'
    )


def get_item_name(pg_conn, item_id):
    r = None
    with pg_conn.cursor() as cursor:
        query = '''
SELECT name FROM item WHERE id = %s'''
        query = cursor.mogrify(query, (item_id, ))
        logger.debug('Executing %s' % query)
        cursor.execute(query)
        r = cursor.fetchone()
        pg_conn.commit()
    return r[0] if r else None


def main(options):
    parser = ArgumentParser(description='tools that count RelevantDocument for company belonging to an index')
    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-o', '--output-file', dest='output_file', help='path to output file', required=True)
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')

    args = parser.parse_args()

    # Setup logger
    global logger
    log_file = args.log_file
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=log_file)
    logger = logging.getLogger(__name__)

    es_conn = get_es_connection()
    reldoc_by_tag = get_relevant_document_count_by_index(es_conn)
    logger.info('Hits: %s' % reldoc_by_tag.get('hits').get('total'))
    logger.info('Took: %sms' % reldoc_by_tag.get('took'))
    logger.info('Timedout: %s' % reldoc_by_tag.get('timed_out'))

    buckets = reldoc_by_tag.get('aggregations').get('0').get('buckets')
    result = []
    smi_item = set([396, 397, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 413, 414, 415, 557, 558])

    with get_pg_core_slave1_connection() as pg_conn, \
            open(args.output_file, 'w') as of:
        csv_writer = UnicodeWriter(of, delimiter=',', quotechar='"')
        csv_writer.writerow(['item_id', 'item_name', 'twitter_count', 'news_count', 'blog_count', 'total'])
        for each in buckets:
            key = each.get('key')
            if key not in smi_item:
                continue

            item_name = get_item_name(pg_conn, key)
            tw_count = n_count = b_count = 0
            for channel in each.get('1').get('buckets'):
                if channel.get('key') == 'twitter':
                    tw_count = channel.get('doc_count')
                elif channel.get('key') == 'news':
                    n_count = channel.get('doc_count')
                elif channel.get('key') == 'blog':
                    b_count = channel.get('doc_count')

            csv_writer.writerow([key, item_name, tw_count, n_count, b_count, each.get('doc_count')])


if __name__ == '__main__':
    main(sys.argv[1:])
