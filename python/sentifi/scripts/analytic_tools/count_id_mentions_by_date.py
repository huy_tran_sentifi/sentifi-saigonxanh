#!/usr/bin/env python
# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 14/05/2015
# @author: trung

from argparse import ArgumentParser
from cassandra.concurrent import execute_concurrent_with_args
from datetime import datetime, timedelta

import logging
import sys
from constant.cassandra_namespace import K_ANALYTIC

from helpers.connection_utils import get_es_connection, get_cas_pr_cluster
from helpers.utils import init_logger


READ_ES_INDEX = 'message'
READ_ES_DOC_TYPE = 'message'


def get_mention_count_by_twitter_user_id(es_conn, start_date, end_date):
    query = {
        'query': {
            'filtered': {
                'query': {
                    'match': {
                        'channel': 'twitter'
                    }
                },
                'filter': {
                    'range': {
                        'created_at': {
                            'gte': start_date,
                            'lt': end_date
                        }
                    }
                }
            }
        },
        'aggs': {
            'group_by_user_mentions': {
                'terms': {
                    'field': 'channel_meta.user_mentions.id',
                    'size': 0
                }
            }
        }
    }
    return es_conn.search(
        body=query,
        index=READ_ES_INDEX,
        doc_type=READ_ES_DOC_TYPE,
        search_type='count'
    )


def yield_buckets_component(buckets):
    for each in buckets:
        yield (int(each.get('key')), each.get('doc_count'))


def main(options):
    parser = ArgumentParser(description='tools that count Twitter\'s user_id mentioned in RelevantTweet')
    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')

    parser.add_argument('-s', '--start-date', dest='start_date', help='`format: YYYY-MM-DD', required=True)
    parser.add_argument('-e', '--end-date', dest='end_date', help='format: YYYY-MM-DD (default: ``start_date``)')
    args = parser.parse_args()

    start_date = datetime.strptime(args.start_date, '%Y-%m-%d').date()
    end_date = datetime.strptime(args.end_date, '%Y-%m-%d').date() if args.end_date else start_date

    # Setup logger
    global logger
    log_file = args.log_file
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=log_file)
    logger = logging.getLogger(__name__)

    es_conn = get_es_connection(timeout=300)  # 5 minutes
    cluster = get_cas_pr_cluster()
    session = cluster.connect(keyspace=K_ANALYTIC)
    working_date = start_date
    while working_date <= end_date:
        logger.info('counting for day %s' % working_date)
        mention_count_by_id = get_mention_count_by_twitter_user_id(es_conn, working_date, working_date + timedelta(days=1))
        logger.info('Hits: %s' % mention_count_by_id.get('hits').get('total'))
        logger.info('Took: %sms' % mention_count_by_id.get('took'))
        logger.info('Timedout: %s' % mention_count_by_id.get('timed_out'))

        buckets = mention_count_by_id.get('aggregations').get('group_by_user_mentions').get('buckets')
        if not buckets:
            logger.info('No data returned!')
            return

        ps = session.prepare('''
INSERT INTO twitter_user_mention_in_relevant (user_mention_id, count_date, count_value)
    VALUES (?, '{count_date}', ?)'''.format(count_date=working_date))
        execute_concurrent_with_args(session, ps, yield_buckets_component(buckets))
        working_date += timedelta(days=1)


if __name__ == '__main__':
    main(sys.argv[1:])
