#!/usr/bin/env python
#  Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 19/05/2015
# @author: trung

from argparse import ArgumentParser
from bson.objectid import ObjectId
from datetime import date, datetime, timedelta
from imp import load_source
from unicodecsv import UnicodeWriter

import logging
import os
import sys

from constant.mongo_namespace import D_TWITTER_TWEET, C_TWITTER_TWEET, \
    D_ANALYTIC, C_RELEVANT_DOCUMENT
from helpers.connection_utils import get_mg_relevant_document_connection, \
    get_mg_twitter_tweet_connection
from helpers.utils import init_logger


setting = load_source('setting', os.environ['SETTING_FILE'])


def main(options):
    parser = ArgumentParser(description='tools extract 5000 unrelevant tweets by date')

    parser.add_argument('-d', '--date', dest='a_date', required=True, help='`format: YYYY-MM-DD')
    parser.add_argument('-o', '--output-csv', dest='output', required=True, help='path to output CSV file')
    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    a_date = datetime.strptime(args.a_date, '%Y-%m-%d').date()

    # Setup logger
    global logger
    log_file = args.log_file
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=log_file)
    logger = logging.getLogger(__name__)

    mg_raw_client = get_mg_twitter_tweet_connection()
    raw_tweet_coll = mg_raw_client[D_TWITTER_TWEET][C_TWITTER_TWEET]
    mg_relevant_client = get_mg_relevant_document_connection()
    rel_tweet_coll = mg_relevant_client[D_ANALYTIC][C_RELEVANT_DOCUMENT]

    with open(args.output, 'w') as of:
        csv_writer = UnicodeWriter(of, delimiter=',', quotechar='"')
        csv_writer.writerow(['id', 'source', 'created_at', 'id_str', 'user_id_str', 'user_screen_name', 'text'])
        count = 0
        for doc in raw_tweet_coll.find(
            spec={'_id': {'$gte': ObjectId('55592b800000000000000000')}},
            fields=['_id', '_source', 'created_at', 'id_str', 'user.id_str', 'user.screen_name', 'text'],
            limit=10000
        ):
            if rel_tweet_coll.find_one(doc['_id']):
                logger.info('id %s found in relevant, skip it' % doc['_id'])
                continue
            count += 1
            csv_writer.writerow([
                doc['_id'],
                doc['_source'],
                doc['created_at'],
                doc['id_str'],
                doc['user']['id_str'],
                doc['user']['screen_name'],
                doc['text']
            ])
            if count >= 5000:
                break


if __name__ == '__main__':
    main(sys.argv[1:])
