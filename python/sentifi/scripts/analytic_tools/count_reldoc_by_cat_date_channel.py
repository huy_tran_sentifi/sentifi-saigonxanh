#!/usr/bin/env python
# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 04/05/2015
# @author: trung

from argparse import ArgumentParser
from datetime import datetime, timedelta
from psycopg2.extras import RealDictCursor
from unicodecsv import UnicodeWriter

import logging
import sys

from helpers.connection_utils import get_es_connection, get_pg_core_slave1_connection
from helpers.utils import init_logger


READ_ES_INDEX = 'analytic'
READ_ES_DOC_TYPE = 'relevant_document'


def get_relevant_document_count_by_tag(es_conn, start_date, end_date, channel=None):
    if channel:
        query = {
            'query': {
                'filtered': {
                    'query': {
                        'match': {
                            'channel': channel
                        }
                    },
                    'filter': {
                        'range': {
                            'timestamp': {
                                'gte': start_date,
                                'lt': end_date
                            }
                        }
                    }
                }
            },
            'aggs': {
                'group_by_tag': {
                    'terms': {
                        'field': 'tag',
                        'size': 0
                    }
                }
            }
        }
    else:
        query = {
            'query': {
                'filtered': {
                    'filter': {
                        'range': {
                            'timestamp': {
                                'gte': start_date,
                                'lt': end_date
                            }
                        }
                    }
                }
            },
            'aggs': {
                'group_by_tag': {
                    'terms': {
                        'field': 'tag',
                        'size': 0
                    }
                }
            }
        }
    return es_conn.search(
        body=query,
        index=READ_ES_INDEX,
        doc_type=READ_ES_DOC_TYPE,
        search_type='count'
    )


def get_item_category(pg_conn, item_id):
    r = None
    result = set()
    with pg_conn.cursor(cursor_factory=RealDictCursor) as cursor:
        query = '''
SELECT
    get_category_name_path_by_mongo_id(itemtype, '>') as c1,
    get_category_name_path_by_mongo_id(itemgroup, '>') as c2,
    get_category_name_path_by_mongo_id(cat1, '>') as c3,
    get_category_name_path_by_mongo_id(cat2, '>') as c4,
    get_category_name_path_by_mongo_id(cat3, '>') as c5,
    get_category_name_path_by_mongo_id(cat4, '>') as c6,
    get_category_name_path_by_mongo_id(cat5, '>') as c7,
    get_category_name_path_by_mongo_id(cat6, '>') as c8
  FROM item
  WHERE id = %s'''
        query = cursor.mogrify(query, (item_id, ))
        logger.debug('Executing %s' % query)
        cursor.execute(query)
        r = cursor.fetchone()
        pg_conn.commit()
    if not r:
        return None

    for k in r:
        if r.get(k):
            result.add(r.get(k))
    return result if result else None


def main(options):
    parser = ArgumentParser(description='tools that replicates Publisher from Mongo to Postgres')
    parser.add_argument('-l', '--log-file', dest='log_file', help='path to log file')
    parser.add_argument('-o', '--output-file', dest='output_file', help='path to output file', required=True)
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')

    parser.add_argument('-s', '--start-date', dest='start_date', help='`format: YYYY-MM-DD', required=True)
    parser.add_argument('-e', '--end-date', dest='end_date', help='format: YYYY-MM-DD (default: ``start_date`` + 1 day)')
    parser.add_argument('-c', '--channel', dest='channel', help='must be twitter | news | blog')
    args = parser.parse_args()

    start_date = datetime.strptime(args.start_date, '%Y-%m-%d').date()
    end_date = datetime.strptime(args.end_date, '%Y-%m-%d').date() if args.end_date else start_date + timedelta(days=1)

    # Setup logger
    global logger
    log_file = args.log_file
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    root_logger = init_logger(name='', log_level=log_level, log_file=log_file)
    logger = logging.getLogger(__name__)

    es_conn = get_es_connection()
    reldoc_by_tag = get_relevant_document_count_by_tag(es_conn, start_date, end_date)
    logger.info('Hits: %s' % reldoc_by_tag.get('hits').get('total'))
    logger.info('Took: %sms' % reldoc_by_tag.get('took'))
    logger.info('Timedout: %s' % reldoc_by_tag.get('timed_out'))

    buckets = reldoc_by_tag.get('aggregations').get('group_by_tag').get('buckets')
    result = {}
    with get_pg_core_slave1_connection() as pg_conn:
        for each in buckets:
            cat_list = get_item_category(pg_conn, each.get('key'))
            if cat_list:
                for cat in cat_list:
                    if cat not in result:
                        result[cat] = 0
                    result[cat] += each.get('doc_count')
    with open(args.output_file, 'w') as of:
        csv_writer = UnicodeWriter(of, delimiter=',', quotechar='"')
        csv_writer.writerow(['category_name_path', 'doc_count'])
        for k in result:
            csv_writer.writerow([k, result[k]])

if __name__ == '__main__':
    main(sys.argv[1:])
