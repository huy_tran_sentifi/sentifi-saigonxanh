#!/usr/bin/env python
# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 2015/04/17
# @author: diep

from argparse import ArgumentParser

import logging
from helpers.connection_utils import get_pg_core_slave1_connection, get_es_connection
from psycopg2.extras import RealDictCursor
from datetime import datetime
from helpers.utils import init_logger
from model.podio_dao import PodioDao
import sys
import os
import requests
from Queue import Queue
from threading import Thread
import csv


def get_released_screens():
    logger.info('Start get item info...')
    result = {}
    with pg_conn.cursor(cursor_factory=RealDictCursor) as pg_cur:
        pg_cur.execute("""SELECT   n.named_entity_id,
                                   n.name,
                                   n.name_de,
                                   n.legalname,
                                   n.legalname_de,
                                   n.status,
                                   n.streaming_released_at,
                                   n.product_released_at,
                                   n.website_url,
                                   get_category_name_path_by_id(n.category_id, '~') AS category_path,
                                   n.analyst_id,
                                   n.country_code,
                                   array_to_string(array_agg(id.name), ', ') AS indices,
                                   n.detail,
                                   n.detail_de
                            FROM named_entity n
                            LEFT JOIN item_indice ii ON n.named_entity_id = ii.item_id
                            LEFT JOIN indice id ON ii.indice_id = id.id
                            WHERE n.status = 4 OR n.status = 1 --status streaming or released
                            GROUP BY n.named_entity_id""")
        pg_conn.commit()
        category_path_label = ['itemtype', 'itemgroup', 'cat1', 'cat2', 'cat3', 'cat4', 'cat5', 'cat6']
        for row in pg_cur:
            categories = row['category_path'].split('~') if row['category_path'] else []
            cat_len = len(categories)
            for idx, col in enumerate(category_path_label):
                if idx < cat_len:
                    row[col] = categories[idx]
                else:
                    row[col] = ''
            result[row['named_entity_id']] = row

    logger.info('End get item info...')
    return result


def count_keywords(item_ids):
    logger.info('Start count keywords...')
    result = {}
    with pg_conn.cursor() as pg_cur:
        pg_cur.execute("""SELECT exp.item_id,
                                   COUNT(DISTINCT exp.awb->>'word') AS kw_count
                            FROM
                              (SELECT item_id,
                                      json_array_elements(active_word_bag) AS awb
                               FROM expression e
                               WHERE item_id IN %s
                                 AND e.status = 1
                                 AND active_word_bag IS NOT NULL
                                 AND active_word_bag::text != ''
                                 AND active_word_bag::text != '[]'
                                 AND active_word_bag::text != 'null') AS exp
                            WHERE exp.awb IS NOT NULL
                              AND exp.awb::text != ''
                              AND exp.awb::text != '[]'
                              AND exp.awb::text != 'null'
                            GROUP BY exp.item_id""", (tuple(item_ids),))
        pg_conn.commit()
        for row in pg_cur:
            result[row[0]] = row[1]
    logger.info('End count keywords...')
    return result


def count_triggering_terms(item_ids):
    logger.info('Start count triggering terms...')
    result = {}
    with pg_conn.cursor() as pg_cur:
        pg_cur.execute("""SELECT tr_term.item_id,
                                   COUNT(DISTINCT tr_term.term->>'word') AS t_count
                            FROM
                              (SELECT item_id,
                                      json_array_elements(active_triggering_term) AS term
                               FROM term t
                               WHERE item_id IN %s
                                 AND active_triggering_term IS NOT NULL
                                 AND active_triggering_term::text != ''
                                 AND active_triggering_term::text != '[]'
                                 AND active_triggering_term::text != 'null') AS tr_term
                            WHERE tr_term.term IS NOT NULL
                              AND tr_term.term::text != ''
                              AND tr_term.term::text != '[]'
                              AND tr_term.term::text != 'null'
                            GROUP BY tr_term.item_id""", (tuple(item_ids),))
        pg_conn.commit()
        for row in pg_cur:
            result[row[0]] = row[1]
    logger.info('End count triggering terms...')
    return result


def count_messages():
    logger.info('Start count messages hit...')
    result = {}
    agg_query = {
        "query": {
            "filtered": {
                "filter": {
                    "range": {
                        "published_at": {
                            "gte": "now-1M",
                            "lte": "now"
                        }
                    }
                }
            }
        },
        "aggs": {
            "tag_agg": {
                "terms": {
                    "size": 0,
                    "field": "ne_mentions.id"
                }
            }
        }
    }
    agg_msgs = es_conn.search(index='rm_search_staging', doc_type='m', body=agg_query,
                              search_type='count')
    for bucket in agg_msgs['aggregations']['tag_agg']['buckets']:
        result[bucket['key']] = bucket['doc_count']
    logger.info('End count messages hit...')
    return result


def report_to_podio(title, file_path):
    logger.info('Start report file to podio...')
    podio = PodioDao()
    podio.init_app_authentication(client_id='sentifi-vietnam',
                                  client_secret='TksSWPQqDPRHDdQg8vUVDpRQqeQCPQVBr9h5xDR0LddejSuPcRg5syW0hKRp91ai',
                                  app_id=13366284,
                                  app_token='1065398529114e899887438688e0ac11')
    podio.create_item(title=title, attached_file_path=file_path)
    logger.info('End report file to podio...')


def query_top_messages(item_id, channel):
    try:
        res = requests.get('http://filter.eu-west-1.compute.internal:10010/message/topmessagenew?top=40&channel=%s'
                           '&itemkey=%s' % (channel, item_id))
        result = len(res.json()['data']['all'])
    except Exception as ex:
        logger.error('Get top messages error error: %s' % ex)
        result = 'api error'

    return result


def count_top_messages_no_thread(item_ids):
    logger.info('Start count top messages...')
    result = {}
    channel = ['twitter', 'blog,news']
    for item_id in item_ids:
        result[item_id] = {}
        for ch in channel:
            result[item_id][ch] = query_top_messages(item_id, ch)

    logger.info('End count top messages...')
    return result


def query_top_messages_worker(task_queue, result_queue):
    result = {}
    channel = ['twitter', 'blog,news']
    while not task_queue.empty():
        task = task_queue.get()
        result[task] = {}
        for ch in channel:
            result[task][ch] = query_top_messages(task, ch)

        task_queue.task_done()

    result_queue.put(result)


def count_top_messages_thread(item_ids):
    logger.info('Start count top messages...')
    task_queue = Queue()
    result_queue = Queue()
    result = {}
    # put tasks to queue
    for item_id in item_ids:
        task_queue.put(item_id)
    # start threads
    thread_num = 20
    for idx in range(thread_num):
        t = Thread(target=query_top_messages_worker, args=(task_queue, result_queue,))
        t.start()
    # wait for all task are done
    task_queue.join()
    # merge result from all threads
    for _ in range(thread_num):
        result.update(result_queue.get())

    logger.info('End count top messages...')
    return result


def main(options):
    global pg_conn, es_conn, logger
    parser = ArgumentParser(description='Tool that gets metrics for KPI dashboard')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    global logger, pg_conn
    # Setup logger
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    # Root logger
    root_logger = init_logger(name='', log_level=log_level)
    # Disable elasticsearch logging
    logging.getLogger('elasticsearch').setLevel(logging.CRITICAL)
    # Module logger
    logger = logging.getLogger(__name__)
    # init db connection
    pg_conn = get_pg_core_slave1_connection()
    es_conn = get_es_connection()

    # get item information
    items = get_released_screens()
    keywords_count = count_keywords(items.keys())
    triggering_terms_count = count_triggering_terms(items.keys())
    messages_count = count_messages()
    top_messages_count = count_top_messages_thread(items.keys())

    # merge item information
    logger.info('Merging item information...')
    for item_id in items:
        items[item_id]['keywords_count'] = keywords_count[item_id] if item_id in keywords_count else 0
        items[item_id]['triggering_terms_count'] = triggering_terms_count[item_id] if item_id in triggering_terms_count \
            else 0
        items[item_id]['messages_count'] = messages_count[item_id] if item_id in messages_count else 0
        items[item_id]['top_twitter_count'] = top_messages_count[item_id]['twitter']
        items[item_id]['top_news_blog_count'] = top_messages_count[item_id]['blog,news']

    # export result to excel
    logger.info('Export result to files...')
    columns = ['named_entity_id', 'name', 'name_de', 'legalname', 'legalname_de', 'status', 'streaming_released_at',
               'product_released_at', 'website_url', 'itemtype',
               'itemgroup', 'cat1', 'cat2', 'cat3', 'cat4', 'cat5', 'cat6', 'analyst_id', 'country_code', 'indices',
               'detail', 'detail_de', 'keywords_count', 'triggering_terms_count', 'messages_count', 'top_twitter_count',
               'top_news_blog_count']
    result = []
    for item in items.itervalues():
        row = []
        for col in columns:
            row.append(item[col])
        result.append(row)
    # sort data
    import operator
    result = sorted(result, key=operator.itemgetter(len(columns) - 3), reverse=True)
    # write to csv
    current_date = datetime.now().strftime('%Y%m%d')
    podio_dir = '/tmp/podio'
    if not os.path.exists(podio_dir):
        os.makedirs(podio_dir)
    file_path = '%s/%s_kpi_released_screens.csv' % (podio_dir, current_date)
    with open(file_path, 'w+') as f:
        csv_writer = csv.writer(f)
        csv_writer.writerow(columns)
        csv_writer.writerows(result)

    report_to_podio('KPI Released Screens for %s' % datetime.now().strftime('%Y/%m/%d'), file_path)
    logger.info('End KPI for %s' % current_date)
    return


if __name__ == '__main__':
    main(sys.argv[1:])
