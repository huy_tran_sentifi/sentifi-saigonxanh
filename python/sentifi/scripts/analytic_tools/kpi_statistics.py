#!/usr/bin/env python
# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 2015/04/17
# @author: diep

from argparse import ArgumentParser
import csv

import logging
import json
from datetime import datetime
import os
from helpers.connection_utils import get_pg_core_connection
from helpers.utils import init_logger
from model.podio_dao import PodioDao


def _count(count_query):
    with pg_conn.cursor() as cursor:
        logger.debug('Executing %s' % count_query)
        cursor.execute(count_query)
        count = cursor.fetchone()[0]
        pg_conn.commit()
    return count


def count_listed_companies():
    query = '''
SELECT count(named_entity_id)
  FROM named_entity
  WHERE is_listed_company IS TRUE'''
    return _count(query)


def count_org_non_listed_companies():
    query = '''
SELECT count(named_entity_id)
  FROM named_entity
  INNER JOIN category USING (category_id)
  WHERE category_id_path[1] = 2 AND is_listed_company IS NOT TRUE'''
    return _count(query)


def count_asset_classes():
    query = '''
SELECT count(named_entity_id)
  FROM named_entity
  INNER JOIN category USING (category_id)
  WHERE category_id_path[2] = 253'''
    return _count(query)


def count_investment_styles():
    query = '''
SELECT count(named_entity_id)
  FROM named_entity
  INNER JOIN category USING (category_id)
  WHERE category_id_path[2] = 4500000'''
    return _count(query)


def count_currencies():
    query = '''
SELECT count(named_entity_id)
  FROM named_entity
  INNER JOIN category USING (category_id)
  WHERE category_id_path[2] = 252'''
    return _count(query)


def count_indices():
    query = '''
SELECT count(named_entity_id)
  FROM named_entity
  INNER JOIN category USING (category_id)
  WHERE category_id_path[2] = 251'''
    return _count(query)


def count_financial_products():
    query = '''
SELECT count(named_entity_id)
  FROM named_entity
  INNER JOIN category USING (category_id)
  WHERE category_id_path[2] = 254'''
    return _count(query)


def count_events():
    query = '''
SELECT count(pattern_id)
  FROM pattern
  WHERE status = 1
    AND (
        rule IS NOT NULL
        OR rule_de IS NOT NULL)'''
    return _count(query)


def count_persons():
    query = '''
SELECT count(named_entity_id)
  FROM named_entity
  INNER JOIN category USING (category_id)
  WHERE category_id_path[1] = 1'''
    return _count(query)


def count_publishers():
    query = '''
SELECT count((sns_name, sns_id)) FROM sns_account'''
    return _count(query)


def count_investor_relations():
    query = '''
SELECT count((sns_name, sns_id, category_id))
  FROM sns_category
  INNER JOIN category USING (category_id)
  WHERE category_id_path[3] = 150500'''
    return _count(query)


def count_screens():
    query = '''
SELECT count(named_entity_id)
  FROM named_entity
  WHERE status = 4 OR status = 1'''
    return _count(query)


def count_listed_companies_screens():
    query = '''
SELECT count(named_entity_id)
  FROM named_entity
  WHERE (status = 4 OR status = 1)
    AND is_listed_company IS TRUE'''
    return _count(query)


def count_released_listed_companies_screens():
    query = '''
SELECT count(named_entity_id)
  FROM named_entity
  WHERE status = 1
    AND is_listed_company IS TRUE'''
    return _count(query)


def count_twitter_users():
    query = '''
SELECT count((sns_name, sns_id)) FROM sns_account
WHERE sns_name = \'tw\''''
    return _count(query)


def report_to_podio(title, file_path):
    logger.info('Start report file to podio...')
    podio = PodioDao()
    podio.init_app_authentication(client_id='sentifi-vietnam',
                                  client_secret='TksSWPQqDPRHDdQg8vUVDpRQqeQCPQVBr9h5xDR0LddejSuPcRg5syW0hKRp91ai',
                                  app_id=15003273,
                                  app_token='e80c0e12750c4127bf06b07ce9b56af4')
    podio.create_item(title=title, attached_file_path=file_path)
    logger.info('End report file to podio...')


if __name__ == '__main__':
    parser = ArgumentParser(description='Tool that gets metrics for KPI dashboard')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set severity level for the logger')
    args = parser.parse_args()

    global logger, pg_conn
    # Setup logger
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    # Root logger
    root_logger = init_logger(name='', log_level=log_level)
    # Module logger
    logger = logging.getLogger(__name__)

    with get_pg_core_connection() as pg_conn:
        logger.info('-------------Start KPI statistics----------------')
        kpi_statistics = dict()
        kpi_statistics['1_listed_companies'] = count_listed_companies()
        kpi_statistics['2_org_non_listed_companies'] = count_org_non_listed_companies()
        kpi_statistics['3_asset_classes'] = count_asset_classes()
        kpi_statistics['4_investment_styles'] = count_investment_styles()
        kpi_statistics['5_currencies'] = count_currencies()
        kpi_statistics['6_indices'] = count_indices()
        kpi_statistics['7_financial_products'] = count_financial_products()
        kpi_statistics['8_events'] = count_events()
        kpi_statistics['9_persons'] = count_persons()
        kpi_statistics['10_publishers'] = count_publishers()
        kpi_statistics['11_twitter_users'] = count_twitter_users()
        kpi_statistics['12_news&blog_users'] = kpi_statistics['10_publishers'] - kpi_statistics['11_twitter_users']
        kpi_statistics['13_investor_relations'] = count_investor_relations()
        kpi_statistics['14_screens'] = count_screens()
        kpi_statistics['15_listed_companies_screens'] = count_listed_companies_screens()
        kpi_statistics['16_released_listed_companies_screens'] = count_released_listed_companies_screens()
        # print result
        logger.info(json.dumps(kpi_statistics, indent=4, sort_keys=True))
        # write to csv
        current_date = datetime.now().strftime('%Y%m%d')
        podio_dir = '/tmp/podio'
        if not os.path.exists(podio_dir):
            os.makedirs(podio_dir)
        file_path = '%s/%s_kpi_statistics.txt' % (podio_dir, current_date)
        with open(file_path, 'w+') as f:
            json.dump(kpi_statistics, f, indent=4, sort_keys=True)

        report_to_podio('[Weekly report] KPI statistics for %s' % datetime.now().strftime('%Y/%m/%d'), file_path)
        logger.info('-------------End KPI statistics------------------')
