#!/usr/bin/env python
# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Sep 22, 2014
# @author: trung

from argparse import ArgumentParser
from imp import load_source

import logging
import os
import sys

from helpers.connection_utils import get_mg_client
from helpers.utils import init_logger


def print_key(d, l=0):
    if isinstance(d, (dict, list)):
        for k in d:
            if isinstance(d[k], dict):
                print '%s%s' % (l * ',', k)
                print_key(d[k], l + 1)
            elif isinstance(d[k], list):
                print '%s%s' % (l * ',', k)
                for each in d[k]:
                    print_key(each, l + 1)
            else:
                print '%s%s,%s' % (l * ',', k, d[k])


def count_key(source_dict, keys_count_dict):
    for k in source_dict:
        if isinstance(source_dict[k], dict):
            if keys_count_dict.get(k) is None:
                keys_count_dict[k] = {}
            count_key(source_dict[k], keys_count_dict[k])
        elif isinstance(source_dict[k], list):
            if keys_count_dict.get(k) is None:
                keys_count_dict[k] = {}
            for each in source_dict[k]:
                if isinstance(each, dict):
                    count_key(each, keys_count_dict[k])
        else:
            if k in keys_count_dict:
                if isinstance(keys_count_dict[k], dict):
                    continue
                keys_count_dict[k] += 1
            else:
                keys_count_dict[k] = 1


def main(options):
    parser = ArgumentParser(
        description='Tool that print out all keys from Publisher')
    parser.add_argument('-H', '--host', dest='host', help='MongoDB host',
                        required=True)
    parser.add_argument('-p', '--port', dest='port', help='MongoDB host',
                        type=int, default=27017)
    parser.add_argument('-d', '--database', dest='db_name',
                        help='database\'s name', required=True)
    parser.add_argument('-c', '--collection', dest='collection',
                        help='collections\'s name', required=True)
    parser.add_argument('-v', '--verbose', dest='verbose', action='count',
                        help='set severity level for the logger')
    args = parser.parse_args()

    global logger, setting
    # Setup logger
    log_level = logging.WARNING
    if args.verbose >= 2:
        log_level = logging.DEBUG
    if args.verbose == 1:
        log_level = logging.INFO
    # Root logger
    root_logger = init_logger(name='', log_level=log_level)
    # Module logger
    logger = logging.getLogger(__name__)

    setting = load_source('setting', os.environ['SETTING_FILE'])

    conn = get_mg_client(args.host, args.port)
    coll = conn[args.db_name][args.collection]
    cursor = coll.find()
    d = {}
    for each in cursor:
        count_key(each, d)
    print_key(d)


if __name__ == '__main__':
    main(sys.argv[1:])
