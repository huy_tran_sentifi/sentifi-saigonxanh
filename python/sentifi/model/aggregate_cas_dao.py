__author__ = 'semantic'

from cassandra.query import tuple_factory
from cassandra.query import named_tuple_factory
from cassandra.query import dict_factory
from cassandra.query import BatchStatement
from cassandra import ConsistencyLevel
from cassandra.concurrent import execute_concurrent

import logging

logger = logging.getLogger('model.aggregation_cas_dao')

ROW_FACTORIES = {
    'dict': dict_factory,
    'tuple': tuple_factory,
    'named_tuple': named_tuple_factory
}


def chunks(input_sequence, n):
    """ Yield successive n-sized chunks from input_sequence.
    """
    for i in xrange(0, len(input_sequence), n):
        yield input_sequence[i:i + n]


class AggregationDao(object):

    """The DAO of the publisher metrics

    Attributes:
        client (cassandra.cluster.Session): The client to the cluster of the current keyspace

    Args:
        connection (cassandra.cluster.Cluster): Cassandra driver connection object
        keyspace (str): the name of the keyspace to connect to
        return_type (str of {'tuple', 'named_tuple', 'dict'})
            Determines the type of the return result from querying.

            - dict (default): return result as a dict
            - tuple: return result as a tuple
            - named_tuple: return result as a named_tuple
        timeout (int): a timeout of queries, measured in seconds, default to no timeout
    """

    def __init__(self, connection, keyspace, return_type='dict', timeout=None):
        self._connection = connection
        self._keyspace = keyspace
        self._cassandra_session = None
        self._timeout = timeout
        self._prepared_statement_insert = None
        self._prepared_statement_batch = None

        if isinstance(return_type, (str, unicode)) and \
           return_type in ['tuple', 'named_tuple' or 'dict']:
            self._row_factory = ROW_FACTORIES[return_type]
        else:
            logger.info("return_type has to be a string of 'tuple', 'named_tuple' or 'dict'.")
            logger.info("return_type is set to the default 'dict' type")
            self._row_factory = ROW_FACTORIES['dict']

    def __str__(self):
        try:
            return 'Connect to keyspace [%s] of cluster [%s].' % (self._keyspace, self._connection.metadata.cluster_name)
        except Exception, e:
            raise e
        else:
            return 'AggregationDao'

    def __repr__(self):
        try:
            return 'Connect to keyspace [%s] of cluster [%s].' % (self._keyspace, self._connection.metadata.cluster_name)
        except Exception, e:
            raise e
        else:
            return 'AggregationDao'

    def __enter__(self):
        return self.connect()

    def __exit__(self, exit_type, value, traceback):
        self.close()
        logger.info('Connection to keyspace [%s] closed.' % str(self._keyspace))

    def close(self):
        """Close the connection to the cluster
        """
        self._cassandra_session.shutdown()

    def connect(self):
        """Connect to the cluster

        Returns:
            MetricDao: the object of connected DAO
        """
        self._cassandra_session = self._connection.connect(self._keyspace)
        self._cassandra_session.row_factory = self._row_factory
        self._cassandra_session.default_timeout = self._timeout
        logger.info('Connected to keyspace [%s].' % str(self._keyspace))

        return self

    @property
    def client(self):
        """The client to the cluster of the current keyspace
        """
        if not self._cassandra_session:
            self.connect()
        return self._cassandra_session

    # def insertOLD(self, metric, table_name):
    #     """Insert a metric into Cassandra clusters
    #
    #     Args:
    #         metric (Metric): the metric to be inserted
    #     """
    #     query = '''
    #     INSERT INTO %s (
    #                     sns_name,
    #                     sns_id,
    #                     metric_id,
    #                     stat_date,
    #                     stat_value
    #                     ) VALUES (?, ?, ?, ?, ?)''' % table_name
    #
    #     if self._prepared_statement_insert is None:sages_staging_write
    #         self._prepared_statement_insert = self._cassandra_session.prepare(query)
    #
    #     bind_statement = self._prepared_statement_insert.bind((metric.sns_name,
    #                                                            str(metric.sns_id),
    #                                                            metric.metric_id,
    #                                                            metric.date,
    #                                                            metric.value))
    #     self._cassandra_session.execute(bind_statement)
    #     return True

    # def insert(self, metrics, table_name):
    #     """Insert a metric into Cassandra clusters
    #
    #     Args:
    #         metric (Metric): the metric to be inserted
    #         table_name (string): the name of the table to insert into
    #     """
    #     if self._prepared_statement_insert is None:
    #         query = '''
    #         INSERT INTO %s (
    #                         sns_name,
    #                         sns_id,
    #                         metric_id,
    #                         stat_date,
    #                         stat_value
    #                         ) VALUES (?, ?, ?, ?, ?)''' % table_name
    #         self._prepared_statement_insert = self._cassandra_session.prepare(query)
    #     statements_and_params = [(self._prepared_statement_insert, (metric.sns_name,
    #                                                                 str(metric.sns_id),
    #                                                                 metric.metric_id,
    #                                                                 metric.date,
    #                                                                 metric.value)) for metric in metrics]
    #
    #     results = execute_concurrent(self._cassandra_session, statements_and_params, raise_on_first_error=False)
    #     reruns = []
    #     for index, (success, result) in enumerate(results):
    #         if not success:
    #             logger.error('%s' % result)
    #             reruns.append(metrics[index])
    #         else:
    #             logger.debug('%s' % result)
    #     if len(reruns) > 0:
    #         logger.info("Re-run errors")
    #         self.insert(reruns, table_name)

    def insert(self, aggregations, table_name):
        """Insert a metric into Cassandra clusters

        Args:
            metric (Metric): the metric to be inserted
            table_name (string): the name of the table to insert into
        """
        if self._prepared_statement_insert is None:
            query = '''
            INSERT INTO %s (
                            stat_date,
                            metric_id,
                            stat_name,
                            stat_value
                            ) VALUES (?, ?, ?, ?)''' % table_name
            self._prepared_statement_insert = self._cassandra_session.prepare(query)
        statements_and_params = [(self._prepared_statement_insert, (agg.date,
                                                                    agg.metric_id,
                                                                    agg.stat_name,
                                                                    agg.value)) for agg in aggregations]

        results = execute_concurrent(self._cassandra_session, statements_and_params, raise_on_first_error=False)
        reruns = []
        for index, (success, result) in enumerate(results):
            if not success:
                logger.error('%s' % result)
                reruns.append(aggregations[index])
            else:
                logger.debug('%s' % result)
        if len(reruns) > 0:
            logger.info("Re-run errors")
            self.insert(reruns, table_name)

    # def insert_batch(self, metrics, table_name='weekly_sns_stats', batch_size=10 ** 5, timeout=3600):
    #     """ TODO
    #     """
    #     query = '''
    #     INSERT INTO %s (
    #                     sns_name,
    #                     sns_id,
    #                     metric_id,
    #                     stat_date,
    #                     stat_value
    #                     ) VALUES (?, ?, ?, ?, ?)''' % table_name
    #     if self._prepared_statement_batch is None:
    #         self._prepared_statement_batch = self._cassandra_session.prepare(query)
    #     for index, batch in enumerate(chunks(metrics, batch_size)):
    #         batch_statement = BatchStatement(consistency_level=ConsistencyLevel.ONE)
    #         for metric in batch:
    #             batch_statement.add(self._prepared_statement_batch, (metric.sns_name,
    #                                                                  str(metric.sns_id),
    #                                                                  metric.metric_id,
    #                                                                  metric.date,
    #                                                                  metric.value))
    #         # if index % batch_size == 0:
    #         #     logger.info('%d batches inserted' % index)
    #
    #         self._cassandra_session.execute(batch_statement, timeout=timeout)
    #     return True

    def update(self):
        """ TODO
        """
        return NotImplementedError

    def get_metric_info(self, metric_table):
        """Get all metrics info from Cassandra

        Returns:
            list: a list of dictionary of metrics info
        """
        query = '''SELECT * FROM %s''' % metric_table
        rows = self._cassandra_session.execute(query)
        return [metric for metric in rows]

    def get_stat_by_date(self, metric_table, start=None):

        query = '''
        SELECT *
        FROM %s
        WHERE
                stat_date = '%s';''' %  (metric_table,
                                     start.strftime('%Y-%m-%d %H:%M:%S%z'))
        rows = self._cassandra_session.execute(query, timeout=3600)
        return [metric for metric in rows]

    def get_stat_by_metric(self, metric_table, metric_id, start=None):
        """Get all metric values in a date range of a single metric_id from Cassandra

        Args:
            metric_table (str): the name of the metric table
            metric_id (int): the metric id to retrieve values
            start (datetime): the start time of a date range for this query, default None

        Returns:
            list: a list of dictionary of retrieved metrics values
        """
        query = '''
        SELECT *
        FROM %s
        WHERE
                stat_date = '%s' AND
                metric_id=%d;''' %  (metric_table,
                                     start.strftime('%Y-%m-%d %H:%M:%S%z'),
                                     metric_id)
        rows = self._cassandra_session.execute(query, timeout=3600)
        return [metric for metric in rows]

    def get_stat_by_name(self, metric_table, metric_id, stat_name, start=None):
        """Get all metric values in a date range of a single metric_id from Cassandra

        Args:
            metric_table (str): the name of the metric table
            metric_id (int): the metric id to retrieve values
            start (datetime): the start time of a date range for this query, default None

        Returns:
            list: a list of dictionary of retrieved metrics values
        """
        query = '''
        SELECT *
        FROM %s
        WHERE
                stat_date = '%s' AND
                metric_id=%d AND
                stat_name='%s';''' %  (metric_table,
                                     start.strftime('%Y-%m-%d %H:%M:%S%z'),
                                     metric_id,
                                        stat_name)
        rows = self._cassandra_session.execute(query, timeout=3600)
        return [metric for metric in rows]

    # def get_metrics_by_sns_id(self, metric_table, sns_id, metric_ids, start=None):
    #     """Get all metric values in a date range of a single metric_id from Cassandra
    #
    #     Args:
    #         metric_table (str): the name of the metric table
    #         sns_id (str): the id of the publisher
    #         metric_ids (list of int): the metric id to retrieve values
    #         start (datetime): the start time of a date range for this query, default None
    #
    #     Returns:
    #         list: a list of dictionary of retrieved metrics values
    #     """
    #
    #     query = '''
    #     SELECT *
    #     FROM %s
    #     WHERE   sns_name='tw' AND
    #             sns_id='%s' AND
    #             metric_id IN (%s) AND
    #             stat_date='%s';''' % (metric_table,
    #                                   sns_id,
    #                                   ','.join([str(each) for each in metric_ids]),
    #                                   start.strftime('%Y-%m-%d %H:%M:%S%z'))
    #     rows = self._cassandra_session.execute(query, timeout=3600)
    #     return [metric for metric in rows]