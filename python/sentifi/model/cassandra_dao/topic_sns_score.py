# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 25/03/2015
# @author: trung

from cassandra.cluster import PagedResult
from itertools import count
from logging import getLogger
from threading import Event
from cassandra.concurrent import execute_concurrent
from cassandra.query import dict_factory

from constant.cassandra_namespace import CF_TOPIC_SNS_SCORE_BY_TOPIC, \
    CF_TOPIC_SNS_SCORE_BY_SNS, PUBLISHER_ANALYTICS
from model import TopicSnsScore


logger = getLogger(__name__)


class TopicSnsScoreDao(object):

    """Class providing a Data Access Layer for TopicSnsScore from/to Cassandra
    """

    def __init__(self, cluster, keyspace=PUBLISHER_ANALYTICS):
        """
        Just be sure to never share any Cluster, Session, or ResponseFuture
        objects across multiple processes. These objects should all be created
        after forking the process, not before

        :param cluster:
        :param keyspace:
        """
        self._cluster = cluster
        self._session = cluster.connect(keyspace)
        self._session.row_factory = dict_factory
        self._model = TopicSnsScore
        self._sentinel = object()

        self.__init_stats()

        self._insert_score_by_sns_ps = None
        self._insert_score_by_topic_ps = None
        self._select_score_by_sns_ps = None

    def __init_stats(self):
        self.statements_count = count()
        self.finished_event = Event()

    def _validate_model(self, var):
        if not isinstance(var, self._model):
            raise TypeError('Type %s is expected, not %s' % (
                self._model.__name__, type(var))
            )

    def _concurrent_insert(self, topic_sns_score, concurrency=100):
        self._validate_model(topic_sns_score)

        if concurrency <= 0:
            raise ValueError("concurrency must be greater than 0")

        if not topic_sns_score or not topic_sns_score.triplets_iter:
            return []

        if not self._insert_score_by_sns_ps:
            self._insert_score_by_sns_ps = self._session.prepare('''
INSERT INTO {table} (metric_id, scored_at, sns_name, topic_id, sns_id, score)
    VALUES ({metric_id}, {scored_at}, {sns_name}, ?, ?, ?)'''.format(
                table=CF_TOPIC_SNS_SCORE_BY_TOPIC,
                metric_id=topic_sns_score.metric_id,
                scored_at=topic_sns_score.scored_at,
                sns_name=topic_sns_score.sns_name
            ))

    #     if not self._insert_score_by_topic_ps:
    #         self._insert_score_by_topic_ps = self._session.prepare('''
    # INSERT INTO {table} (metric_id, scored_at, sns_name, topic_id, sns_id, score)
    #     VALUES ({metric_id}, {scored_at}, {sns_name}, ?, ?, ?)'''.format(
    #             table=CF_TOPIC_SNS_SCORE_BY_TOPIC,
    #             metric_id=topic_sns_score.metric_id,
    #             scored_at=topic_sns_score.scored_at,
    #             sns_name=topic_sns_score.sns_name
    #         ))

        triplets_iter = topic_sns_score.triplets_iter
        # first_error = [] if raise_on_first_error else None
        results = []

        self._insert_next(self._sentinel, triplets_iter, None)

        self.finished_event.wait()
        return

    def _handle_error(self):
        pass

    def _insert_next(self, result, params_iter, future):
        if result is not self._sentinel:
            if future.has_more_pages:
                result = PagedResult(future, result)
                future.clear_callbacks()

        try:
            params = params_iter.next()
            future = self._session.execute_async(self._insert_score_by_sns_ps, params)
            # future_by_topic = self._session.execute_async(self._insert_score_by_topic_ps, params)
            args = (result, params_iter, future)
            future.add_callbacks(
                callback=self._insert_next, callback_args=args,
                errback=self._handle_error, errback_args=args)
            self.statements_count.next()
        except StopIteration:
            self.finished_event.set()
            return
        except Exception:
            logger.error('Error on insert: %s' % result)
            self.finished_event.set()
            return

    def close(self):
        self._cluster.shutdown()

    def insert_score_by_sns(self, topic_sns_score):
        self._validate_model(topic_sns_score)
        self._insert_score_by_sns_ps = self._session.prepare('''
INSERT INTO {table} (metric_id, scored_at, sns_name, topic_id, sns_id, score)
VALUES ({metric_id}, '{scored_at}', '{sns_name}', ?, ?, ?)'''.format(
            table=CF_TOPIC_SNS_SCORE_BY_SNS,
            metric_id=topic_sns_score.metric_id,
            scored_at=topic_sns_score.scored_at,
            sns_name=topic_sns_score.sns_name
        ))

        # TODO: Fix either this or contribute to the driver to make it works
        # with iterables objects naturally
        statements_and_params = [(self._insert_score_by_sns_ps, _) for _ in topic_sns_score.triplets_list]

        results = execute_concurrent(self._session, statements_and_params)

        for (success, result) in results:
            if not success:
                logger.error('%s' % result)
            else:
                logger.debug('%s' % result)

    def insert_score_by_topic(self, topic_sns_score):
        self._validate_model(topic_sns_score)
        self._insert_score_by_topic_ps = self._session.prepare('''
INSERT INTO {table} (metric_id, scored_at, sns_name, topic_id, sns_id, score)
VALUES ({metric_id}, '{scored_at}', '{sns_name}', ?, ?, ?)'''.format(
            table=CF_TOPIC_SNS_SCORE_BY_TOPIC,
            metric_id=topic_sns_score.metric_id,
            scored_at=topic_sns_score.scored_at,
            sns_name=topic_sns_score.sns_name
        ))

        statements_and_params = [(self._insert_score_by_topic_ps, _) for _ in topic_sns_score.triplets_list]

        results = execute_concurrent(self._session, statements_and_params)

        for (success, result) in results:
            if not success:
                logger.error('%s' % result)
            else:
                logger.debug('%s' % result)

    def insert(self, topic_sns_score):
        self.insert_score_by_sns(topic_sns_score)
        self.insert_score_by_topic(topic_sns_score)

    def get_topic_score_by_sns_id(self, metric_table, sns_id, metric_id, scored_at=None):
        """Get all metric values in a date range of a single metric_id from Cassandra

        Args:
            metric_table (str): the name of the metric table
            sns_id (str): the id of the publisher
            metric_id (int): the metric id to retrieve values
            scored_at (datetime): the scored_at time of a date range for this query, default None

        Returns:
            list: a list of dictionary of retrieved metrics values
        """

        query = '''
        SELECT *
        FROM %s
        WHERE   sns_name='tw' AND
                sns_id='%s' AND
                metric_id=%s AND
                scored_at='%s';''' % (metric_table,
                                      sns_id,
                                      str(metric_id),
                                      scored_at.strftime('%Y-%m-%d %H:%M:%S%z'))
        rows = self._session.execute(query, timeout=3600)
        return [metric for metric in rows]

    def get_topic_scores_by_sns_ids(self, metric_table, sns_ids, metric_id, scored_at=None):
        """Get all metric values in a date range of a single metric_id from Cassandra

        Args:
            metric_table (str): the name of the metric table
            sns_ids (list of str): the list of ids of the publishers
            metric_id (int): the metric id to retrieve values
            scored_at (datetime): the scored_at time of a date range for this query, default None

        Returns:
            list: a list of dictionary of retrieved metrics values
        """
        query = '''
        SELECT *
        FROM %s
        WHERE   sns_name='tw' AND
                sns_id=? AND
                metric_id=%s AND
                scored_at='%s';''' % (metric_table,
                                      str(metric_id),
                                      scored_at.strftime('%Y-%m-%d %H:%M:%S%z'))
        self._select_score_by_sns_ps = self._session.prepare(query)
        statements_and_params = [(self._select_score_by_sns_ps, (pub_id, )) for pub_id in sns_ids]
        results = execute_concurrent(self._session, statements_and_params)
        return results

    def get_topic_score_by_topic_id(self, metric_table, topic_id, metric_id, scored_at=None):
        """Get all scores of a topic in a date range of a single metric_id from Cassandra

        Args:
            metric_table (str): the name of the metric table
            topic_id (str): the id of the topic
            metric_id (int): the metric id to retrieve values
            scored_at (datetime): the scored_at time of a date range for this query, default None

        Returns:
            list: a list of dictionary of retrieved metrics values
        """

        query = '''
        SELECT *
        FROM %s
        WHERE   topic_id=%d AND
                metric_id=%d AND
                scored_at='%s';''' % (metric_table,
                                      topic_id,
                                      metric_id,
                                      scored_at.strftime('%Y-%m-%d %H:%M:%S%z'))
        rows = self._session.execute(query, timeout=3600)
        return [metric for metric in rows]
