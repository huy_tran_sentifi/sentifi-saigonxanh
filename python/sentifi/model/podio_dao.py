__author__ = 'diepdt'

from pypodio2 import api
from os.path import basename


class PodioDao(object):
    """
    Class for interacting with podio: https://podio.com
    """
    def __init__(self, client=None):
        self.client = client
        self.app_id = None

    def init_app_authentication(self, client_id, client_secret, app_id, app_token):
        """
        Init application authentication for podio
        :param client_id (str): client id
        :param client_secret: client secret
        :param app_id (int): application id (must be passed as integer)
        :param app_token: application token
        :return:
        """
        self.client = api.OAuthAppClient(client_id=client_id, client_secret=client_secret, app_id=app_id,
                                         app_token=app_token)
        self.app_id = app_id

    def create_item(self, title, attached_file_path=None):
        """
        Create new item on podio in specific application
        :param title (str):
        :param attached_file_path (str):
        :return:
        """
        # upload file if have any attached
        uploaded_file = None
        if attached_file_path:
            with open(attached_file_path) as f:
                uploaded_file = self.client.Files.create(basename(attached_file_path), f)
        # create new item
        item = self.client.Item.create(self.app_id, attributes={'fields': {
            'title': title
        }})
        # attach file to item if have any
        if uploaded_file:
            self.client.Files.attach(file_id=uploaded_file['file_id'], ref_type='item', ref_id=item['item_id'])
