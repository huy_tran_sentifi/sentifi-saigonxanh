# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Jun 28, 2014
# @author: trung

import gzip


class DictionaryDAO():

    def __init__(self, connection, prefix):
        self._conn = connection
        self._prefix = prefix
        self._file_path = '/tmp/test.gz'
        self._gz_file = gzip.open(self._file_path, 'wb')

    def __enter__(self):
        return self

    def __exit__(self):
        self.close()

    def add(self, json_string):
        '''
        @summary: Add a JSON string to the archive. A line separator will be
            added automatically
        '''
        self._gz_file.write(json_string)
        self._gz_file.write('\n')

    def close(self):
        self._gz_file.close()
        self._conn.close()

    def find(self, document_id=None, mongo_id=None):
        raise NotImplementedError

    def find_recent(self, a_timestamp):
        raise NotImplementedError

    def flush(self):
        raise NotImplementedError

    def get_latest_timestamp(self):
        raise NotImplementedError

    def insert(self, document):
        raise NotImplementedError
