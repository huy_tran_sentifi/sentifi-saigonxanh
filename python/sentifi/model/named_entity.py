# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Sep 26, 2014
# @author: trung

import ujson as json


class Category(object):
    ''' Class representing a Category
    '''

    def __init__(self, category_id=None, status=None, created_at=None,
                 updated_at=None, parent_category_id=None,
                 category_mongo_id=None, parent_category_mongo_id=None,
                 category_name=None, detail=None, category_level=None,
                 category_id_path=None):
        self.category_id = category_id
        self.status = status
        self.created_at = created_at
        self.updated_at = updated_at
        self.parent_category_id = parent_category_id
        self.category_mongo_id = category_mongo_id
        self.parent_category_mongo_id = parent_category_mongo_id
        self.category_name = category_name
        self.detail = detail
        self.category_level = category_level
        self.category_id_path = category_id_path

    @staticmethod
    def from_json(payload):
        return Category(
            category_id=payload.get('category_id'),
            status=payload.get('status'),
            created_at=payload.get('created_at'),
            updated_at=payload.get('updated_at'),
            parent_category_id=payload.get('parent_category_id'),
            category_mongo_id=payload.get('category_mongo_id'),
            parent_category_mongo_id=payload.get('parent_category_mongo_id'),
            category_name=payload.get('category_name'),
            detail=payload.get('detail'),
            category_level=payload.get('category_level'),
            category_id_path=payload.get('category_id_path')
        )

    @property
    def id(self):
        return self.category_id

    def to_json_string(self):
        return json.dumps(self.__dict__)


class NamedEntity(object):
    def __init__(self, named_entity_id=None, status=None, created_at=None,
                 updated_at=None, ready_at=None, old_id=None, object_id=None,
                 category_id=None, updated_by_analyst_id=None,
                 named_entity_name=None, named_entity_name_de=None,
                 named_entity_type=None, detail=None, detail_de=None,
                 named_entity_url=None, is_topic=None, audited_field=None,
                 synonym=None, released=None, sentifi_o=None, config=None,
                 blacklist=None, keyword=None, profile_cat=None, rss=None,
                 businessweek_url=None, sector=None, company=None,
                 issue_curation=None, ready=None, alert_enabled=None, slug=None,
                 indice=None, legal_misspelled=None, sector_local=None,
                 sector_bw=None, industry_subsector_local=None,
                 industry_subsector_bw=None, stock_exchange=None,
                 ticker_local=None, ticker_bloomberg=None, ir=None,
                 se_mainstock=None, se_sourceurl=None, se_document=None,
                 se_documenturl=None, cashtag=None, cashtag_local=None,
                 market=None):
        self.named_entity_id = named_entity_id
        self.status = status
        self.created_at = created_at
        self.updated_at = updated_at
        self.ready_at = ready_at
        self.old_id = old_id
        self.object_id = object_id
        self.category_id = category_id
        self.updated_by_analyst_id = updated_by_analyst_id
        self.named_entity_name = named_entity_name
        self.named_entity_name_de = named_entity_name_de
        self.named_entity_type = named_entity_type
        self.detail = detail
        self.detail_de = detail_de
        self.named_entity_url = named_entity_url
        self.is_topic = is_topic
        self.audited_field = audited_field
        self.synonym = synonym
        self.released = released
        self.sentifi_o = sentifi_o
        self.config = config
        self.blacklist = blacklist
        self.keyword = keyword
        self.profile_cat = profile_cat
        self.rss = rss
        self.businessweek_url = businessweek_url
        self.sector = sector
        self.company = company
        self.issue_curation = issue_curation
        self.ready = ready
        self.alert_enabled = alert_enabled
        self.slug = slug
        self.indice = indice
        self.legal_misspelled = legal_misspelled
        self.sector_local = sector_local
        self.sector_bw = sector_bw
        self.industry_subsector_local = industry_subsector_local
        self.industry_subsector_bw = industry_subsector_bw
        self.stock_exchange = stock_exchange
        self.ticker_local = ticker_local
        self.ticker_bloomberg = ticker_bloomberg
        self.ir = ir
        self.se_mainstock = se_mainstock
        self.se_sourceurl = se_sourceurl
        self.se_document = se_document
        self.se_documenturl = se_documenturl
        self.cashtag = cashtag
        self.cashtag_local = cashtag_local
        self.market = market

    @staticmethod
    def from_json(payload):
        return NamedEntity(
            named_entity_id=payload.get('named_entity_id'),
            status=payload.get('status'),
            created_at=payload.get('created_at'),
            updated_at=payload.get('updated_at'),
            ready_at=payload.get('ready_at'),
            old_id=payload.get('old_id'),
            object_id=payload.get('object_id'),
            category_id=payload.get('category_id'),
            updated_by_analyst_id=payload.get('updated_by_analyst_id'),
            named_entity_name=payload.get('named_entity_name'),
            named_entity_name_de=payload.get('named_entity_name_de'),
            named_entity_type=payload.get('named_entity_type'),
            detail=payload.get('detail'),
            detail_de=payload.get('detail_de'),
            named_entity_url=payload.get('named_entity_url'),
            is_topic=payload.get('is_topic'),
            audited_field=payload.get('audited_field'),
            synonym=payload.get('synonym'),
            released=payload.get('released'),
            sentifi_o=payload.get('sentifi_o'),
            config=payload.get('config'),
            blacklist=payload.get('blacklist'),
            keyword=payload.get('keyword'),
            profile_cat=payload.get('profile_cat'),
            rss=payload.get('rss'),
            businessweek_url=payload.get('businessweek_url'),
            sector=payload.get('sector'),
            company=payload.get('company'),
            issue_curation=payload.get('issue_curation'),
            ready=payload.get('ready'),
            alert_enabled=payload.get('alert_enabled'),
            slug=payload.get('slug'),
            indice=payload.get('indice'),
            legal_misspelled=payload.get('legal_misspelled'),
            sector_local=payload.get('sector_local'),
            sector_bw=payload.get('sector_bw'),
            industry_subsector_local=payload.get('industry_subsector_local'),
            industry_subsector_bw=payload.get('industry_subsector_bw'),
            stock_exchange=payload.get('stock_exchange'),
            ticker_local=payload.get('ticker_local'),
            ticker_bloomberg=payload.get('ticker_bloomberg'),
            ir=payload.get('ir'),
            se_mainstock=payload.get('se_mainstock'),
            se_sourceurl=payload.get('se_sourceurl'),
            se_document=payload.get('se_document'),
            se_documenturl=payload.get('se_documenturl'),
            cashtag=payload.get('cashtag'),
            cashtag_local=payload.get('cashtag_local'),
            market=payload.get('market')
        ) if payload else None

    @property
    def id(self):
        return self.named_entity_id

    def to_json_string(self):
        return json.dumps(self.__dict__)


# class NamedEntityAuditInformation(object):
#     def __init__(self, named_entity_id=None, created_at=None, updated_at=None,
#                  analyst_id=None, audited_field=None, synonym=None,
#                  released=None, sentifi_o=None, config=None, blacklist=None,
#                  keyword=None, profile_cat=None, rss=None,
#                  businessweek_url=None, sector=None, company=None,
#                  issue_curation=None, ready=None, alert_enabled=None,
#                  named_entity_type=None, last_ready=None, slug=None,
#                  indice=None, legal_misspelled=None, sector_local=None,
#                  sector_bw=None, industry_subsector_local=None,
#                  industry_subsector_bw=None, stock_exchange=None,
#                  ticker_local=None, ticker_bloomberg=None, ir=None,
#                  se_mainstock=None, se_sourceurl=None, se_document=None,
#                  se_documenturl=None, cashtag=None, cashtag_local=None,
#                  market=None):
#         self.named_entity_id = named_entity_id
#         self.created_at = created_at
#         self.updated_at = updated_at
#         self.analyst_id = analyst_id
#         self.audited_field = audited_field
#         self.synonym = synonym
#         self.released = released
#         self.sentifi_o = sentifi_o
#         self.config = config
#         self.blacklist = blacklist
#         self.keyword = keyword
#         self.profile_cat = profile_cat
#         self.rss = rss
#         self.businessweek_url = businessweek_url
#         self.sector = sector
#         self.company = company
#         self.issue_curation = issue_curation
#         self.ready = ready
#         self.alert_enabled = alert_enabled
#         self.named_entity_type = named_entity_type
#         self.last_ready = last_ready
#         self.slug = slug
#         self.indice = indice
#         self.legal_misspelled = legal_misspelled
#         self.sector_local = sector_local
#         self.sector_bw = sector_bw
#         self.industry_subsector_local = industry_subsector_local
#         self.industry_subsector_bw = industry_subsector_bw
#         self.stock_exchange = stock_exchange
#         self.ticker_local = ticker_local
#         self.ticker_bloomberg = ticker_bloomberg
#         self.ir = ir
#         self.se_mainstock = se_mainstock
#         self.se_sourceurl = se_sourceurl
#         self.se_document = se_document
#         self.se_documenturl = se_documenturl
#         self.cashtag = cashtag
#         self.cashtag_local = cashtag_local
#         self.market = market
#
#     @staticmethod
#     def from_json(payload):
#         return NamedEntityAuditInformation(
#             named_entity_id=payload.get('named_entity_id'),
#             created_at=payload.get('created_at'),
#             updated_at=payload.get('updated_at'),
#             analyst_id=payload.get('analyst_id'),
#             audited_field=payload.get('audited_field'),
#             synonym=payload.get('synonym'),
#             released=payload.get('released'),
#             sentifi_o=payload.get('sentifi_o'),
#             config=payload.get('config'),
#             blacklist=payload.get('blacklist'),
#             keyword=payload.get('keyword'),
#             profile_cat=payload.get('profile_cat'),
#             rss=payload.get('rss'),
#             businessweek_url=payload.get('businessweek_url'),
#             sector=payload.get('sector'),
#             company=payload.get('company'),
#             issue_curation=payload.get('issue_curation'),
#             ready=payload.get('ready'),
#             alert_enabled=payload.get('alert_enabled'),
#             named_entity_type=payload.get('named_entity_type'),
#             last_ready=payload.get('last_ready'),
#             slug=payload.get('slug'),
#             indice=payload.get('indice'),
#             legal_misspelled=payload.get('legal_misspelled'),
#             sector_local=payload.get('sector_local'),
#             sector_bw=payload.get('sector_bw'),
#             industry_subsector_local=payload.get('industry_subsector_local'),
#             industry_subsector_bw=payload.get('industry_subsector_bw'),
#             stock_exchange=payload.get('stock_exchange'),
#             ticker_local=payload.get('ticker_local'),
#             ticker_bloomberg=payload.get('ticker_bloomberg'),
#             ir=payload.get('ir'),
#             se_mainstock=payload.get('se_mainstock'),
#             se_sourceurl=payload.get('se_sourceurl'),
#             se_document=payload.get('se_document'),
#             se_documenturl=payload.get('se_documenturl'),
#             cashtag=payload.get('cashtag'),
#             cashtag_local=payload.get('cashtag_local'),
#             market=payload.get('market')
#         ) if payload else None
#
#     @property
#     def id(self):
#         return self.named_entity_id
#
#     def to_json_string(self):
#         return json.dumps(self.__dict__)


class NamedEntitySnsLink(object):
    def __init__(self, named_entity_id=None, created_at=None, updated_at=None,
                 sns_name=None, sns_url=None, sns_id=None):
        self.named_entity_id = named_entity_id
        self.created_at = created_at
        self.updated_at = updated_at
        self.sns_name = sns_name
        self.sns_url = sns_url
        self.sns_id = sns_id

    @staticmethod
    def from_json(payload):
        return NamedEntitySnsLink(
            named_entity_id=payload.get('named_entity_id'),
            created_at=payload.get('created_at'),
            updated_at=payload.get('updated_at'),
            sns_name=payload.get('sns_name'),
            sns_url=payload.get('sns_url'),
            sns_id=payload.get('sns_id')
        ) if payload else None

    @property
    def id(self):
        return self.named_entity_id

    def to_json_string(self):
        return json.dumps(self.__dict__)
