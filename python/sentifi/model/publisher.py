# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Aug 11, 2014
# @author: trung

import ujson as json


class Publisher(object):
    '''Class representing a Publisher
    '''

    def __init__(self, publisher_id=None, status=None, created_at=None,
                 updated_at=None, released_at=None, parent_publisher_id=None,
                 publisher_mongo_id=None, parent_publisher_mongo_id=None,
                 object_id=None, updated_by_analyst_id=None,
                 publisher_name=None, detail=None, lang=None,
                 publisher_url=None, photo_url=None, priority=None,
                 news_aggregator=None, is_restricted=None, is_verified=None,
                 is_virtual=None, source=None, survey=None, update_note=None,
                 priority_excluded=None, item_key=None, top=None):
        self.publisher_id = publisher_id
        self.status = status
        self.created_at = created_at
        self.updated_at = updated_at
        self.released_at = released_at
        self.parent_publisher_id = parent_publisher_id
        self.publisher_mongo_id = publisher_mongo_id
        self.parent_publisher_mongo_id = parent_publisher_mongo_id
        self.object_id = object_id
        self.updated_by_analyst_id = updated_by_analyst_id
        self.publisher_name = publisher_name
        self.detail = detail
        self.lang = lang
        self.publisher_url = publisher_url
        self.photo_url = photo_url
        self.priority = priority
        self.news_aggregator = news_aggregator
        self.is_restricted = is_restricted
        self.is_verified = is_verified
        self.is_virtual = is_virtual
        self.source = source
        self.survey = survey
        self.update_note = update_note
        self.priority_excluded = priority_excluded
        self.item_key = item_key
        self.top = top

    @staticmethod
    def from_json(payload):
        return Publisher(
            publisher_id=payload.get('publisher_id'),
            status=payload.get('status'),
            created_at=payload.get('created_at'),
            updated_at=payload.get('updated_at'),
            released_at=payload.get('released_at'),
            parent_publisher_id=payload.get('parent_publisher_id'),
            publisher_mongo_id=payload.get('publisher_mongo_id'),
            parent_publisher_mongo_id=payload.get('parent_publisher_mongo_id'),
            object_id=payload.get('object_id'),
            updated_by_analyst_id=payload.get('updated_by_analyst_id'),
            publisher_name=payload.get('publisher_name'),
            detail=payload.get('detail'),
            lang=payload.get('lang'),
            publisher_url=payload.get('publisher_url'),
            photo_url=payload.get('photo_url'),
            priority=payload.get('priority'),
            news_aggregator=payload.get('news_aggregator'),
            is_restricted=payload.get('is_restricted'),
            is_verified=payload.get('is_verified'),
            is_virtual=payload.get('is_virtual'),
            source=payload.get('source'),
            survey=payload.get('survey'),
            update_note=payload.get('update_note'),
            priority_excluded=payload.get('priority_excluded'),
            item_key=payload.get('item_key'),
            top=payload.get('top')
        ) if payload else None

    @property
    def id(self):
        return self.publisher_id

    @property
    def mongo_id(self):
        return self.publisher_mongo_id

    def to_json_string(self):
        return json.dumps(self.__dict__)


# class PublisherAuditInformation(object):
#     def __init__(self, publisher_id=None, created_at=None, updated_at=None,
#                  analyst_id=None, last_released_analyst_id=None,
#                  last_updated_analyst_id=None, released_at=None, source=None,
#                  survey=None, update_note=None, priority_exclusive=None,
#                  item_key=None, top=None):
#         self.publisher_id = publisher_id
#         self.created_at = created_at
#         self.updated_at = updated_at
#         self.analyst_id = analyst_id
#         self.last_released_analyst_id = last_released_analyst_id
#         self.last_updated_analyst_id = last_updated_analyst_id
#         self.released_at = released_at
#         self.source = source
#         self.survey = survey
#         self.update_note = update_note
#         self.priority_exclusive = priority_exclusive
#         self.item_key = item_key
#         self.top = top
#
#     @staticmethod
#     def from_json(payload):
#         return PublisherAuditInformation(
#             publisher_id=payload.get('publisher_id'),
#             created_at=payload.get('created_at'),
#             updated_at=payload.get('updated_at'),
#             analyst_id=payload.get('analyst_id'),
#             last_released_analyst_id=payload.get('last_released_analyst_id'),
#             last_updated_analyst_id=payload.get('last_updated_analyst_id'),
#             released_at=payload.get('released_at'),
#             source=payload.get('source'),
#             survey=payload.get('survey'),
#             update_note=payload.get('update_note'),
#             priority_exclusive=payload.get('priority_exclusive'),
#             item_key=payload.get('item_key'),
#             top=payload.get('top')
#         ) if payload else None
#
#     @property
#     def id(self):
#         return self.publisher_id
#
#     def to_json_string(self):
#         return json.dumps(self.__dict__)


class PublisherRanking(object):
    def __init__(self, publisher_id=None, created_at=None, updated_at=None,
                 alexa_rank_1m=None, alexa_rank_3m=None, company_level=None,
                 finance_dedication_level=None, klout_score=None,
                 moreover_rank=None, profile_quality_level=None):
        self.publisher_id = publisher_id
        self.created_at = created_at
        self.updated_at = updated_at
        self.alexa_rank_1m = alexa_rank_1m
        self.alexa_rank_3m = alexa_rank_3m
        self.company_level = company_level
        self.finance_dedication_level = finance_dedication_level
        self.klout_score = klout_score
        self.moreover_rank = moreover_rank
        self.profile_quality_level = profile_quality_level

    @staticmethod
    def from_json(payload):
        return PublisherRanking(
            publisher_id=payload.get('publisher_id'),
            created_at=payload.get('created_at'),
            updated_at=payload.get('updated_at'),
            alexa_rank_1m=payload.get('alexa_rank_1m'),
            alexa_rank_3m=payload.get('alexa_rank_3m'),
            company_level=payload.get('company_level'),
            finance_dedication_level=payload.get('finance_dedication_level'),
            klout_score=payload.get('klout_score'),
            moreover_rank=payload.get('moreover_rank'),
            profile_quality_level=payload.get('profile_quality_level')
        ) if payload else None

    @property
    def id(self):
        return self.publisher_id

    def to_json_string(self):
        return json.dumps(self.__dict__)
