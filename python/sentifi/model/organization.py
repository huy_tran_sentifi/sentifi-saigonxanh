# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Oct 20, 2014
# @author: trung

import ujson as json


class Organization(object):
    def __init__(self, organization_id=None, parent_organization_id=None,
                 status=None, created_at=None, updated_at=None,
                 organization_name=None, legal_name=None, short_name=None,
                 detail=None, address=None, city=None, country_code=None,
                 headquarter=None, chairman=None, ceo=None, cfo=None,
                 industry=None, founded=None, organization_size=None, isin=None,
                 ticker=None, valor=None, organization_url=None,
                 photo_url=None):
        self.organization_id = organization_id
        self.status = status
        self.created_at = created_at
        self.updated_at = updated_at
        self.parent_organization_id = parent_organization_id
        self.organization_name = organization_name
        self.legal_name = legal_name
        self.short_name = short_name
        self.detail = detail
        self.address = address
        self.city = city
        self.country_code = country_code
        self.headquarter = headquarter
        self.chairman = chairman
        self.ceo = ceo
        self.cfo = cfo
        self.industry = industry
        self.founded = founded
        self.organization_size = organization_size
        self.isin = isin
        self.ticker = ticker
        self.valor = valor
        self.organization_url = organization_url
        self.photo_url = photo_url

    @staticmethod
    def from_json(payload):
        return Organization(
            organization_id=payload.get('organization_id'),
            status=payload.get('status'),
            created_at=payload.get('created_at'),
            updated_at=payload.get('updated_at'),
            parent_organization_id=payload.get('parent_organization_id'),
            organization_name=payload.get('organization_name'),
            legal_name=payload.get('legal_name'),
            short_name=payload.get('short_name'),
            detail=payload.get('detail'),
            address=payload.get('address'),
            city=payload.get('city'),
            country_code=payload.get('country_code'),
            headquarter=payload.get('headquarter'),
            chairman=payload.get('chairman'),
            ceo=payload.get('ceo'),
            cfo=payload.get('cfo'),
            industry=payload.get('industry'),
            founded=payload.get('founded'),
            organization_size=payload.get('organization_size'),
            isin=payload.get('isin'),
            ticker=payload.get('ticker'),
            valor=payload.get('valor'),
            organization_url=payload.get('organization_url'),
            photo_url=payload.get('photo_url')
        ) if payload else None

    @property
    def id(self):
        return self.organization_id

    def to_json_string(self):
        return json.dumps(self.__dict__)
