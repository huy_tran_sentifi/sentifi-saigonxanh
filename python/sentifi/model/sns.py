# Copyright (C) 2015 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 12/08/2015
# @author: trung

from constant import ACCEPTED_SNS_NAME, STATUS_ACTIVE


class SnsAccount(object):
    """Class representing account from Social Network Services (SNS)
    """

    def __init__(self, sns_name, sns_id, status=None, created_at=None,
                 updated_at=None, human_updated_at=None, named_entity_id=None,
                 payload=None, geo_level=None, display_name=None,
                 avatar_url=None, country_code=None, sns_tag=None,
                 named_entity_id_excluded=None):
        if sns_name not in ACCEPTED_SNS_NAME:
            raise ValueError('sns_name %s not in %s' % (sns_name, ACCEPTED_SNS_NAME))

        self.sns_name = sns_name
        self.sns_id = sns_id
        self.status = status if status is not None else STATUS_ACTIVE
        self.created_at = created_at
        self.updated_at = updated_at
        self.human_updated_at = human_updated_at
        self.named_entity_id = named_entity_id
        self.payload = payload
        self.geo_level = geo_level
        self.display_name = display_name
        self.avatar_url = avatar_url
        self.country_code = country_code
        self.sns_tag = sns_tag
        self.named_entity_id_excluded = named_entity_id_excluded

    @staticmethod
    def from_json(payload):
        return SnsAccount(
            sns_name=payload.get('sns_name'),
            sns_id=payload.get('sns_id'),
            status=payload.get('status'),
            created_at=payload.get('created_at'),
            updated_at=payload.get('updated_at'),
            human_updated_at=payload.get('human_updated_at'),
            named_entity_id=payload.get('named_entity_id'),
            payload=payload.get('payload'),
            geo_level=payload.get('geo_level'),
            display_name=payload.get('display_name'),
            avatar_url=payload.get('avatar_url'),
            country_code=payload.get('country_code'),
            sns_tag=payload.get('sns_tag'),
            named_entity_id_excluded=payload.get('named_entity_id_excluded')
        ) if payload else None


class SnsCategory(object):
    """Class representing link between SNS Account and Category
    """

    def __init__(self, sns_name, sns_id, category_id, created_at=None,
                 human_updated_at=None):
        if not (sns_name and  sns_id and category_id):
            raise ValueError('sns_name, sns_id, category_id can not be empty')

        self.sns_name = sns_name
        self.sns_id = sns_id
        self.category_id = category_id
        self.created_at = created_at
        self.human_updated_at = human_updated_at

    @staticmethod
    def from_json(payload):
        return SnsCategory(
            sns_name=payload.get('sns_name'),
            sns_id=payload.get('sns_id'),
            category_id=payload.get('category_id'),
            created_at=payload.get('created_at'),
            human_updated_at=payload.get('human_updated_at')
        ) if payload else None


class SnsRole(object):
    """Class representing link between SNS Account and Role
    """

    def __init__(self, sns_name, sns_id, role_id, created_at=None,
                 human_updated_at=None):
        if not (sns_name and  sns_id and role_id):
            raise ValueError('sns_name, sns_id, role_id can not be empty')

        self.sns_name = sns_name
        self.sns_id = sns_id
        self.role_id = role_id
        self.created_at = created_at
        self.human_updated_at = human_updated_at

    @staticmethod
    def from_json(payload):
        return SnsRole(
            sns_name=payload.get('sns_name'),
            sns_id=payload.get('sns_id'),
            role_id=payload.get('role_id'),
            created_at=payload.get('created_at'),
            human_updated_at=payload.get('human_updated_at')
        ) if payload else None
