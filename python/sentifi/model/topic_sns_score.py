# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 25/03/2015
# @author: trung

from datetime import datetime

from constant import ACCEPTED_SNS_NAME


class TopicSnsScore(object):
    """Class representing scores for all pairs of Topic - SNS at a specific time
    and of a same specific metric
    """

    def __init__(self, scored_at, metric_id, sns_name, triplets_list):
        """
        :type scored_at: datetime.datetime
        :type metric_id: Integer
        :type sns_name: String. Valid values are one of %s
        :type triplets_list: List/Tuple of triplets (topic_id, sns_id, score)
        """ % ACCEPTED_SNS_NAME
        if isinstance(scored_at, datetime):
            self.scored_at = scored_at
        else:
            raise TypeError('scored_at must be a datetime.datetime object')

        self.metric_id = int(metric_id)

        if sns_name in ACCEPTED_SNS_NAME:
            self.sns_name = sns_name
        else:
            raise ValueError('sns_name %s is not in the accepted list %s' % (self.sns_name, ACCEPTED_SNS_NAME))

        if isinstance(triplets_list, (list, tuple)):
            self.triplets_list = triplets_list
        else:
            raise TypeError('triplets_list must be a list/tuple object')
