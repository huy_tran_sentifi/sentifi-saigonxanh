# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Jun 10, 2014
# @author: trung

from bson import ObjectId

import logging
import pymongo

from constant import DEFAULT_BATCH_SIZE
from helpers.utils import bson2json
from model import Dictionary


logger = logging.getLogger(__name__)


class ModelDao(object):
    def __init__(self, client, db_name, collection_name, model):
        self._client = client
        self._db = self._client[db_name]
        self._coll = self._db[collection_name]
        self._model = model
        self.find = self._coll.find
        self.find_one = self._coll.find_one
        self.insert = self._coll.insert
        self.remove = self._coll.remove
        self.update = self._coll.update

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def _preprocess_row(self, row):
        bson2json(row)

    def _materialize_model_object_from_cursor(self, cursor):
        for row in cursor:
            self._preprocess_row(row)
            yield self._model.from_json(row)

    def close(self):
        self._client.close()

    def find_by_id(self, id_):
        row = self._coll.find_one(spec_or_id={'_id': ObjectId(id_)})
        if row:
            self._preprocess_row(row)
        return self._model.from_json(row)

    def find_by_time_range(self, start_time, end_time):
        '''Find documents that was assigned an ObjectId having timestamp
        within range [``start_time``, ``end_time``[
        '''
        logger.info('Querying %s for document created from %s to %s...' % (
            self._coll,
            start_time.strftime('%Y%m%d-%H%M%S'),
            end_time.strftime('%Y%m%d-%H%M%S')
        ))

        cursor = self._coll.find(
            spec={
                '_id': {
                    '$gte': ObjectId.from_datetime(start_time),
                    '$lt': ObjectId.from_datetime(end_time),
                }
            },
            sort=[('_id', pymongo.ASCENDING)]
        )
        if cursor.count():
            return self._materialize_model_object_from_cursor(cursor)
        return []

    def find_recent(self, time_field, a_timestamp=None, fields=None,
                    limit=DEFAULT_BATCH_SIZE):
        if a_timestamp:
            cursor = self._coll.find(
                spec={time_field: {'$gte': a_timestamp}},
                sort=[(time_field, pymongo.ASCENDING)],
                fields=fields,
                limit=limit
            )
        else:
            cursor = self._coll.find(
                sort=[(time_field, pymongo.ASCENDING)],
                fields=fields,
                limit=limit
            )
        if cursor.count():
            return self._materialize_model_object_from_cursor(cursor)
        return []

    def insert(self, an_object):
        raise NotImplementedError


class DictionaryDao(ModelDao):
    def __init__(self, client, db_name, collection_name):
        ModelDao.__init__(self, client, db_name, collection_name, Dictionary)
