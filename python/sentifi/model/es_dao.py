# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Jun 14, 2014
# @author: trung

from elasticsearch.helpers import bulk


class ModelDao(object):

    def __init__(self, connection, chunk_size=1000):
        self._es = connection
        self._chunk_size = chunk_size
        self.__init_action_stack()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.commit()

    def __init_action_stack(self):
        self._actions = []
        self._action_count = 0

    def commit(self):
        if self._actions:
            bulk(self._es, self._actions)
            self.__init_action_stack()

    def _index(self, index, doc_type, obj_body, obj_id=None, synchronously=True):
        if synchronously:
            self._es.index(
                index=index,
                doc_type=doc_type,
                body=obj_body,
                id=obj_id
            )
        else:
            self._actions.append({
                '_index': index,
                '_type': doc_type,
                '_id': obj_id,
                '_source': obj_body
            })
            self._action_count += 1
            if self._action_count >= self._chunk_size:
                self.commit()

    def index(self, index, doc_type, obj_body, obj_id=None, synchronously=True):
        return self._index(index, doc_type, obj_body, obj_id, synchronously)

    def find(self, object_id):
        raise NotImplementedError

    def search(self, query):
        raise NotImplementedError

class DictionaryDao(ModelDao): pass

class MessageDao(ModelDao):

    def __init__(self, connection, index, doc_type, chunk_size=1000):
        ModelDao.__init__(self, connection, chunk_size)
        self._i = index
        self._type = doc_type

    def index(self, message, synchronously=True):
        return self._index(self._i, self._type, message.to_json_string(), message.id, synchronously)
