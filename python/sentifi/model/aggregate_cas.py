__author__ = 'semantic'

from datetime import datetime
from dateutil import parser


class Aggregation(object):

    """A metric for a social network account in the Publisher Analytics DB.

    Attributes:
        stat_date (timestamp): The inserting day (week)
        metric_id (int): The id of the metric
        stat_name (text): the name of the aggregation (min, max, avg)
        stat_value (double): the value of the aggregation
    """

    def __init__(self,
                 stat_date=None,
                 metric_id=None,
                 stat_name=None,
                 stat_value=None):
        self._stat_name = stat_name
        self._metric_id = metric_id

        if isinstance(stat_date, datetime):
            self._stat_date = stat_date
        elif isinstance(stat_date, (str, unicode)):
            try:
                self._stat_date = parser.parse(stat_date)
            except Exception, e:
                raise e

        if isinstance(stat_value, (int, long, float, complex)):
            self._stat_value = stat_value
        else:
            raise TypeError("%s is not a number!" % str(stat_value))

    def __str__(self):
        return """Metric [%s] of [%s] has value [%s] measured on [%s]""" % \
            (str(self._metric_id), str(self._stat_name),
             str(self._stat_value), str(self._stat_date))

    def __repr__(self):
        return """Metric [%s] of [%s] has value [%s] measured on [%s]""" % \
            (str(self._metric_id), str(self._stat_name),
              str(self._stat_value), str(self._stat_date))

    @property
    def stat_name(self):
        """Gets or sets the name of the social network
        """
        return self._stat_name

    @stat_name.setter
    def stat_name(self, value):
        self._stat_name = value

    @property
    def metric_id(self):
        """Gets or sets the id of the metric
        """
        return self._metric_id

    @metric_id.setter
    def metric_id(self, value):
        self._metric_id = value

    @property
    def date(self):
        """Gets or sets the count date (datetime) of this value in this metric
        """
        return self._stat_date

    @date.setter
    def stat_date(self, value):
        if isinstance(value, datetime):
            self._count_date = value
        elif isinstance(value, (str, unicode)):
            try:
                self._stat_date = parser.parse(value)
            except Exception, e:
                raise e

    @property
    def value(self):
        """Gets or sets the value in this metric
        """
        return self._stat_value

    @value.setter
    def stat_value(self, value):
        if isinstance(value, (int, long, float, complex)):
            self._stat_value = value
        else:
            raise TypeError("%s is not a number!" % str(value))

    @staticmethod
    def create_query_info(stat_date, metric_id, stat_name):
        return Aggregation(stat_date, metric_id, datetime.now(), 0)