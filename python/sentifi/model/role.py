# Copyright (C) 2015 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 02/06/2015
# @author: trung

import ujson as json


ROOT_ROLE_ID = 0


class Role(object):
    """Class representing Role's definition for Publisher
    """

    def __init__(self, role_key, role_name, role_id=None, created_at=None,
                 updated_at=None, detail=None, role_id_path=None,
                 is_hidden=None):
        self.role_key = role_key
        self.role_name = role_name
        self.role_id = role_id
        self.created_at = created_at
        self.updated_at = updated_at
        self.detail = detail
        self.role_id_path = role_id_path
        self.is_hidden = is_hidden

    @staticmethod
    def from_json(payload):
        return Role(
            role_key=payload.get('role_key'),
            role_name=payload.get('role_name'),
            role_id=payload.get('role_id'),
            created_at=payload.get('created_at'),
            updated_at=payload.get('updated_at'),
            detail=payload.get('detail'),
            role_id_path=payload.get('role_id_path'),
            is_hidden=payload.get('is_hidden')
        ) if payload else None

    def to_json_string(self):
        return json.dumps(self)


class RoleNetwork(object):
    """Class representing Role's parent-child relationship. If Role are vertices
    so RoleNetwork are edges.
    """

    def __init__(self, role_id, parent_role_id=None, created_at=None):
        self.role_id = role_id
        self.parent_role_id = parent_role_id
        self.created_at = created_at

    def to_json_string(self):
        return json.dumps(self)
