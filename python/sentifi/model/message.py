# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Jun 9, 2014
# @author: trung

from bson import ObjectId

import ujson as json


class Message(object):

    def __init__(self, message_id=None, message_mongo_id=None, channel=None,
            source=None, publisher_id=None, publisher=None, lang=None,
            title=None, content=None, published_at=None, uri=None,
            is_active=None, marked_as_favorite=None, trunk=None):
        self.message_id = message_id
        self.message_mongo_id = message_mongo_id
        self.channel = channel
        self.source = source
        self.publisher_id = publisher_id
        self.publisher_mongo_id = publisher.get('_id') if publisher else None
        self.publisher = publisher
        self.lang = lang
        self.title = title
        self.content = content
        self.published_at = published_at
        self.uri = uri
        self.is_active = is_active
        self.marked_as_favorite = marked_as_favorite

        # TODO: drop and leave this attribute to Postgres to handle when we stop
        # tailing relevant_document from MongoDB
        self.created_at = None
        if message_mongo_id:
            self.created_at = ObjectId(message_mongo_id).generation_time

        self.trunk = trunk

    @staticmethod
    def from_json(payload, json_map=None):
        p = json_map.map(payload) if json_map else payload
        return Message(
            message_id=p.get('message_id'),
            message_mongo_id=p.get('message_mongo_id'),
            channel=p.get('channel'),
            source=p.get('source'),
            publisher_id=p.get('publisher_id'),
            publisher=p.get('publisher'),
            lang=p.get('lang'),
            title=p.get('title'),
            content=p.get('content'),
            published_at=p.get('published_at'),
            uri=p.get('uri'),
            is_active=p.get('is_active'),
            marked_as_favorite=p.get('marked_as_favorite'),
            trunk=p.get('trunk')
        ) if p else None

    @property
    def id(self):
        return self.message_id

    @property
    def mongo_id(self):
        return self.message_mongo_id

    def to_json_string(self):
        return json.dumps(self.__dict__)
