# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Oct 16, 2014
# @author: trung

from logging import getLogger

from model import Publisher, PublisherRanking
from model.postgres_dao.base import ModelDao


logger = getLogger('model.postgres_dao.publisher')


class PublisherDao(ModelDao):
    def __init__(self, connection, publisher_table_name):
        ModelDao.__init__(self, connection, publisher_table_name, Publisher)

    # Override
    def is_existed(self, publisher_id):
        with self._conn.cursor() as cursor:
            query = '''
SELECT true FROM {} WHERE publisher_id = %s;'''.format(self._table)
            query = cursor.mogrify(query, [publisher_id])
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            result = cursor.fetchone()
        return result[0] is True if result else False

    # Override
    def insert(self, publisher):
        self._validate_model(publisher)

        with self._conn.cursor() as cursor:
            query = '''
INSERT INTO {} (
    publisher_id,
    status,
    created_at,
    updated_at,
    released_at,
    parent_publisher_id,
    publisher_mongo_id,
    parent_publisher_mongo_id,
    object_id,
    updated_by_analyst_id,
    publisher_name,
    detail,
    lang,
    publisher_url,
    photo_url,
    priority,
    news_aggregator,
    is_restricted,
    is_verified,
    is_virtual,
    source,
    survey,
    update_note,
    priority_excluded,
    item_key,
    top
) VALUES (
    COALESCE(%(publisher_id)s, uuid_generate_v1()),
    %(status)s,
    COALESCE(%(created_at)s, now()),
    COALESCE(%(updated_at)s, now()),
    %(released_at)s,
    COALESCE(
        %(parent_publisher_id)s,
        (
            SELECT publisher_id
                FROM {}
                WHERE publisher_mongo_id = %(parent_publisher_mongo_id)s
        )
    ),
    %(publisher_mongo_id)s,
    %(parent_publisher_mongo_id)s,
    %(object_id)s,
    %(updated_by_analyst_id)s,
    %(publisher_name)s,
    %(detail)s,
    %(lang)s,
    %(publisher_url)s,
    %(photo_url)s,
    %(priority)s,
    %(news_aggregator)s,
    %(is_restricted)s,
    %(is_verified)s,
    %(is_virtual)s,
    %(source)s,
    %(survey)s,
    %(update_note)s,
    %(priority_excluded)s,
    %(item_key)s,
    %(top)s
) RETURNING publisher_id;'''.format(self._table, self._table)
            query = cursor.mogrify(query, publisher.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result

    # Override
    def update(self, publisher):
        self._validate_model(publisher)

        with self._conn.cursor() as cursor:
            query = '''
UPDATE {} SET
    status = %(status)s,
    updated_at = COALESCE(%(updated_at)s, now()),
    released_at = %(released_at)s,
    parent_publisher_id = COALESCE(
        %(parent_publisher_id)s,
        (
            SELECT publisher_id
                FROM {}
                WHERE publisher_mongo_id = %(parent_publisher_mongo_id)s
        )
    ),
    publisher_mongo_id = %(publisher_mongo_id)s,
    parent_publisher_mongo_id = %(parent_publisher_mongo_id)s,
    object_id = %(object_id)s,
    updated_by_analyst_id = %(updated_by_analyst_id)s,
    publisher_name = %(publisher_name)s,
    detail = %(detail)s,
    lang = %(lang)s,
    publisher_url = %(publisher_url)s,
    photo_url = %(photo_url)s,
    priority = %(priority)s,
    news_aggregator = %(news_aggregator)s,
    is_restricted = %(is_restricted)s,
    is_verified = %(is_verified)s,
    is_virtual = %(is_virtual)s,
    source = %(source)s,
    survey = %(survey)s,
    update_note = %(update_note)s,
    priority_excluded = %(priority_excluded)s,
    item_key = %(item_key)s,
    top = %(top)s
  WHERE publisher_id = %(publisher_id)s
  RETURNING publisher_id;'''.format(self._table, self._table)
            query = cursor.mogrify(query, publisher.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result

    # Override
    def upsert(self, publisher):
        self._validate_model(publisher)

        with self._conn.cursor() as cursor:
            update_sql = '''
UPDATE {} SET
    status = %(status)s,
    updated_at = COALESCE(%(updated_at)s, now()),
    released_at = %(released_at)s,
    parent_publisher_id = COALESCE(
        %(parent_publisher_id)s,
        (
            SELECT publisher_id
                FROM {}
                WHERE publisher_mongo_id = %(parent_publisher_mongo_id)s
        )
    ),
    publisher_mongo_id = %(publisher_mongo_id)s,
    parent_publisher_mongo_id = %(parent_publisher_mongo_id)s,
    object_id = %(object_id)s,
    updated_by_analyst_id = %(updated_by_analyst_id)s,
    publisher_name = %(publisher_name)s,
    detail = %(detail)s,
    lang = %(lang)s,
    publisher_url = %(publisher_url)s,
    photo_url = %(photo_url)s,
    priority = %(priority)s,
    news_aggregator = %(news_aggregator)s,
    is_restricted = %(is_restricted)s,
    is_verified = %(is_verified)s,
    is_virtual = %(is_virtual)s,
    source = %(source)s,
    survey = %(survey)s,
    update_note = %(update_note)s,
    priority_excluded = %(priority_excluded)s,
    item_key = %(item_key)s,
    top = %(top)s
  WHERE publisher_id = %(publisher_id)s
  RETURNING publisher_id;'''.format(self._table, self._table)
            update_sql = cursor.mogrify(update_sql, publisher.__dict__)
            insert_sql = '''
INSERT INTO {} (
    publisher_id,
    status,
    created_at,
    updated_at,
    released_at,
    parent_publisher_id,
    publisher_mongo_id,
    parent_publisher_mongo_id,
    object_id,
    updated_by_analyst_id,
    publisher_name,
    detail,
    lang,
    publisher_url,
    photo_url,
    priority,
    news_aggregator,
    is_restricted,
    is_verified,
    is_virtual,
    source,
    survey,
    update_note,
    priority_excluded,
    item_key,
    top
) VALUES (
    COALESCE(%(publisher_id)s, uuid_generate_v1()),
    %(status)s,
    COALESCE(%(created_at)s, now()),
    COALESCE(%(updated_at)s, now()),
    %(released_at)s,
    COALESCE(
        %(parent_publisher_id)s,
        (
            SELECT publisher_id
                FROM {}
                WHERE publisher_mongo_id = %(parent_publisher_mongo_id)s
        )
    ),
    %(publisher_mongo_id)s,
    %(parent_publisher_mongo_id)s,
    %(object_id)s,
    %(updated_by_analyst_id)s,
    %(publisher_name)s,
    %(detail)s,
    %(lang)s,
    %(publisher_url)s,
    %(photo_url)s,
    %(priority)s,
    %(news_aggregator)s,
    %(is_restricted)s,
    %(is_verified)s,
    %(is_virtual)s,
    %(source)s,
    %(survey)s,
    %(update_note)s,
    %(priority_excluded)s,
    %(item_key)s,
    %(top)s
) RETURNING publisher_id;'''.format(self._table, self._table)
            insert_sql = cursor.mogrify(insert_sql, publisher.__dict__)
            query = '''
SELECT upsert(%s, %s);'''
            query = cursor.mogrify(query, [update_sql, insert_sql])
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result


class PublisherAuditHistoryDao(ModelDao):
    def __init__(self, connection, publisher_audit_history_table_name):
        ModelDao.__init__(self, connection, publisher_audit_history_table_name,
                          Publisher)

    # Override
    def insert(self, publisher):
        self._validate_model(publisher)

        with self._conn.cursor() as cursor:
            query = '''
INSERT INTO {} (
    publisher_id,
    status,
    updated_at,
    released_at,
    parent_publisher_id,
    publisher_mongo_id,
    parent_publisher_mongo_id,
    object_id,
    updated_by_analyst_id,
    publisher_name,
    detail,
    lang,
    publisher_url,
    photo_url,
    priority,
    news_aggregator,
    is_restricted,
    is_verified,
    is_virtual,
    source,
    survey,
    update_note,
    priority_excluded,
    item_key,
    top
) VALUES (
    COALESCE(%(publisher_id)s, uuid_generate_v1()),
    %(status)s,
    COALESCE(%(updated_at)s, now()),
    %(released_at)s,
    COALESCE(
        %(parent_publisher_id)s,
        (
            SELECT publisher_id
                FROM {}
                WHERE publisher_mongo_id = %(parent_publisher_mongo_id)s
                LIMIT 1
        )
    ),
    %(publisher_mongo_id)s,
    %(parent_publisher_mongo_id)s,
    %(object_id)s,
    %(updated_by_analyst_id)s,
    %(publisher_name)s,
    %(detail)s,
    %(lang)s,
    %(publisher_url)s,
    %(photo_url)s,
    %(priority)s,
    %(news_aggregator)s,
    %(is_restricted)s,
    %(is_verified)s,
    %(is_virtual)s,
    %(source)s,
    %(survey)s,
    %(update_note)s,
    %(priority_excluded)s,
    %(item_key)s,
    %(top)s
) RETURNING publisher_id;'''.format(self._table, self._table)
            query = cursor.mogrify(query, publisher.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result


# class PublisherAuditInformationDao(ModelDao):
# def __init__(self, connection, publisher_table_name):
#         ModelDao.__init__(self, connection, publisher_table_name,
#                           PublisherAuditInformation)
#
#     # Override
#     def is_existed(self, publisher_id):
#         with self._conn.cursor() as cursor:
#             query = '''
# SELECT true FROM {} WHERE publisher_id = %s;'''.format(self._table)
#             query = cursor.mogrify(query, [publisher_id])
#             logger.debug('Executing %s' % query)
#             cursor.execute(query)
#             result = cursor.fetchone()
#         return result[0] is True if result else False
#
#     # Override
#     def insert(self, publisher_audit_information):
#         self._validate_model(publisher_audit_information)
#
#         with self._conn.cursor() as cursor:
#             query = '''
# INSERT INTO {} (
#     publisher_id,
#     created_at,
#     updated_at,
#     analyst_id,
#     last_released_analyst_id,
#     last_updated_analyst_id,
#     released_at,
#     source,
#     survey,
#     update_note,
#     priority_excluded,
#     item_key,
#     top
# ) VALUES (
#     %(publisher_id)s,
#     COALESCE(%(created_at)s, now()),
#     COALESCE(%(updated_at)s, now()),
#     %(analyst_id)s,
#     %(last_released_analyst_id)s,
#     %(last_updated_analyst_id)s,
#     %(released_at)s,
#     %(source)s,
#     %(survey)s,
#     %(update_note)s,
#     %(priority_excluded)s,
#     %(item_key)s,
#     %(top)s
# ) RETURNING publisher_id;'''.format(self._table)
#             query = cursor.mogrify(query, publisher_audit_information.__dict__)
#             logger.debug('Executing %s' % query)
#             cursor.execute(query)
#             self._conn.commit()
#             result = cursor.fetchone()
#         return result
#
#     # Override
#     def update(self, publisher_audit_information):
#         self._validate_model(publisher_audit_information)
#
#         with self._conn.cursor() as cursor:
#             query = '''
# UPDATE {} SET
#     updated_at = COALESCE(%(updated_at)s, now()),
#     analyst_id = %(analyst_id)s,
#     last_released_analyst_id = %(last_released_analyst_id)s,
#     last_updated_analyst_id = %(last_updated_analyst_id)s,
#     released_at = %(released_at)s,
#     source = %(source)s,
#     survey = %(survey)s,
#     update_note = %(update_note)s,
#     priority_excluded = %(priority_excluded)s,
#     item_key = %(item_key)s,
#     top = %(top)s
#   WHERE publisher_id = %(publisher_id)s
#   RETURNING publisher_id;'''.format(self._table)
#             query = cursor.mogrify(query, publisher_audit_information.__dict__)
#             logger.debug('Executing %s' % query)
#             cursor.execute(query)
#             self._conn.commit()
#             result = cursor.fetchone()
#         return result
#
#     # Override
#     def upsert(self, publisher):
#         self._validate_model(publisher)
#
#         with self._conn.cursor() as cursor:
#             update_sql = '''
# UPDATE {} SET
#     updated_at = COALESCE(%(updated_at)s, now()),
#     analyst_id = %(analyst_id)s,
#     last_released_analyst_id = %(last_released_analyst_id)s,
#     last_updated_analyst_id = %(last_updated_analyst_id)s,
#     released_at = %(released_at)s,
#     source = %(source)s,
#     survey = %(survey)s,
#     update_note = %(update_note)s,
#     priority_excluded = %(priority_excluded)s,
#     item_key = %(item_key)s,
#     top = %(top)s
#   WHERE publisher_id = %(publisher_id)s
#   RETURNING publisher_id;'''.format(self._table)
#             update_sql = cursor.mogrify(update_sql, publisher.__dict__)
#             insert_sql = '''
# INSERT INTO {} (
#     publisher_id,
#     created_at,
#     updated_at,
#     analyst_id,
#     last_released_analyst_id,
#     last_updated_analyst_id,
#     released_at,
#     source,
#     survey,
#     update_note,
#     priority_excluded,
#     item_key,
#     top
# ) VALUES (
#     %(publisher_id)s,
#     COALESCE(%(created_at)s, now()),
#     COALESCE(%(updated_at)s, now()),
#     %(analyst_id)s,
#     %(last_released_analyst_id)s,
#     %(last_updated_analyst_id)s,
#     %(released_at)s,
#     %(source)s,
#     %(survey)s,
#     %(update_note)s,
#     %(priority_excluded)s,
#     %(item_key)s,
#     %(top)s
# ) RETURNING publisher_id;'''.format(self._table)
#             insert_sql = cursor.mogrify(insert_sql, publisher.__dict__)
#             query = '''
# SELECT upsert(%s, %s);'''
#             query = cursor.mogrify(query, (update_sql, insert_sql))
#             logger.debug('Executing %s' % query)
#             cursor.execute(query)
#             self._conn.commit()
#             result = cursor.fetchone()
#         return result


class PublisherRankingDao(ModelDao):
    def __init__(self, connection, publisher_ranking_table_name):
        ModelDao.__init__(self, connection, publisher_ranking_table_name,
                          PublisherRanking)

    # Override
    def is_existed(self, publisher_id):
        with self._conn.cursor() as cursor:
            query = '''
SELECT true FROM {} WHERE publisher_id = %s;'''.format(self._table)
            query = cursor.mogrify(query, [publisher_id])
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            result = cursor.fetchone()
        return result[0] is True if result else False

    # Override
    def insert(self, publisher_ranking):
        self._validate_model(publisher_ranking)

        with self._conn.cursor() as cursor:
            query = '''
INSERT INTO {} (
    publisher_id,
    created_at,
    updated_at,
    alexa_rank_1m,
    alexa_rank_3m,
    company_level,
    finance_dedication_level,
    klout_score,
    moreover_rank,
    profile_quality_level
) VALUES (
    %(publisher_id)s,
    COALESCE(%(created_at)s, now()),
    COALESCE(%(updated_at)s, now()),
    %(alexa_rank_1m)s,
    %(alexa_rank_3m)s,
    %(company_level)s,
    %(finance_dedication_level)s,
    %(klout_score)s,
    %(moreover_rank)s,
    %(profile_quality_level)s
) RETURNING publisher_id;'''.format(self._table)
            query = cursor.mogrify(query, publisher_ranking.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result

    # Override
    def update(self, publisher_ranking):
        self._validate_model(publisher_ranking)

        with self._conn.cursor() as cursor:
            query = '''
UPDATE {} SET
    updated_at = COALESCE(%(updated_at)s, now()),
    alexa_rank_1m = %(alexa_rank_1m)s,
    alexa_rank_3m = %(alexa_rank_3m)s,
    company_level = %(company_level)s,
    finance_dedication_level = %(finance_dedication_level)s,
    klout_score = %(klout_score)s,
    moreover_rank = %(moreover_rank)s,
    profile_quality_level = %(profile_quality_level)s
  WHERE publisher_id = %(publisher_id)s
  RETURNING publisher_id;'''.format(self._table)
            query = cursor.mogrify(query, publisher_ranking.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result

    # Override
    def upsert(self, publisher_ranking):
        self._validate_model(publisher_ranking)

        with self._conn.cursor() as cursor:
            update_sql = '''
UPDATE {} SET
    updated_at = COALESCE(%(updated_at)s, now()),
    alexa_rank_1m = %(alexa_rank_1m)s,
    alexa_rank_3m = %(alexa_rank_3m)s,
    company_level = %(company_level)s,
    finance_dedication_level = %(finance_dedication_level)s,
    klout_score = %(klout_score)s,
    moreover_rank = %(moreover_rank)s,
    profile_quality_level = %(profile_quality_level)s
  WHERE publisher_id = %(publisher_id)s
  RETURNING publisher_id;'''.format(self._table)
            update_sql = cursor.mogrify(update_sql, publisher_ranking.__dict__)
            insert_sql = '''
INSERT INTO {} (
    publisher_id,
    created_at,
    updated_at,
    alexa_rank_1m,
    alexa_rank_3m,
    company_level,
    finance_dedication_level,
    klout_score,
    moreover_rank,
    profile_quality_level
) VALUES (
    %(publisher_id)s,
    COALESCE(%(created_at)s, now()),
    COALESCE(%(updated_at)s, now()),
    %(alexa_rank_1m)s,
    %(alexa_rank_3m)s,
    %(company_level)s,
    %(finance_dedication_level)s,
    %(klout_score)s,
    %(moreover_rank)s,
    %(profile_quality_level)s
) RETURNING publisher_id;'''.format(self._table)
            insert_sql = cursor.mogrify(insert_sql, publisher_ranking.__dict__)
            query = '''
SELECT upsert(%s, %s);'''
            query = cursor.mogrify(query, (update_sql, insert_sql))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result
