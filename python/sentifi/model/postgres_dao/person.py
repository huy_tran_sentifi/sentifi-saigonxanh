# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Oct 21, 2014
# @author: trung

from logging import getLogger

from constant import STATUS_INACTIVE
from model import Person
from model.postgres_dao.base import ModelDao


logger = getLogger('model.postgres_dao.person')


class PersonDao(ModelDao):

    def __init__(self, connection, person_table_name):
        ModelDao.__init__(self, connection, person_table_name, Person)

    def disable_by_id(self, organization_id):
        return self.set_field_by_id('organization_id', organization_id,
                                    'status', STATUS_INACTIVE)

    # Override
    def insert(self, person):
        self._validate_model(person)

        with self._conn.cursor() as cursor:
            query = '''
INSERT INTO {} (
    person_id,
    status,
    created_at,
    updated_at,
    title,
    person_name,
    first_name,
    middle_name,
    last_name,
    suffix,
    dob,
    detail,
    email,
    address,
    city,
    country_code,
    person_url,
    photo_url,
    guru
) VALUES (
    COALESCE(%(person_id)s, uuid_generate_v1()),
    %(status)s,
    COALESCE(%(created_at)s, now()),
    COALESCE(%(updated_at)s, now()),
    %(title)s,
    %(person_name)s,
    %(first_name)s,
    %(middle_name)s,
    %(last_name)s,
    %(suffix)s,
    %(dob)s,
    %(detail)s,
    %(email)s,
    %(address)s,
    %(city)s,
    %(country_code)s,
    %(person_url)s,
    %(photo_url)s,
    %(guru)s
) RETURNING person_id;'''.format(self._table)
            query = cursor.mogrify(query, person.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result

    # Override
    def update(self, person):
        self._validate_model(person)

        with self._conn.cursor() as cursor:
            query = '''
UPDATE {} SET
    status = %(status)s,
    updated_at = COALESCE(%(updated_at)s, now()),
    title = %(title)s,
    person_name = %(person_name)s,
    first_name = %(first_name)s,
    middle_name = %(middle_name)s,
    last_name = %(last_name)s,
    suffix = %(suffix)s,
    dob = %(dob)s,
    detail = %(detail)s,
    email = %(email)s,
    address = %(address)s,
    city = %(city)s,
    country_code = %(country_code)s,
    person_url = %(person_url)s,
    photo_url = %(photo_url)s,
    guru = %(guru)s
  WHERE person_id = %(person_id)s
  RETURNING person_id;'''.format(self._table)
            query = cursor.mogrify(query, person.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result

    # Override
    def upsert(self, person):
        self._validate_model(person)

        with self._conn.cursor() as cursor:
            update_sql = '''
UPDATE {} SET
    status = %(status)s,
    updated_at = COALESCE(%(updated_at)s, now()),
    title = %(title)s,
    person_name = %(person_name)s,
    first_name = %(first_name)s,
    middle_name = %(middle_name)s,
    last_name = %(last_name)s,
    suffix = %(suffix)s,
    dob = %(dob)s,
    detail = %(detail)s,
    email = %(email)s,
    address = %(address)s,
    city = %(city)s,
    country_code = %(country_code)s,
    person_url = %(person_url)s,
    photo_url = %(photo_url)s,
    guru = %(guru)s
  WHERE person_id = %(person_id)s
  RETURNING person_id;'''.format(self._table)
            update_sql = cursor.mogrify(update_sql, person.__dict__)
            insert_sql = '''
INSERT INTO {} (
    person_id,
    status,
    created_at,
    updated_at,
    title,
    person_name,
    first_name,
    middle_name,
    last_name,
    suffix,
    dob,
    detail,
    email,
    address,
    city,
    country_code,
    person_url,
    photo_url,
    guru
) VALUES (
    COALESCE(%(person_id)s, uuid_generate_v1()),
    %(status)s,
    COALESCE(%(created_at)s, now()),
    COALESCE(%(updated_at)s, now()),
    %(title)s,
    %(person_name)s,
    %(first_name)s,
    %(middle_name)s,
    %(last_name)s,
    %(suffix)s,
    %(dob)s,
    %(detail)s,
    %(email)s,
    %(address)s,
    %(city)s,
    %(country_code)s,
    %(person_url)s,
    %(photo_url)s,
    %(guru)s
) RETURNING person_id;'''.format(self._table)
            insert_sql = cursor.mogrify(insert_sql, person.__dict__)
            query = '''
SELECT upsert(%s, %s);'''
            query = cursor.mogrify(query, (update_sql, insert_sql))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result
