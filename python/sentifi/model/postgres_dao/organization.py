# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Oct 21, 2014
# @author: trung

from logging import getLogger

from constant import STATUS_INACTIVE
from model import Organization
from model.postgres_dao.base import ModelDao


logger = getLogger('model.postgres_dao.organization')


class OrganizationDao(ModelDao):
    def __init__(self, connection, organization_table_name):
        ModelDao.__init__(self, connection, organization_table_name,
                          Organization)

    def disable_by_id(self, organization_id):
        return self.set_field_by_id('organization_id', organization_id,
                                    'status', STATUS_INACTIVE)

    # Override
    def insert(self, organization):
        self._validate_model(organization)

        with self._conn.cursor() as cursor:
            query = '''
INSERT INTO {} (
    organization_id,
    status,
    created_at,
    updated_at,
    parent_organization_id,
    organization_name,
    legal_name,
    short_name,
    detail,
    address,
    city,
    country_code,
    headquarter,
    chairman,
    ceo,
    cfo,
    industry,
    founded,
    organization_size,
    isin,
    ticker,
    valor,
    organization_url,
    photo_url
) VALUES (
    COALESCE(%(organization_id)s, uuid_generate_v1()),
    %(status)s,
    COALESCE(%(created_at)s, now()),
    COALESCE(%(updated_at)s, now()),
    %(parent_organization_id)s,
    %(organization_name)s,
    %(legal_name)s,
    %(short_name)s,
    %(detail)s,
    %(address)s,
    %(city)s,
    %(country_code)s,
    %(headquarter)s,
    %(chairman)s,
    %(ceo)s,
    %(cfo)s,
    %(industry)s,
    %(founded)s,
    %(organization_size)s,
    %(isin)s,
    %(ticker)s,
    %(valor)s,
    %(organization_url)s,
    %(photo_url)s
) RETURNING organization_id;'''.format(self._table)
            query = cursor.mogrify(query, organization.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result

    # Override
    def update(self, organization):
        self._validate_model(organization)

        with self._conn.cursor() as cursor:
            query = '''
UPDATE {} SET
    status = %(status)s,
    updated_at = COALESCE(%(updated_at)s, now()),
    parent_organization_id = %(parent_organization_id)s,
    organization_name = %(organization_name)s,
    legal_name = %(legal_name)s,
    short_name = %(short_name)s,
    detail = %(detail)s,
    address = %(address)s,
    city = %(city)s,
    country_code = %(country_code)s,
    headquarter = %(headquarter)s,
    chairman = %(chairman)s,
    ceo = %(ceo)s,
    cfo = %(cfo)s,
    industry = %(industry)s,
    founded = %(founded)s,
    organization_size = %(organization_size)s,
    isin = %(isin)s,
    ticker = %(ticker)s,
    valor = %(valor)s,
    organization_url = %(organization_url)s,
    photo_url = %(photo_url)s
  WHERE organization_id = %(organization_id)s
  RETURNING organization_id;'''.format(self._table)
            query = cursor.mogrify(query, organization.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result

    # Override
    def upsert(self, organization):
        self._validate_model(organization)

        with self._conn.cursor() as cursor:
            update_sql = '''
UPDATE {} SET
    status = %(status)s,
    updated_at = COALESCE(%(updated_at)s, now()),
    parent_organization_id = %(parent_organization_id)s,
    organization_name = %(organization_name)s,
    legal_name = %(legal_name)s,
    short_name = %(short_name)s,
    detail = %(detail)s,
    address = %(address)s,
    city = %(city)s,
    country_code = %(country_code)s,
    headquarter = %(headquarter)s,
    chairman = %(chairman)s,
    ceo = %(ceo)s,
    cfo = %(cfo)s,
    industry = %(industry)s,
    founded = %(founded)s,
    organization_size = %(organization_size)s,
    isin = %(isin)s,
    ticker = %(ticker)s,
    valor = %(valor)s,
    organization_url = %(organization_url)s,
    photo_url = %(photo_url)s
  WHERE organization_id = %(organization_id)s
  RETURNING organization_id;'''.format(self._table)
            update_sql = cursor.mogrify(update_sql, organization.__dict__)
            insert_sql = '''
INSERT INTO {} (
    organization_id,
    status,
    created_at,
    updated_at,
    parent_organization_id,
    organization_name,
    legal_name,
    short_name,
    detail,
    address,
    city,
    country_code,
    headquarter,
    chairman,
    ceo,
    cfo,
    industry,
    founded,
    organization_size,
    isin,
    ticker,
    valor,
    organization_url,
    photo_url
) VALUES (
    COALESCE(%(organization_id)s, uuid_generate_v1()),
    %(status)s,
    COALESCE(%(created_at)s, now()),
    COALESCE(%(updated_at)s, now()),
    %(parent_organization_id)s,
    %(organization_name)s,
    %(legal_name)s,
    %(short_name)s,
    %(detail)s,
    %(address)s,
    %(city)s,
    %(country_code)s,
    %(headquarter)s,
    %(chairman)s,
    %(ceo)s,
    %(cfo)s,
    %(industry)s,
    %(founded)s,
    %(organization_size)s,
    %(isin)s,
    %(ticker)s,
    %(valor)s,
    %(organization_url)s,
    %(photo_url)s
) RETURNING organization_id;'''.format(self._table)
            insert_sql = cursor.mogrify(insert_sql, organization.__dict__)
            query = '''
SELECT upsert(%s, %s);'''
            query = cursor.mogrify(query, (update_sql, insert_sql))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result
