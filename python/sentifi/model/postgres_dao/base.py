# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Oct 16, 2014
# @author: trung

from logging import getLogger
from psycopg2.extensions import register_adapter
from psycopg2.extras import register_default_json, Json

from helpers.utils import oid_hex_to_datetime


logger = getLogger(__name__)


class ModelDao(object):

    def __init__(self, connection, table_name, model):
        self._conn = connection
        self._table = table_name
        self._model = model

    def _validate_model(self, var):
        if not isinstance(var, self._model):
            raise TypeError('Type %s is expected, not %s' % (
                self._model.__name__, type(var))
            )

    def close(self, commit=True):
        if commit:
            self._conn.commit()
        else:
            self._conn.rollback()
        self._conn.close()

    def find_by_id(self, *args, **kwargs):
        '''To override
        '''
        raise NotImplementedError

    def find_recent(self, a_timestamp):
        '''To override
        '''
        raise NotImplementedError

    def get_connection(self):
        return self._conn

    def get_latest_timestamp(self):
        return self.max('updated_at')

    def insert(self, an_object):
        '''To override
        '''
        raise NotImplementedError

    def is_existed(self, object_id):
        '''To override
        '''
        raise NotImplementedError

    def max(self, field_name):
        with self._conn.cursor() as cursor:
            query = '''
SELECT max(%s) FROM %s;''' % (field_name, self._table)
            logger.debug('Executing %s' % cursor.mogrify(query))
            cursor.execute(query)
            result = cursor.fetchone()
        return result[0] if result else None

    def rollback(self):
        self._conn.rollback()

    def set_field_by_id(self, id_field_name, object_id, field_name, field_value):
        with self._conn.cursor() as cursor:
            query = '''
UPDATE {}
  SET {} = %s
  WHERE {} = %s
  RETURNING {};'''.format(self._table, field_name, id_field_name, id_field_name)
            query = cursor.mogrify(query, (field_value, object_id))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result[0] if result else None

    def update(self, an_object):
        '''To override
        '''
        raise NotImplementedError

    def upsert(self, an_object):
        '''To override

        Use the store procedure upsert(update_sql, insert_sql) but be aware,
        do not abuse this functionality because it may fall (really really rare)
        in an infinite loop.
        http://www.depesz.com/2012/06/10/why-is-upsert-so-complicated/
        www.postgresql.org/docs/current/static/plpgsql-control-structures.html#PLPGSQL-UPSERT-EXAMPLEhttp://www.postgresql.org/docs/current/static/plpgsql-control-structures.html#PLPGSQL-UPSERT-EXAMPLE
        '''
        raise NotImplementedError

class DictionaryDao(ModelDao):
    '''Class that helps manipulating dict-like objects from PostgreSQL
    '''

    def __init__(self, connection, table_name):
        ModelDao.__init__(self, connection, table_name, dict)

    # Override
    def insert(self, a_dict, object_id=None, created_at=None, updated_at=None):
        self._validate_model(a_dict)

        with self._conn.cursor() as cursor:
            register_default_json(cursor)
            register_adapter(dict, Json)
            object_id = object_id or a_dict.get('_id')
            created_at = created_at or oid_hex_to_datetime(a_dict.get('_id'))
            query = '''
INSERT INTO {} (
    object_id,
    created_at,
    updated_at,
    object_payload
) VALUES (
    %s,
    COALESCE(%s, now()),
    COALESCE(%s, now()),
    %s
) RETURNING object_id;'''.format(self._table)
            query = cursor.mogrify(query, (object_id, created_at, updated_at, a_dict))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result[0] if result else None

    # Override
    def update(self, a_dict, object_id=None, created_at=None, updated_at=None):
        self._validate_model(a_dict)

        with self._conn.cursor() as cursor:
            register_default_json(cursor)
            register_adapter(dict, Json)
            object_id = object_id or a_dict.get('_id')
            query = '''
UPDATE {} SET
    updated_at = COALESCE(%s, now()),
    object_payload = %s
  WHERE object_id = %s
  RETURNING object_id;'''.format(self._table)
            query = cursor.mogrify(query, (updated_at, a_dict, object_id))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result[0] if result else None

    # Override
    def upsert(self, a_dict, object_id=None, created_at=None, updated_at=None):
        self._validate_model(a_dict)

        with self._conn.cursor() as cursor:
            register_default_json(cursor)
            register_adapter(dict, Json)
            object_id = object_id or a_dict.get('_id')
            created_at = created_at or oid_hex_to_datetime(a_dict.get('_id'))
            update_sql = '''
UPDATE {} SET
    updated_at = COALESCE(%s, now()),
    object_payload = %s
  WHERE object_id = %s
  RETURNING object_id;'''.format(self._table)
            update_sql = cursor.mogrify(update_sql, (updated_at, a_dict, object_id))
            insert_sql = '''
INSERT INTO {} (
    object_id,
    created_at,
    updated_at,
    object_payload
) VALUES (
    %s,
    COALESCE(%s, now()),
    COALESCE(%s, now()),
    %s
) RETURNING object_id;'''.format(self._table)
            insert_sql = cursor.mogrify(insert_sql, (object_id, created_at, updated_at, a_dict))
            query = '''
SELECT upsert(%s, %s);'''
            query = cursor.mogrify(query, (update_sql, insert_sql))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result
