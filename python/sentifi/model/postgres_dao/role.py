# Copyright (C) 2015 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 02/06/2015
# @author: trung

from logging import getLogger
from psycopg2.extras import RealDictCursor

from model.postgres_dao.base import ModelDao
from model.role import Role, RoleNetwork


logger = getLogger(__name__)


class RoleDao(ModelDao):

    def __init__(self, connection, table_name):
        ModelDao.__init__(self, connection, table_name, Role)
        self._role_cache_by_key = self.create_role_dict_by_key()
        self._update_cache_by_key = self.find_by_key

    def create_role_dict_by_key(self):
        role_dict = {}
        for role in self.findall():
            role_dict[role.role_key.lower()] = role
        return role_dict

    def findall(self):
        logger.info('Fetching all Role from database')
        with self._conn.cursor(cursor_factory=RealDictCursor) as cursor:
            query = 'SELECT * FROM {};'.format(self._table)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            for row in cursor:
                yield Role.from_json(row)

    def qfind_by_key(self, role_key):
        return self._role_cache_by_key.get(role_key)

    def find_by_key(self, role_key):
        with self._conn.cursor(cursor_factory=RealDictCursor) as cursor:
            query = 'SELECT * FROM {} WHERE lower(role_key) = %s;'.format(self._table)
            query = cursor.mogrify(query, (role_key.strip().lower(), ))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            role = Role.from_json(cursor.fetchone())
            if role:
                self._role_cache_by_key[role.role_key.lower()] = role

        return role

    # Override
    def insert(self, role):
        self._validate_model(role)

        with self._conn.cursor() as cursor:
            query = '''
INSERT INTO {} (
    role_key,
    role_name,
    role_id,
    created_at,
    updated_at,
    detail,
    role_id_path,
    is_hidden
) VALUES (
    %(role_key)s,
    %(role_name)s,
    COALESCE(%(role_id)s,  nextval('role_role_id_seq'::regclass)),
    COALESCE(%(created_at)s, now()),
    COALESCE(%(updated_at)s, now()),
    %(detail)s,
    %(role_id_path)s,
    %(is_hidden)s
) RETURNING role_id;'''.format(self._table)
            query = cursor.mogrify(query, role.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result[0] if result else None

    # Override
    def update(self, role):
        self._validate_model(role)

        if role.role_id:
            query = '''
UPDATE {} SET
    role_name = %(role_name)s,
    updated_at = COALESCE(%(updated_at)s, now()),
    detail = %(detail)s,
    role_id_path = COALESCE(%(role_id_path)s, get_role_id_path_by_role_id(role_id)),
    is_hidden = %(is_hidden)s
  WHERE role_id = %(role_id)s
  RETURNING role_id;'''.format(self._table)
        else:
            query = '''
UPDATE {} SET
    role_name = %(role_name)s,
    updated_at = COALESCE(%(updated_at)s, now()),
    detail = %(detail)s,
    role_id_path = COALESCE(%(role_id_path)s, get_role_id_path_by_role_id(role_id)),
    is_hidden = %(is_hidden)s
  WHERE lower(role_key) = lower(trim(both from %(role_key)s))
  RETURNING role_id;'''.format(self._table)

        with self._conn.cursor() as cursor:
            query = cursor.mogrify(query, role.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result[0] if result else None

    def is_existed_by_key(self, role_key):
        return self.qfind_by_key(role_key) is not None

    def update_cache(self):
        self._role_cache_by_key = self.create_role_dict_by_key()


class RoleNetworkDao(ModelDao):

    def __init__(self, connection, table_name):
        ModelDao.__init__(self, connection, table_name, RoleNetwork)

    def delete(self, role_network):
        self._validate_model(role_network)

        with self._conn.cursor() as cursor:
            query = '''
DELETE
  FROM {}
  WHERE role_id = %s
    AND parent_role_id = %s;'''.format(self._table)
            query = cursor.mogrify(query, (role_network.role_id, role_network.parent_role_id))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
        return cursor.rowcount

    def get_parent_role_id_list(self, role_id):
        with self._conn.cursor(cursor_factory=RealDictCursor) as cursor:
            query = '''
SELECT array_agg(parent_role_id) AS parent_role_id_list
  FROM {}
  WHERE role_id = %s;'''.format(self._table)
            query = cursor.mogrify(query, (role_id, ))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result['parent_role_id_list'] if result else None

    # Override
    def insert(self, role_network):
        self._validate_model(role_network)

        with self._conn.cursor() as cursor:
            query = '''
INSERT INTO {} (
    role_id,
    parent_role_id,
    created_at
) VALUES (
    %(role_id)s,
    %(parent_role_id)s,
    COALESCE(%(created_at)s, now()));'''.format(self._table)
            query = cursor.mogrify(query, role_network.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()

    def set_root(self, role_id):
        with self._conn.cursor() as cursor:
            logger.info('role_id %s is set to root => remove all links to direct parents if any' % role_id)
            query = '''
DELETE FROM {} WHERE role_id = %s;
INSERT INTO {} (role_id, parent_role_id) VALUES (%s, 0);'''.format(self._table, self._table)
            query = cursor.mogrify(query, (role_id, role_id))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
