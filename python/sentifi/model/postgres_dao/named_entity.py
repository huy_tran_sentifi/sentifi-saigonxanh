# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Oct 20, 2014
# @author: trung

from logging import getLogger

from model import NamedEntity, NamedEntitySnsLink
from model.postgres_dao.base import ModelDao


logger = getLogger('model.postgres_dao.named_entity')


class NamedEntityDao(ModelDao):
    def __init__(self, connection, named_entity_table_name):
        ModelDao.__init__(self, connection, named_entity_table_name,
                          NamedEntity)

    # Override
    def insert(self, named_entity):
        self._validate_model(named_entity)

        with self._conn.cursor() as cursor:
            query = '''
INSERT INTO {} (
    named_entity_id,
    status,
    created_at,
    updated_at,
    ready_at,
    old_id,
    object_id,
    category_id,
    updated_by_analyst_id,
    named_entity_name,
    named_entity_name_de,
    named_entity_type,
    detail,
    detail_de,
    named_entity_url,
    is_topic,
    audited_field,
    synonym,
    released,
    sentifi_o,
    config,
    blacklist,
    keyword,
    profile_cat,
    rss,
    businessweek_url,
    sector,
    company,
    issue_curation,
    ready,
    alert_enabled,
    slug,
    indice,
    legal_misspelled,
    sector_local,
    sector_bw,
    industry_subsector_local,
    industry_subsector_bw,
    stock_exchange,
    ticker_local,
    ticker_bloomberg,
    ir,
    se_mainstock,
    se_sourceurl,
    se_document,
    se_documenturl,
    cashtag,
    cashtag_local,
    market
) VALUES (
    COALESCE(%(named_entity_id)s, uuid_generate_v1()),
    %(status)s,
    COALESCE(%(created_at)s, now()),
    COALESCE(%(updated_at)s, now()),
    %(ready_at)s,
    %(old_id)s,
    %(object_id)s,
    %(category_id)s,
    %(updated_by_analyst_id)s,
    %(named_entity_name)s,
    %(named_entity_name_de)s,
    %(named_entity_type)s,
    %(detail)s,
    %(detail_de)s,
    %(named_entity_url)s,
    %(is_topic)s,
    %(audited_field)s,
    %(synonym)s,
    %(released)s,
    %(sentifi_o)s,
    %(config)s,
    %(blacklist)s,
    %(keyword)s,
    %(profile_cat)s,
    %(rss)s,
    %(businessweek_url)s,
    %(sector)s,
    %(company)s,
    %(issue_curation)s,
    %(ready)s,
    %(alert_enabled)s,
    %(slug)s,
    %(indice)s,
    %(legal_misspelled)s,
    %(sector_local)s,
    %(sector_bw)s,
    %(industry_subsector_local)s,
    %(industry_subsector_bw)s,
    %(stock_exchange)s,
    %(ticker_local)s,
    %(ticker_bloomberg)s,
    %(ir)s,
    %(se_mainstock)s,
    %(se_sourceurl)s,
    %(se_document)s,
    %(se_documenturl)s,
    %(cashtag)s,
    %(cashtag_local)s,
    %(market)s
) RETURNING named_entity_id;'''.format(self._table)
            query = cursor.mogrify(query, named_entity.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result

    # Override
    def update(self, named_entity):
        self._validate_model(named_entity)

        with self._conn.cursor() as cursor:
            query = '''
UPDATE {} SET
    status = %(status)s,
    updated_at = COALESCE(%(updated_at)s, now()),
    ready_at = %(ready_at)s,
    old_id = %(old_id)s,
    object_id = %(object_id)s,
    category_id = %(category_id)s,
    updated_by_analyst_id = %(updated_by_analyst_id)s,
    named_entity_name = %(named_entity_name)s,
    named_entity_name_de = %(named_entity_name_de)s,
    named_entity_type = %(named_entity_type)s,
    detail = %(detail)s,
    detail_de = %(detail_de)s,
    named_entity_url = %(named_entity_url)s,
    is_topic = %(is_topic)s,
    audited_field = %(audited_field)s,
    synonym = %(synonym)s,
    released = %(released)s,
    sentifi_o = %(sentifi_o)s,
    config = %(config)s,
    blacklist = %(blacklist)s,
    keyword = %(keyword)s,
    profile_cat = %(profile_cat)s,
    rss = %(rss)s,
    businessweek_url = %(businessweek_url)s,
    sector = %(sector)s,
    company = %(company)s,
    issue_curation = %(issue_curation)s,
    ready = %(ready)s,
    alert_enabled = %(alert_enabled)s,
    slug = %(slug)s,
    indice = %(indice)s,
    legal_misspelled = %(legal_misspelled)s,
    sector_local = %(sector_local)s,
    sector_bw = %(sector_bw)s,
    industry_subsector_local = %(industry_subsector_local)s,
    industry_subsector_bw = %(industry_subsector_bw)s,
    stock_exchange = %(stock_exchange)s,
    ticker_local = %(ticker_local)s,
    ticker_bloomberg = %(ticker_bloomberg)s,
    ir = %(ir)s,
    se_mainstock = %(se_mainstock)s,
    se_sourceurl = %(se_sourceurl)s,
    se_document = %(se_document)s,
    se_documenturl = %(se_documenturl)s,
    cashtag = %(cashtag)s,
    cashtag_local = %(cashtag_local)s,
    market = %(market)s
  WHERE named_entity_id = %(named_entity_id)s
  RETURNING named_entity_id;'''.format(self._table)
            query = cursor.mogrify(query, named_entity.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result

    # Override
    def upsert(self, named_entity):
        self._validate_model(named_entity)

        with self._conn.cursor() as cursor:
            update_sql = '''
UPDATE {} SET
    status = %(status)s,
    updated_at = COALESCE(%(updated_at)s, now()),
    ready_at = %(ready_at)s,
    old_id = %(old_id)s,
    object_id = %(object_id)s,
    category_id = %(category_id)s,
    updated_by_analyst_id = %(updated_by_analyst_id)s,
    named_entity_name = %(named_entity_name)s,
    named_entity_name_de = %(named_entity_name_de)s,
    named_entity_type = %(named_entity_type)s,
    detail = %(detail)s,
    detail_de = %(detail_de)s,
    named_entity_url = %(named_entity_url)s,
    is_topic = %(is_topic)s,
    audited_field = %(audited_field)s,
    synonym = %(synonym)s,
    released = %(released)s,
    sentifi_o = %(sentifi_o)s,
    config = %(config)s,
    blacklist = %(blacklist)s,
    keyword = %(keyword)s,
    profile_cat = %(profile_cat)s,
    rss = %(rss)s,
    businessweek_url = %(businessweek_url)s,
    sector = %(sector)s,
    company = %(company)s,
    issue_curation = %(issue_curation)s,
    ready = %(ready)s,
    alert_enabled = %(alert_enabled)s,
    slug = %(slug)s,
    indice = %(indice)s,
    legal_misspelled = %(legal_misspelled)s,
    sector_local = %(sector_local)s,
    sector_bw = %(sector_bw)s,
    industry_subsector_local = %(industry_subsector_local)s,
    industry_subsector_bw = %(industry_subsector_bw)s,
    stock_exchange = %(stock_exchange)s,
    ticker_local = %(ticker_local)s,
    ticker_bloomberg = %(ticker_bloomberg)s,
    ir = %(ir)s,
    se_mainstock = %(se_mainstock)s,
    se_sourceurl = %(se_sourceurl)s,
    se_document = %(se_document)s,
    se_documenturl = %(se_documenturl)s,
    cashtag = %(cashtag)s,
    cashtag_local = %(cashtag_local)s,
    market = %(market)s
  WHERE named_entity_id = %(named_entity_id)s
  RETURNING named_entity_id;'''.format(self._table)
            update_sql = cursor.mogrify(update_sql, named_entity.__dict__)
            insert_sql = '''
INSERT INTO {} (
    named_entity_id,
    status,
    created_at,
    updated_at,
    ready_at,
    old_id,
    object_id,
    category_id,
    updated_by_analyst_id,
    named_entity_name,
    named_entity_name_de,
    named_entity_type,
    detail,
    detail_de,
    named_entity_url,
    is_topic,
    audited_field,
    synonym,
    released,
    sentifi_o,
    config,
    blacklist,
    keyword,
    profile_cat,
    rss,
    businessweek_url,
    sector,
    company,
    issue_curation,
    ready,
    alert_enabled,
    slug,
    indice,
    legal_misspelled,
    sector_local,
    sector_bw,
    industry_subsector_local,
    industry_subsector_bw,
    stock_exchange,
    ticker_local,
    ticker_bloomberg,
    ir,
    se_mainstock,
    se_sourceurl,
    se_document,
    se_documenturl,
    cashtag,
    cashtag_local,
    market
) VALUES (
    COALESCE(%(named_entity_id)s, uuid_generate_v1()),
    %(status)s,
    COALESCE(%(created_at)s, now()),
    COALESCE(%(updated_at)s, now()),
    %(ready_at)s,
    %(old_id)s,
    %(object_id)s,
    %(category_id)s,
    %(updated_by_analyst_id)s,
    %(named_entity_name)s,
    %(named_entity_name_de)s,
    %(named_entity_type)s,
    %(detail)s,
    %(detail_de)s,
    %(named_entity_url)s,
    %(is_topic)s,
    %(audited_field)s,
    %(synonym)s,
    %(released)s,
    %(sentifi_o)s,
    %(config)s,
    %(blacklist)s,
    %(keyword)s,
    %(profile_cat)s,
    %(rss)s,
    %(businessweek_url)s,
    %(sector)s,
    %(company)s,
    %(issue_curation)s,
    %(ready)s,
    %(alert_enabled)s,
    %(slug)s,
    %(indice)s,
    %(legal_misspelled)s,
    %(sector_local)s,
    %(sector_bw)s,
    %(industry_subsector_local)s,
    %(industry_subsector_bw)s,
    %(stock_exchange)s,
    %(ticker_local)s,
    %(ticker_bloomberg)s,
    %(ir)s,
    %(se_mainstock)s,
    %(se_sourceurl)s,
    %(se_document)s,
    %(se_documenturl)s,
    %(cashtag)s,
    %(cashtag_local)s,
    %(market)s
) RETURNING named_entity_id;'''.format(self._table)
            insert_sql = cursor.mogrify(insert_sql, named_entity.__dict__)
            query = '''
SELECT upsert(%s, %s);'''
            query = cursor.mogrify(query, (update_sql, insert_sql))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result


class NamedEntityAuditHistoryDao(ModelDao):
    def __init__(self, connection, named_entity_table_name):
        ModelDao.__init__(self, connection, named_entity_table_name,
                          NamedEntity)

    # Override
    def insert(self, named_entity):
        self._validate_model(named_entity)

        with self._conn.cursor() as cursor:
            query = '''
INSERT INTO {} (
    named_entity_id,
    status,
    updated_at,
    ready_at,
    old_id,
    object_id,
    category_id,
    updated_by_analyst_id,
    named_entity_name,
    named_entity_name_de,
    named_entity_type,
    detail,
    detail_de,
    named_entity_url,
    is_topic,
    audited_field,
    synonym,
    released,
    sentifi_o,
    config,
    blacklist,
    keyword,
    profile_cat,
    rss,
    businessweek_url,
    sector,
    company,
    issue_curation,
    ready,
    alert_enabled,
    slug,
    indice,
    legal_misspelled,
    sector_local,
    sector_bw,
    industry_subsector_local,
    industry_subsector_bw,
    stock_exchange,
    ticker_local,
    ticker_bloomberg,
    ir,
    se_mainstock,
    se_sourceurl,
    se_document,
    se_documenturl,
    cashtag,
    cashtag_local,
    market
) VALUES (
    COALESCE(%(named_entity_id)s, uuid_generate_v1()),
    %(status)s,
    COALESCE(%(updated_at)s, now()),
    %(ready_at)s,
    %(old_id)s,
    %(object_id)s,
    %(category_id)s,
    %(updated_by_analyst_id)s,
    %(named_entity_name)s,
    %(named_entity_name_de)s,
    %(named_entity_type)s,
    %(detail)s,
    %(detail_de)s,
    %(named_entity_url)s,
    %(is_topic)s,
    %(audited_field)s,
    %(synonym)s,
    %(released)s,
    %(sentifi_o)s,
    %(config)s,
    %(blacklist)s,
    %(keyword)s,
    %(profile_cat)s,
    %(rss)s,
    %(businessweek_url)s,
    %(sector)s,
    %(company)s,
    %(issue_curation)s,
    %(ready)s,
    %(alert_enabled)s,
    %(slug)s,
    %(indice)s,
    %(legal_misspelled)s,
    %(sector_local)s,
    %(sector_bw)s,
    %(industry_subsector_local)s,
    %(industry_subsector_bw)s,
    %(stock_exchange)s,
    %(ticker_local)s,
    %(ticker_bloomberg)s,
    %(ir)s,
    %(se_mainstock)s,
    %(se_sourceurl)s,
    %(se_document)s,
    %(se_documenturl)s,
    %(cashtag)s,
    %(cashtag_local)s,
    %(market)s
) RETURNING named_entity_id;'''.format(self._table)
            query = cursor.mogrify(query, named_entity.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result


# class NamedEntityAuditInformationDao(ModelDao):
#     def __init__(self, connection, named_entity_audit_information_table_name):
#         ModelDao.__init__(self, connection,
#                           named_entity_audit_information_table_name,
#                           NamedEntityAuditInformation)
#
#     # Override
#     def insert(self, named_entity_audit_information):
#         self._validate_model(named_entity_audit_information)
#
#         with self._conn.cursor() as cursor:
#             query = '''
# INSERT INTO {} (
#     named_entity_id,
#     created_at,
#     updated_at,
#     analyst_id,
#     audited_field,
#     synonym,
#     released,
#     sentifi_o,
#     config,
#     blacklist,
#     keyword,
#     profile_cat,
#     rss,
#     businessweek_url,
#     sector,
#     company,
#     issue_curation,
#     ready,
#     alert_enabled,
#     named_entity_type,
#     last_ready,
#     slug,
#     indice,
#     legal_misspelled,
#     sector_local,
#     sector_bw,
#     industry_subsector_local,
#     industry_subsector_bw,
#     stock_exchange,
#     ticker_local,
#     ticker_bloomberg,
#     ir,
#     se_mainstock,
#     se_sourceurl,
#     se_document,
#     se_documenturl,
#     cashtag,
#     cashtag_local,
#     market
# ) VALUES (
#     %(named_entity_id)s,
#     COALESCE(%(created_at)s, now()),
#     COALESCE(%(updated_at)s, now()),
#     %(analyst_id)s,
#     %(audited_field)s,
#     %(synonym)s,
#     %(released)s,
#     %(sentifi_o)s,
#     %(config)s,
#     %(blacklist)s,
#     %(keyword)s,
#     %(profile_cat)s,
#     %(rss)s,
#     %(businessweek_url)s,
#     %(sector)s,
#     %(company)s,
#     %(issue_curation)s,
#     %(ready)s,
#     %(alert_enabled)s,
#     %(named_entity_type)s,
#     %(last_ready)s,
#     %(slug)s,
#     %(indice)s,
#     %(legal_misspelled)s,
#     %(sector_local)s,
#     %(sector_bw)s,
#     %(industry_subsector_local)s,
#     %(industry_subsector_bw)s,
#     %(stock_exchange)s,
#     %(ticker_local)s,
#     %(ticker_bloomberg)s,
#     %(ir)s,
#     %(se_mainstock)s,
#     %(se_sourceurl)s,
#     %(se_document)s,
#     %(se_documenturl)s,
#     %(cashtag)s,
#     %(cashtag_local)s,
#     %(market)s
# ) RETURNING named_entity_id;'''.format(self._table)
#             query = cursor.mogrify(query,
#                                    named_entity_audit_information.__dict__)
#             logger.debug('Executing %s' % query)
#             cursor.execute(query)
#             self._conn.commit()
#             result = cursor.fetchone()
#         return result
#
#     # Override
#     def update(self, named_entity_audit_information):
#         self._validate_model(named_entity_audit_information)
#
#         with self._conn.cursor() as cursor:
#             query = '''
# UPDATE {} SET
#     updated_at = COALESCE(%(updated_at)s, now()),
#     analyst_id = %(analyst_id)s,
#     audited_field = %(audited_field)s,
#     synonym = %(synonym)s,
#     released = %(released)s,
#     sentifi_o = %(sentifi_o)s,
#     config = %(config)s,
#     blacklist = %(blacklist)s,
#     keyword = %(keyword)s,
#     profile_cat = %(profile_cat)s,
#     rss = %(rss)s,
#     businessweek_url = %(businessweek_url)s,
#     sector = %(sector)s,
#     company = %(company)s,
#     issue_curation = %(issue_curation)s,
#     ready = %(ready)s,
#     alert_enabled = %(alert_enabled)s,
#     named_entity_type = %(named_entity_type)s,
#     last_ready = %(last_ready)s,
#     slug = %(slug)s,
#     indice = %(indice)s,
#     legal_misspelled = %(legal_misspelled)s,
#     sector_local = %(sector_local)s,
#     sector_bw = %(sector_bw)s,
#     industry_subsector_local = %(industry_subsector_local)s,
#     industry_subsector_bw = %(industry_subsector_bw)s,
#     stock_exchange = %(stock_exchange)s,
#     ticker_local = %(ticker_local)s,
#     ticker_bloomberg = %(ticker_bloomberg)s,
#     ir = %(ir)s,
#     se_mainstock = %(se_mainstock)s,
#     se_sourceurl = %(se_sourceurl)s,
#     se_document = %(se_document)s,
#     se_documenturl = %(se_documenturl)s,
#     cashtag = %(cashtag)s,
#     cashtag_local = %(cashtag_local)s,
#     market = %(market)s
#   WHERE named_entity_id = %(named_entity_id)s
#   RETURNING named_entity_id;'''.format(self._table)
#             query = cursor.mogrify(query,
#                                    named_entity_audit_information.__dict__)
#             logger.debug('Executing %s' % query)
#             cursor.execute(query)
#             self._conn.commit()
#             result = cursor.fetchone()
#         return result
#
#     # Override
#     def upsert(self, named_entity_audit_information):
#         self._validate_model(named_entity_audit_information)
#
#         with self._conn.cursor() as cursor:
#             update_sql = '''
# UPDATE {} SET
#     updated_at = COALESCE(%(updated_at)s, now()),
#     analyst_id = %(analyst_id)s,
#     audited_field = %(audited_field)s,
#     synonym = %(synonym)s,
#     released = %(released)s,
#     sentifi_o = %(sentifi_o)s,
#     config = %(config)s,
#     blacklist = %(blacklist)s,
#     keyword = %(keyword)s,
#     profile_cat = %(profile_cat)s,
#     rss = %(rss)s,
#     businessweek_url = %(businessweek_url)s,
#     sector = %(sector)s,
#     company = %(company)s,
#     issue_curation = %(issue_curation)s,
#     ready = %(ready)s,
#     alert_enabled = %(alert_enabled)s,
#     named_entity_type = %(named_entity_type)s,
#     last_ready = %(last_ready)s,
#     slug = %(slug)s,
#     indice = %(indice)s,
#     legal_misspelled = %(legal_misspelled)s,
#     sector_local = %(sector_local)s,
#     sector_bw = %(sector_bw)s,
#     industry_subsector_local = %(industry_subsector_local)s,
#     industry_subsector_bw = %(industry_subsector_bw)s,
#     stock_exchange = %(stock_exchange)s,
#     ticker_local = %(ticker_local)s,
#     ticker_bloomberg = %(ticker_bloomberg)s,
#     ir = %(ir)s,
#     se_mainstock = %(se_mainstock)s,
#     se_sourceurl = %(se_sourceurl)s,
#     se_document = %(se_document)s,
#     se_documenturl = %(se_documenturl)s,
#     cashtag = %(cashtag)s,
#     cashtag_local = %(cashtag_local)s,
#     market = %(market)s
#   WHERE named_entity_id = %(named_entity_id)s
#   RETURNING named_entity_id;'''.format(self._table)
#             update_sql = cursor.mogrify(update_sql,
#                                         named_entity_audit_information.__dict__)
#             insert_sql = '''
# INSERT INTO {} (
#     named_entity_id,
#     created_at,
#     updated_at,
#     analyst_id,
#     audited_field,
#     synonym,
#     released,
#     sentifi_o,
#     config,
#     blacklist,
#     keyword,
#     profile_cat,
#     rss,
#     businessweek_url,
#     sector,
#     company,
#     issue_curation,
#     ready,
#     alert_enabled,
#     named_entity_type,
#     last_ready,
#     slug,
#     indice,
#     legal_misspelled,
#     sector_local,
#     sector_bw,
#     industry_subsector_local,
#     industry_subsector_bw,
#     stock_exchange,
#     ticker_local,
#     ticker_bloomberg,
#     ir,
#     se_mainstock,
#     se_sourceurl,
#     se_document,
#     se_documenturl,
#     cashtag,
#     cashtag_local,
#     market
# ) VALUES (
#     %(named_entity_id)s,
#     COALESCE(%(created_at)s, now()),
#     COALESCE(%(updated_at)s, now()),
#     %(analyst_id)s,
#     %(audited_field)s,
#     %(synonym)s,
#     %(released)s,
#     %(sentifi_o)s,
#     %(config)s,
#     %(blacklist)s,
#     %(keyword)s,
#     %(profile_cat)s,
#     %(rss)s,
#     %(businessweek_url)s,
#     %(sector)s,
#     %(company)s,
#     %(issue_curation)s,
#     %(ready)s,
#     %(alert_enabled)s,
#     %(named_entity_type)s,
#     %(last_ready)s,
#     %(slug)s,
#     %(indice)s,
#     %(legal_misspelled)s,
#     %(sector_local)s,
#     %(sector_bw)s,
#     %(industry_subsector_local)s,
#     %(industry_subsector_bw)s,
#     %(stock_exchange)s,
#     %(ticker_local)s,
#     %(ticker_bloomberg)s,
#     %(ir)s,
#     %(se_mainstock)s,
#     %(se_sourceurl)s,
#     %(se_document)s,
#     %(se_documenturl)s,
#     %(cashtag)s,
#     %(cashtag_local)s,
#     %(market)s
# ) RETURNING named_entity_id;'''.format(self._table)
#             insert_sql = cursor.mogrify(insert_sql,
#                                         named_entity_audit_information.__dict__)
#             query = '''
# SELECT upsert(%s, %s);'''
#             query = cursor.mogrify(query, (update_sql, insert_sql))
#             logger.debug('Executing %s' % query)
#             cursor.execute(query)
#             self._conn.commit()
#             result = cursor.fetchone()
#         return result


class NamedEntitySnsLinkDao(ModelDao):
    def __init__(self, connection, named_entity_sns_link_table_name):
        ModelDao.__init__(self, connection, named_entity_sns_link_table_name,
                          NamedEntitySnsLink)

    # Override
    def insert(self, named_entity_sns_link):
        self._validate_model(named_entity_sns_link)

        with self._conn.cursor() as cursor:
            query = '''
INSERT INTO {} (
    named_entity_id,
    created_at,
    updated_at,
    sns_name,
    sns_url,
    sns_id
) VALUES (
    %(named_entity_id)s,
    COALESCE(%(created_at)s, now()),
    COALESCE(%(updated_at)s, now()),
    %(sns_name)s,
    %(sns_url)s,
    %(sns_id)s
) RETURNING named_entity_id;'''.format(self._table)
            query = cursor.mogrify(query, named_entity_sns_link.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result

    # Override
    def update(self, named_entity_sns_link):
        self._validate_model(named_entity_sns_link)

        with self._conn.cursor() as cursor:
            query = '''
UPDATE {} SET
    updated_at = COALESCE(%(updated_at)s, now()),
    sns_name = %(sns_name)s,
    sns_url = %(sns_url)s,
    sns_id = %(sns_id)s
  WHERE named_entity_id = %(named_entity_id)s
  RETURNING named_entity_id;'''.format(self._table)
            query = cursor.mogrify(query, named_entity_sns_link.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result

    # Override
    def upsert(self, named_entity):
        self._validate_model(named_entity)

        with self._conn.cursor() as cursor:
            update_sql = '''
UPDATE {} SET
    updated_at = COALESCE(%(updated_at)s, now()),
    sns_name = %(sns_name)s,
    sns_url = %(sns_url)s,
    sns_id = %(sns_id)s
  WHERE named_entity_id = %(named_entity_id)s
  RETURNING named_entity_id;'''.format(self._table)
            update_sql = cursor.mogrify(update_sql, named_entity.__dict__)
            insert_sql = '''
INSERT INTO {} (
    named_entity_id,
    created_at,
    updated_at,
    sns_name,
    sns_url,
    sns_id
) VALUES (
    %(named_entity_id)s,
    COALESCE(%(created_at)s, now()),
    COALESCE(%(updated_at)s, now()),
    %(sns_name)s,
    %(sns_url)s,
    %(sns_id)s
) RETURNING named_entity_id;'''.format(self._table)
            insert_sql = cursor.mogrify(insert_sql, named_entity.__dict__)
            query = '''
SELECT upsert(%s, %s);'''
            query = cursor.mogrify(query, (update_sql, insert_sql))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result
