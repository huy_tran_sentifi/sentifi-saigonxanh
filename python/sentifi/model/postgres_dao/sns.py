# Copyright (C) 2015 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 15/08/2015
# @author: trung

from logging import getLogger
from psycopg2.extensions import register_adapter
from psycopg2.extras import Json, RealDictCursor, register_default_json

from model.postgres_dao.base import ModelDao
from model.sns import SnsAccount, SnsCategory, SnsRole


logger = getLogger(__name__)


class SnsAccountDao(ModelDao):

    def __init__(self, connection, table_name):
        ModelDao.__init__(self, connection, table_name, SnsAccount)

    # Override
    def find_by_id(self, sns_name, sns_id):
        with self._conn.cursor(cursor_factory=RealDictCursor) as cursor:
            query = '''
SELECT
    sns_name,
    sns_id,
    status,
    created_at,
    updated_at,
    named_entity_id,
    payload,
    geo_level,
    display_name,
    avatar_url,
    country_code,
    sns_tag,
    named_entity_id_excluded
  FROM {}
  WHERE sns_name = %s
    AND sns_id = %s;'''.format(self._table)
            query = cursor.mogrify(query, (sns_name, sns_id))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return SnsAccount.from_json(result)

    # Override
    def insert(self, sns_account):
        self._validate_model(sns_account)

        with self._conn.cursor() as cursor:
            query = '''
INSERT INTO {} (
    sns_name,
    sns_id,
    status,
    created_at,
    updated_at,
    human_updated_at,
    named_entity_id,
    payload,
    geo_level,
    display_name,
    avatar_url,
    country_code,
    sns_tag,
    named_entity_id_excluded
) VALUES (
    %(sns_name)s,
    %(sns_id)s,
    %(status)s,
    COALESCE(%(created_at)s, now()),
    COALESCE(%(updated_at)s, now()),
    %(human_updated_at)s,
    %(named_entity_id)s,
    %(payload)s,
    %(geo_level)s,
    %(display_name)s,
    %(avatar_url)s,
    %(country_code)s,
    %(sns_tag)s,
    %(named_entity_id_excluded)s
) RETURNING sns_name, sns_id;'''.format(self._table)
            query = cursor.mogrify(query, sns_account.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result[0], result[1] if result else None

    def merge(self, sns_account):
        self._validate_model(sns_account)

        existed_sns_account = self.find_by_id(sns_account.sns_name, sns_account.sns_id)
        if not existed_sns_account:
            return self.insert(sns_account)

        existed_sns_account_dict = existed_sns_account.__dict__
        for key, value in sns_account.__dict__.iteritems():
            if key not in existed_sns_account_dict or value is None:
                continue

            if isinstance(value, basestring):
                value = value.strip()
            if value == '#EMPTY#':
                existed_sns_account_dict[key] = None
            elif key == 'sns_tag':
                new = list(set((existed_sns_account_dict[key] or []) + (value or [])))
                new.sort()
                existed_sns_account_dict[key] = new if new else None
            elif value != '':
                existed_sns_account_dict[key] = value

        return self.update(SnsAccount.from_json(existed_sns_account_dict))

    # Override
    def update(self, sns_account):
        self._validate_model(sns_account)

        with self._conn.cursor() as cursor:
            register_default_json(cursor)
            register_adapter(dict, Json)

            query = '''
UPDATE {} SET
    status = %(status)s,
    updated_at = COALESCE(%(updated_at)s, now()),
    human_updated_at = %(human_updated_at)s,
    named_entity_id = %(named_entity_id)s,
    payload = %(payload)s,
    geo_level = COALESCE(%(geo_level)s, geo_level),
    display_name = %(display_name)s,
    avatar_url = %(avatar_url)s,
    country_code = %(country_code)s,
--    sns_tag = (SELECT array_agg(distinct foo) FROM unnest(sns_tag || %(sns_tag)s) as foo),
    sns_tag = %(sns_tag)s,
    named_entity_id_excluded = %(named_entity_id_excluded)s
  WHERE sns_name = %(sns_name)s
    AND sns_id = %(sns_id)s
  RETURNING sns_name, sns_id;'''.format(self._table)
            query = cursor.mogrify(query, sns_account.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result[0], result[1] if result else None


class SnsCategoryDao(ModelDao):

    def __init__(self, connection, table_name):
        ModelDao.__init__(self, connection, table_name, SnsCategory)

    def find_by_sns_id(self, sns_name, sns_id):
        with self._conn.cursor(cursor_factory=RealDictCursor) as cursor:
            query = '''
SELECT
    sns_name,
    sns_id,
    created_at,
    human_updated_at,
    category_id
  FROM {}
  WHERE sns_name = %s
    AND sns_id = %s;'''.format(self._table)
            query = cursor.mogrify(query, (sns_name, sns_id))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = [SnsCategory.from_json(_) for _ in cursor]
        return result if result else None

    # Override
    def insert(self, sns_category):
        self._validate_model(sns_category)

        with self._conn.cursor() as cursor:
            query = '''
INSERT INTO {} (
    sns_name,
    sns_id,
    created_at,
    human_updated_at,
    category_id
) VALUES (
    %(sns_name)s,
    %(sns_id)s,
    COALESCE(%(created_at)s, now()),
    %(human_updated_at)s,
    %(category_id)s
) RETURNING sns_name, sns_id, category_id;'''.format(self._table)
            query = cursor.mogrify(query, sns_category.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result[0], result[1] if result else None

    def remove(self, sns_category):
        self._validate_model(sns_category)

        with self._conn.cursor(cursor_factory=RealDictCursor) as cursor:
            query = '''
DELETE
  FROM {}
  WHERE sns_name = %(sns_name)s
    AND sns_id = %(sns_id)s
    AND category_id = %(category_id)s;'''.format(self._table)
            query = cursor.mogrify(query, sns_category.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()

        return cursor.rowcount

    # Override
    def update(self, sns_category):
        self._validate_model(sns_category)

        with self._conn.cursor() as cursor:
            query = '''
UPDATE {} SET
    human_updated_at = %(human_updated_at)s
  WHERE sns_name = %(sns_name)s
    AND sns_id = %(sns_id)s
    AND category_id = %(category_id)s
  RETURNING sns_name, sns_id, category_id;'''.format(self._table)
            query = cursor.mogrify(query, sns_category.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result[0], result[1] if result else None

    def update_by_sns_id(self, sns_category_list, clean_human_entries=False, clean_machine_entries=False):
        """

        :param sns_category_list: list of sns_category of a same SNS Account
        :return:
        """
        if not sns_category_list:
            raise ValueError('sns_category_list cannot be empty')

        sns_name = sns_category_list[0].sns_name
        sns_id = sns_category_list[0].sns_id
        sns_category_dict = {}
        existed_sns_category_dict = {}

        for sns_category in sns_category_list:
            self._validate_model(sns_category)
            if sns_category.sns_name != sns_name or sns_category.sns_id != sns_id:
                raise ValueError('sns_category_list must contain sns_category of a same SnsAccount (same (sns_name, sns_id))')
            key = '%s:%s:%s' % (sns_category.sns_name, sns_category.sns_id, sns_category.category_id)
            sns_category_dict[key] = sns_category

        existed_sns_account_list = self.find_by_sns_id(sns_name, sns_id)
        for sns_category in existed_sns_account_list or []:
            key = '%s:%s:%s' % (sns_category.sns_name, sns_category.sns_id, sns_category.category_id)
            existed_sns_category_dict[key] = sns_category

        insert_list = []
        update_list = []
        remove_list = []
        for key in set(sns_category_dict.keys() + existed_sns_category_dict.keys()):
            if key in sns_category_dict:
                sns_category = sns_category_dict[key]
                if key in existed_sns_category_dict:
                    sns_category.human_updated_at = sns_category.human_updated_at or existed_sns_category_dict[key].human_updated_at
                    update_list.append(sns_category)
                else:
                    insert_list.append(sns_category)
            else:
                human_flag = existed_sns_category_dict[key].human_updated_at is not None
                if human_flag and clean_human_entries:
                    remove_list.append(existed_sns_category_dict[key])
                elif not human_flag and clean_machine_entries:
                    remove_list.append(existed_sns_category_dict[key])

        for each in insert_list:
            self.insert(each)
        for each in update_list:
            self.update(each)
        for each in remove_list:
            self.remove(each)


class SnsRoleDao(ModelDao):

    def __init__(self, connection, table_name):
        ModelDao.__init__(self, connection, table_name, SnsRole)

    def find_by_sns_id(self, sns_name, sns_id):
        with self._conn.cursor(cursor_factory=RealDictCursor) as cursor:
            query = '''
SELECT
    sns_name,
    sns_id,
    created_at,
    human_updated_at,
    role_id
  FROM {}
  WHERE sns_name = %s
    AND sns_id = %s;'''.format(self._table)
            query = cursor.mogrify(query, (sns_name, sns_id))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = [SnsRole.from_json(_) for _ in cursor]
        return result if result else None

    # Override
    def insert(self, sns_role):
        self._validate_model(sns_role)

        with self._conn.cursor() as cursor:
            query = '''
INSERT INTO {} (
    sns_name,
    sns_id,
    created_at,
    human_updated_at,
    role_id
) VALUES (
    %(sns_name)s,
    %(sns_id)s,
    COALESCE(%(created_at)s, now()),
    %(human_updated_at)s,
    %(role_id)s
) RETURNING sns_name, sns_id, role_id;'''.format(self._table)
            query = cursor.mogrify(query, sns_role.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result[0], result[1] if result else None

    def remove(self, sns_role):
        self._validate_model(sns_role)

        with self._conn.cursor(cursor_factory=RealDictCursor) as cursor:
            query = '''
DELETE
  FROM {}
  WHERE sns_name = %(sns_name)s
    AND sns_id = %(sns_id)s
    AND role_id = %(role_id)s;'''.format(self._table)
            query = cursor.mogrify(query, sns_role.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()

        return cursor.rowcount

    # Override
    def update(self, sns_role):
        self._validate_model(sns_role)

        with self._conn.cursor() as cursor:
            query = '''
UPDATE {} SET
    human_updated_at = %(human_updated_at)s
  WHERE sns_name = %(sns_name)s
    AND sns_id = %(sns_id)s
    AND role_id = %(role_id)s
  RETURNING sns_name, sns_id, role_id;'''.format(self._table)
            query = cursor.mogrify(query, sns_role.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result[0], result[1] if result else None

    def update_by_sns_id(self, sns_role_list, clean_human_entries=False, clean_machine_entries=False):
        """

        :param sns_role_list: list of sns_role of a same SNS Account
        :return:
        """
        if not sns_role_list:
            raise ValueError('sns_role_list cannot be empty')

        sns_name = sns_role_list[0].sns_name
        sns_id = sns_role_list[0].sns_id
        sns_role_dict = {}
        existed_sns_role_dict = {}

        for sns_role in sns_role_list:
            self._validate_model(sns_role)
            if sns_role.sns_name != sns_name or sns_role.sns_id != sns_id:
                raise ValueError('sns_role_list must contain sns_role of a same SnsAccount (same (sns_name, sns_id))')
            key = '%s:%s:%s' % (sns_role.sns_name, sns_role.sns_id, sns_role.role_id)
            sns_role_dict[key] = sns_role

        existed_sns_account_list = self.find_by_sns_id(sns_name, sns_id)
        for sns_role in existed_sns_account_list or []:
            key = '%s:%s:%s' % (sns_role.sns_name, sns_role.sns_id, sns_role.role_id)
            existed_sns_role_dict[key] = sns_role

        insert_list = []
        update_list = []
        remove_list = []
        for key in set(sns_role_dict.keys() + existed_sns_role_dict.keys()):
            if key in sns_role_dict:
                sns_role = sns_role_dict[key]
                if key in existed_sns_role_dict:
                    sns_role.human_updated_at = sns_role.human_updated_at or existed_sns_role_dict[key].human_updated_at
                    update_list.append(sns_role)
                else:
                    insert_list.append(sns_role)
            else:
                human_flag = existed_sns_role_dict[key].human_updated_at is not None
                if human_flag and clean_human_entries:
                    remove_list.append(existed_sns_role_dict[key])
                elif not human_flag and clean_machine_entries:
                    remove_list.append(existed_sns_role_dict[key])

        for each in insert_list:
            self.insert(each)
        for each in update_list:
            self.update(each)
        for each in remove_list:
            self.remove(each)
