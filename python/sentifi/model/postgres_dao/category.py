# Copyright (C) 2015 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 18/08/2015
# @author: trung

from logging import getLogger
from psycopg2.extras import RealDictCursor

from constant import STATUS_ACTIVE, CSV_MULTI_VALUE_DELIMITER
from model import Category
from model.postgres_dao.base import ModelDao


logger = getLogger(__name__)


class CategoryDao(ModelDao):
    def __init__(self, connection, table_name):
        ModelDao.__init__(self, connection, table_name, Category)
        self._category_cache_by_level_and_name = self.create_category_dict_by_level_and_name()
        self._update_cache_by_level_and_name = self.find_by_level_and_name

    def create_category_dict_by_level_and_name(self):
        category_dict = {}
        for category in self.findall():
            key = '%s%s%s' % (category.category_level, ':', category.category_name.lower())
            category_dict[key] = category
        return category_dict

    def findall(self):
        logger.info('Fetching all Category from database')
        with self._conn.cursor(cursor_factory=RealDictCursor) as cursor:
            query = 'SELECT * FROM {} WHERE status = 1;'.format(self._table)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            for row in cursor:
                yield Category.from_json(row)

    def qfind_by_level_and_name(self, category_level, category_name):
        key = '%s%s%s' % (category_level, ':', category_name.lower().strip())
        return self._category_cache_by_level_and_name.get(key)

    def find_by_level_and_name(self, category_level, category_name):
        with self._conn.cursor(cursor_factory=RealDictCursor) as cursor:
            query = '''
SELECT *
  FROM {}
  WHERE category_level = %s,
    AND lower(category_name) = %s
    AND status = %s;'''.format(self._table)
            query = cursor.mogrify(query, (category_level, category_name.lower.strip(), STATUS_ACTIVE))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            category = Category.from_json(cursor.fetchone())
            if category:
                key = '%s%s%s' % (category.category_level, ':', category.category_name.lower())
                self._category_cache_by_level_and_name[key] = category

        return category

    def fix_parent_category_id(self):
        with self._conn.cursor() as cursor:
            query = '''
UPDATE {} c1 SET
    parent_category_id = (
        SELECT category_id
          FROM {} c2
          WHERE c2.category_mongo_id = c1.parent_category_mongo_id
            AND c2.status = 1
    ),
    updated_at = now()
  WHERE status = 1;'''.format(self._table, self._table)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            row_count = cursor.rowcount
        return row_count

    def get_category_dict_with_mongo_id_key(self):
        category_dict = {}
        with self._conn.cursor(cursor_factory=RealDictCursor) as cursor:
            query = '''
SELECT *
  FROM {}
  WHERE status = %s;'''.format(self._table)
            query = cursor.mogrify(query, (STATUS_ACTIVE, ))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            for row in cursor:
                category_dict[row['category_mongo_id']] = row
        return category_dict

    def get_category_dict_with_level_and_name_key(self):
        category_dict = {}
        with self._conn.cursor(cursor_factory=RealDictCursor) as cursor:
            query = '''
SELECT *
  FROM {}
  WHERE status = %s;'''.format(self._table)
            query = cursor.mogrify(query, (STATUS_ACTIVE, ))
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            for row in cursor:
                key = '%s%s%s' % (row['category_level'], ':', row['category_name'].lower())
                category_dict[key] = row
        return category_dict

    # Override
    def insert(self, category):
        self._validate_model(category)

        with self._conn.cursor() as cursor:
            query = '''
INSERT INTO {} (
    category_id,
    status,
    created_at,
    updated_at,
    parent_category_id,
    category_mongo_id,
    parent_category_mongo_id,
    category_name,
    detail,
    category_level
) VALUES (
    %(category_id)s,
    %(status)s,
    COALESCE(%(created_at)s, now()),
    COALESCE(%(updated_at)s, now()),
    %(parent_category_id)s,
    %(category_mongo_id)s,
    %(parent_category_mongo_id)s,
    %(category_name)s,
    %(detail)s,
    %(category_level)s
) RETURNING category_id;'''.format(self._table)
            query = cursor.mogrify(query, category.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result

    # Override
    def is_existed(self, category_id):
        with self._conn.cursor() as cursor:
            query = '''
SELECT true FROM {} WHERE category_id = %s;'''.format(self._table)
            query = cursor.mogrify(query, [category_id])
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            result = cursor.fetchone()
        return result[0] is True if result else False

    def is_existed_by_mongo_id(self, category_mongo_id):
        with self._conn.cursor() as cursor:
            query = '''
SELECT true FROM {} WHERE category_mongo_id = %s;'''.format(self._table)
            query = cursor.mogrify(query, [category_mongo_id])
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            result = cursor.fetchone()
        return result[0] is True if result else False

    # Override
    def update(self, category):
        self._validate_model(category)

        with self._conn.cursor() as cursor:
            query = '''
UPDATE {} SET
    status = %(status)s,
    updated_at = COALESCE(%(updated_at)s, now()),
    parent_category_id = %(parent_category_id)s,
    category_mongo_id = %(category_mongo_id)s,
    parent_category_mongo_id = %(parent_category_mongo_id)s,
    category_name = %(category_name)s,
    detail = %(detail)s,
    category_level = %(category_level)s
  WHERE category_id = %(category_id)s
  RETURNING category_id;'''.format(self._table)
            query = cursor.mogrify(query, category.__dict__)
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self._conn.commit()
            result = cursor.fetchone()
        return result

    def update_cache(self):
        self._category_cache_by_level_and_name = self.create_category_dict_by_level_and_name()
