# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Oct 18, 2014
# @author: trung

import ujson as json


class Person(object):
    def __init__(self, person_id=None, status=None, created_at=None,
                 updated_at=None, person_name=None, title=None, first_name=None,
                 middle_name=None, last_name=None, suffix=None, dob=None,
                 detail=None, email=None, address=None, city=None,
                 country_code=None, person_url=None, photo_url=None, guru=None):
        self.person_id = person_id
        self.status = status
        self.created_at = created_at
        self.updated_at = updated_at
        self.person_name = person_name
        self.title = title
        self.first_name = first_name
        self.middle_name = middle_name
        self.last_name = last_name
        self.suffix = suffix
        self.dob = dob
        self.detail = detail
        self.email = email
        self.address = address
        self.city = city
        self.country_code = country_code
        self.person_url = person_url
        self.photo_url = photo_url
        self.guru = guru

    @staticmethod
    def from_json(payload):
        return Person(
            person_id=payload.get('person_id'),
            status=payload.get('status'),
            created_at=payload.get('created_at'),
            updated_at=payload.get('updated_at'),
            person_name=payload.get('person_name'),
            title=payload.get('title'),
            first_name=payload.get('first_name'),
            middle_name=payload.get('middle_name'),
            last_name=payload.get('last_name'),
            suffix=payload.get('suffix'),
            dob=payload.get('dob'),
            detail=payload.get('detail'),
            address=payload.get('address'),
            city=payload.get('city'),
            country_code=payload.get('country_code'),
            person_url=payload.get('person_url'),
            photo_url=payload.get('photo_url'),
            guru=payload.get('guru'),
        ) if payload else None

    @property
    def id(self):
        return self.person_id

    def to_json_string(self):
        return json.dumps(self.__dict__)
