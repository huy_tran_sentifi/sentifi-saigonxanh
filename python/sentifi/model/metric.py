# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Dec 18, 2014
# @author: hoan

from datetime import datetime
from dateutil import parser


class Metric(object):

    """A metric for a social network account in the Publisher Analytics DB.

    Attributes:
        sns_name (str): The name of the social network
        sns_id (str): The id of the social network
        metric_id (int): the id of the metric
        count_date (datetime): the count date of this value in this metric
        count_value (int or float): the value of this metric
    """

    def __init__(self,
                 sns_name=None,
                 sns_id=None,
                 metric_id=None,
                 count_date=None,
                 count_value=None):
        self._sns_name = sns_name
        self._sns_id = sns_id
        self._metric_id = metric_id

        if isinstance(count_date, datetime):
            self._count_date = count_date
        elif isinstance(count_date, (str, unicode)):
            try:
                self._count_date = parser.parse(count_date)
            except Exception, e:
                raise e

        if isinstance(count_value, (int, long, float, complex)):
            self._count_value = count_value
        else:
            raise TypeError("%s is not a number!" % str(count_value))

    def __str__(self):
        return """Metric [%s] of [%s, %s] has value [%s] measured on [%s]""" % \
            (str(self._metric_id), str(self._sns_id), str(self._sns_name),
             str(self._count_value), str(self._count_date))

    def __repr__(self):
        return """Metric [%s] of [%s, %s] has value [%s] measured on [%s]""" % \
            (str(self._metric_id), str(self._sns_id), str(self._sns_name),
             str(self._count_value), str(self._count_date))

    @property
    def sns_name(self):
        """Gets or sets the name of the social network
        """
        return self._sns_name

    @sns_name.setter
    def sns_name(self, value):
        self._sns_name = value

    @property
    def sns_id(self):
        """Gets or sets the id of the social network
        """
        return self._sns_id

    @sns_id.setter
    def sns_id(self, value):
        self._sns_id = value

    @property
    def metric_id(self):
        """Gets or sets the id of the metric
        """
        return self._metric_id

    @metric_id.setter
    def metric_id(self, value):
        self._metric_id = value

    @property
    def date(self):
        """Gets or sets the count date (datetime) of this value in this metric
        """
        return self._count_date

    @date.setter
    def count_date(self, value):
        if isinstance(value, datetime):
            self._count_date = value
        elif isinstance(value, (str, unicode)):
            try:
                self._count_date = parser.parse(value)
            except Exception, e:
                raise e

    @property
    def value(self):
        """Gets or sets the value in this metric
        """
        return self._count_value

    @value.setter
    def count_value(self, value):
        if isinstance(value, (int, long, float, complex)):
            self._count_value = value
        else:
            raise TypeError("%s is not a number!" % str(value))

    @staticmethod
    def create_query_info(sns_name, sns_id, metric_id):
        return Metric(sns_name, sns_id, metric_id, datetime.now(), 0)
