# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Aug 15, 2014
# @author: trung

from datetime import datetime
from kombu.common import maybe_declare
from kombu.log import get_logger
from kombu.mixins import ConsumerMixin
from kombu.pools import producers
from psycopg2.extras import RealDictCursor
from time import sleep
from urlparse import urlparse

from constant import DEFAULT_BATCH_SIZE
from constant.mongo_namespace import C_NEWS, C_PUBLISHER, C_PUBLISHER_NEWS, \
    C_PUBLISHER_TWITTER, D_CORE
from constant.postgres_namespace import T_CATEGORY
from etl.mongo_to_postgres import *
from model.mongo_dao import DictionaryDao as MgDictionaryDao
from model.postgres_dao import CategoryDao
from workers.queue import publisher_created_exchange, publisher_updated_exchange


logger = get_logger(__name__)


def make_publisher_mg_status(pg_status):
    if pg_status == STATUS_ACTIVE:
        return 2
    elif pg_status == STATUS_NEW:
        return 0
    elif pg_status == STATUS_HIDED:
        return 3
    elif pg_status == STATUS_MACHINE_AUDITED or pg_status == STATUS_INACTIVE:
        return 1
    elif pg_status is None:
        return None
    else:
        raise ValueError('Unknown Publisher status %s' % pg_status)


def make_publisher_category(item_dict, category_dict):
    categories = []
    cat3 = category_dict.get(item_dict.get('cat3'))
    if cat3:
        categories.append(dict(
            _id=ObjectId(cat3.get('category_mongo_id')),
            level=cat3.get('category_level'),
            name=cat3.get('category_name'),
            numericId=cat3.get('category_id'),
            parentNumericId=cat3.get('parent_category_id'),
            parentId=ObjectId(cat3.get('parent_category_mongo_id'))
        ))
    cat2 = category_dict.get(item_dict.get('cat2'))
    if cat2:
        categories.append(dict(
            _id=ObjectId(cat2.get('category_mongo_id')),
            level=cat2.get('category_level'),
            name=cat2.get('category_name'),
            numericId=cat2.get('category_id'),
            parentNumericId=cat2.get('parent_category_id'),
            parentId=ObjectId(cat2.get('parent_category_mongo_id'))
        ))
    cat1 = category_dict.get(item_dict.get('cat1'))
    if cat1:
        categories.append(dict(
            _id=ObjectId(cat1.get('category_mongo_id')),
            level=cat1.get('category_level'),
            name=cat1.get('category_name'),
            numericId=cat1.get('category_id'),
            parentNumericId=cat1.get('parent_category_id'),
            parentId=ObjectId(cat1.get('parent_category_mongo_id'))
        ))
    itemgroup = category_dict.get(item_dict.get('itemgroup'))
    if itemgroup:
        categories.append(dict(
            _id=ObjectId(itemgroup.get('category_mongo_id')),
            level=itemgroup.get('category_level'),
            name=itemgroup.get('category_name'),
            numericId=itemgroup.get('category_id'),
            parentNumericId=itemgroup.get('parent_category_id'),
            parentId=ObjectId(itemgroup.get('parent_category_mongo_id'))
        ))
    itemtype = category_dict.get(item_dict.get('itemtype'))
    if itemtype:
        categories.append(dict(
            _id=ObjectId(itemtype.get('category_mongo_id')),
            level=itemtype.get('category_level'),
            name=itemtype.get('category_name'),
            numericId=itemtype.get('category_id'),
            parentNumericId=itemtype.get('parent_category_id'),
            parentId=ObjectId(itemtype.get('parent_category_mongo_id'))
        ))
    return categories


def make_mg_publisher_from_pg_search_item(search_item_dict, category_dict):
    mg_publisher = dict(
        active=search_item_dict.get('publisher_status') > STATUS_INACTIVE,
        address=search_item_dict.get('address'),
        categories=make_publisher_category(search_item_dict, category_dict),
        countryCode=search_item_dict.get('isocode'),
        description=search_item_dict.get('about_us') or search_item_dict.get('detail'),
        email=search_item_dict.get('email'),
        extras=dict(
            itemKey=search_item_dict.get('id')
        ),
        image=search_item_dict.get('photo'),
        itemId=search_item_dict.get('id'),
        name=search_item_dict.get('name'),
        newsAggregator=0,
        releasedAt=search_item_dict.get('publisher_released_at'),
        score=dict(),
        status=make_publisher_mg_status(search_item_dict.get('publisher_status')),
        tracking=dict(
            auditAuthorId=search_item_dict.get('analyst_id'),
            source='SearchItem from Postgres',
            updatedAt=search_item_dict.get('updated')
        )
    )
    empty_string_to_none(mg_publisher['tracking'])
    empty_string_to_none(mg_publisher)
    return mg_publisher


class SearchItemToMongoPublisherWorker(ConsumerMixin):
    '''Consumer that fetchs IDs of changed Publishers from queue, get the full
    Publisher from Mongo and then import to Postgres
    '''

    def __init__(self, amqp_connection, input_queues, mg_client, pg_connection):
        self.connection = amqp_connection
        self.input_queues = input_queues
        self._mg_publisher_dao = MgDictionaryDao(mg_client, D_CORE, C_PUBLISHER)
        self._mg_publisher_news = MgDictionaryDao(mg_client, D_CORE, C_PUBLISHER_NEWS)
        self._mg_news = MgDictionaryDao(mg_client, D_CORE, C_NEWS)
        self._mg_publisher_twitter = MgDictionaryDao(mg_client, D_CORE, C_PUBLISHER_TWITTER)
        self.pg_connection = pg_connection
        category_dao = CategoryDao(pg_connection, T_CATEGORY)
        logger.info('Loading Category hierarchy into memory...')
        self._category_dict = category_dao.get_category_dict_with_mongo_id_key()
        self._producer = producers[amqp_connection].acquire(block=True)
        maybe_declare(publisher_created_exchange, self._producer.channel)
        maybe_declare(publisher_updated_exchange, self._producer.channel)

    def _get_search_item_with_sns_info_by_id(self, search_item_id):
        """Get all SearchItem fields from Postgres to a dictionary, including
        its SNS IDs

        :param search_item_id:
        :return: Dictionary of SearchItem
        """
        with self.pg_connection.cursor(cursor_factory=RealDictCursor) as cursor:
            query = '''
SELECT
    *,
    (
        SELECT array_agg(sns_id)
          FROM public.item_sns
          WHERE item_id = %(search_item_id)s
            AND status = %(status_active)s
            AND sns_name = 'blog'
    ) AS blog_url,
    (
        SELECT array_agg(sns_id)
          FROM public.item_sns
          WHERE item_id = %(search_item_id)s
            AND status = %(status_active)s
            AND sns_name = 'rss'
    ) AS rss_url,
    (
        SELECT array_agg(sns_id)
          FROM public.item_sns
          WHERE item_id = %(search_item_id)s
            AND status = %(status_active)s
            AND sns_name = 'tw'
    ) AS twitter_id
  FROM public.item i
  INNER JOIN public.item_audit ia USING (id)
  WHERE id = %(search_item_id)s
  LIMIT 1''' % ({
                'search_item_id': search_item_id,
                'status_active': STATUS_ACTIVE
            })
            logger.debug('Executing %s' % query)
            cursor.execute(query)
            self.pg_connection.commit()
            result = cursor.fetchone()
        return result

    def _get_news_info(self, publisher_mongo_id):
        """Get all Publishers News information (URLs, RSS feeds) from Mongo

        :param publisher_mongo_id:
        :return:
        """
        news_info_list = []
        for doc in self._mg_publisher_news.find(
                spec={'publisherId': publisher_mongo_id}):
            news_id = doc.get('newsId')
            if news_id:
                news_info = self._mg_news.find_one(news_id)
                if news_info:
                    news_info_list.append(news_info)
        return news_info_list

    def _upsert_mg_publisher_twitter(self, publisher_mongo_id, twitter_id_list):
        """Upsert linked ``twitter_id`` list to Mongo

        :param publisher_mongo_id:
        :param twitter_id_list:
        :return:
        """
        for twitter_id in twitter_id_list or []:
            self._mg_publisher_twitter.update(
                spec={
                    'publisherId': publisher_mongo_id,
                    'twitterId': twitter_id
                },
                document={
                    '$set': {
                        'publisherId': publisher_mongo_id,
                        'twitterId': twitter_id
                    }
                },
                upsert=True
            )

    def _upsert_mg_publisher_rss(self, publisher_mongo_id, rss_list, publisher_url):
        if not isinstance(rss_list, list):
            logger.warning('Ignore rss_list %s: list is expected, not %s' % (rss_list, type(rss_list)))
            return

        if not rss_list and not publisher_url:
            logger.warning('Invalid pair (rss_list, publisher_url): (%s, %s)' % (rss_list, publisher_url))
            return

        existed_news_id = None
        # Some News documents exist already, check if any of them contains RSS
        # Find the document containing RSS list from the existed News documents
        for news_info in self._get_news_info(publisher_mongo_id):
            rss = news_info.get('rss')
            if isinstance(rss, list) and rss:
                existed_news_id = news_info.get('_id')
                break

        # Document containing RSS found, update it
        if existed_news_id:
            result = self._mg_news.update(
                spec={
                    '_id': existed_news_id
                },
                document={
                    '$set': {
                        'rss': rss_list
                    }
                },
                upsert=True
            )
            logger.info('Upsert RSS url %s to News %s returning %s' % (rss_list, existed_news_id, result))
        # No document containing RSS is found, create one
        else:
            url = publisher_url
            if not url and rss_list:
                url = urlparse(rss_list[0]).netloc
            if url:
                news = self._mg_news.find_one(spec_or_id={'url': url})
                # Remove existed News having same url if any. This help to
                # prevent violating unique url constraint
                if news:
                    news_id = news.get('_id')
                    result = self._mg_news.remove(spec_or_id=news_id)
                    logger.info('Remove News containing url %s returning %s' % (url, result))
                    if result.get('ok') and result.get('n'):
                        result = self._mg_publisher_news.remove(spec_or_id={'newsId': news_id})
                        logger.info('Remove PublisherNews containing newsId %s returning %s' % (news_id, result))
                # Insert a News document holding all RSS
                news_id = self._mg_news.insert({
                    'rss': rss_list,
                    'url': url
                })
                logger.info('Insert News url %s returning: %s' % (rss_list, news_id))
                self._mg_publisher_news.insert({
                    'publisherId': publisher_mongo_id,
                    'newsId': news_id
                })

    def _upsert_mg_publisher_blog(self, publisher_mongo_id, blog_list):
        for blog_url in blog_list or []:
            blog_url = blog_url.strip().lower()
            news_info = self._mg_news.find_one(spec_or_id={'url': blog_url})
            # Document containing blog url found, update the link in
            # PublisherNews
            if news_info:
                news_id = news_info.get('_id')
                result = self._mg_publisher_news.update(
                    spec={
                        'newsId': news_id
                    },
                    document={
                        '$set': {
                            'publisherId': publisher_mongo_id,
                            # Fix the publisherId link
                            'newsId': news_id
                        }
                    },
                    upsert=True
                )
                logger.info('Update PublisherNews having newsId %s returning: %s' % (news_id, result))
            # No document found, create new entry
            else:
                news_id = self._mg_news.insert({
                    'rss': None,
                    'url': blog_url
                })
                logger.info('Insert blog url %s returning: %s' % (blog_url, news_id))
                publisher_news_id = self._mg_publisher_news.insert({
                    'publisherId': publisher_mongo_id,
                    # Fix the publisherId link
                    'newsId': news_id
                })
                logger.info('Insert link for newsId %s returning: %s' % (news_id, publisher_news_id))


    def _upsert_mg_publisher_from_search_item(self, search_item):
        search_item_id = search_item.get('id')
        mg_publisher = make_mg_publisher_from_pg_search_item(search_item, self._category_dict)
        logger.debug('SearchItem %s is transformed to Publisher %s' % (search_item_id, mg_publisher))
        mg_existed_publisher = self._mg_publisher_dao.find_one(spec_or_id={'itemId': search_item_id})
        if not mg_existed_publisher:
            logger.info('Publisher with itemId %s not found, inserting...' % search_item_id)
            # Insert into Publisher
            mg_publisher['releasedAt'] = datetime.now() # Set releasedAt to now() for new Publisher
            publisher_mongo_id = self._mg_publisher_dao.insert(mg_publisher)
            # Insert into PublisherTwitter
            self._upsert_mg_publisher_twitter(publisher_mongo_id, search_item.get('twitter_id', []))
            # Insert into PublisherNews and News
            self._upsert_mg_publisher_rss(publisher_mongo_id, search_item.get('rss_url', []), search_item.get('www'))
            self._upsert_mg_publisher_blog(publisher_mongo_id, search_item.get('blog_url', []))
            # Post ``publisher_mongo_id`` to AMQP
            self._post_created_publisher(publisher_mongo_id)
        else:
            logger.debug('Existed Publisher with itemId %s found from Mongo %s' % (search_item_id, mg_existed_publisher))
            publisher_mongo_id = mg_existed_publisher.get('_id')
            for k in mg_publisher:
                if mg_publisher[k] is not None:
                    if isinstance(mg_publisher[k], dict):
                        mg_existed_publisher[k].update(mg_publisher[k])
                    else:
                        mg_existed_publisher[k] = mg_publisher[k]
            logger.debug('Merge Publishers with itemId %s and obtain %s' % (search_item_id, mg_existed_publisher))
            logger.info('Updating Publisher with itemId %s...' % search_item_id)
            # Update Publisher
            del mg_existed_publisher['_id']
            del mg_existed_publisher['releasedAt']
            self._mg_publisher_dao.update(
                spec={'_id': publisher_mongo_id},
                document={'$set': mg_existed_publisher}
            )
            # Update PublisherTwitter
            self._upsert_mg_publisher_twitter(publisher_mongo_id, search_item.get('twitter_id', []))
            # Update PublisherNews
            self._upsert_mg_publisher_rss(publisher_mongo_id, search_item.get('rss_url', []), search_item.get('www'))
            self._upsert_mg_publisher_blog(publisher_mongo_id, search_item.get('blog_url', []))
            # Post ``publisher_mongo_id`` to AMQP
            self._post_updated_publisher(publisher_mongo_id)

    def _post_created_publisher(self, publisher_mongo_id):
        logger.info('Posting %s to %s' % (str(publisher_mongo_id), publisher_created_exchange.name))
        self._producer.publish(str(publisher_mongo_id), exchange=publisher_created_exchange, retry=True)

    def _post_updated_publisher(self, publisher_mongo_id):
        logger.info('Posting %s to %s' % (str(publisher_mongo_id), publisher_updated_exchange.name))
        self._producer.publish(str(publisher_mongo_id), exchange=publisher_updated_exchange, retry=True)

    def get_consumers(self, Consumer, channel):
        consumer = Consumer(
            queues=self.input_queues,
            no_ack=False,
            auto_declare=True,
            callbacks=[self.on_message],
            on_message=None,
            accept=['json']
        )
        consumer.qos(prefetch_count=DEFAULT_BATCH_SIZE)
        return [consumer]

    def log_decode_error(self, message, exc):
        logger.error('Exeption %s catched while decoding messasge %s' % (
            exc, message.body))

    def on_message(self, body, message):
        if not body or body == 'null':
            logger.warning('Empty message received')
            message.reject(requeue=False)
            return

        logger.debug('Message received from queue: %s' % body)
        search_item_id = int(body)
        sleep(10) # TODO: remove this when Frontend team fix the transaction issue
        search_item = self._get_search_item_with_sns_info_by_id(search_item_id)
        logger.debug('SearchItem %s found from Postgres: %s' % (
            search_item_id, search_item))
        if not search_item or not isinstance(search_item, dict):
            logger.warning('SearchItem %s not valid' % search_item_id)
            message.reject(requeue=False)
            return

        self._upsert_mg_publisher_from_search_item(search_item)
        message.ack()
