# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 15/04/2015
# @author: trung

from kombu.log import get_logger
from kombu.mixins import ConsumerMixin
from psycopg2.extras import RealDictCursor

from constant import DEFAULT_BATCH_SIZE
from constant.mongo_namespace import C_FACEBOOK, C_GOOGLEPLUS, C_LINKEDIN, \
    C_NEWS, C_PUBLISHER, C_PUBLISHER_FACEBOOK, C_PUBLISHER_GOOGLEPLUS, \
    C_PUBLISHER_LINKEDIN, C_PUBLISHER_NEWS, C_PUBLISHER_TWITTER, C_TWITTER, \
    D_CORE
from constant.postgres_namespace import T_FACEBOOK, T_GOOGLE_PLUS, T_LINKEDIN, \
    T_NEWS, T_PUBLISHER_FROM_MONGO, T_PUBLISHER_SNS, T_TWITTER
from etl.mongo_to_postgres import *
from helpers.utils import bson2json
from model.mongo_dao import DictionaryDao as MgDictionaryDao
from model.postgres_dao import DictionaryDao as PgDictionaryDao


logger = get_logger(__name__)

FIELD_FACEBOOK_ID = 'facebookId'
FIELD_GOOGLE_PLUS_ID = 'googlePlusId'
FIELD_LINKEDIN_ID = 'linkedinId'
FIELD_NEWS_ID = 'newsId'
FIELD_PUBLISHER_ID = 'publisherId'
FIELD_TWITTER_ID = 'twitterId'


def get_mg_sns_info(publisher_mongo_id, mg_publisher_sns, mg_sns,
        sns_id_field_name):
    """Get SNS information of a Publisher from Mongo by its mongo_id

    :param publisher_mongo_id: Mongo identification of the Publisher
    :param mg_publisher_sns: Collection (Cursor) containing links
    Publisher - SNS
    :param mg_sns: Collection containing SNS information
    :param sns_id_field_name: Name of SNS identification field in Mongo
    :return: List of SNS information linking to this Publisher
    """
    pub_links = mg_publisher_sns.find(
        spec={FIELD_PUBLISHER_ID: ObjectId(publisher_mongo_id)},
        fields={sns_id_field_name: 1})
    result = []
    for link in pub_links or []:
        sns_info = []
        if link and link.get(sns_id_field_name):
            sns_info = mg_sns.find(spec={'_id': link.get(sns_id_field_name)})
        for each in sns_info or []:
            if each:
                bson2json(each)
                result.append(each)
    return result


def get_mg_facebook_info(publisher_mongo_id, mg_publisher_facebook,
        mg_facebook):
    """Get Facebook information of a Publisher from Mongo by its mongo_id

    :param publisher_mongo_id: Mongo identification of the Publisher
    :param mg_publisher_facebook: Collection (Cursor) containing links
    Publisher - Facebook
    :param mg_facebook: Collection containing Facebook information
    :return: List of Facebook information linking to this Publisher
    """
    return get_mg_sns_info(publisher_mongo_id, mg_publisher_facebook,
            mg_facebook, FIELD_FACEBOOK_ID)


def get_mg_googleplus_info(publisher_mongo_id, mg_publisher_googleplus,
        mg_googleplus):
    """Get GooglePlus information of a Publisher from Mongo by its mongo_id

    :param publisher_mongo_id: Mongo identification of the Publisher
    :param mg_publisher_googleplus: Collection (Cursor) containing links
    Publisher - GooglePlus
    :param mg_googleplus: Collection containing GooglePlus information
    :return: List of GooglePlus information linking to this Publisher
    """
    return get_mg_sns_info(publisher_mongo_id, mg_publisher_googleplus,
            mg_googleplus, FIELD_GOOGLE_PLUS_ID)


def get_mg_linkedin_info(publisher_mongo_id, mg_publisher_linkedin,
        mg_linked_in):
    """Get LinkedIn information of a Publisher from Mongo by its mongo_id

    :param publisher_mongo_id: Mongo identification of the Publisher
    :param mg_publisher_linkedin: Collection (Cursor) containing links
    Publisher - LinkedIn
    :param mg_linked_in: Collection containing LinkedIn information
    :return: List of LinkedIn information linking to this Publisher
    """
    return get_mg_sns_info(publisher_mongo_id, mg_publisher_linkedin,
            mg_linked_in, FIELD_LINKEDIN_ID)


def get_mg_news_info(publisher_mongo_id, mg_publisher_news, mg_news):
    """Get News information of a Publisher from Mongo by its mongo_id

    :param publisher_mongo_id: Mongo identification of the Publisher
    :param mg_publisher_news: Collection (Cursor) containing links
    Publisher - News
    :param mg_news: Collection containing News information
    :return: List of News information linking to this Publisher
    """
    return get_mg_sns_info(publisher_mongo_id, mg_publisher_news, mg_news,
            FIELD_NEWS_ID)


def get_mg_twitter_info(publisher_mongo_id, mg_publisher_twitter, mg_twitter):
    """Get Twitter information of a Publisher from Mongo by its mongo_id

    :param publisher_mongo_id: Mongo identification of the Publisher
    :param mg_publisher_twitter: Collection (Cursor) containing links
    Publisher - Twitter
    :param mg_twitter: Collection containing Twitter information
    :return: List of Twitter information linking to this Publisher
    """
    pub_twitter_links = mg_publisher_twitter.find(
        spec={FIELD_PUBLISHER_ID: ObjectId(publisher_mongo_id)},
        fields={FIELD_TWITTER_ID: 1})
    result = []
    for link in pub_twitter_links or []:
        twitter_info = []
        if link and link.get(FIELD_TWITTER_ID):
            twitter_info = mg_twitter.find(
                spec={'id_str': link.get(FIELD_TWITTER_ID)})
        for each in twitter_info or []:
            if each:
                bson2json(each)
                result.append(each)
    return result


def get_publisher_sns_from_postgres(pg_conn, publisher_mongo_id):
    """Select all links Publisher - SNS from Postgres

    :param pg_conn:
    :param publisher_mongo_id:
    :return: List of dicts defining links
    """
    if not publisher_mongo_id:
        return None

    with pg_conn.cursor(cursor_factory=RealDictCursor) as cursor:
        query = '''
SELECT *
  FROM {}
  WHERE publisher_mongo_id = %s;'''.format(T_PUBLISHER_SNS)
        query = cursor.mogrify(query, (publisher_mongo_id, ))
        logger.debug('Executing %s' % query)
        cursor.execute(query)
        pg_conn.commit()
        return cursor.fetchall() if cursor.rowcount else []


def delete_publisher_sns_from_postgres(pg_conn, publisher_mongo_id, sns_list):
    """Delete links Publisher - SNS from Postgres

    :param pg_conn:
    :param publisher_mongo_id:
    :param sns_list: List of tuple (sns_name, sns_id)
    :return: Number of rows deleted
    """
    if not publisher_mongo_id or not sns_list:
        return 0

    with pg_conn.cursor(cursor_factory=RealDictCursor) as cursor:
        query = '''
DELETE
  FROM {}
  WHERE publisher_mongo_id = %s
    AND sns_name = %s
    AND sns_id = %s;'''.format(T_PUBLISHER_SNS)
        logger.debug('Deleting %s records %s - %s' % (len(sns_list), publisher_mongo_id, sns_list))
        cursor.executemany(query, [(publisher_mongo_id, _[0], _[1]) for _ in sns_list])
        return cursor.rowcount


def insert_publisher_sns(pg_conn, publisher_mongo_id, sns_list):
    """Insert links Publisher - SNS into Postgres

    :param pg_conn:
    :param publisher_mongo_id:
    :param sns_list: List of tuple (sns_name, sns_id)
    :return: Number of rows inserted
    """
    if not publisher_mongo_id or not sns_list:
        return 0

    with pg_conn.cursor(cursor_factory=RealDictCursor) as cursor:
        query = '''
INSERT INTO {} (publisher_mongo_id, sns_name, sns_id)
  VALUES (%s, %s, %s);'''.format(T_PUBLISHER_SNS)
        logger.debug('Inserting %s records %s - %s' % (len(sns_list), publisher_mongo_id, sns_list))
        cursor.executemany(query, [(publisher_mongo_id, _[0], _[1]) for _ in sns_list])
        return cursor.rowcount


def upsert_mg_publisher_sns(pg_conn, publisher_mongo_id, sns_list):
    """Replace all links Publisher - SNS from Postgres with new links

    :param pg_conn:
    :param publisher_mongo_id:
    :param sns_list: List of tuple (sns_name, sns_id)
    :return:
    """
    if not publisher_mongo_id or not sns_list:
        return 0

    sns_set = set(sns_list)
    existed_publisher_sns_set = set([(_.get('sns_name'), _.get('sns_id')) for _ in get_publisher_sns_from_postgres(pg_conn, publisher_mongo_id)])
    sns_list_to_remove = list(existed_publisher_sns_set - sns_set)
    sns_list_to_insert = list(sns_set - existed_publisher_sns_set)

    if sns_list_to_remove:
        row_count = delete_publisher_sns_from_postgres(pg_conn, publisher_mongo_id, sns_list_to_remove)
        logger.debug('%s row removed' % row_count)

    if sns_list_to_insert:
        row_count = insert_publisher_sns(pg_conn, publisher_mongo_id, sns_list_to_insert)
        logger.debug('%s row inserted' % row_count)


def publisher_to_postgres(publisher_mongo_id, mg_publisher,
        mg_publisher_facebook, mg_publisher_google_plus, mg_publisher_linkedin,
        mg_publisher_news, mg_publisher_twitter, mg_facebook, mg_google_plus,
        mg_linkedin, mg_news, mg_twitter, pg_publisher, pg_facebook,
        pg_google_plus, pg_linkedin, pg_news, pg_twitter):
    pub_doc = mg_publisher.find_by_id(publisher_mongo_id)
    logger.debug('Publisher %s received from Mongo: %s' % (publisher_mongo_id, pub_doc))

    if not pub_doc:
        return

    sns_list = []

    fb_info_list = get_mg_facebook_info(publisher_mongo_id, mg_publisher_facebook, mg_facebook)
    for each in fb_info_list:
        if each and each.get('_id'):
            pg_facebook.upsert(each)
            sns_list.append(('fb', each.get('_id')))

    gp_info_list = get_mg_googleplus_info(publisher_mongo_id, mg_publisher_google_plus, mg_google_plus)
    for each in gp_info_list:
        if each and each.get('_id'):
            pg_google_plus.upsert(each)
            sns_list.append(('gp', each.get('_id')))

    li_info_list = get_mg_linkedin_info(publisher_mongo_id, mg_publisher_linkedin, mg_linkedin)
    for each in li_info_list:
        if each and each.get('_id'):
            pg_linkedin.upsert(each)
            sns_list.append(('li', each.get('_id')))

    news_info_list = get_mg_news_info(publisher_mongo_id, mg_publisher_news, mg_news)
    for each in news_info_list:
        if each and each.get('_id'):
            pg_news.upsert(each)
            sns_list.append(('news', each.get('_id')))

    tw_info_list = get_mg_twitter_info(publisher_mongo_id, mg_publisher_twitter, mg_twitter)
    for each in tw_info_list:
        if each and each.get('id_str'):
            pg_twitter.upsert(each)
            sns_list.append(('tw', each.get('id_str')))

    pg_publisher.upsert(pub_doc)
    upsert_mg_publisher_sns(pg_publisher.get_connection(), publisher_mongo_id, sns_list)


class PublisherToPostgresWorker(ConsumerMixin):
    '''Consumer that fetchs IDs of changed Publishers from queue, get the full
    Publisher from Mongo and then import to Postgres
    '''

    def __init__(self, amqp_connection, input_queues, mg_client, pg_connection):
        self.connection = amqp_connection
        self.input_queues = input_queues
        self._mg_publisher_facebook = MgDictionaryDao(mg_client, D_CORE, C_PUBLISHER_FACEBOOK)
        self._mg_publisher_googleplus = MgDictionaryDao(mg_client, D_CORE, C_PUBLISHER_GOOGLEPLUS)
        self._mg_publisher_linkedin = MgDictionaryDao(mg_client, D_CORE, C_PUBLISHER_LINKEDIN)
        self._mg_publisher_news = MgDictionaryDao(mg_client, D_CORE, C_PUBLISHER_NEWS)
        self._mg_publisher_twitter = MgDictionaryDao(mg_client, D_CORE, C_PUBLISHER_TWITTER)
        self._mg_publisher = MgDictionaryDao(mg_client, D_CORE, C_PUBLISHER)
        self._mg_facebook = MgDictionaryDao(mg_client, D_CORE, C_FACEBOOK)
        self._mg_googleplus = MgDictionaryDao(mg_client, D_CORE, C_GOOGLEPLUS)
        self._mg_linkedin = MgDictionaryDao(mg_client, D_CORE, C_LINKEDIN)
        self._mg_news = MgDictionaryDao(mg_client, D_CORE, C_NEWS)
        self._mg_twitter = MgDictionaryDao(mg_client, D_CORE, C_TWITTER)
        self.pg_connection = pg_connection
        self._pg_publisher = PgDictionaryDao(pg_connection, T_PUBLISHER_FROM_MONGO)
        self._pg_facebook = PgDictionaryDao(pg_connection, T_FACEBOOK)
        self._pg_googleplus = PgDictionaryDao(pg_connection, T_GOOGLE_PLUS)
        self._pg_linkedin = PgDictionaryDao(pg_connection, T_LINKEDIN)
        self._pg_news = PgDictionaryDao(pg_connection, T_NEWS)
        self._pg_twitter = PgDictionaryDao(pg_connection, T_TWITTER)

    def get_consumers(self, Consumer, channel):
        consumer = Consumer(
            queues=self.input_queues,
            no_ack=False,
            auto_declare=True,
            callbacks=[self.on_message],
            on_message=None,
            accept=['json']
        )
        consumer.qos(prefetch_count=DEFAULT_BATCH_SIZE)
        return [consumer]

    def log_decode_error(self, message, exc):
        logger.error('Exeption %s catched while decoding message %s' % (exc, message.body))

    def on_message(self, body, message):
        if not body:
            logger.warn('Empty message received')
            message.reject(requeue=False)
            return

        logger.debug('Message received from queue: %s type: %s' % (body, type(body)))
        publisher_to_postgres(
            publisher_mongo_id=str(body),
            mg_publisher=self._mg_publisher,
            mg_publisher_facebook=self._mg_publisher_facebook,
            mg_publisher_google_plus=self._mg_publisher_googleplus,
            mg_publisher_linkedin=self._mg_publisher_linkedin,
            mg_publisher_news=self._mg_publisher_news,
            mg_publisher_twitter=self._mg_publisher_twitter,
            mg_facebook=self._mg_facebook,
            mg_google_plus=self._mg_googleplus,
            mg_linkedin=self._mg_linkedin,
            mg_news=self._mg_news,
            mg_twitter=self._mg_twitter,
            pg_publisher=self._pg_publisher,
            pg_facebook=self._pg_facebook,
            pg_google_plus=self._pg_googleplus,
            pg_linkedin=self._pg_linkedin,
            pg_news=self._pg_news,
            pg_twitter=self._pg_twitter
        )
        message.ack()
