# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on Jul 11, 2014
# @author: trung

from kombu import Exchange, Queue
from kombu.entity import PERSISTENT_DELIVERY_MODE


#===============================================================================
# publisher_mongo_id that was recently inserted or updated
#===============================================================================
publisher_injected_exchange = Exchange(
    name='PublisherInjectedExchange',
    type='fanout',
    durable=True,
    auto_delete=False,
    delivery_mode=PERSISTENT_DELIVERY_MODE,
)

mongodb_merge_worker_publisher_exchange = Exchange(
    name='MongodbMergeWorker_PublisherExchange',
    type='fanout',
    durable=True,
    auto_delete=False,
    delivery_mode=PERSISTENT_DELIVERY_MODE,
)

publisher_created_exchange = Exchange(
    name='PublisherCreatedExchange',
    type='fanout',
    durable=True,
    auto_delete=False,
    delivery_mode=PERSISTENT_DELIVERY_MODE,
)

publisher_updated_exchange = Exchange(
    name='PublisherUpdatedExchange',
    type='fanout',
    durable=True,
    auto_delete=False,
    delivery_mode=PERSISTENT_DELIVERY_MODE,
)

publisher_to_postgres_queues = [
    Queue(
        name='PublisherFromMongoWatcher_MigrateInjectedPublisherToPostgres',
        exchange=publisher_injected_exchange
    ),
    Queue(
        name='PublisherFromMongoWatcher_MigrateMergedPublisherToPostgres',
        exchange=mongodb_merge_worker_publisher_exchange
    ),
    Queue(
        name='PublisherFromMongoWatcher_MigrateCreatedPublisherToPostgres',
        exchange=publisher_created_exchange
    ),
    Queue(
        name='PublisherFromMongoWatcher_MigrateUpdatedPublisherToPostgres',
        exchange=publisher_updated_exchange
    )
]


#===============================================================================
# Message ready to import to Postgres and to send to ES for index
#===============================================================================
message_exchange = Exchange(
    name='MessageExchange',
    type='fanout',
    durable=True,
    auto_delete=False,
    delivery_mode=PERSISTENT_DELIVERY_MODE,
)

message_pg_queues = [
    Queue(
        name='MessageToPgQueue',
        exchange=message_exchange
    )
]

message_es_queues = [
    Queue(
        name='MessageToEsQueue',
        exchange=message_exchange
    )
]


#===============================================================================
# RelevantDocument's IDs that was recently inserted or updated to Mongo
#===============================================================================
relevant_document_publisher_ready_exchange = Exchange(
    name='RelevantDocumentPublisherReadyExchange',
    type='fanout',
    durable=True,
    auto_delete=False,
    delivery_mode=PERSISTENT_DELIVERY_MODE,
)

relevant_document_id_queues = [
    Queue(
        name='RelevantDocumentIdQueue',
        exchange=relevant_document_publisher_ready_exchange
    )
]


#===============================================================================
# search_item's IDs that was recently inserted or updated to Postgres
#===============================================================================
search_item_created_exchange = Exchange(
    name='SearchItemCreatedExchange',
    type='fanout',
    durable=True,
    auto_delete=False,
    delivery_mode=PERSISTENT_DELIVERY_MODE,
)

search_item_updated_exchange = Exchange(
    name='SearchItemUpdatedExchange',
    type='fanout',
    durable=True,
    auto_delete=False,
    delivery_mode=PERSISTENT_DELIVERY_MODE,
)

search_item_to_mongo_publisher_queues = [
    Queue(
        name='SearchItemToMongoPublisherWorker_TransformCreatedSearchItem',
        exchange=search_item_created_exchange
    ),
    Queue(
        name='SearchItemToMongoPublisherWorker_TransformUpdatedSearchItem',
        exchange=search_item_updated_exchange
    )
]

#===============================================================================
# Twitter's user_id that was recently inserted or updated
#===============================================================================
twitter_user_id_to_crawl_exchange = Exchange(
    name='PublisherTwitterOplogWorker_PublisherExchange',
    type='fanout',
    durable=True,
    auto_delete=False,
    delivery_mode=PERSISTENT_DELIVERY_MODE,
)
