# Copyright (C) 2015 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on 19/05/2015
# @author: trung

from datetime import datetime
from kombu.common import maybe_declare
from kombu.log import get_logger
from kombu.mixins import ConsumerMixin
from kombu.pools import producers
from psycopg2.extras import RealDictCursor
from time import sleep
from urlparse import urlparse

from constant import DEFAULT_BATCH_SIZE
from constant.postgres_namespace import T_CATEGORY, T_NEWS, \
    T_PUBLISHER_FROM_MONGO
from etl.mongo_to_postgres import *
from model.postgres_dao import CategoryDao, DictionaryDao
from workers.queue import publisher_created_exchange, \
    publisher_updated_exchange, twitter_user_id_to_crawl_exchange

logger = get_logger(__name__)


def get_existed_mg_publisher_by_item_id(pg_connection, search_item_id):
    """Get MgPublisher from Postgres by the linked ``item_id``

    :param search_item_id:
    :return: Dictionary of MgPublisher
    """
    result = None
    with pg_connection.cursor() as cursor:
        query = '''
SELECT object_payload
  FROM mongo.mg_publisher
  WHERE (object_payload->>'itemId')::int = %s'''
        query = cursor.mogrify(query, (search_item_id, ))
        logger.debug('Executing %s' % query)
        cursor.execute(query)
        pg_connection.commit()
        result = cursor.fetchone()
    return result[0] if result else None


def get_search_item_with_sns_info_by_id(pg_connection, search_item_id):
    """Get all SearchItem fields from Postgres to a dictionary, including its
    SNS IDs

    :param search_item_id:
    :return: Dictionary of SearchItem
    """
    with pg_connection.cursor(cursor_factory=RealDictCursor) as cursor:
        query = '''
SELECT
    *,
    (
        SELECT array_agg(sns_id)
          FROM public.item_sns
          WHERE item_id = %(search_item_id)s
            AND status = %(status_active)s
            AND sns_name = 'blog'
    ) AS blog_url,
    (
        SELECT array_agg(sns_id)
          FROM public.item_sns
          WHERE item_id = %(search_item_id)s
            AND status = %(status_active)s
            AND sns_name = 'rss'
    ) AS rss_url,
    (
        SELECT array_agg(sns_id)
          FROM public.item_sns
          WHERE item_id = %(search_item_id)s
            AND status = %(status_active)s
            AND sns_name = 'tw'
    ) AS twitter_id
  FROM public.item i
  INNER JOIN public.item_audit ia USING (id)
  WHERE id = %(search_item_id)s
    AND itemtype IN ('52d4bd98e4b08a17cb0aa9c0', '52d4bd98e4b08a17cb0aa9bf') -- O & P''' % ({
            'search_item_id': search_item_id,
            'status_active': STATUS_ACTIVE
        })
        logger.debug('Executing %s' % query)
        cursor.execute(query)
        pg_connection.commit()
        result = cursor.fetchone()
    return result


def make_mg_publisher_from_pg_search_item(search_item_dict, category_dict):
    mg_publisher = dict(
        active=search_item_dict.get('publisher_status') > STATUS_INACTIVE,
        address=search_item_dict.get('address'),
        categories=make_publisher_category(search_item_dict, category_dict),
        countryCode=search_item_dict.get('isocode'),
        description=search_item_dict.get('about_us') or search_item_dict.get(
            'detail'),
        email=search_item_dict.get('email'),
        extras=dict(
            itemKey=search_item_dict.get('id')
        ),
        image=search_item_dict.get('photo'),
        itemId=search_item_dict.get('id'),
        name=search_item_dict.get('name'),
        newsAggregator=0,
        score=dict(),
        status=make_publisher_mg_status(
            search_item_dict.get('publisher_status')),
        tracking=dict(
            auditAuthorId=search_item_dict.get('analyst_id'),
            source='SearchItem from Postgres',
            updatedAt=search_item_dict.get('updated').isoformat()
        )
    )
    empty_string_to_none(mg_publisher['tracking'])
    empty_string_to_none(mg_publisher)
    return mg_publisher


def make_publisher_category(item_dict, category_dict):
    categories = []

    itemtype = category_dict.get(item_dict.get('itemtype'))
    if itemtype:
        categories.append(dict(
            _id=itemtype.get('category_mongo_id'),
            level=itemtype.get('category_level'),
            name=itemtype.get('category_name'),
            numericId=itemtype.get('category_id'),
            parentNumericId=itemtype.get('parent_category_id'),
            parentId=itemtype.get('parent_category_mongo_id')
        ))

    itemgroup = category_dict.get(item_dict.get('itemgroup'))
    if itemgroup:
        categories.append(dict(
            _id=itemgroup.get('category_mongo_id'),
            level=itemgroup.get('category_level'),
            name=itemgroup.get('category_name'),
            numericId=itemgroup.get('category_id'),
            parentNumericId=itemgroup.get('parent_category_id'),
            parentId=itemgroup.get('parent_category_mongo_id')
        ))

    cat1 = category_dict.get(item_dict.get('cat1'))
    if cat1:
        categories.append(dict(
            _id=cat1.get('category_mongo_id'),
            level=cat1.get('category_level'),
            name=cat1.get('category_name'),
            numericId=cat1.get('category_id'),
            parentNumericId=cat1.get('parent_category_id'),
            parentId=cat1.get('parent_category_mongo_id')
        ))

    cat2 = category_dict.get(item_dict.get('cat2'))
    if cat2:
        categories.append(dict(
            _id=cat2.get('category_mongo_id'),
            level=cat2.get('category_level'),
            name=cat2.get('category_name'),
            numericId=cat2.get('category_id'),
            parentNumericId=cat2.get('parent_category_id'),
            parentId=cat2.get('parent_category_mongo_id')
        ))

    cat3 = category_dict.get(item_dict.get('cat3'))
    if cat3:
        categories.append(dict(
            _id=cat3.get('category_mongo_id'),
            level=cat3.get('category_level'),
            name=cat3.get('category_name'),
            numericId=cat3.get('category_id'),
            parentNumericId=cat3.get('parent_category_id'),
            parentId=cat3.get('parent_category_mongo_id')
        ))

    cat4 = category_dict.get(item_dict.get('cat4'))
    if cat4:
        categories.append(dict(
            _id=cat4.get('category_mongo_id'),
            level=cat4.get('category_level'),
            name=cat4.get('category_name'),
            numericId=cat4.get('category_id'),
            parentNumericId=cat4.get('parent_category_id'),
            parentId=cat4.get('parent_category_mongo_id')
        ))

    cat5 = category_dict.get(item_dict.get('cat5'))
    if cat5:
        categories.append(dict(
            _id=cat5.get('category_mongo_id'),
            level=cat5.get('category_level'),
            name=cat5.get('category_name'),
            numericId=cat5.get('category_id'),
            parentNumericId=cat5.get('parent_category_id'),
            parentId=cat5.get('parent_category_mongo_id')
        ))

    cat6 = category_dict.get(item_dict.get('cat6'))
    if cat6:
        categories.append(dict(
            _id=cat6.get('category_mongo_id'),
            level=cat6.get('category_level'),
            name=cat6.get('category_name'),
            numericId=cat6.get('category_id'),
            parentNumericId=cat6.get('parent_category_id'),
            parentId=cat6.get('parent_category_mongo_id')
        ))

    return categories


def make_publisher_mg_status(pg_status):
    if pg_status == STATUS_ACTIVE:
        return 2
    elif pg_status == STATUS_NEW:
        return 0
    elif pg_status == STATUS_HIDED:
        return 3
    elif pg_status == STATUS_MACHINE_AUDITED or pg_status == STATUS_INACTIVE:
        return 1
    elif pg_status is None:
        return None
    else:
        raise ValueError('Unknown Publisher status %s' % pg_status)


def upsert_mg_blog(pg_conn, publisher_mongo_id, blog_list):
    """Upsert MgNews with blog's urls

    :param pg_conn:
    :param publisher_mongo_id:
    :param blog_list:
    :return:
    """
    if not publisher_mongo_id or not blog_list:
        logger.info('publisher_mongo_id, blog_list can\'t be None or empty => DO NOTHING')
        return

    blog_set = set(_.strip().lower() for _ in blog_list if _ and _.strip().lower() not in ('', 'null'))
    mg_news_dao = DictionaryDao(pg_conn, T_NEWS)
    with pg_conn.cursor(cursor_factory=RealDictCursor) as cursor:
        # Query for existed url(s)
        select_query = '''
SELECT
    s.sns_id,
    n.object_payload
  FROM mongo.mg_publisher_sns s
  INNER JOIN mongo.mg_news n ON (s.sns_id = n.object_id)
  WHERE publisher_mongo_id = %s
    AND sns_name = 'news'
    AND (
        (object_payload->'rss') is null
        OR (object_payload->>'rss') is null
        OR (object_payload->>'rss') = '[]')'''
        select_query = cursor.mogrify(select_query, (publisher_mongo_id, ))
        logger.debug('Executing %s' % select_query)
        cursor.execute(select_query)
        pg_conn.commit()
        existed_blog_url = set([_['object_payload'].get('url') for _ in cursor])

        # Calculate diff. between 2 sets
        logger.info('List of blog urls: %s' % blog_set)
        logger.info('List of existed blog urls: %s' % existed_blog_url)
        blog_url_to_delete = existed_blog_url - blog_set
        blog_url_to_insert = blog_set - existed_blog_url

        # Delete the existed one(s) that are not in the new blog_url list
        if blog_url_to_delete:
            blog_url_to_delete = set(blog_url_to_delete)
            logger.info('List of blog urls to delete: %s' % blog_url_to_delete)
            delete_query = '''
DELETE
  FROM mongo.mg_news
  WHERE object_id IN %s;
DELETE
  FROM mongo.mg_publisher_sns s
  WHERE publisher_mongo_id = %s
    AND sns_name = 'news'
    AND sns_id IN %s'''
            delete_query = cursor.mogrify(delete_query, (tuple(blog_url_to_delete), publisher_mongo_id, tuple(blog_url_to_delete)))
            logger.debug('Executing %s' % delete_query)
            cursor.execute(delete_query)
            pg_conn.commit()
            logger.info('Deleted %s row(s)' % cursor.rowcount)

        # Insert the new one(s)
        if blog_url_to_insert:
            blog_url_to_insert = set(blog_url_to_insert)
            logger.info('List of blog urls to insert: %s' % blog_url_to_insert)
            # Insert into the table mg_news
            for blog_url in blog_url_to_insert:
                news_id = str(ObjectId())
                mg_news_dao.insert(a_dict={'_id': news_id, 'url': blog_url, 'rss': None})
            # Insert the link
            insert_query = '''
INSERT INTO mongo.mg_publisher_sns (publisher_mongo_id, sns_name, sns_id)
VALUES (%s, 'news', %s)'''
            cursor.executemany(insert_query, [(publisher_mongo_id, _.strip()) for _ in blog_url_to_insert])
            pg_conn.commit()
            logger.debug('Executed %s' % cursor.query)


def upsert_mg_rss(pg_conn, publisher_mongo_id, rss_list, publisher_url=None):
    """Upsert MgNews with RSS urls

    :param pg_conn:
    :param publisher_mongo_id:
    :param rss_list:
    :param publisher_url:
    :return:
    """
    if not publisher_mongo_id or not rss_list:
        logger.info('publisher_mongo_id, rss_list can\'t be None or empty => DO NOTHING')
        return

    rss_set = set([_.strip().lower() for _ in rss_list if _ and _.strip().lower() not in ('', 'null')])
    if not rss_set:
        return

    mg_news_dao = DictionaryDao(pg_conn, T_NEWS)
    with pg_conn.cursor() as cursor:
        # Query for existed RSS feed(s)
        select_query = '''
SELECT array_agg(sns_id)
  FROM mongo.mg_publisher_sns s
  INNER JOIN mongo.mg_news n ON (s.sns_id = n.object_id)
  WHERE publisher_mongo_id = %s
    AND sns_name = 'news'
    AND (object_payload->>'rss') != '[]' '''
        select_query = cursor.mogrify(select_query, (publisher_mongo_id, ))
        logger.debug('Executing %s' % select_query)
        cursor.execute(select_query)
        pg_conn.commit()
        result = cursor.fetchone()
        existed_rss_sns_id = set(result[0]) if result and result[0] else set()


        # Delete all the existed one(s)
        if existed_rss_sns_id:
            logger.info('List of rss sns_id to delete: %s' % existed_rss_sns_id)
            existed_rss_sns_id = tuple(existed_rss_sns_id)
            delete_query = '''
DELETE
  FROM mongo.mg_news
  WHERE object_id IN %s;
DELETE
  FROM mongo.mg_publisher_sns s
  WHERE publisher_mongo_id = %s
    AND sns_name = 'news'
    AND sns_id IN %s;'''
            delete_query = cursor.mogrify(delete_query, (tuple(existed_rss_sns_id), publisher_mongo_id, tuple(existed_rss_sns_id)))
            logger.debug('Executing %s' % delete_query)
            cursor.execute(delete_query)
            pg_conn.commit()

        # Insert the new one(s)
        logger.info('List of rss urls to insert: %s' % rss_set)
        # Insert into the table mg_news
        url = publisher_url.strip() if publisher_url and publisher_url.strip() else \
            urlparse(rss_list[0]).netloc

        news_id = str(ObjectId())
        mg_news_dao.insert(a_dict={'_id': news_id, 'url': url.strip().lower(), 'rss': list(rss_set)})
        # Insert the link
        insert_query = '''
INSERT INTO mongo.mg_publisher_sns (publisher_mongo_id, sns_name, sns_id)
  VALUES (%s, 'news', %s)'''
        insert_query = cursor.mogrify(insert_query, (publisher_mongo_id, news_id))
        logger.debug('Executing %s' % insert_query)
        cursor.execute(insert_query)
        pg_conn.commit()


def upsert_mg_twitter(pg_conn, publisher_mongo_id, twitter_id_list):
    """Upsert MgTwitter with blog's urls

    :param pg_conn:
    :param publisher_mongo_id:
    :param blog_list:
    :return:
    """
    if not publisher_mongo_id or not twitter_id_list:
        logger.info('publisher_mongo_id, twitter_id_list can\'t be None or empty => DO NOTHING')
        return

    twitter_id_set = set([_.strip().lower() for _ in twitter_id_list if _ and _.strip().lower() not in ('', 'null')])
    with pg_conn.cursor() as cursor:
        # Query for existed twitter_id
        select_query = '''
SELECT array_agg(sns_id)
  FROM mongo.mg_publisher_sns
  WHERE publisher_mongo_id = %s
    AND sns_name = 'tw' '''
        select_query = cursor.mogrify(select_query, (publisher_mongo_id, ))
        logger.debug('Executing %s' % select_query)
        cursor.execute(select_query)
        pg_conn.commit()
        result = cursor.fetchone()
        existed_twitter_id = set(result[0]) if result and result[0] else set()

        # Calculate diff. between 2 sets
        twitter_id_to_delete = existed_twitter_id - twitter_id_set
        twitter_id_to_insert = twitter_id_set - existed_twitter_id

        # Delete the existed one(s) that are not in the new blog_url list
        if twitter_id_to_delete:
            logger.info('List of twitter_id to delete: %s' % twitter_id_to_delete)
            delete_query = '''
DELETE
  FROM mongo.mg_publisher_sns s
  WHERE publisher_mongo_id = %s
    AND sns_name = 'tw'
    AND sns_id IN %s'''
            delete_query = cursor.mogrify(delete_query, (publisher_mongo_id, tuple(twitter_id_to_delete)))
            logger.debug('Executing %s' % delete_query)
            cursor.execute(delete_query)
            pg_conn.commit()
            logger.info('Deleted %s row(s)' % cursor.rowcount)

        # Insert the new one(s)
        if twitter_id_to_insert:
            logger.info('List of twitter_id to insert: %s' % twitter_id_to_insert)
            # Insert the link
            insert_query = '''
INSERT INTO mongo.mg_publisher_sns (publisher_mongo_id, sns_name, sns_id)
  VALUES (%s, 'tw', %s)'''
            cursor.executemany(insert_query, [(publisher_mongo_id, _.strip()) for _ in twitter_id_to_insert])
            pg_conn.commit()
            logger.debug('Executed %s' % cursor.query)


class SearchItemToPublisherWorker(ConsumerMixin):
    """Consumer that fetchs IDs of changed SearchItem from queue, get the full
    SearchItem info, convert to MgPublisher and then import to Postgres
    """

    def __init__(self, amqp_connection, input_queues, pg_connection):
        self.connection = amqp_connection
        self.input_queues = input_queues
        self.pg_conn = pg_connection
        category_dao = CategoryDao(pg_connection, T_CATEGORY)
        self.pg_publisher_dao = DictionaryDao(pg_connection, T_PUBLISHER_FROM_MONGO)
        logger.info('Loading Category hierarchy into memory...')
        self._category_dict = category_dao.get_category_dict_with_mongo_id_key()
        self._producer = producers[amqp_connection].acquire(block=True)
        maybe_declare(publisher_created_exchange, self._producer.channel)
        maybe_declare(publisher_updated_exchange, self._producer.channel)
        maybe_declare(twitter_user_id_to_crawl_exchange, self._producer.channel)

    def _post_publisher_mongo_id_created(self, publisher_mongo_id):
        if not publisher_mongo_id:
            return

        logger.info('Posting %s to %s' % (str(publisher_mongo_id), publisher_created_exchange.name))
        self._producer.publish(str(publisher_mongo_id), exchange=publisher_created_exchange, retry=True)

    def _post_publisher_mongo_id_updated(self, publisher_mongo_id):
        if not publisher_mongo_id:
            return

        logger.info('Posting %s to %s' % (str(publisher_mongo_id), publisher_updated_exchange.name))
        self._producer.publish(str(publisher_mongo_id), exchange=publisher_updated_exchange, retry=True)

    def _post_twitter_user_id_list_to_crawl(self, user_id_list):
        if not user_id_list:
            return

        for each in user_id_list:
            if each:
                logger.info('Posting %s to %s' % (str(each), twitter_user_id_to_crawl_exchange.name))
                self._producer.publish(str(each), exchange=twitter_user_id_to_crawl_exchange, retry=True)

    def _upsert_mg_publisher_from_search_item(self, search_item):
        search_item_id = search_item.get('id')
        mg_publisher = make_mg_publisher_from_pg_search_item(search_item, self._category_dict)
        logger.debug('SearchItem %s is transformed to Publisher %s' % (search_item_id, mg_publisher))
        mg_existed_publisher = get_existed_mg_publisher_by_item_id(self.pg_conn, search_item_id)
        if not mg_existed_publisher:
            logger.info('Publisher with itemId %s not found, inserting...' % search_item_id)
            # Insert into Publisher
            publisher_mongo_id = str(ObjectId())
            mg_publisher['_id'] = publisher_mongo_id
            mg_publisher['releasedAt'] = datetime.now().isoformat()  # Set releasedAt to now() for new Publisher
            self.pg_publisher_dao.insert(mg_publisher)
            # Insert into PublisherTwitter
            if search_item.get('twitter_id'):
                upsert_mg_twitter(self.pg_conn, publisher_mongo_id, search_item.get('twitter_id'))
            # Insert into PublisherNews and News
            if search_item.get('blog_url'):
                upsert_mg_blog(self.pg_conn, publisher_mongo_id, search_item.get('blog_url'))
            if search_item.get('rss_url'):
                upsert_mg_rss(self.pg_conn, publisher_mongo_id, search_item.get('rss_url'), search_item.get('www'))
            # Post ``publisher_mongo_id`` to AMQP
            self._post_publisher_mongo_id_created(publisher_mongo_id)
        else:
            logger.debug('Publisher with itemId %s found' % search_item_id)
            publisher_mongo_id = mg_existed_publisher.get('_id')
            for k in mg_publisher:
                if mg_publisher[k] is not None:
                    if isinstance(mg_publisher[k], dict):
                        mg_existed_publisher[k].update(mg_publisher[k])
                    else:
                        mg_existed_publisher[k] = mg_publisher[k]
            logger.debug('Merge Publishers with itemId %s and obtain %s' % (
            search_item_id, mg_existed_publisher))
            logger.info('Updating Publisher with itemId %s...' % search_item_id)
            # Update Publisher
            self.pg_publisher_dao.update(mg_existed_publisher)
            # Update PublisherTwitter
            if search_item.get('twitter_id'):
                upsert_mg_twitter(self.pg_conn, publisher_mongo_id, search_item.get('twitter_id'))
            # Update PublisherNews
            if search_item.get('blog_url'):
                upsert_mg_blog(self.pg_conn, publisher_mongo_id, search_item.get('blog_url'))
            if search_item.get('rss_url'):
                upsert_mg_rss(self.pg_conn, publisher_mongo_id, search_item.get('rss_url'), search_item.get('www'))
            # Post ``publisher_mongo_id`` to AMQP
            self._post_publisher_mongo_id_updated(publisher_mongo_id)
        # Post all Twitter's user_id to crawl
        self._post_twitter_user_id_list_to_crawl(search_item.get('twitter_id'))

    def get_consumers(self, Consumer, channel):
        consumer = Consumer(
            queues=self.input_queues,
            no_ack=False,
            auto_declare=True,
            callbacks=[self.on_message],
            on_message=None,
            accept=['json']
        )
        consumer.qos(prefetch_count=DEFAULT_BATCH_SIZE)
        return [consumer]

    def log_decode_error(self, message, exc):
        logger.error('Exeption %s catched while decoding messasge %s' % (
            exc, message.body))

    def on_message(self, body, message):
        if not body or body == 'null':
            logger.warning('Empty message received')
            message.reject(requeue=False)
            return

        logger.debug('Message received from queue: %s' % body)
        search_item_id = int(body)
        sleep(5)  # TODO: remove this when Frontend team fix the transaction issue
        search_item = get_search_item_with_sns_info_by_id(self.pg_conn, search_item_id)
        logger.debug('SearchItem %s found from Postgres: %s' % (search_item_id, search_item))
        if not search_item or not isinstance(search_item, dict):
            logger.warning('Returned SearchItem %s not valid' % search_item_id)
            message.reject(requeue=False)
            return

        self._upsert_mg_publisher_from_search_item(search_item)
        message.ack()
