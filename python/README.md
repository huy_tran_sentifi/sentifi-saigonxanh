# Sentifi Saigonxanh

**DEPRECATED** repo for Data Management tools that was used before Publisher ID Migration.
For now, it's used to track SQL, CQL DDLs only.

## Overview Diagram

## Getting Started

### Overview

* Development code will be deployed in the developer's account on development
server.
* Production code will be deployed with `sentifi`'s account on production
server.
* All of the services in development will be managed manually by developers.
`Supervisord` manages production services ONLY.
* [Fabric](http://www.fabfile.org/) is used to automate deployment.

Available commands:

```
$ fab -l
Available commands:

    deploy           executes sequentially: `update_code`, `update_env` and then `restart` all
    init_env         initialize the virtualenv for the project
    push_cron        copy the crontab file to cron's cron.d
    restart          restart production services
    set_credentials  ask for required credentials and store it as template for a specific host
    setup            setup all system packages required and virtualenvwrapper
    update_code      update code on the remote host
    update_env       update environment on the remote host by installing all new dependencies
```

### Prerequisites

Ubuntu 14.04 LTS

### Installing

#### Init Development On Local Machine

You can make use of `fabfile.py` to quickly initialize a Development on `localhost`.

Clone the code repo

```
git clone git@bitbucket.org:sentifidev/sentifi-saigonxanh.git
```

Install `Fabric` and `fabtools` and then run tasks with `localhost` as host string

```
pip install fabric fabtools
fab setup
fab init_env
```

#### Deploy On Remote Server

On a fresh new server, an user with `sudo` privilege will need to setup all of
system packages, python global packages that are required. I.e.:

```
cd <path_to_code_repo>
fab setup
```

Developer can initialize his/her `virtualenv` on development server with

```
fab init_env
```

This will put all of the virtual environments, projects's code and log under
`$HOME/virtualenvs`.

Later on, deployment on development environment can be done by using
`update_code`, `update_env` and/or `deploy`

```
fab deploy
```

The deployment on production will be done via git checkout tag

```
fab deploy:prod
```

__Tips__: To prevent typing password and/or full hostname each time you need to
deploy, create a ssh config file in `~/.ssh/config`.

```
Host backend-dev-1
    Hostname backend-dev-1.ireland.sentifi.internal
    IdentityFile <path-to-your-key>
    Port 22
    User <your-username>
```

### Tests

## Authors

* [Trung Huynh](mailto: trung.huynh@sentifi.com)