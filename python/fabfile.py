# Copyright (C) 2014 Sentifi.  All rights reserved.
#
# This software is the confidential and proprietary information of
# Sentifi or one of its subsidiaries. You shall not disclose this
# confidential information and shall use it only in accordance with
# the terms of the license agreement or other applicable agreement you
# entered into with Sentifi.
#
# SENTIFI MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
# SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SENTIFI
# SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE
# AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
# DERIVATIVES.
#
# Created on May 31, 2014
# @author: trung

import os

from fabric.api import abort, cd, env, prefix, run, sudo, task
from fabric.contrib.console import confirm
from fabric.contrib.files import append
from fabric.contrib.project import rsync_project
from fabric.operations import prompt
from fabtools import files, python, require, supervisor, system
from logging import getLogger
from posixpath import join

from etc.setting_prod import AMQP_USER, CASSANDRA_USER, MYSQL_USERNAME, \
    PG_CORE_USER, PG_DA0_USER, S3_KEY_ID, S3_READ_ONLY_KEY_ID


env.use_ssh_config = True
logger = getLogger(__name__)

ACCEPTED_ENVIRONMENT = ['dev', 'prod']
PRODUCTION_DEPLOY_USER = 'sentifi'
PROJECT_NAME = 'sentifi-saigonxanh'
VIRTUALENV_ROOT = 'virtualenvs'

# Paths
CREDENTIALS_DIR = join('/root', PROJECT_NAME)
CODE_DIR = 'python'
LOG_DIR = 'log'
PRODUCTION_DEPLOY_ROOT = '/srv'


def _create_virtualenv_dev():
    """creates a new Development virtualenv using virtualenvwrapper and
    $PROJECT_NAME inside developer's home
    """
    workon_home = join('/home', env.user, VIRTUALENV_ROOT)
    require.directory(workon_home, mode=755)

    venv_dir = join(workon_home, PROJECT_NAME)
    bashrc_config = [
        'WORKON_HOME=%s' % workon_home,
        'source `which virtualenvwrapper.sh`'
    ]
    append(join('/home', env.user, '.bashrc'), bashrc_config)
    if not python.virtualenv_exists(venv_dir):
        run('WORKON_HOME=%s && source `which virtualenvwrapper.sh` && mkvirtualenv %s' % (workon_home, PROJECT_NAME))

    with prefix('WORKON_HOME=%s && source `which virtualenvwrapper.sh` && workon %s' % (workon_home, PROJECT_NAME)):
            run('python -V')


def _create_virtualenv_prod():
    """creates a new Production virtualenv using virtualenvwrapper and
    $PROJECT_NAME at $PRODUCTION_DEPLOY_ROOT
    """
    workon_home = join('/home', PRODUCTION_DEPLOY_USER, VIRTUALENV_ROOT)
    require.directory(workon_home, use_sudo=True, owner=PRODUCTION_DEPLOY_USER, group=PRODUCTION_DEPLOY_USER, mode=755)

    venv_dir = join(workon_home, PROJECT_NAME)
    bashrc_config = [
        'WORKON_HOME=%s' % workon_home,
        'source `which virtualenvwrapper.sh`'
    ]
    append(join('/home', PRODUCTION_DEPLOY_USER, '.bashrc'), bashrc_config, use_sudo=True)
    if not files.is_file(join(venv_dir, 'bin', 'python'), use_sudo=True):
        sudo('WORKON_HOME=%s && source `which virtualenvwrapper.sh` && mkvirtualenv %s' % (workon_home, PROJECT_NAME), user=PRODUCTION_DEPLOY_USER)

    sudo('WORKON_HOME=%s && source `which virtualenvwrapper.sh` && workon %s && python -V' % (workon_home, PROJECT_NAME), user=PRODUCTION_DEPLOY_USER)


def _create_directories_dev():
    """creates required directories for Development environment
    """
    venv_dir = join('/home', env.user, VIRTUALENV_ROOT, PROJECT_NAME)
    # Create log folder
    require.directory(join(venv_dir, LOG_DIR), mode=755)


def _create_directories_prod():
    """creates required directories for Development environment
    """
    venv_dir = join('/home', PRODUCTION_DEPLOY_USER, VIRTUALENV_ROOT, PROJECT_NAME)
    # Create log folder
    require.directory(join(venv_dir, LOG_DIR), use_sudo=True, owner=PRODUCTION_DEPLOY_USER, group=PRODUCTION_DEPLOY_USER, mode=755)


def _ensure_credentials(env_type='dev'):
    """ensures that the project has all required credentials
    """
    if env_type.lower() == 'dev':
        user = env.user
    elif env_type.lower() == 'prod':
        user = PRODUCTION_DEPLOY_USER
    else:
        abort('Environment %s is invalid' % env_type)

    destination = join('/home', user, VIRTUALENV_ROOT, PROJECT_NAME, 'bin')
    files.copy(source=join(CREDENTIALS_DIR, 'postactivate'), destination=destination, use_sudo=True)
    files.copy(source=join(CREDENTIALS_DIR, 'predeactivate'), destination=destination, use_sudo=True)
    sudo('chown -R %s:%s %s' % (user, user, destination))
    sudo('chmod 600 %s %s' % (join(destination, 'postactivate'), join(destination, 'predeactivate')))
    append(join(destination, 'postactivate'), 'export SETTING_FILE=$VIRTUAL_ENV/python/sentifi/etc/setting_%s.py' % env_type, use_sudo=True)


def _ensure_system_packages():
    """ensures that the system has all required packages
    """
    if system.distrib_family() == 'debian':
        require.deb.packages([
            'build-essential',
            'git',
            'htop',
            'iftop',
            'libffi-dev',  # required to patch Heartbleed vulnerability
            'libpq-dev',  # required for psycopg2
            'libssl-dev',  # required to patch Heartbleed vulnerability
            'python-dev',
            'python-pip',
            'python-setuptools',
            'supervisor',
            'sysstat',
            'tmux'
        ], update=True)
    else:
        abort('Distribution family %s is not supported yet!' % system.distrib_family())


def _ensure_virtualenvwrapper():
    """ensures that virtualenvwrapper is installed at global level
    """
    if not python.is_installed('virtualenvwrapper'):
        python.install('virtualenvwrapper', use_sudo=True)


def _push_supervisor_conf():
    """pushes a symlink of program's configuration file to supervisor's conf.d
    """
    filename = '%s.conf' % PROJECT_NAME
    source = join('/home', PRODUCTION_DEPLOY_USER, VIRTUALENV_ROOT, PROJECT_NAME, CODE_DIR, 'sentifi', 'etc', 'supervisor', 'conf.d', filename)
    destination = join('/etc', 'supervisor', 'conf.d', filename)
    if not files.is_link(destination, use_sudo=True):
        files.symlink(source, destination, use_sudo=True)


@task
def deploy(env_type='dev'):
    """executes sequentially: `update_code`, `update_env` and then `restart` all
    services if and only if it's Production

    :param env_type: environment type in $ACCEPTED_ENVIRONMENT
    """
    update_code(env_type)
    update_env(env_type)
    if env_type.lower() == 'prod':
        restart()


@task
def init_env(env_type='dev'):
    """initializes the virtualenv for the project

    :param env_type: environment type in $ACCEPTED_ENVIRONMENT
    """
    if env_type.lower() == 'dev':
        _create_virtualenv_dev()
        _create_directories_dev()
    elif env_type.lower() == 'prod':
        _create_virtualenv_prod()
        _create_directories_prod()
        _push_supervisor_conf()
    else:
        abort('Environment %s is invalid' % env_type)

    _ensure_credentials(env_type)
    update_code(env_type)
    update_env(env_type)


@task
def push_cron():
    """copies the crontab file to cron's cron.d
    """
    filename = PROJECT_NAME
    source = join('/home', PRODUCTION_DEPLOY_USER, VIRTUALENV_ROOT, PROJECT_NAME, CODE_DIR, 'sentifi', 'etc', 'cron.d', filename)
    destination = join('/etc', 'cron.d', filename)

    if confirm('This will REPLACE your crontab file %s on PRODUCTION. Are you sure?' % destination, default=False):
        if not files.exists(source, use_sudo=True):
            abort('''Production environment must be initialized first. Hint: try
fab init_env:prod''')
        if files.exists(destination, use_sudo=True):
            files.remove(destination, use_sudo=True)
        files.copy(source, destination, use_sudo=True)


@task
def restart():
    """restarts production services
    """
    supervisor.reload_config()


@task
def set_credentials():
    """asks for required credentials and store it as template for a specific
    host
    """
    template_dir = join(os.path.dirname(__file__), 'sentifi', 'etc', 'virtualenvwrapper')
    # Prompt for credentials
    context_dict = {}
    context_dict['AMQP_PASSWORD'] = prompt('Enter AMQP_PASSWORD for %s: ' % AMQP_USER)
    context_dict['CASSANDRA_PASSWORD'] = prompt('Enter CASSANDRA_PASSWORD for %s: ' % CASSANDRA_USER)
    context_dict['MYSQL_PASSWORD'] = prompt('Enter MYSQL_PASSWORD for %s: ' % MYSQL_USERNAME)
    context_dict['PG_CORE_PASSWORD'] = prompt('Enter PG_CORE_PASSWORD for %s: ' % PG_CORE_USER)
    context_dict['PG_DA0_PASSWORD'] = prompt('Enter PG_DA0_PASSWORD for %s: ' % PG_DA0_USER)
    context_dict['S3_READ_ONLY_SECRET_KEY'] = prompt('Enter S3_READ_ONLY_SECRET_KEY for %s: ' % S3_READ_ONLY_KEY_ID)
    context_dict['S3_SECRET_KEY'] = prompt('Enter S3_SECRET_KEY for %s: ' % S3_KEY_ID)

    if not files.is_dir(CREDENTIALS_DIR, use_sudo=True):
        require.directory(CREDENTIALS_DIR, use_sudo=True, owner='root', group='root', mode=700)

    # Upload file `postactivate`
    filename = 'postactivate.template'
    destination = join(CREDENTIALS_DIR, 'postactivate')
    files.upload_template(
        filename,
        destination,
        context=context_dict,
        template_dir=template_dir,
        use_sudo=True,
        mode=0600,
        chown=True,
        user='root'
    )

    # Upload file `predeactivate`
    filename = 'predeactivate.template'
    destination = join(CREDENTIALS_DIR, 'predeactivate')
    files.upload_template(
        filename,
        destination,
        template_dir=template_dir,
        use_sudo=True,
        mode=0600,
        chown=True,
        user='root'
    )


@task
def setup():
    """setup all system packages required and virtualenvwrapper
    """
    _ensure_system_packages()
    _ensure_virtualenvwrapper()
    if confirm('Do you want to set/update credentials for this host?'):
        set_credentials()


@task(default=True)
def update_code(env_type='dev'):
    """updates code on the remote host

    :param env_type: environment type in $ACCEPTED_ENVIRONMENT
    """
    if env_type.lower() == 'dev':
        remote_dir = join('/home', env.user, VIRTUALENV_ROOT, PROJECT_NAME)
        rsync_project(remote_dir=remote_dir, exclude=['*.pyc', '.*'], delete=True)
    elif env_type.lower() == 'prod':
        remote_dir = join('/home', PRODUCTION_DEPLOY_USER, VIRTUALENV_ROOT, PROJECT_NAME)
        code_dir = join(remote_dir, CODE_DIR)
        if files.is_dir(code_dir, use_sudo=True) or files.is_link(code_dir, use_sudo=True):
            with cd(code_dir):
                sudo('git pull', user=PRODUCTION_DEPLOY_USER)
        else:
            project_tmp = join('/tmp', PROJECT_NAME)
            if files.exists(project_tmp):
                files.remove(project_tmp, recursive=True, use_sudo=True)
            run('git clone https://bitbucket.org/sentifidev/sentifi-saigonxanh.git %s' % project_tmp)
            sudo('chown -R %s:%s %s' % (PRODUCTION_DEPLOY_USER, PRODUCTION_DEPLOY_USER, project_tmp))
            files.move(project_tmp, remote_dir, use_sudo=True)
            sudo('ln -s %s %s' % (join(remote_dir, PROJECT_NAME, CODE_DIR), code_dir), user=PRODUCTION_DEPLOY_USER)
            sudo('chown %s:%s %s' % (PRODUCTION_DEPLOY_USER, PRODUCTION_DEPLOY_USER, code_dir))
    else:
        abort('Environment %s is invalid' % env_type)


@task
def update_env(env_type='dev'):
    """updates environment on the remote host by installing all new dependencies

    :param env_type: environment type in $ACCEPTED_ENVIRONMENT
    """
    if env_type.lower() == 'dev':
        workon_home = join('/home', env.user, VIRTUALENV_ROOT)
        with prefix('WORKON_HOME=%s && source `which virtualenvwrapper.sh` && workon %s' % (workon_home, PROJECT_NAME)):
            python.install_requirements(join(workon_home, PROJECT_NAME, CODE_DIR, 'requirements.txt'), upgrade=True)
    elif env_type.lower() == 'prod':
        workon_home = join('/home', PRODUCTION_DEPLOY_USER, VIRTUALENV_ROOT)
        sudo('su sentifi -c "WORKON_HOME=%s && source `which virtualenvwrapper.sh` && workon %s && pip install --upgrade -r %s"' % (workon_home, PROJECT_NAME, join(workon_home, PROJECT_NAME, CODE_DIR, 'requirements.txt')))
    else:
        abort('Environment %s is invalid' % env_type)
