CREATE TABLE public.ga_record (
    ga_record_id    bigserial   PRIMARY KEY,
    status          smallint    NOT NULL DEFAULT 1,
    created_at      timestamp   NOT NULL DEFAULT now(),
    updated_at      timestamp   NOT NULL DEFAULT now(),
    partner_id      int         REFERENCES public.partner ON DELETE SET NULL,
    sponsor_name    text,
    ads_type        text,
    s_action        text,
    ga_record_type  smallint,
    ga_screen_id    text,
    widget          text,
    page            text,
    country         text,
    browser         text,
    device          text,
    action          text,
    count_date      date        NOT NULL,
    count_value     int         NOT NULL
);
COMMENT ON COLUMN public.ga_record.status IS '0: inactive/removed, 1: active';
COMMENT ON COLUMN public.ga_record.ga_record_type IS '1: widget, 2: screen, 3: ads';
CREATE INDEX idx_ga_record_partner_id ON public.ga_record USING btree(partner_id);
CREATE INDEX idx_ga_record_count_at ON public.ga_record USING btree(count_date);

CREATE TABLE public.ga_partner_topic_analytic (
    ga_partner_topic_analytic_id    bigserial   PRIMARY KEY,
    status                          smallint    NOT NULL DEFAULT 1,
    created_at                      timestamp   NOT NULL DEFAULT now(),
    updated_at                      timestamp   NOT NULL DEFAULT now(),
    partner_id                      int         REFERENCES public.partner ON DELETE SET NULL,
    screen_id                       int,
    widget                          text,
    page                            text,
    s_action                        text,
    from_filename                   text,
    count_date                      date        NOT NULL,
    count_value                     int         NOT NULL
);
COMMENT ON COLUMN public.ga_partner_topic_analytic.status IS '0: inactive/removed, 1: active';
CREATE INDEX idx_ga_partner_topic_analytic_partner_id ON public.ga_partner_topic_analytic USING btree(partner_id);
CREATE INDEX idx_ga_partner_topic_analytic_screen_id ON public.ga_partner_topic_analytic USING btree(screen_id);


GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, removed, staging TO dbo, dbw, root;

GRANT SELECT ON ALL TABLES IN SCHEMA public, removed, staging TO dbr;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public, removed, staging TO dbr;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public, removed, staging TO dbr;
