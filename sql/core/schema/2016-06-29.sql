CREATE TABLE public.user_device_endpoint_arn (
    user_device_endpoint_arn_id BIGSERIAL PRIMARY KEY,
    uuid UUID NOT NULL UNIQUE,
    user_id INTEGER NOT NULL,
    endpoint_arn TEXT NOT NULL,
    device_type SMALLINT NOT NULL DEFAULT 0
);
CREATE UNIQUE INDEX idx_uuid
    ON public.user_device_endpoint_arn
    USING BTREE (uuid);

GRANT USAGE ON SCHEMA mongo, public, staging TO dbo, dbw, dbr, root;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, mongo, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, mongo, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, mongo, staging TO dbo, dbw, root;

GRANT SELECT ON ALL TABLES IN SCHEMA public, mongo, staging TO dbr;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public, mongo, staging TO dbr;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public, mongo, staging TO dbr;
