CREATE TABLE public.user_subscribed_product (
    user_id         int         NOT NULL REFERENCES public.sentifi_user ON DELETE CASCADE,
    product_id      smallint    NOT NULL,
    subscribed_at   timestamp   NOT NULL DEFAULT now(),
    UNIQUE(user_id, product_id)
);
COMMENT ON COLUMN public.user_subscribed_product.product_id IS 'ID of the product that User subscribes to
1: hybrid iOs, 2: hybrid Android, 3: iOs, 4: Android, 5: Web';

GRANT USAGE ON SCHEMA mongo, public, staging TO dbo, dbw, dbr, root;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, mongo, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, mongo, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, mongo, staging TO dbo, dbw, root;

GRANT SELECT ON ALL TABLES IN SCHEMA public, mongo, staging TO dbr;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public, mongo, staging TO dbr;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public, mongo, staging TO dbr;
