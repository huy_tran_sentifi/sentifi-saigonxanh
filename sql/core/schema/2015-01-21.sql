CREATE TABLE public.exchange (
    exchange_id             serial      PRIMARY KEY,
    status                  smallint    NOT NULL DEFAULT 1,
    created_at              timestamp   NOT NULL DEFAULT now(),
    updated_at              timestamp   NOT NULL DEFAULT now(),
    exchange_name           text,
    acronym                 text,
    alternative_name        text[],
    url                     text,
    isocode                 text,
    bloomberg_name          text,
    google_finance_name     text,
    local_name              text,
    reuters_name            text,
    bloomberg_exchange      text[],
    google_finance_exchange text[],
    reuters_exchange        text[]
);
CREATE INDEX idx_exchange_bloomberg_exchange ON public.exchange USING gin(bloomberg_exchange);
CREATE INDEX idx_exchange_google_finance_exchange ON public.exchange USING gin(google_finance_exchange);
CREATE INDEX idx_exchange_reuters_exchange ON public.exchange USING gin(reuters_exchange);

CREATE TABLE public.ticker (
    ticker_id               serial      PRIMARY KEY,
    status                  smallint    NOT NULL DEFAULT 1, -- 0: inactive, 1: active, 2: new (to review due to conflict might have with table ``exchange``)
    created_at              timestamp   NOT NULL DEFAULT now(),
    updated_at              timestamp   NOT NULL DEFAULT now(),
    item_id                 int         NOT NULL REFERENCES public.item ON DELETE CASCADE,
    isin                    text,
    security_name           text,
    security_type           smallint,
    exchange_id             int, --         NOT NULL REFERENCES public.exchange ON DELETE SET NULL,
    exchange_name           text,
    bloomberg_exchange      text[],
    google_finance_exchange text[],
    reuters_exchange        text[],
    bloomberg_ticker        text[],
    google_finance_ticker   text[],
    local_ticker            text[],
    reuters_ticker          text[],
    stock_index_id          bigint[] --         NOT NULL REFERENCES public.indice ON DELETE SET NULL
);
COMMENT ON COLUMN public.ticker.status IS '0: inactive, 1: active, 2: new (to review due to conflict might have with table ``exchange``)';
CREATE INDEX idx_ticker_item_id ON public.ticker USING btree(item_id);
CREATE INDEX idx_ticker_exchange_id ON public.ticker USING btree(exchange_id);
CREATE INDEX idx_ticker_stock_index_id ON public.ticker USING gin(stock_index_id);


GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, removed, staging TO dbo, dbw, root;

GRANT SELECT ON ALL TABLES IN SCHEMA public, removed, staging TO dbr;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public, removed, staging TO dbr;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public, removed, staging TO dbr;
