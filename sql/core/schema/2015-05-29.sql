CREATE TABLE public.item_network (
    source_item_id      int         NOT NULL REFERENCES public.item ON DELETE CASCADE,
    created_at          timestamp   NOT NULL DEFAULT now(),
    destination_item_id int         NOT NULL REFERENCES public.item ON DELETE CASCADE,
    relation_label      text        NOT NULL
);
ALTER TABLE public.item_network ADD CONSTRAINT chk_item_network_item_id_loop CHECK (source_item_id != destination_item_id);
COMMENT ON COLUMN public.item_network.relation_label IS '"c": source_item_id is child of destination_item_id, "w": source_item_id wor for destination_item_id';
CREATE UNIQUE INDEX idx_item_network_source_item_id_destination_item_id ON public.item_network USING btree(source_item_id, destination_item_id);
