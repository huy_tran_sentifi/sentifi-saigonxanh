/*
 * TABLE public.country_profile
 */
CREATE TABLE IF NOT EXISTS public.country_profile(
    alpha_code_2    char(2)     PRIMARY KEY,
    alpha_code_3    char(3)     NOT NULL,
    status          smallint    NOT NULL DEFAULT 1,
    created_at      timestamp   NOT NULL DEFAULT now(),
    updated_at      timestamp   NOT NULL DEFAULT now(),
    country_name    text        NOT NULL
);
CREATE UNIQUE INDEX idx_country_profile_alpha_code_3 ON public.country_profile USING btree(alpha_code_3);
CREATE UNIQUE INDEX idx_country_profile_country_name ON public.country_profile USING btree(country_name);

/*
 * TABLE public.city_profile
 */
CREATE TABLE IF NOT EXISTS public.city_profile(
    city_id         serial      PRIMARY KEY,
    status          smallint    NOT NULL DEFAULT 1,
    created_at      timestamp   NOT NULL DEFAULT now(),
    updated_at      timestamp   NOT NULL DEFAULT now(),
    country_code    char(2)     NOT NULL REFERENCES public.country_profile ON UPDATE CASCADE,
    city_name       text        NOT NULL
);
CREATE UNIQUE INDEX idx_city_profile_country_code_city_name ON public.city_profile USING btree(country_code, city_name);

/*
 * GRANT PERMISSION
 */
GRANT USAGE ON SCHEMA mongo, public, removed, staging TO dbo, dbw, dbr, root;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;

GRANT SELECT ON ALL TABLES IN SCHEMA public, mongo, removed, staging TO dbr;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public, mongo, removed, staging TO dbr;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public, mongo, removed, staging TO dbr;

ALTER ROLE dbo SET search_path = public, mongo, removed, staging;
ALTER ROLE dbw SET search_path = public, mongo, removed, staging;
ALTER ROLE dbr SET search_path = public, mongo, removed, staging;
ALTER ROLE root SET search_path = public, mongo, removed, staging;