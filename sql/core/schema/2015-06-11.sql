CREATE TABLE public.sns_flag (
    sns_name                text        NOT NULL,
    sns_id                  text        NOT NULL,
    status                  smallint    NOT NULL DEFAULT 1,
    created_at              timestamp   NOT NULL DEFAULT now(),
    updated_at              timestamp   NOT NULL DEFAULT now(),
    updated_by_human_at     timestamp,
    flag                    text,
    UNIQUE (sns_name, sns_id, flag)
);
COMMENT ON COLUMN public.sns_flag.sns_name IS 'name of the SNS, accepted values are:
"fb": Facebook,
"gp": GooglePlus,
"li": LinkedIn,
"news": News/Blog,
"tw": Twitter';


CREATE TABLE public.sns_tag (
    sns_name                text        NOT NULL,
    sns_id                  text        NOT NULL,
    status                  smallint    NOT NULL DEFAULT 1,
    created_at              timestamp   NOT NULL DEFAULT now(),
    updated_at              timestamp   NOT NULL DEFAULT now(),
    updated_by_human_at     timestamp,
    tag_list                text[],
    UNIQUE (sns_name, sns_id)
);

CREATE TABLE public.sentifi_user (
    user_id             int         PRIMARY KEY,
    status              smallint    NOT NULL DEFAULT 1,
    created_at          timestamp   NOT NULL DEFAULT now(),
    updated_at          timestamp   NOT NULL DEFAULT now(),
    last_visit_at       timestamp,
    username            text        NOT NULL UNIQUE,
    password            text        NOT NULL,
    email               text        NOT NULL UNIQUE,
    is_superuser        smallint    NOT NULL DEFAULT 0,
    activation_key      text,
    authorization_key   text
);

CREATE TABLE public.user_profile (
    user_id             int         NOT NULL UNIQUE REFERENCES public.sentifi_user ON DELETE CASCADE,
    status              smallint    NOT NULL DEFAULT 1,
    created_at          timestamp   NOT NULL DEFAULT now(),
    updated_at          timestamp   NOT NULL DEFAULT now(),
    first_name          text,
    last_name           text,
    avatar_url          text,
    bio                 text,
    privacy_level       smallint    NOT NULL DEFAULT 1,
    country_code        text,
    profession_status   text,
    industry            text[],
    occupation          text[],
    website             text[]
);
COMMENT ON COLUMN public.user_profile.country_code IS '2 characters, uppercase conforming to ISO 3166-1 Alpha-2 code';
COMMENT ON COLUMN public.user_profile.industry IS 'industry list entered by user, the link is managed in public.user_category';
COMMENT ON COLUMN public.user_profile.occupation IS 'occupation list entered by user, the link is managed in public.user_role';
COMMENT ON COLUMN public.user_profile.privacy_level IS 'Accepted values:
1: public,
2: visible to Sentifi users,
3: private';

CREATE TABLE public.user_category (
    user_id         int         NOT NULL REFERENCES public.sentifi_user ON DELETE CASCADE,
    created_at      timestamp   NOT NULL DEFAULT now(),
    user_updated_at timestamp,
    category_id     int         NOT NULL REFERENCES public.category ON DELETE CASCADE,
    UNIQUE (user_id, category_id)
);
COMMENT ON COLUMN public.user_category.category_id IS 'category_id of the leaf category only';

CREATE TABLE public.user_contact_information (
    user_id         int         NOT NULL REFERENCES public.sentifi_user ON DELETE CASCADE,
    created_at      timestamp   NOT NULL DEFAULT now(),
    property_name   text        NOT NULL,
    property_value  text        NOT NULL
);
COMMENT ON COLUMN public.user_contact_information.property_name IS 'E.g: "email", "fax", "phone"';
CREATE INDEX idx_user_contact_information_user_id_property_name
  ON public.user_contact_information
  USING btree(user_id, property_name);

CREATE TABLE public.user_role (
    user_id             int         NOT NULL REFERENCES public.sentifi_user ON DELETE CASCADE,
    created_at          timestamp   NOT NULL DEFAULT now(),
    user_role_label     text        NOT NULL,
    organization_name   text,
    item_id             int         REFERENCES public.item ON DELETE SET NULL,
    role_name           text[],
    role_id             int[]
);
COMMENT ON COLUMN public.user_role.user_role_label IS 'label of the relationship user - role, accepted values are:
"c": current company,
"l": last company,
"p": past company,
"s": school';
COMMENT ON COLUMN public.user_role.organization_name IS 'UGC data - name of the company/organization/school referenced by this relation';
COMMENT ON COLUMN public.user_role.item_id IS 'resolved item_id from the organization_name';
COMMENT ON COLUMN public.user_role.role_name IS 'UGC data - role list of this user';
COMMENT ON COLUMN public.user_role.role_id IS 'resolved role_id list from the role_id_list';
CREATE INDEX idx_user_role_user_id_user_role_label_organization_name
  ON public.user_role
  USING btree(user_id, user_role_label, organization_name);

CREATE TABLE public.user_sns (
    user_id             int         NOT NULL REFERENCES public.sentifi_user ON DELETE CASCADE,
    status              smallint    NOT NULL DEFAULT 1,
    created_at          timestamp   NOT NULL DEFAULT now(),
    updated_at          timestamp   NOT NULL DEFAULT now(),
    sns_name            text        NOT NULL,
    sns_id              text        NOT NULL,
    application_id      text        NOT NULL,
    access_token        text,
    access_token_secret text,
    refresh_token       text,
    token_updated_at    timestamp,
    token_expired_at    timestamp,
    publisher_mongo_id  text,
    named_entity_id     int,        -- FOREIGN KEY NEEDED
    UNIQUE (user_id, sns_name, sns_id)
);
CREATE INDEX idx_user_sns_sns_name_sns_id ON public.user_sns USING btree(sns_name, sns_id);


/*
 * GRANT PERMISSION
 */
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;
GRANT SELECT ON ALL TABLES IN SCHEMA public, mongo, removed, staging TO dbr;
