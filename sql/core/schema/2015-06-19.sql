/*
 * TABLE public.company_type
 */
CREATE TABLE IF NOT EXISTS public.company_type(
    company_type_id         serial          PRIMARY KEY,
    status                  smallint        NOT NULL DEFAULT 1,
    created_at              timestamp       NOT NULL DEFAULT now(),
    updated_at              timestamp       NOT NULL DEFAULT now(),
    country                 text            NOT NULL,
    language                text            NOT NULL,
    type_name               text            NOT NULL,
    type_abbreviation       text[]          NOT NULL
);

/*
 * TABLE public.abbreviation
 */
CREATE TABLE IF NOT EXISTS public.abbreviation(
    abbreviation_id         serial      PRIMARY KEY,
    status                  smallint    NOT NULL DEFAULT 1,
    created_at              timestamp   NOT NULL DEFAULT now(),
    updated_at              timestamp   NOT NULL DEFAULT now(),
    word                    text        NOT NULL,
    abbreviation            text[]      NOT NULL
);

CREATE UNIQUE INDEX idx_abbreviation_word ON public.abbreviation(word);

/*
 * GRANT PERMISSION
 */
GRANT USAGE ON SCHEMA mongo, public, removed, staging TO dbo, dbw, dbr, root;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;

GRANT SELECT ON ALL TABLES IN SCHEMA public, mongo, removed, staging TO dbr;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public, mongo, removed, staging TO dbr;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public, mongo, removed, staging TO dbr;

ALTER ROLE dbo SET search_path = public, mongo, removed, staging;
ALTER ROLE dbw SET search_path = public, mongo, removed, staging;
ALTER ROLE dbr SET search_path = public, mongo, removed, staging;
ALTER ROLE root SET search_path = public, mongo, removed, staging;