/*
 * TABLE SCHEMA
 */
-- Publisher - Category, 1-N link
CREATE TABLE public.publisher_category (
    publisher_mongo_id  text        REFERENCES mongo.mg_publisher ON DELETE CASCADE,
    created_at          timestamp   NOT NULL DEFAULT now(),
    category_mongo_id   text        REFERENCES public.category(category_mongo_id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX idx_publisher_category_publisher_mongo_id_category_mongo_id
  ON public.publisher_category
  USING btree(publisher_mongo_id, category_mongo_id);
COMMENT ON COLUMN public.publisher_category.category_mongo_id IS 'mongo_id of the leaf category only';

-- Channels - Category, 1-N link
CREATE TABLE public.sns_category (
    sns_name            text,
    sns_id              text,
    created_at          timestamp   NOT NULL DEFAULT now(),
    updated_human_at    timestamp   NULL,
    category_id         int         REFERENCES public.category(category_id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX idx_sns_category_sns_name_sns_id_category_id
  ON public.sns_category
  USING btree(sns_name, sns_id, category_id);
COMMENT ON COLUMN public.sns_category.category_id IS 'id of the leaf category only';

-- Publisher - Role, 1-N link
CREATE TABLE public.role (
    role_id         serial      PRIMARY KEY,
    created_at      timestamp   NOT NULL DEFAULT now(),
    updated_at      timestamp   NOT NULL DEFAULT now(),
    role_key        text        NOT NULL,
    role_name       text        NOT NULL,
    detail          text,
    role_id_path    int[],
    is_hidden       boolean     NOT NULL DEFAULT FALSE
);
COMMENT ON COLUMN public.role.role_key IS 'name of the ontology class';
COMMENT ON COLUMN public.role.role_id_path IS 'list of role_id representing the path from this node to root';
CREATE UNIQUE INDEX idx_role_lower_role_key
  ON public.role
  USING btree(lower(role_key));
INSERT INTO public.role (role_id, role_key, role_name, detail)
  VALUES (0, 'Root', 'Root', 'All roles at root level must have their parent_role_id point to this id');

CREATE TABLE public.role_network (
    role_id         int         NOT NULL REFERENCES public.role ON DELETE CASCADE,
    created_at      timestamp   NOT NULL DEFAULT now(),
    parent_role_id  int         NOT NULL REFERENCES public.role ON DELETE SET NULL
);
ALTER TABLE public.role_network ADD CONSTRAINT chk_role_network_role_id_loop CHECK (role_id != parent_role_id);
CREATE UNIQUE INDEX idx_role_network_role_id_parent_role_id ON public.role_network USING btree(role_id, parent_role_id);
CREATE OR REPLACE FUNCTION public.role_network_insert_update_delete_trigger()
  RETURNS TRIGGER
AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE public.role SET
            role_id_path = get_role_id_path_by_role_id(role_id),
            updated_at = now()
          WHERE role_id_path && ARRAY[OLD.role_id, OLD.parent_role_id]
            OR role_id IN (OLD.role_id, OLD.parent_role_id);
        IF NOT FOUND THEN RETURN NULL; END IF;
        RETURN OLD;
    ELSIF (TG_OP = 'INSERT') THEN
        UPDATE public.role SET
            role_id_path = get_role_id_path_by_role_id(role_id),
            updated_at = now()
          WHERE role_id_path && ARRAY[NEW.role_id, NEW.parent_role_id]
            OR role_id IN (NEW.role_id, NEW.parent_role_id);
        IF NOT FOUND THEN RETURN NULL; END IF;
        RETURN NEW;
    ELSIF (TG_OP = 'UPDATE') THEN
        UPDATE public.role SET
            role_id_path = get_role_id_path_by_role_id(role_id),
            updated_at = now()
          WHERE role_id_path && ARRAY[NEW.role_id, NEW.parent_role_id, OLD.role_id, OLD.parent_role_id]
            OR role_id IN (NEW.role_id, NEW.parent_role_id, OLD.role_id, OLD.parent_role_id);
         IF NOT FOUND THEN RETURN NULL; END IF;
         RETURN NEW;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trigger_role_network
    AFTER INSERT OR UPDATE OR DELETE ON public.role_network
    FOR EACH ROW EXECUTE PROCEDURE role_network_insert_update_delete_trigger();

CREATE TABLE public.publisher_role (
    publisher_mongo_id  text        NOT NULL REFERENCES mongo.mg_publisher ON DELETE CASCADE,
    created_at          timestamp   NOT NULL DEFAULT now(),
    role_id             int         NOT NULL REFERENCES public.role ON DELETE CASCADE
);
CREATE UNIQUE INDEX idx_publisher_role_publisher_mongo_id_role_id
  ON public.publisher_role
  USING btree(publisher_mongo_id, role_id);
COMMENT ON COLUMN public.publisher_role.role_id IS 'role_id of the leaf role only';


/*
 * STORED PROCEDURES / HELPERS
 */
CREATE OR REPLACE FUNCTION public.get_category_name_path_by_mongo_id(IN category_mongo_id text, IN delimiter text) RETURNS text AS $$
WITH RECURSIVE t(category_mongo_id, parent_category_mongo_id, category_name, category_level) AS (
    SELECT category_mongo_id, parent_category_mongo_id, category_name, category_level
      FROM category
      WHERE category_mongo_id = $1
        AND status = 1
    UNION
    SELECT c2.category_mongo_id, c2.parent_category_mongo_id, c2.category_name, c2.category_level
      FROM t c1, category c2
      WHERE c1.parent_category_mongo_id = c2.category_mongo_id
        AND c2.status = 1
)
SELECT string_agg(category_name, $2)
  FROM (
    SELECT category_name
      FROM t
      ORDER BY category_level
  ) AS foo;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION public.get_category_name_path_by_id(IN category_id int, IN delimiter text) RETURNS text AS $$
WITH RECURSIVE t(category_id, parent_category_id, category_name, category_level) AS (
    SELECT category_id, parent_category_id, category_name, category_level
      FROM category
      WHERE category_id = $1
        AND status = 1
    UNION
    SELECT c2.category_id, c2.parent_category_id, c2.category_name, c2.category_level
      FROM t c1, category c2
      WHERE c1.parent_category_id = c2.category_id
        AND c2.status = 1
)
SELECT string_agg(category_name, $2)
  FROM (
    SELECT category_name
      FROM t
      ORDER BY category_level
  ) AS foo;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION public.get_role_id_path_by_role_id(IN role_id int) RETURNS int[] AS $$
WITH RECURSIVE t(role_id, parent_role_id) AS (
    SELECT role_id, parent_role_id
      FROM role_network
      WHERE role_id = $1
    UNION
    SELECT r2.role_id, r2.parent_role_id
      FROM t r1, role_network r2
      WHERE r1.parent_role_id = r2.role_id
)
SELECT array_agg(DISTINCT role_id)
  FROM t;
$$ LANGUAGE SQL;


/*
 * MIGRATION
 */
WITH pc AS (
    SELECT object_id, json_array_elements(object_payload->'categories')
      FROM mongo.mg_publisher
)
INSERT INTO public.publisher_category(publisher_mongo_id, category_mongo_id)
    SELECT
        object_id,
        (
            SELECT foo->>'_id'
              FROM (
                SELECT json_array_elements(object_payload->'categories') AS foo
                  FROM mongo.mg_publisher p2
                  WHERE p2.object_id = p1.object_id
                    AND (object_payload->>'categories') NOT IN (NULL, '{}')
              ) bar
--              WHERE (foo->'_id') IS NOT NULL
              ORDER BY (foo->>'level')::smallint DESC
              LIMIT 1
        )
      FROM mongo.mg_publisher p1;


/*
 * GRANT PERMISSION
 */
GRANT USAGE ON SCHEMA mongo, public, removed, staging TO dbo, dbw, dbr, root;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;

GRANT SELECT ON ALL TABLES IN SCHEMA public, mongo, removed, staging TO dbr;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public, mongo, removed, staging TO dbr;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public, mongo, removed, staging TO dbr;

ALTER ROLE dbo SET search_path = public, mongo, removed, staging;
ALTER ROLE dbw SET search_path = public, mongo, removed, staging;
ALTER ROLE dbr SET search_path = public, mongo, removed, staging;
ALTER ROLE root SET search_path = public, mongo, removed, staging;
