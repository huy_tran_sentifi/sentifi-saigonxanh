CREATE TABLE public.impact_portfolio (
    portfolio_id        serial      PRIMARY KEY,
    status              smallint    NOT NULL DEFAULT 1,
    created_at          timestamp   NOT NULL DEFAULT now(),
    updated_at          timestamp   NOT NULL DEFAULT now(),
    user_id             int,
    portfolio_index     smallint    NOT NULL,
    portfolio_name      text        NOT NULL,
    alert               smallint    NOT NULL DEFAULT 1,
    impact_id           text[],
    UNIQUE(user_id) -- To remove when an User can have multiples portfolios
);
COMMENT ON TABLE public.impact_portfolio IS 'User''s impact portfolio';
COMMENT ON COLUMN public.impact_portfolio.portfolio_index IS 'order of the portfolio in frontend''s layout';
CREATE INDEX idx_impact_portfolio_impact_id ON public.impact_portfolio USING GIN(impact_id);


/*
 * GRANT PERMISSION
 */
GRANT USAGE ON SCHEMA mongo, public, staging TO dbo, dbw, dbr, root;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, mongo, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, mongo, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, mongo, staging TO dbo, dbw, root;

GRANT SELECT ON ALL TABLES IN SCHEMA public, mongo, staging TO dbr;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public, mongo, staging TO dbr;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public, mongo, staging TO dbr;

ALTER ROLE dbo SET search_path = public, mongo, staging;
ALTER ROLE dbw SET search_path = public, mongo, staging;
ALTER ROLE dbr SET search_path = public, mongo, staging;
ALTER ROLE root SET search_path = public, mongo, staging;
