CREATE TABLE public.item_portfolio (
    portfolio_id        serial     PRIMARY KEY,
    status              smallint    NOT NULL DEFAULT 1,
    created_at          timestamp   NOT NULL DEFAULT now(),
    updated_at          timestamp   NOT NULL DEFAULT now(),
    user_id             int,
    portfolio_index     smallint    NOT NULL,
    portfolio_name      text        NOT NULL,
    portfolio_type      smallint    NOT NULL DEFAULT 0,
    alert               smallint    NOT NULL DEFAULT 1,
    apply_to_all_item   smallint    NOT NULL DEFAULT 0,
    category_mongo_id   text[],
    indice_id           int[],
    isocode             text[],
    item_id             int[],
    cached_item_id      int[],
    listed_company      smallint
);
COMMENT ON COLUMN public.item_portfolio.portfolio_index IS 'order of the portfolio in frontend''s layout';
COMMENT ON COLUMN public.item_portfolio.portfolio_type IS '"0": portfolio by item_id, "1": portfolio by rules';
COMMENT ON COLUMN public.item_portfolio.apply_to_all_item IS '"0": false, "1": true';
CREATE INDEX idx_item_portfolio_user_id ON public.item_portfolio USING btree(user_id);
CREATE INDEX idx_item_portfolio_category_mongo_id ON public.item_portfolio USING gin(category_mongo_id);
CREATE INDEX idx_item_portfolio_indice_id ON public.item_portfolio USING gin(indice_id);
CREATE INDEX idx_item_portfolio_isocode ON public.item_portfolio USING gin(isocode);
CREATE INDEX idx_item_portfolio_item_id ON public.item_portfolio USING gin(item_id);
CREATE INDEX idx_item_portfolio_cached_item_id ON public.item_portfolio USING gin(cached_item_id);

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, removed, staging TO dbo, dbw, root;
GRANT SELECT ON ALL TABLES IN SCHEMA public, removed, staging TO dbr;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, removed, staging TO dbo, dbw, root;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public, removed, staging TO dbr;


INSERT INTO public.item_portfolio (
    portfolio_id,
    status,
    created_at,
    updated_at,
    user_id,
    portfolio_index,
    portfolio_name,
    alert
)
    SELECT
        id,
        status,
        created,
        COALESCE(updated, now()),
        user_id,
        weight,
        name,
        alert
      FROM public.portfolio;


-- Add item_id list
WITH tmp_portfolio AS (
    SELECT
        id, value
      FROM public.portfolio p, json_array_elements(rule::json->'ids') e
) -- SELECT replace(value::text, '"', '')::int FROM tmp_portfolio;
UPDATE public.item_portfolio ip SET
    item_id = (SELECT array_agg(DISTINCT replace(value::text, '"', '')::int) FROM tmp_portfolio WHERE tmp_portfolio.id = ip.portfolio_id GROUP BY id);


-- Add category_mongo_id list
select rule::json->'type' from portfolio where (rule::json->'type') is not null and nullif((rule::json->>'type'), '') not in ('', 'all');
select rule::json->'group' from portfolio where (rule::json->'group') is not null and nullif((rule::json->>'group'), '') not in ('', 'all');
select rule::json->'cat1' from portfolio where (rule::json->'cat1') is not null and nullif((rule::json->>'cat1'), '') not in ('', 'all');
select rule::json->'cat2' from portfolio where (rule::json->'cat2') is not null and nullif((rule::json->>'cat2'), '') not in ('', 'all');
select rule::json->'cat3' from portfolio where (rule::json->'cat3') is not null and nullif((rule::json->>'cat3'), '') not in ('', 'all');

WITH c5 AS (
    SELECT
        id,
        rule::json->>'cat3' AS cl5
      FROM public.portfolio p
      WHERE (rule::json->'cat3') IS NOT NULL
        AND (rule::json->>'cat3') NOT IN ('', 'all')
), c4 AS (
    SELECT
        id,
        rule::json->>'cat2' AS cl4
      FROM public.portfolio p
      WHERE (rule::json->'cat2') IS NOT NULL
        AND (rule::json->>'cat2') NOT IN ('', 'all')
), c3 AS (
    SELECT
        id,
        rule::json->>'cat1' AS cl3
      FROM public.portfolio p
      WHERE (rule::json->'cat1') IS NOT NULL
        AND (rule::json->>'cat1') NOT IN ('', 'all')
), c2 AS (
    SELECT
        id,
        rule::json->>'group' AS cl2
      FROM public.portfolio p
      WHERE (rule::json->'group') IS NOT NULL
        AND (rule::json->>'group') NOT IN ('', 'all')
), c1 AS (
    SELECT
        id,
        rule::json->>'type' AS cl1
      FROM public.portfolio p
      WHERE (rule::json->'type') IS NOT NULL
        AND (rule::json->>'type') NOT IN ('', 'all')
)
UPDATE public.item_portfolio ip SET
    category_mongo_id = (
      SELECT ARRAY[COALESCE(cl5, COALESCE(cl4, COALESCE(cl3, COALESCE(cl2, cl1))))]
        FROM c1
        FULL OUTER JOIN c2 USING (id)
        FULL OUTER JOIN c3 USING (id)
        FULL OUTER JOIN c4 USING (id)
        FULL OUTER JOIN c5 USING (id)
        WHERE id = ip.portfolio_id);

SELECT portfolio_id, id, category_mongo_id, rule
  FROM public.item_portfolio ip
  FULL OUTER JOIN public.portfolio p ON (ip.portfolio_id = p.id)
  WHERE ip.category_mongo_id IS NOT NULL
    AND (p.rule::json->'type') IS NOT NULL;


-- Add isocode list
select rule::json->'isocode' from portfolio where (rule::json->'isocode') is not null and nullif((rule::json->>'isocode'), '') not in ('', 'ALL');

WITH tmp_portfolio AS (
    SELECT
        id, rule::json->>'isocode' AS isocode
      FROM public.portfolio p
      WHERE (rule::json->'isocode') IS NOT NULL
        AND NULLIF((rule::json->>'isocode'), '') NOT IN ('', 'ALL')
) -- SELECT * FROM tmp_portfolio;
UPDATE public.item_portfolio ip SET
    isocode = (SELECT array_agg(DISTINCT isocode) FROM tmp_portfolio WHERE tmp_portfolio.id = ip.portfolio_id GROUP BY id);


-- Add indice_id list
select rule::json->'indices' from portfolio where (rule::json->'indices') is not null and nullif((rule::json->>'indices'), '') not in ('', '0');

WITH tmp_portfolio AS (
    SELECT
        id, rule::json->>'indices' AS indices
      FROM public.portfolio p
      WHERE (rule::json->'indices') IS NOT NULL
        AND NULLIF((rule::json->>'indices'), '') NOT IN ('', '0')
) -- SELECT * FROM tmp_portfolio;
UPDATE public.item_portfolio ip SET
    indice_id = (SELECT array_agg(DISTINCT indices::int) FROM tmp_portfolio WHERE tmp_portfolio.id = ip.portfolio_id GROUP BY id);

UPDATE public.item_portfolio SET
    portfolio_type = 1
  WHERE item_id IS NULL;


-- Update SEQUENCE
SELECT max(portfolio_id) + 1 FROM public.item_portfolio;
ALTER SEQUENCE item_portfolio_portfolio_id_seq RESTART 78663;
