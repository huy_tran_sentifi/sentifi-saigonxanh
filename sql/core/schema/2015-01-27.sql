CREATE TABLE public.expression (
    expression_id           serial      PRIMARY KEY,
    status                  smallint    NOT NULL DEFAULT 1,
    created_at              timestamp   NOT NULL DEFAULT now(),
    updated_at              timestamp   NOT NULL DEFAULT now(),
    item_id                 int         NOT NULL REFERENCES public.item ON DELETE CASCADE,
    channel                 text[]      NOT NULL,
    expression_type         text,
    expression_index        smallint,
    active_word_bag         json,
    semantic_only_word_bag  json,
    disabled_word_bag       json,
    and_word_bag            json,
    not_word_bag            json
);
COMMENT ON COLUMN public.expression.channel IS 'list of channels to apply this expression. Accepted values are:
"fb": Facebook,
"gp": GooglePlus,
"li": LinkedIn,
"news": News/Blog,
"twitter": Twitter';
COMMENT ON COLUMN public.expression.expression_type IS 'Accepted values are:
"@": mention tag,
"$": cash tag,
"k": keyword,
"n": named_entity alias';
COMMENT ON COLUMN public.expression.active_word_bag IS 'word_bag is a list of objects which can be:
  - Ticker (type 0)
    {
        "type": 0,
        "ticker_id": int,
        "word": "$BCS",
        "created_by": <0: machine, 1: human>,
        "updated_by": <0: machine, 1: human>
    }
  - Word (type 1)
    {
        "type": 1,
        "word": "$BARC",
        "created_by": <0: machine, 1: human>,
        "updated_by": <0: machine, 1: human>
    }
  - Dictionary (type 2)
    {
        "type": 2,
        "dictionary_id": 1,
        "excluded_word_list": <list of excluded vocabulary_id>,
        "created_by": <0: machine, 1: human>,
        "updated_by": <0: machine, 1: human>
    }';
COMMENT ON COLUMN public.expression.semantic_only_word_bag IS 'idem';
COMMENT ON COLUMN public.expression.disabled_word_bag IS 'idem';
COMMENT ON COLUMN public.expression.and_word_bag IS 'idem';
COMMENT ON COLUMN public.expression.not_word_bag IS 'idem';
CREATE INDEX idx_expression_item_id ON public.expression USING btree(item_id);

CREATE TABLE public.dictionary (
    dictionary_id   smallserial PRIMARY KEY,
    status          smallint    NOT NULL DEFAULT 1,
    created_at      timestamp   NOT NULL DEFAULT now(),
    updated_at      timestamp   NOT NULL DEFAULT now(),
    purpose         text,
    dictionary_name text,
    lang            text,
    detail          text
);
COMMENT ON COLUMN public.dictionary.purpose IS 'Accepted values are:
"b": Blacklist Token,
"k": Keywords Generator,
"m": Messages Ranking,
"w": Whitelist Token';

CREATE TABLE public.vocabulary (
    vocabulary_id   bigserial   PRIMARY KEY,
    status          smallint    NOT NULL DEFAULT 1,
    created_at      timestamp   NOT NULL DEFAULT now(),
    updated_at      timestamp   NOT NULL DEFAULT now(),
    dictionary_id   smallint    NOT NULL REFERENCES public.dictionary ON DELETE CASCADE,
    word            text,
    definition      text,
    score           smallint,
    note            text,
    UNIQUE (dictionary_id, word)
);

CREATE TABLE public.term (
    item_id                     int         UNIQUE NOT NULL REFERENCES public.item ON DELETE CASCADE,
    created_at                  timestamp   NOT NULL DEFAULT now(),
    updated_at                  timestamp   NOT NULL DEFAULT now(),
    active_triggering_term      json,
    disabled_triggering_term    json,
    blacklist_word_bag          json
);
COMMENT ON COLUMN public.term.active_triggering_term IS 'active_triggering_term can contain Ticker (type 0) or Word (type 1) but not Dictionary (type 2)';
COMMENT ON COLUMN public.term.disabled_triggering_term IS 'disabled_triggering_term can contain Ticker (type 0) or Word (type 1) but not Dictionary (type 2)';
COMMENT ON COLUMN public.term.blacklist_word_bag IS 'blacklist_word_bag can contain Dictionary (type 2) only';

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, removed, staging TO dbo, dbw, root;

GRANT SELECT ON ALL TABLES IN SCHEMA public, removed, staging TO dbr;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public, removed, staging TO dbr;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public, removed, staging TO dbr;
