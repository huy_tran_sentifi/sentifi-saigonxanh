ALTER TABLE mongo.mg_publisher ADD COLUMN partner_id int REFERENCES public.partner;
CREATE INDEX idx_mg_publisher_partner_id ON mongo.mg_publisher USING btree(partner_id) WHERE partner_id IS NOT NULL;
