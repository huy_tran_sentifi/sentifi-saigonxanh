CREATE SCHEMA IF NOT EXISTS mongo;


CREATE TABLE mongo.mg_publisher (
    object_id       text        PRIMARY KEY,
    created_at      timestamp   NOT NULL DEFAULT now(),
    updated_at      timestamp   NOT NULL DEFAULT now(),
    object_payload  json
);
CREATE INDEX idx_mg_publisher_item_id ON mongo.mg_publisher USING btree(((object_payload->>'itemId')::int));

CREATE TABLE mongo.mg_publisher_sns (
    publisher_mongo_id  text        REFERENCES mongo.mg_publisher ON DELETE CASCADE,
    created_at          timestamp   NOT NULL DEFAULT now(),
    updated_at          timestamp   NOT NULL DEFAULT now(),
    sns_name            text,
    sns_id              text,
    sns_tag             text[],
    item_id_excluded    int[]
);
COMMENT ON COLUMN mongo.mg_publisher_sns.sns_name IS 'name of the SNS, accepted values are:
"fb": Facebook,
"gp": GooglePlus,
"li": LinkedIn,
"news": News/Blog,
"tw": Twitter';
COMMENT ON COLUMN mongo.mg_publisher_sns.sns_tag IS 'list of tag at SNS level, must be lowercase';
CREATE INDEX idx_mg_publisher_sns_publisher_mongo_id ON mongo.mg_publisher_sns USING btree(publisher_mongo_id);
CREATE INDEX idx_mg_publisher_sns_sns_name_sns_id ON mongo.mg_publisher_sns USING btree(sns_name, sns_id);


CREATE TABLE mongo.mg_facebook (
    object_id       text        PRIMARY KEY,
    created_at      timestamp   NOT NULL DEFAULT now(),
    updated_at      timestamp   NOT NULL DEFAULT now(),
    object_payload  json
);

CREATE TABLE mongo.mg_google_plus (
    object_id       text        PRIMARY KEY,
    created_at      timestamp   NOT NULL DEFAULT now(),
    updated_at      timestamp   NOT NULL DEFAULT now(),
    object_payload  json
);

CREATE TABLE mongo.mg_linkedin (
    object_id       text        PRIMARY KEY,
    created_at      timestamp   NOT NULL DEFAULT now(),
    updated_at      timestamp   NOT NULL DEFAULT now(),
    object_payload  json
);

CREATE TABLE mongo.mg_news (
    object_id       text        PRIMARY KEY,
    created_at      timestamp   NOT NULL DEFAULT now(),
    updated_at      timestamp   NOT NULL DEFAULT now(),
    object_payload  json
);
CREATE UNIQUE INDEX idx_mg_news_object_payload_url ON mongo.mg_news USING btree((object_payload->>'url'));

CREATE TABLE mongo.mg_twitter (
    object_id       text        PRIMARY KEY,
    created_at      timestamp   NOT NULL DEFAULT now(),
    updated_at      timestamp   NOT NULL DEFAULT now(),
    object_payload  json
);
CREATE INDEX idx_mg_twitter_object_payload_id_str ON mongo.mg_twitter USING btree((object_payload->>'id_str'));
CREATE INDEX idx_mg_twitter_lower_object_payload_screen_name ON mongo.mg_twitter USING btree(lower(object_payload->>'screen_name'));


GRANT USAGE ON SCHEMA mongo TO dbo, dbw, dbr, root;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA mongo TO dbo, dbw, root;
--GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;
--GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;

GRANT SELECT ON ALL TABLES IN SCHEMA mongo TO dbr;
--GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public, mongo, removed, staging TO dbr;
--GRANT SELECT ON ALL SEQUENCES IN SCHEMA public, mongo, removed, staging TO dbr;

ALTER ROLE dbo SET search_path = public, mongo, removed, staging;
ALTER ROLE dbw SET search_path = public, mongo, removed, staging;
ALTER ROLE dbr SET search_path = public, mongo, removed, staging;
ALTER ROLE root SET search_path = public, mongo, removed, staging;
