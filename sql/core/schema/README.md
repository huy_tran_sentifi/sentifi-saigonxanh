* ``2016-05-18.sql`` Schema for UserSubscribedProduct
* ``2015-11-18.sql`` Schema for UserSetting and UserSuggestedPerson
* ``2015-09-04.sql`` Schema for ImpactPortfolio
* ``2015-07-16.sql`` Schema for item alias
* ``2015-06-29.sql`` Schema for NamedEntity
* ``2015-06-19.sql`` Schema for Company type and Word abbreviation
* ``2015-06-18.sql`` Schema for Country and City information
* ``2015-06-11.sql`` Schema for User and User* for pre-launch
* ``2015-06-08.sql`` Schema for Partner - Publisher's relationship
* ``2015-05-29.sql`` Schema for ItemNetwork
* ``2015-04-18.sql`` Schema for multi roles Publisher
* ``2015-04-13.sql`` Schema for Publisher migration (away from Mongo)
* ``2015-04-10.sql`` Schema for ItemPortfolio
* ``2015-03-30.sql`` Schema for Event (Pattern) and Impact for Staging and Production's use
* ``2015-03-11.sql`` Schema for GoogleAnalytics Records
* ``2015-01-27.sql`` Schema for Expression, Dictionary, Vocabulary and Term
* ``2015-01-21.sql`` Schema for Exchange and Ticker
* ``2015-07-02.sql`` Schema for Publisher cleanup (cleanup channels, publishers, items)
