-- scheme for item alias
CREATE TABLE public.item_alias (
    item_id             int             NOT NULL REFERENCES public.item ON DELETE CASCADE,
    status              smallint        NOT NULL DEFAULT 1,
    created_at          timestamp       NOT NULL DEFAULT now(),
    updated_at          timestamp       NOT NULL DEFAULT now(),
    alias               text            NOT NULL CHECK (trim(alias) <> '' AND trim(alias) <> 'null'),
    with_and            smallint        NOT NULL DEFAULT 1,
    source              text,
    note                text,
    PRIMARY KEY (item_id, alias)
);

CREATE UNIQUE INDEX idx_item_alias_item_id_alias ON public.item_alias(item_id, lower(trim(alias)));
COMMENT ON COLUMN public.item_alias.alias IS 'alternative name of item';
COMMENT ON COLUMN public.item_alias.with_and IS 'alias is used as keyword with protected by keywords_and or not, accepted values are:
1: protected by keywords_and
0: not protected by keywords_and';

/*
 * GRANT PERMISSION
 */
GRANT USAGE ON SCHEMA mongo, public, removed, staging TO dbo, dbw, dbr, root;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;

GRANT SELECT ON ALL TABLES IN SCHEMA public, mongo, removed, staging TO dbr;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public, mongo, removed, staging TO dbr;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public, mongo, removed, staging TO dbr;

ALTER ROLE dbo SET search_path = public, mongo, removed, staging;
ALTER ROLE dbw SET search_path = public, mongo, removed, staging;
ALTER ROLE dbr SET search_path = public, mongo, removed, staging;
ALTER ROLE root SET search_path = public, mongo, removed, staging;