CREATE EXTENSION pg_trgm;


CREATE TABLE public.named_entity (
    named_entity_id         serial      PRIMARY KEY,
    status                  smallint    NOT NULL DEFAULT 2,
    created_at              timestamp   NOT NULL DEFAULT now(),
    updated_at              timestamp   NOT NULL DEFAULT now(),
    product_released_at     timestamp,
    streaming_released_at   timestamp,
    analyst_id              int,  -- FOREIGN KEY NEEDED
    category_id             int         REFERENCES public.category ON DELETE SET NULL,
    partner_id              int         REFERENCES public.partner ON DELETE SET NULL,
    name                    text        NOT NULL,
    name_de                 text,
    shortname               text,
    legalname               text,
    legalname_de            text,
    detail                  text,
    detail_de               text,
    history                 text,
    history_de              text,
    about                   text,
    about_de                text,
    profile_url             text,
    avatar_url              text,
    country_code            text,
    website_url             text,
    is_listed_company       boolean,
    released_to             text[],
    isin                    text[],  -- TEMPORARY FOR BACKWARD COMPATIBILITY
    valor                   text[],  -- TEMPORARY FOR BACKWARD COMPATIBILITY
    ticker                  text,  -- TEMPORARY FOR BACKWARD COMPATIBILITY
    ceo                     text,  -- TEMPORARY FOR BACKWARD COMPATIBILITY
    headquarter             text,  -- TEMPORARY FOR BACKWARD COMPATIBILITY
    company_size            text   -- TEMPORARY FOR BACKWARD COMPATIBILITY
);
CREATE INDEX idx_named_entity_lower_name ON public.named_entity USING GIST(lower(name) gist_trgm_ops);
CREATE INDEX idx_named_entity_lower_legalname ON public.named_entity USING GIST(lower(legalname) gist_trgm_ops) WHERE legalname IS NOT NULL;
CREATE INDEX idx_named_entity_lower_name_de ON public.named_entity USING GIST(lower(name_de) gist_trgm_ops) WHERE name_de IS NOT NULL;


-- Relationship between named_entities
CREATE TABLE public.named_entity_network (
    source_named_entity_id      int         NOT NULL REFERENCES public.named_entity ON DELETE CASCADE,
    created_at                  timestamp   NOT NULL DEFAULT now(),
    destination_named_entity_id int         NOT NULL REFERENCES public.named_entity ON DELETE CASCADE,
    relation_label              text        NOT NULL,
    UNIQUE (source_named_entity_id, destination_named_entity_id, relation_label)
);
ALTER TABLE public.named_entity_network ADD CONSTRAINT chk_named_entity_network_named_entity_id_loop CHECK (source_named_entity_id != destination_named_entity_id);
COMMENT ON COLUMN public.named_entity_network.relation_label IS 'Accepted values:
"c": source_named_entity_id is child of destination_named_entity_id,
"w": source_named_entity_id works for destination_named_entity_id';

-- Detail information of SNS accounts
CREATE TABLE public.sns_account (
    sns_name            text        NOT NULL,
    sns_id              text        NOT NULL,
    status              smallint    NOT NULL DEFAULT 1,
    created_at          timestamp   NOT NULL DEFAULT now(),
    updated_at          timestamp   NOT NULL DEFAULT now(),
    human_updated_at    timestamp,
    named_entity_id     int         REFERENCES public.named_entity ON DELETE SET NULL,
    payload             json,
    geo_level           text,
    display_name        text,
    avatar_url          text,
    country_code        text,
    sns_tag             text[],
    item_id_excluded    int[],
    PRIMARY KEY (sns_name, sns_id)
);
ALTER TABLE public.sns_account ADD CONSTRAINT chk_sns_name CHECK (sns_name IN ('fb', 'gp', 'li', 'news', 'tw'));
ALTER TABLE public.sns_account ADD CONSTRAINT chk_geo_level CHECK (geo_level IN ('filing', 'general', 'googlefin', 'regional'));
CREATE INDEX idx_sns_account_named_entity_id ON public.sns_account USING btree(named_entity_id) WHERE named_entity_id IS NOT NULL;

-- SNS - Category, 1-N link
CREATE TABLE public.sns_category (
    sns_name            text,
    sns_id              text,
    created_at          timestamp   NOT NULL    DEFAULT now(),
    human_updated_at    timestamp,
    category_id         int         REFERENCES  public.category(category_id) ON DELETE CASCADE
);
ALTER TABLE public.sns_category
  ADD FOREIGN KEY (sns_name, sns_id)
  REFERENCES public.sns_account (sns_name, sns_id)
  ON DELETE CASCADE;
COMMENT ON COLUMN public.sns_category.category_id IS 'id of the leaf category';
CREATE UNIQUE INDEX idx_sns_category_sns_name_sns_id_category_id
  ON public.sns_category
  USING btree(sns_name, sns_id, category_id);

-- SNS - Role, 1-N link
CREATE TABLE public.sns_role (
    sns_name            text,
    sns_id              text,
    created_at          timestamp   NOT NULL    DEFAULT now(),
    human_updated_at    timestamp,
    role_id             int         REFERENCES  public.role(role_id) ON DELETE CASCADE
);
ALTER TABLE public.sns_role
  ADD FOREIGN KEY (sns_name, sns_id)
  REFERENCES public.sns_account (sns_name, sns_id)
  ON DELETE CASCADE;
COMMENT ON COLUMN public.sns_role.role_id IS 'id of the leaf role';
CREATE UNIQUE INDEX idx_sns_role_sns_name_sns_id_role_id
  ON public.sns_role
  USING btree(sns_name, sns_id, role_id);


-- SNS - Flag, 1-N link
-- This table must be off-production and serve as input for
-- public.sns_account.sns_tag via an ETL process
CREATE TABLE public.sns_flag (
    sns_name                text        NOT NULL,
    sns_id                  text        NOT NULL,
    status                  smallint    NOT NULL DEFAULT 1,
    created_at              timestamp   NOT NULL DEFAULT now(),
    updated_at              timestamp   NOT NULL DEFAULT now(),
    human_updated_at        timestamp,
    flag                    text,
    UNIQUE (sns_name, sns_id, flag)
);
COMMENT ON COLUMN public.sns_flag.sns_name IS 'name of the SNS, accepted values are:
"fb": Facebook,
"gp": GooglePlus,
"li": LinkedIn,
"news": News/Blog,
"tw": Twitter';


/*
 * GRANT PERMISSION
 */
GRANT USAGE ON SCHEMA mongo, public, removed, staging TO dbo, dbw, dbr, root;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;

GRANT SELECT ON ALL TABLES IN SCHEMA public, mongo, removed, staging TO dbr;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public, mongo, removed, staging TO dbr;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public, mongo, removed, staging TO dbr;

ALTER ROLE dbo SET search_path = public, mongo, removed, staging;
ALTER ROLE dbw SET search_path = public, mongo, removed, staging;
ALTER ROLE dbr SET search_path = public, mongo, removed, staging;
ALTER ROLE root SET search_path = public, mongo, removed, staging;
