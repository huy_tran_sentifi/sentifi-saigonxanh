/* -- Archive and drop current tables
CREATE TABLE removed.impact AS (SELECT * FROM public.impact);
CREATE TABLE removed.impact_event AS (SELECT * FROM public.impact_event);
CREATE TABLE removed.pattern AS (SELECT * FROM public.pattern);
DROP TABLE public.impact_event;
DROP TABLE public.impact;
DROP TABLE public.pattern;
*/


-- Create new schema and tables
CREATE SCHEMA staging;

-- Create tables for staging's data
-- This table is to remove
CREATE TABLE staging.stg_impact (
    impact_id   text        PRIMARY KEY,
    status      smallint    NOT NULL DEFAULT 1,
    created_at  timestamp   NOT NULL DEFAULT now(),
    updated_at  timestamp   NOT NULL DEFAULT now(),
    impact_name text        NOT NULL,
    itemgroup   text
);

-- This table is to remove
CREATE TABLE staging.stg_pattern (
  pattern_id        int         PRIMARY KEY,
  status            smallint    NOT NULL DEFAULT 1,
  created_at        timestamp   NOT NULL DEFAULT now(),
  updated_at        timestamp   NOT NULL DEFAULT now(),
  category_mongo_id text, -- ``mongo_id`` of the deepest category level. Its parents can be deduced by using function ``get_parent_category_mongo_id_list(category_mongo_id)`` or ``get_parent_category_mongo_id_by_level(category_mongo_id, category_level)``
  pattern_class     text,
  pattern_name      text,
  pattern_name_de   text,
  display_name      text,
  detail            text,
  rule              json,
  rule_de           json,
  score             smallint,
  group_id          integer,
  impact_id         text        REFERENCES staging.stg_impact,
  pattern_id_excluded   int[]
);
COMMENT ON COLUMN staging.stg_pattern.category_mongo_id IS '``mongo_id`` of the deepest category level. Its parents can be deduced by using function ``get_parent_category_mongo_id_list(category_mongo_id)`` or ``get_parent_category_mongo_id_by_level(category_mongo_id, category_level)``';
CREATE INDEX idx_stg_pattern_impact_id ON staging.stg_pattern USING btree(impact_id);

-- Create tables for production's data
CREATE TABLE public.impact (
    impact_id           int         PRIMARY KEY,
    status              smallint    NOT NULL DEFAULT 1,
    created_at          timestamp   NOT NULL DEFAULT now(),
    updated_at          timestamp   NOT NULL DEFAULT now(),
    impact_parent_id    text,
    impact_name         text        NOT NULL,
    impact_name_de      text,
    itemgroup           text
);

CREATE TABLE public.pattern (
  pattern_id        int         PRIMARY KEY,
  status            smallint    NOT NULL DEFAULT 1,
  created_at        timestamp   NOT NULL DEFAULT now(),
  updated_at        timestamp   NOT NULL DEFAULT now(),
  category_mongo_id text, -- ``mongo_id`` of the deepest category level. Its parents can be deduced by using function ``get_parent_category_mongo_id_list(category_mongo_id)`` or ``get_parent_category_mongo_id_by_level(category_mongo_id, category_level)``
  pattern_class     text,
  pattern_name      text,
  pattern_name_de   text,
  display_name      text,
  detail            text,
  rule              json,
  rule_de           json,
  score             smallint,
  group_id          integer,
  impact_id         text        REFERENCES public.impact,
  pattern_id_excluded   int[]
);
COMMENT ON COLUMN public.pattern.category_mongo_id IS '``mongo_id`` of the deepest category level. Its parents can be deduced by using function ``get_parent_category_mongo_id_list(category_mongo_id)`` or ``get_parent_category_mongo_id_by_level(category_mongo_id, category_level)``';
CREATE INDEX idx_pattern_impact_id ON public.pattern USING btree(impact_id);

ALTER USER dbo SET
    search_path = public, removed, staging;
ALTER USER dbw SET
    search_path = public, removed, staging;
ALTER USER dbr SET
    search_path = public, removed, staging;
ALTER USER root SET
    search_path = public, removed, staging;

GRANT USAGE ON SCHEMA public, removed, staging TO dbo, dbw, dbr, root;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, removed, staging TO dbo, dbw, root;

GRANT SELECT ON ALL TABLES IN SCHEMA public, removed, staging TO dbr;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public, removed, staging TO dbr;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public, removed, staging TO dbr;

ALTER ROLE dbo SET search_path = public, removed, staging;
ALTER ROLE dbw SET search_path = public, removed, staging;
ALTER ROLE dbr SET search_path = public, removed, staging;
ALTER ROLE root SET search_path = public, removed, staging;
