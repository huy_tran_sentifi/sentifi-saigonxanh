CREATE TABLE public.user_setting (
    user_id             int         NOT NULL REFERENCES public.sentifi_user ON DELETE CASCADE,
    status              smallint    NOT NULL DEFAULT 1,
    created_at          timestamp   NOT NULL DEFAULT now(),
    updated_at          timestamp   NOT NULL DEFAULT now(),
    product_id          smallint    NOT NULL,
    email_notification  smallint
);
COMMENT ON TABLE public.user_setting IS 'User''s settings on different Sentifi products';
COMMENT ON COLUMN public.user_setting.product_id IS '1: Sentifi One, 2: myMarkets';
CREATE UNIQUE INDEX idx_user_setting_user_id_product_id ON public.user_setting USING btree(user_id, product_id);


CREATE TABLE public.user_suggested_person (
    usp_id                  serial      PRIMARY KEY,
    user_id                 int         NOT NULL REFERENCES public.sentifi_user ON DELETE CASCADE,
    status                  smallint    NOT NULL DEFAULT 1,
    created_at              timestamp   NOT NULL DEFAULT now(),
    updated_at              timestamp   NOT NULL DEFAULT now(),
    released_at             timestamp,
    named_entity_id_created int         REFERENCES public.named_entity ON DELETE SET NULL,
    first_name              text,
    last_name               text,
    job_title               text,
    company                 text,
    twitter_url             text,
    linkedin_url            text
);
COMMENT ON TABLE public.user_suggested_person IS 'Persons to watch, suggested by User ';
COMMENT ON COLUMN public.user_suggested_person.status IS '0: removed, 1: released, 2: new, 3: not found';
COMMENT ON COLUMN public.user_suggested_person.released_at IS 'timestamp when a named_entity is created thanks to this row';
COMMENT ON COLUMN public.user_suggested_person.named_entity_id_created IS 'reference to the named_entity which is created from this row';


GRANT USAGE ON SCHEMA mongo, public, staging TO dbo, dbw, dbr, root;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, mongo, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, mongo, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, mongo, staging TO dbo, dbw, root;

GRANT SELECT ON ALL TABLES IN SCHEMA public, mongo, staging TO dbr;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public, mongo, staging TO dbr;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public, mongo, staging TO dbr;

ALTER ROLE dbo SET search_path = public, mongo, staging;
ALTER ROLE dbw SET search_path = public, mongo, staging;
ALTER ROLE dbr SET search_path = public, mongo, staging;
ALTER ROLE root SET search_path = public, mongo, staging;
