CREATE EXTENSION tsearch2;

CREATE SCHEMA mongo;

CREATE TABLE mongo.mg_publisher (
    object_id       text        PRIMARY KEY,
    created_at      timestamp   NOT NULL DEFAULT now(),
    updated_at      timestamp   NOT NULL DEFAULT now(),
    object_payload  json
);
CREATE INDEX idx_mg_publisher_created_at ON mongo.mg_publisher USING btree(created_at);

CREATE TABLE mongo.mg_normalized_publisher (
    publisher_mongo_id  text    REFERENCES mongo.mg_publisher ON DELETE CASCADE,
    status              int,
    created_at          timestamp,
    updated_at          timestamp,
    name                text,
    display_name        text,
    first_name          text,
    last_name           text,
    address             text,
    description         text,
    url                 text,
    phone               text,
    fb_url              text[],
    gp_url              text[],
    li_url              text[],
    news_name           text[],
    news_url            text[],
    tw_name             text[],
    tw_screen_name      text[],
    tw_description      text[],
    UNIQUE(publisher_mongo_id)
);
CREATE INDEX idx_mg_normalized_publisher_tsv_name
  ON mongo.mg_normalized_publisher USING gin(to_tsvector(name));

CREATE TABLE mongo.publisher_graph (
    src_pmi     text    REFERENCES mongo.mg_publisher ON DELETE CASCADE,
    label       text,
    dest_pmi    text    REFERENCES mongo.mg_publisher ON DELETE CASCADE,
    val         numeric,
    UNIQUE (src_pmi, label, dest_pmi)
);




CREATE TABLE mongo.mg_removed_publisher (
    publisher_mongo_id  text        PRIMARY KEY,
    removed_at          timestamp   NOT NULL DEFAULT now(),
    reason              text,
    payload             json
);

INSERT INTO mongo.mg_removed_publisher (publisher_mongo_id, reason, payload)
    SELECT
        object_id,
        'status = ' || (object_payload->>'status'),
        object_payload -- DELETE
      FROM
        mongo.mg_publisher
      WHERE
        (object_payload->>'status')::float NOT IN (1, 2); -- status (MACHINE_AUDITED, RELEASED)

INSERT INTO mongo.mg_removed_publisher (publisher_mongo_id, reason, payload)
    SELECT
        object_id,
        'active = ' || (object_payload->>'active'),
        object_payload -- DELETE
      FROM
        mongo.mg_publisher
      WHERE
        object_payload->>'active' = 'false';

SELECT
    reason,
    COUNT(*)
  FROM mongo.mg_removed_publisher
  GROUP BY reason;


-- Name token's distribution
WITH publisher_name AS (
    SELECT regexp_split_to_table(name, E'\\s+') AS name_token
      FROM mongo.mg_normalized_publisher
) SELECT
    name_token,
    COUNT(*)
  FROM publisher_name
  GROUP BY name_token
  ORDER BY COUNT DESC;


INSERT INTO mongo.publisher_graph (src_pmi, label, dest_pmi)
    SELECT
        np1.publisher_mongo_id AS pmi1,
        'name_matched',
        np2.publisher_mongo_id AS pmi2 -- SELECT COUNT(*)
      FROM
        mg_normalized_publisher np1,
        mg_normalized_publisher np2
      WHERE np1.name = np2.name
        AND np1.publisher_mongo_id != np2.publisher_mongo_id;

INSERT INTO mongo.publisher_graph (src_pmi, label, dest_pmi)
    SELECT
        np1.publisher_mongo_id AS pmi1,
        'src_name_in_dest_name',
        np2.publisher_mongo_id AS pmi2 -- SELECT COUNT(*)
      FROM
        mg_normalized_publisher np1,
        mg_normalized_publisher np2
      WHERE strpos(np2.name, np1.name) > 0
        AND trim(both from np1.name) != ''
        AND np1.publisher_mongo_id != np2.publisher_mongo_id;



CREATE TABLE mongo.publisher_duplet (
    pmi1    text                REFERENCES mongo.mg_publisher ON DELETE CASCADE,
    pmi2    text                REFERENCES mongo.mg_publisher ON DELETE CASCADE,
    name_matched                boolean,
    name_in_name                boolean,
    first_last_in_name          boolean,
    name_in_description         boolean,
    first_last_in_description   boolean,
    url_related                 boolean,
    phone_in_phone              boolean,
    name_in_url                 boolean,
    first_last_in_url           boolean,
    UNIQUE (pmi1, pmi2)
);

CREATE TABLE mongo._pd_name_matched AS
    SELECT
        np1.publisher_mongo_id AS pmi1,
        np2.publisher_mongo_id AS pmi2 -- SELECT COUNT(*)
      FROM mg_normalized_publisher np1,
        mg_normalized_publisher np2
      WHERE np1.name = np2.name
        AND np1.publisher_mongo_id > np2.publisher_mongo_id;

INSERT INTO mongo.publisher_duplet (pmi1, pmi2, true)
    SELECT

CREATE INDEX idx_pd_name_matched ON mongo._pd_name_matched USING btree(pmi1, pmi2);

DELETE
  FROM _pd_name_matched t1
  WHERE EXISTS (
    SELECT NULL
      FROM _pd_name_matched t2
      WHERE t2.pmi2 = t1.pmi1
        AND t2.pmi1 = t1.pmi2
  );

INSERT INTO mongo.publisher_duplet ()

















select count(*) from mg_normalized_publisher where trim(both from name) = '';
CREATE INDEX idx_mg_normalized_publisher_name ON mongo.mg_normalized_publisher USING btree(name);

CREATE INDEX idx_mg_normalized_publisher_publisher_mongo_id ON mongo.mg_normalized_publisher USING btree(publisher_mongo_id);


INSERT INTO mongo.mg_normalized_publisher (
    publisher_mongo_id,
    name,
    display_name,
--    first_name,
--    last_name,
    address,
    url,
    phone
)
    SELECT
        object_id,
        regexp_replace(unaccent(lower(object_payload->>'name')), '[^a-zA-Z0-9 ]', '', 'g'),
        regexp_replace(unaccent(lower(object_payload->>'displayName')), '[^a-zA-Z0-9 ]', '', 'g'),
--        regexp_replace(unaccent(lower(object_payload->'person'->>'firstName')), '[^a-zA-Z0-9 ]', '', 'g'),
--        regexp_replace(unaccent(lower(object_payload->'person'->>'lastName')), '[^a-zA-Z0-9 ]', '', 'g'),
        regexp_replace(unaccent(lower(object_payload->>'address')), '[^a-zA-Z0-9 ]', '', 'g'),
        regexp_replace(unaccent(lower(object_payload->>'url')), '[^a-zA-Z0-9:/?&]', '', 'g'),
        regexp_replace(unaccent(lower(object_payload->>'phone')), '[^a-zA-Z0-9]', '', 'g')
      FROM
        mongo.mg_publisher;


GRANT USAGE ON SCHEMA mongo, public, removed, staging TO dbo, dbw, dbr, root;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;

GRANT SELECT ON ALL TABLES IN SCHEMA public, mongo, removed, staging TO dbr;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public, mongo, removed, staging TO dbr;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public, mongo, removed, staging TO dbr;

ALTER ROLE dbo SET search_path = public, mongo, removed, staging;
ALTER ROLE dbw SET search_path = public, mongo, removed, staging;
ALTER ROLE dbr SET search_path = public, mongo, removed, staging;
ALTER ROLE root SET search_path = public, mongo, removed, staging;


time .pg2es.py -H dev.ssh.sentifi.com -d sentifi_dev -u dbo -q "
    SELECT
        object_id,
        regexp_replace(unaccent(lower(object_payload->>'name')), '[^a-zA-Z0-9 ]', '', 'g'),
        regexp_replace(unaccent(lower(object_payload->>'displayName')), '[^a-zA-Z0-9 ]', '', 'g'),
        regexp_replace(unaccent(lower(object_payload->'person'->>'firstName')), '[^a-zA-Z0-9 ]', '', 'g'),
        regexp_replace(unaccent(lower(object_payload->'person'->>'lastName')), '[^a-zA-Z0-9 ]', '', 'g'),
        regexp_replace(unaccent(lower(object_payload->>'address')), '[^a-zA-Z0-9 ]', '', 'g'),
      FROM
        mongo.mg_publisher
" -e es0.ssh.sentifi.com:9200,es8.ssh.sentifi.com:9200,es9.ssh.sentifi.com:9200 -i named_entity_index -t named_entity > $VIRTUAL_ENV/log/index_pub.log 2>&1
