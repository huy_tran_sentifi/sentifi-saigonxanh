COPY public.pattern(pattern_id, pattern_class, pattern_name, score) FROM STDIN WITH CSV;
471,Business,Corporate-governance,3
472,Business,Investor-relation,3
\.

UPDATE public.pattern SET pattern_name = 'Reverse-stock_split', updated_at = now() WHERE pattern_id = 462;