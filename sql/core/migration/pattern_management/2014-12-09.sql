COPY public.pattern(pattern_id, pattern_class, pattern_name, score) FROM STDIN WITH CSV;
456,Risk,Insolvency,3
457,Business,Production,3
458,Business,Production_up,3
459,Business,Production_down,3
460,EQUITY-ACTIONS,Coupon_payment,3
461,Stock,Share_placement,3
462,Stock,Reserve-stock-split,3
463,Stock,Forward-stock_split,3
464,Stock,Stock-Rights_offering,3
\.
