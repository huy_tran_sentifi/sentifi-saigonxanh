ALTER TABLE public.message_tagging ADD COLUMN channel text;

COPY public.pattern(pattern_id, pattern_class, pattern_name, score) FROM STDIN WITH CSV;
398,Staff,Staff-hiring,3
399,Business,Corporate-Social-Responsibility (CSR),3
400,Business,Environmental-Social-Governance (ESG),3
401,Business,Business-sustainability,3
402,Business,Exploration-failed,3
403,Business,Business-liquidation,3
404,Auditing,Auditor-change,3
\.

UPDATE public.pattern SET status = 0 WHERE pattern_id = 135;
UPDATE public.pattern SET status = 0 WHERE pattern_id = 348;
UPDATE public.pattern SET pattern_name = 'Ownership-decrease_holder' WHERE pattern_id = 76;
UPDATE public.pattern SET pattern_name = 'Ownership-increase_holder' WHERE pattern_id = 78;
UPDATE public.pattern SET pattern_name_de = 'Investition-Unternehmensdesinvestition' WHERE pattern_id = 102;
UPDATE public.pattern SET pattern_name = 'Exploration-successful' WHERE pattern_id = 358;
