COPY public.pattern(pattern_id, pattern_class, pattern_name, score) FROM STDIN WITH CSV;
405,Cash-flow,Cash-flow,3
406,Cash-flow,Cash-flow-positive,3
407,Cash-flow,Cash-flow-negative,3
408,Wrong-doings,Wrong_doings-Price_fixing,3
409,Wrong-doings,Wrong_doings-Antitrust,3
410,COMPANY,Interim-financial-result,3
411,Earnings,EPS-guidance,3
412,Earnings,EPS-guidance_above_expectations,3
413,Earnings,EPS-guidance_below_expectations,3
414,Earnings,EPS-guidance_down,3
415,Earnings,EPS-guidance_meet_expectations,3
416,Earnings,EPS-guidance_up,3
417,COMPANY,Transfer-pricing,3
418,COMPANY,Tax-avoidance,3
419,Wrong-doings,Wrong_doings-Tax_fraud,3
420,Wrong-doings,Wrong_doings-Tax_evasion,3
421,Wrong-doings,Wrong_doings-Money_laundering,3
422,Bond,Bond,3
423,Bond,Bond-yield,3
424,Bond,Bond-boost,3
425,Bond,Bond_rating-other,3
426,Bond,Bond_rating-confirmed,3
427,Bond,Bond_rating-upgrade,3
428,Bond,Bond_rating-downgrade,3
429,Stock,Share-dilution,3
430,Debt,Debt-buyback,3
431,Earnings,Gross_margin,3
432,Earnings,Gross_margin-positive,3
433,Earnings,Gross_margin-negative,3
434,Earnings,Gross_margin-up,3
435,Earnings,Gross_margin-down,3
436,Earnings,Gross_margin-above_expectations,3
437,Earnings,Gross_margin-below_expectations,3
438,Earnings,Gross_margin-meet_expectations,3
439,Earnings,Gross_margin-guidance,3
440,Earnings,Gross_margin-guidance_up,3
441,Earnings,Gross_margin-guidance_down,3
442,Earnings,Gross_margin-meet_guidance,3
443,Earnings,Gross_margin-above_guidance,3
444,Earnings,Gross_margin-below_guidance,3
445,Earnings,Gross_margin-guidance_meet_expectations,3
446,Earnings,Gross_margin-guidance_above_expectations,3
447,Earnings,Gross_margin-guidance_below_expectations,3
\.


UPDATE public.pattern SET pattern_name = 'Ownership-decrease' WHERE pattern_id = 76;
UPDATE public.pattern SET pattern_name = 'Ownership-increase' WHERE pattern_id = 78;

UPDATE public.pattern SET pattern_name_de = 'Geschäft-Konkurs' WHERE pattern_id = 151;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Konkurrenz' WHERE pattern_id = 152;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Entscheidung' WHERE pattern_id = 153;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Entscheidung-Beendung' WHERE pattern_id = 154;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Entscheidung-Expansion' WHERE pattern_id = 155;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Entscheidung-Expansion-gescheitert' WHERE pattern_id = 350;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Entscheidung-Ausrichtung' WHERE pattern_id = 156;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Entscheidung-Ausgliederung' WHERE pattern_id = 157;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Finanzierung' WHERE pattern_id = 158;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Inventar' WHERE pattern_id = 160;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Marktanteil' WHERE pattern_id = 161;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Geschäftsmodell' WHERE pattern_id = 162;
UPDATE public.pattern SET pattern_name_de = 'Geschäftsausblick' WHERE pattern_id = 163;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Ausblick-Negativ' WHERE pattern_id = 164;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Ausblick-Positiv' WHERE pattern_id = 165;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Produktivität' WHERE pattern_id = 166;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Produktivität_erhöht' WHERE pattern_id = 351;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Produktivität_gesunken' WHERE pattern_id = 352;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Verlagerung' WHERE pattern_id = 167;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Beibehaltung-Bestätigt' WHERE pattern_id = 168;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Beibehaltung-Spekulation' WHERE pattern_id = 169;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Verkaufsrefinanzierung' WHERE pattern_id = 170;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Verkleinerung' WHERE pattern_id = 171;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Strategien' WHERE pattern_id = 172;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Ziel_gesetzt' WHERE pattern_id = 173;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Update' WHERE pattern_id = 177;
UPDATE public.pattern SET pattern_name_de = 'Erschliessung_erfolgreich' WHERE pattern_id = 358;
UPDATE public.pattern SET pattern_name_de = 'Erschliessung_fehlgeschlagen' WHERE pattern_id = 402;
UPDATE public.pattern SET pattern_name_de = 'Personal-Einstellung' WHERE pattern_id = 398;
UPDATE public.pattern SET pattern_name_de = 'Soziale-Verantwortung (CSR)' WHERE pattern_id = 400;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Nachhaltigkeit' WHERE pattern_id = 401;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Liquidation' WHERE pattern_id = 403;

UPDATE public.pattern SET status = 0 WHERE pattern_id = 77;
UPDATE public.pattern SET status = 0 WHERE pattern_id = 79;
UPDATE public.pattern SET status = 0 WHERE pattern_id = 175;
UPDATE public.pattern SET status = 0 WHERE pattern_id = 176;
