CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE public.pattern (
    pattern_id      smallserial PRIMARY KEY,
    status          smallint    NOT NULL DEFAULT 1,
    created_at      timestamp   NOT NULL DEFAULT now(),
    updated_at      timestamp   NOT NULL DEFAULT now(),
    pattern_class   text,
    pattern_name    text,
    score           smallint,
    detail          text,
    rule            text
);

CREATE TABLE public.message_tagging (
    message_id          uuid        NOT NULL DEFAULT uuid_generate_v1(), -- For future usage
    message_mongo_id    text        NOT NULL, -- Will be deprecated
    status              smallint    NOT NULL DEFAULT 1,
    created_at          timestamp   NOT NULL DEFAULT now(),
    updated_at          timestamp   NOT NULL DEFAULT now(),
    named_entity_id     uuid        NOT NULL DEFAULT uuid_generate_v1(), -- For future usage
    named_entity_old_id int         NOT NULL, -- Will be deprecated
    matched_pattern     int[]       NOT NULL,
    PRIMARY KEY(message_mongo_id, named_entity_old_id)
);
CREATE INDEX idx_message_tagging_matched_pattern ON public.message_tagging USING GIN(matched_pattern);
CREATE INDEX idx_message_tagging_named_entity_id ON public.message_tagging USING BTREE(named_entity_id);
COMMENT ON COLUMN message_tagging.message_mongo_id IS 'Will be deprecated. Use message_id instead.';
COMMENT ON COLUMN message_tagging.named_entity_old_id IS 'Will be deprecated. Use named_entity_id instead.';
