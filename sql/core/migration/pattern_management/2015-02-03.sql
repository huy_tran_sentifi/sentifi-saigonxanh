UPDATE public.pattern SET pattern_name_de = 'Insolvenz', updated_at = now() WHERE pattern_id = 456;
UPDATE public.pattern SET status = 0, updated_at = now() WHERE pattern_id = 273;

COPY public.pattern(pattern_id, pattern_class, pattern_name, score) FROM STDIN WITH CSV;
473,Financial_ratios,Financial_ratios,3
474,Financial_ratios,Financial_ratios-up,3
475,Financial_ratios,Financial_ratios-down,3
476,Sponsor,Sponsor,3
-48,Organization,Blacklist,0
-49,Organization,Blacklist_begin,0
\.
