COPY pattern (pattern_id, pattern_class, pattern_name, score) FROM stdin;
324	Profits	Profits	3
325	Profits	Profits-above_expectations	3
326	Profits	Profits-analyst_estimate	3
327	Profits	Profits-analyst_estimate_downgrade	3
328	Profits	Profits-analyst_estimate_upgrade	3
329	Profits	Profits-below_expectations	3
330	Profits	Profits-down	3
331	Profits	Profits-guidance	3
332	Profits	Profits-guidance_above_expectations	3
333	Profits	Profits-guidance_below_expectations	3
334	Profits	Profits-guidance_down	3
335	Profits	Profits-guidance_meet_expectations	3
336	Profits	Profits-guidance_up	3
337	Profits	Profits-meet_expectations	3
338	Profits	Profits-up	3
\.
