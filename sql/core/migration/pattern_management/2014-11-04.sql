COPY public.pattern(pattern_id, pattern_class, pattern_name, score) FROM STDIN WITH CSV;
357,Business,Exploration,3
358,Business,Exploration-result,3
359,Business,Drilling,3
360,Business,Drilling-completed,3
361,Debt,Debt-offering,3
362,Asset,Asset-under-management,3
363,Asset,Asset-under-management_up,3
364,Asset,Asset-under-management_down,3
365,COMPANY,Stress-test,3
366,COMPANY,Stress-test_pass,3
367,COMPANY,Stress-test_failed,3
368,COMPANY,Financial-results,3
369,Stock,Stock-Rebound,3
370,Stock,Share-Redemption,3
371,COMPANY,Conference-Participation,3
372,Cost,Cost,3
373,Product-Services,Product-Services_New,3
374,Copyright,Patent,3
375,Product-Services,Clinical-Collaboration,3
376,Product-Services,Clinical-Study,3
377,EQUITY-ACTIONS,Short_Interest,3
378,EQUITY-ACTIONS,PPI-provision,3
-39,Organization,FX_Recommendation,0
-40,Organization,Bank_Stock_Price_Target,0
-41,Organization,Salutation,0
\.

UPDATE public.pattern
   SET pattern_name = 'Natural-resources-discovery' WHERE pattern_id = 348;
