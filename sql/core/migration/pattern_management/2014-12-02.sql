COPY public.pattern(pattern_id, pattern_class, pattern_name, score) FROM STDIN WITH CSV;
448,COMPANY,Tax-refund,3
449,Business,Operation-suspended,3
450,Wrong-doings,Wrong_doings-Bribery,3
451,Wrong-doings,Wrong_doings-Market_manipulation,3
452,Accounting,Balance_sheet,3
453,COMPANY,Tax-liability,3
454,Business,Drilling-update,3
455,Executives,Executive-statement,3
-42,Organization,Advertising,0
-43,Organization,Vote,0
\.


UPDATE public.pattern SET pattern_name_de = 'Personal-Einstellung' WHERE pattern_id = 398;
UPDATE public.pattern SET pattern_name_de = 'Soziale-Verantwortung (CSR)' WHERE pattern_id = 400;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Nachhaltigkeit' WHERE pattern_id = 401;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-Liquidation' WHERE pattern_id = 403;
UPDATE public.pattern SET pattern_name_de = 'Vermögen-unter-verwaltung' WHERE pattern_id = 362;
UPDATE public.pattern SET pattern_name_de = 'Vermögen-unter-verwaltung_erhöht' WHERE pattern_id = 363;
UPDATE public.pattern SET pattern_name_de = 'Vermögen-unter-verwaltung_reduziert' WHERE pattern_id = 364;
UPDATE public.pattern SET pattern_name_de = 'Ertrag_pro_Aktie_Prognose' WHERE pattern_id = 411;
UPDATE public.pattern SET pattern_name_de = 'Ertrag_pro_Aktie_Prognose-über_den_Erwartungen' WHERE pattern_id = 412;
UPDATE public.pattern SET pattern_name_de = 'Ertrag_pro_Aktie_Prognose-unter_den_Erwartungen' WHERE pattern_id = 413;
UPDATE public.pattern SET pattern_name_de = 'Ertrag_pro_Aktie_Prognose-gesunken' WHERE pattern_id = 414;
UPDATE public.pattern SET pattern_name_de = 'Ertrag_pro_Aktie_Prognose-gemäss_den_Erwartungen' WHERE pattern_id = 415;
UPDATE public.pattern SET pattern_name_de = 'Ertrag_pro_Aktie_Prognose-erhöht' WHERE pattern_id = 416;
UPDATE public.pattern SET pattern_name_de = 'Zwischenergebnisse' WHERE pattern_id = 410;
UPDATE public.pattern SET pattern_name_de = 'Fremdkapital-Emission' WHERE pattern_id = 361;
UPDATE public.pattern SET pattern_name_de = 'Fremdkapital-Rückkauf' WHERE pattern_id = 430;
UPDATE public.pattern SET pattern_name_de = 'Environmental-Social-Governance_DE (ESG)' WHERE pattern_id = 400;
UPDATE public.pattern SET pattern_name_de = 'Geschäft-abgelehnt' WHERE pattern_id = 174;
