ALTER TABLE public.item_portfolio ADD COLUMN category_id int[];

UPDATE public.item_portfolio ip SET
    category_id = (
        SELECT array_agg(DISTINCT category_id)
          FROM category c
          WHERE c.category_mongo_id = ANY(ip.category_mongo_id)
    ),
    updated_at = now();
