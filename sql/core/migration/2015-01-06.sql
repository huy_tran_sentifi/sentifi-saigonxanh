-- Create related tables
CREATE TABLE public._108k_rss (
    item_id int,
    rss     text
);

CREATE TABLE public._108k_twitter (
    item_id     int,
    screen_name text
);

CREATE TABLE public._affected_id (id int);


-- Insert data from CSV
COPY public._108k_rss(item_id, rss) FROM '/tmp/108k_rss.csv' WITH CSV DELIMITER ',' QUOTE '"' HEADER;
COPY public._108k_twitter(item_id, screen_name) FROM '/tmp/108k_twitter.csv' WITH CSV DELIMITER ',' QUOTE '"' HEADER;
-- Verify that there is no SPACE character in the field ``screen_name``
SELECT * FROM _108k_twitter WHERE position(' ' in screen_name) > 0;
-- Unnest ``screen_name`` list
CREATE TABLE public._item_twitter AS
    SELECT
        item_id,
        TRIM(BOTH FROM regexp_split_to_table(screen_name, ',')) AS screen_name -- SELECT COUNT(*)
      FROM public._108k_twitter t1;


-- Insert RSS
INSERT INTO public._affected_id(id)
    SELECT DISTINCT item_id -- SELECT COUNT(*)
      FROM public._108k_rss t1
      WHERE EXISTS (
            SELECT NULL
              FROM item i
              WHERE i.id = t1.item_id
        ) AND NOT EXISTS (
            SELECT NULL
              FROM item_sns t2
              WHERE t2.item_id = t1.item_id
                AND t2.sns_name = 'rss'
                AND lower(t2.sns_id) = lower(t1.rss)
        );

INSERT INTO public.item_sns (
    id,
    item_id,
    sns_name,
    sns_geo_level,
    sns_id,
    tag
)
    SELECT
        nextval('item_sns_seq'::regclass),
        item_id,
        'rss',
        'general',
        rss,
        'Found by crawlers'
      FROM public._108k_rss t1
      WHERE EXISTS (
            SELECT NULL
              FROM item i
              WHERE i.id = t1.item_id
        ) AND NOT EXISTS (
            SELECT NULL
              FROM item_sns t2
              WHERE t2.item_id = t1.item_id
                AND t2.sns_name = 'rss'
                AND lower(t2.sns_id) = lower(t1.rss)
        );


-- Insert Twitter
INSERT INTO public._affected_id(id)
    SELECT DISTINCT item_id -- SELECT COUNT(*)
      FROM public._item_twitter t1
      WHERE NULLIF(screen_name, '') IS NOT NULL
        AND EXISTS (
            SELECT NULL
              FROM item i
              WHERE i.id = t1.item_id
        ) AND NOT EXISTS (
            SELECT NULL
              FROM item_sns t2
              WHERE t2.item_id = t1.item_id
                AND t2.sns_name = 'tw'
                AND lower(t2.sns_id) = lower(t1.screen_name)
        );

INSERT INTO public.item_sns (
    id,
    item_id,
    sns_name,
    sns_geo_level,
    sns_id,
    tag
)
    SELECT
        nextval('item_sns_seq'::regclass),
        item_id,
        'tw',
        'general',
        screen_name,
        'Found by crawlers' -- SELECT COUNT(*)
      FROM public._item_twitter t1
      WHERE NULLIF(screen_name, '') IS NOT NULL
        AND EXISTS (
            SELECT NULL
              FROM item i
              WHERE i.id = t1.item_id
        ) AND NOT EXISTS (
            SELECT NULL
              FROM item_sns t2
              WHERE t2.item_id = t1.item_id
                AND t2.sns_name = 'tw'
                AND lower(t2.sns_id) = lower(t1.screen_name)
        );
