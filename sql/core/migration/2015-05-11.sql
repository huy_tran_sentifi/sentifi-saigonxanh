CREATE TABLE public.publisher_tag (
    publisher_mongo_id  text REFERENCES mongo.mg_publisher ON DELETE CASCADE,
    tag_list            text[]
);
CREATE INDEX idx_publisher_tag_tag_list ON public.publisher_tag USING gin(tag_list);


WITH pt AS (
    SELECT
        object_id,
        (json_array_elements(object_payload->'publisherTag'))::text AS tag
      FROM mongo.mg_publisher
      WHERE (object_payload->'publisherTag') is not null
)
INSERT INTO public.publisher_tag(publisher_mongo_id, tag_list)
    SELECT
        object_id,
        array_agg(trim(both '"' FROM tag))
      FROM pt
      GROUP BY object_id;


GRANT USAGE ON SCHEMA mongo, public, removed, staging TO dbo, dbw, dbr, root;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, mongo, removed, staging TO dbo, dbw, root;

GRANT SELECT ON ALL TABLES IN SCHEMA public, mongo, removed, staging TO dbr;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public, mongo, removed, staging TO dbr;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public, mongo, removed, staging TO dbr;

ALTER ROLE dbo SET search_path = public, mongo, removed, staging;
ALTER ROLE dbw SET search_path = public, mongo, removed, staging;
ALTER ROLE dbr SET search_path = public, mongo, removed, staging;
ALTER ROLE root SET search_path = public, mongo, removed, staging;
