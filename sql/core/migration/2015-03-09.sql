-- Break table ``public.pattern`` to ``public.pattern``, ``public.impact`` and ``public.impact_event``
-- Update public.``pattern`` schema
ALTER TABLE public.pattern ALTER COLUMN rule TYPE json USING rule::json;
ALTER TABLE public.pattern ADD COLUMN rule_de json;
ALTER TABLE public.pattern ADD COLUMN category_mongo_id text REFERENCES public.category (category_mongo_id) ON DELETE SET NULL;
COMMENT ON COLUMN public.pattern.category_mongo_id IS '``mongo_id`` of the deepest category level. Its parents can be deduced by using function ``get_parent_category_mongo_id_list(category_mongo_id)`` or ``get_parent_category_mongo_id_by_level(category_mongo_id, category_level)``';

-- Update ``public.category`` schema
DELETE FROM public.category WHERE status = 0;
ALTER TABLE public.category ADD UNIQUE (category_mongo_id);
DROP INDEX idx_category_category_mongo_id;
CREATE INDEX idx_category_parent_category_mongo_id ON public.category USING btree(parent_category_mongo_id);

CREATE OR REPLACE FUNCTION public.get_parent_category_mongo_id_list(IN category_mongo_id text) RETURNS varchar[] AS $$
    WITH RECURSIVE t(category_mongo_id, parent_category_mongo_id, category_level) AS (
        SELECT category_mongo_id, parent_category_mongo_id, category_level
          FROM category
          WHERE category_mongo_id = $1
            AND status = 1
        UNION
        SELECT c2.category_mongo_id, c2.parent_category_mongo_id, c2.category_level
          FROM category c2, t c1
          WHERE c2.category_mongo_id = c1.parent_category_mongo_id
            AND c2.status = 1
    )
    SELECT ARRAY(
        SELECT category_mongo_id
          FROM t
          ORDER BY category_level
      ) AS foo;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION public.get_parent_category_mongo_id_by_level(IN category_mongo_id text, IN category_level int) RETURNS text AS $$
    WITH RECURSIVE t(category_mongo_id, parent_category_mongo_id, category_level) AS (
        SELECT category_mongo_id, parent_category_mongo_id, category_level
          FROM category
          WHERE category_mongo_id = $1
            AND status = 1
        UNION
        SELECT c2.category_mongo_id, c2.parent_category_mongo_id, c2.category_level
          FROM category c2, t c1
          WHERE c2.category_mongo_id = c1.parent_category_mongo_id
            AND c2.status = 1
    )
    SELECT category_mongo_id
      FROM t
      WHERE category_level = $2
$$ LANGUAGE SQL;


-- Create table ``public.impact`` and ``public.impact_event``
CREATE TABLE public.impact (
    impact_id           int         PRIMARY KEY,
    status              smallint    NOT NULL DEFAULT 1,
    created_at          timestamp   NOT NULL DEFAULT now(),
    updated_at          timestamp   NOT NULL DEFAULT now(),
    impact_parent_id    text,
    impact_name         text        NOT NULL,
    impact_name_de      text,
    itemgroup           text
);

CREATE TABLE public.impact_event (
    impact_id   int         NOT NULL REFERENCES public.impact ON DELETE CASCADE,
    event_id    smallint    NOT NULL REFERENCES public.pattern ON DELETE CASCADE
);

CREATE UNIQUE INDEX idx_impact_event_impact_id_event_id ON public.impact_event (impact_id, event_id);

INSERT INTO public.impact (impact_id, status, created_at, updated_at, impact_name)
    SELECT
        group_id,
        1,
        min(created_at),
        max(updated_at),
        display_name
      FROM public.pattern
      WHERE status = 1
        AND group_id IS NOT NULL
      GROUP BY group_id,
        display_name;

INSERT INTO public.impact_event (impact_id, event_id)
    SELECT
        group_id,
        pattern_id
      FROM public.pattern
      WHERE status = 1
        AND group_id IS NOT NULL
      GROUP BY group_id,
        pattern_id;


-- Add ``cat6`` to table ``public.item``
ALTER TABLE public.item ADD COLUMN cat6 text NOT NULL DEFAULT '';
