ALTER TABLE public.item ADD COLUMN cat4 text NOT NULL DEFAULT '';
ALTER TABLE public.item ADD COLUMN cat5 text NOT NULL DEFAULT '';

CREATE INDEX idx_category_parent_category_id ON public.category USING btree(parent_category_id);
