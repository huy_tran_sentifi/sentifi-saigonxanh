CREATE TABLE public._sg_tw_pub (
    twitter_id  text,
    screen_name text
);

COPY public._sg_tw_pub (twitter_id, screen_name) FROM '/tmp/sg_tw_pub.csv' WITH CSV HEADER;


-- Table that tracks existed ``twitter_id``
CREATE TABLE public._existed_id (twitter_id text);

INSERT INTO public._existed_id (twitter_id)
    SELECT twitter_id
      FROM public._sg_tw_pub
      WHERE replace(lower(screen_name), '@', '') IN (
        SELECT replace(lower(detail), '@', '')
          FROM item_sns
          WHERE status = 1
            AND sns_name = 'tw'
      );

INSERT INTO public._existed_id (twitter_id)
    SELECT twitter_id
      FROM public._sg_tw_pub
      WHERE replace(lower(screen_name), '@', '') IN (
        SELECT replace(lower(sns_id), '@', '')
          FROM item_sns
          WHERE status = 1
            AND sns_name = 'tw'
      );

INSERT INTO public._existed_id (twitter_id)
    SELECT twitter_id
      FROM public._sg_tw_pub
      WHERE replace(lower(twitter_id), '@', '') IN (
        SELECT replace(lower(sns_id), '@', '')
          FROM item_sns
          WHERE status = 1
            AND sns_name = 'tw'
      );


-- Update ``sns_id`` with ``twitter_id``
UPDATE public.item_sns i SET
    sns_id = (
        SELECT twitter_id
          FROM public._sg_tw_pub sg
          WHERE replace(lower(i.detail), '@', '') = replace(lower(sg.screen_name), '@', '')
          LIMIT 1
    ),
    updated_at = now() -- SELECT sns_id, detail FROM public.item_sns i
  WHERE status = 1
    AND sns_name = 'tw'
    AND replace(lower(i.detail), '@', '') IN (SELECT replace(lower(screen_name), '@', '') FROM public._sg_tw_pub);

SELECT
    item_id,
    sns_id,
    detail
  FROM public.item_sns i
  WHERE status = 1
    AND sns_name = 'tw'
    AND replace(lower(i.detail), '@', '') IN (SELECT replace(lower(screen_name), '@', '') FROM public._sg_tw_pub)
  ORDER BY sns_id::bigint;
