-- Create link between sentifi_user and named_entity
ALTER TABLE public.sentifi_user ADD COLUMN named_entity_id int REFERENCES named_entity ON DELETE SET NULL;
COMMENT ON COLUMN public.sentifi_user.named_entity_id IS 'User owning/representing NamedEntity';
COMMENT ON COLUMN public.user_sns.named_entity_id IS 'SNS linking to NamedEntity by User';
COMMENT ON COLUMN public.user_sns.named_entity_id IS 'SNS linking to NamedEntity by Sentifi';


-- Update Foreign Keys to reference named_entity.named_entity_id instead of item.id
ALTER TABLE public.item_indice DROP CONSTRAINT IF EXISTS "ITEM_INDICE_ITEM_FK";
ALTER TABLE public.expression DROP CONSTRAINT IF EXISTS expression_item_id_fkey;
ALTER TABLE public.item_alias DROP CONSTRAINT IF EXISTS item_alias_item_id_fkey;
ALTER TABLE public.item_attribute DROP CONSTRAINT IF EXISTS item_attribute_item_id_fkey;
ALTER TABLE public.item_sns DROP CONSTRAINT IF EXISTS item_sns_item_id_fk;
ALTER TABLE public.term DROP CONSTRAINT IF EXISTS term_item_id_fkey;
ALTER TABLE public.ticker1 DROP CONSTRAINT IF EXISTS ticker1_item_id_fkey;
ALTER TABLE public.ticker DROP CONSTRAINT IF EXISTS ticker_item_id_fkey;
ALTER TABLE public.user_role DROP CONSTRAINT IF EXISTS user_role_item_id_fkey;

ALTER TABLE public.item_indice ADD FOREIGN KEY (item_id) REFERENCES named_entity(named_entity_id) ON DELETE CASCADE;
ALTER TABLE public.expression ADD FOREIGN KEY (item_id) REFERENCES named_entity(named_entity_id) ON DELETE CASCADE;
ALTER TABLE public.item_alias ADD FOREIGN KEY (item_id) REFERENCES named_entity(named_entity_id) ON DELETE CASCADE;
ALTER TABLE public.item_attribute ADD FOREIGN KEY (item_id) REFERENCES named_entity(named_entity_id) ON DELETE CASCADE;
ALTER TABLE public.item_sns ADD FOREIGN KEY (item_id) REFERENCES named_entity(named_entity_id) ON DELETE CASCADE;
ALTER TABLE public.term ADD FOREIGN KEY (item_id) REFERENCES named_entity(named_entity_id) ON DELETE CASCADE;
ALTER TABLE public.ticker1 ADD FOREIGN KEY (item_id) REFERENCES named_entity(named_entity_id) ON DELETE CASCADE;
ALTER TABLE public.ticker ADD FOREIGN KEY (item_id) REFERENCES named_entity(named_entity_id) ON DELETE CASCADE;
ALTER TABLE public.user_role ADD FOREIGN KEY (item_id) REFERENCES named_entity(named_entity_id) ON DELETE SET NULL;


-- Fix status of named_entity which was created from publishers
SELECT COUNT(*)
  FROM public.publisher p1
  WHERE p1.object_payload->>'active' = 'true'
    AND (p1.object_payload->'itemId') IS NULL
    AND (p1.object_payload->>'status') IN ('1', '2', '2.0')
    AND EXISTS (
        SELECT NULL
          FROM public.named_entity ne
          WHERE ne.named_entity_id = p1.named_entity_id
            AND ne.status = 2
    )
    AND EXISTS (
        SELECT NULL
          FROM mongo.mg_publisher_sns ps
          WHERE ps.publisher_mongo_id = p1.object_id
    );

SELECT COUNT(*)
  FROM public.named_entity ne  -- UPDATE public.named_entity ne SET status = 3, updated_at = now()
  WHERE status = 2
    AND EXISTS (
      SELECT NULL
        FROM public.publisher p
        INNER JOIN mongo.mg_publisher_sns ps ON (p.object_id = ps.publisher_mongo_id)
        WHERE p.object_payload->>'active' = 'true'
          AND (p.object_payload->'itemId') IS NULL
          AND (p.object_payload->>'status') IN ('1', '2', '2.0')
          AND p.named_entity_id = ne.named_entity_id
    );


-- Other fixes
ALTER TABLE public.sns_account DROP CONSTRAINT IF EXISTS chk_sns_name;
ALTER TABLE public.sns_account ADD CONSTRAINT chk_sns_name CHECK (sns_name IN ('fb', 'gp', 'li', 'news', 'tw'));
ALTER TABLE public.sns_account ADD FOREIGN KEY (named_entity_id) REFERENCES public.named_entity ON DELETE SET NULL;
ALTER TABLE public.sns_account ADD COLUMN machine_country_code text;
ALTER TABLE public.sns_account ADD COLUMN machine_sns_tag text[];

ALTER TABLE public.sns_category ALTER COLUMN sns_name SET NOT NULL;
ALTER TABLE public.sns_category ALTER COLUMN sns_id SET NOT NULL;
ALTER TABLE public.sns_category ALTER COLUMN category_id SET NOT NULL;

ALTER TABLE public.sns_role ALTER COLUMN sns_name SET NOT NULL;
ALTER TABLE public.sns_role ALTER COLUMN sns_id SET NOT NULL;
ALTER TABLE public.sns_role ALTER COLUMN role_id SET NOT NULL;

ALTER TABLE public.named_entity DROP CONSTRAINT named_entity_category_id_fkey;
ALTER TABLE public.named_entity ADD FOREIGN KEY (category_id) REFERENCES category ON DELETE SET NULL;
ALTER TABLE public.named_entity DROP CONSTRAINT named_entity_partner_id_fkey;
ALTER TABLE public.named_entity ADD FOREIGN KEY (partner_id) REFERENCES partner ON DELETE SET NULL;


-- Update named_entity_id for SnsAccounts which was missing in Publisher ID Migration
WITH _t AS (
    SELECT n.object_id AS sns_id, (p.object_payload->>'itemId')::int AS named_entity_id
      FROM mongo.mg_publisher p
      INNER JOIN mongo.mg_publisher_sns ps ON (p.object_id = ps.publisher_mongo_id)
      INNER JOIN mongo.mg_news n ON (ps.sns_name = 'news' AND ps.sns_id = n.object_id)
      WHERE (n.object_payload->>'rss') IS NOT NULL
        AND (p.object_payload->>'itemId') IS NOT NULL
      GROUP BY n.object_id, (p.object_payload->>'itemId')::int
)
SELECT COUNT(*)
  FROM sns_account sa
  WHERE sns_name = 'news'
    AND sa.named_entity_id IS NULL
    AND EXISTS (
    SELECT NULL
      FROM _t
      WHERE _t.sns_id = sa.sns_id
  );

WITH _t AS (
    SELECT n.object_id AS sns_id, (p.object_payload->>'itemId')::int AS named_entity_id
      FROM mongo.mg_publisher p
      INNER JOIN mongo.mg_publisher_sns ps ON (p.object_id = ps.publisher_mongo_id)
      INNER JOIN mongo.mg_news n ON (ps.sns_name = 'news' AND ps.sns_id = n.object_id)
      WHERE (n.object_payload->>'rss') IS NOT NULL
        AND (p.object_payload->>'itemId') IS NOT NULL
      GROUP BY n.object_id, (p.object_payload->>'itemId')::int
)
UPDATE public.sns_account sa SET
    named_entity_id = _t.named_entity_id,
    updated_at = now()
  FROM _t
  WHERE sa.sns_name = 'news'
    AND sa.sns_id = _t.sns_id
    AND sa.named_entity_id IS NULL
    AND EXISTS (
      SELECT NULL
        FROM public.named_entity ne
        WHERE ne.named_entity_id = _t.named_entity_id
    );


-- Fix ISIN data in named_entity
UPDATE public.named_entity SET isin = NULL WHERE nullif(isin, '{""}') IS NULL;


-- Migrate Publisher released_at
ALTER TABLE public.sns_account ADD COLUMN released_at timestamp;

WITH t1 AS (
    SELECT
        ps.sns_name,
        ps.sns_id,
        (
          CASE
            WHEN p.object_payload->>'releasedAt' LIKE '201%' THEN (p.object_payload->>'releasedAt')::timestamp
            ELSE to_timestamp((p.object_payload->>'releasedAt')::numeric/1000)
          END
        ) AS released_at
      FROM mongo.mg_publisher p
      INNER JOIN mongo.mg_publisher_sns ps ON (p.object_id = ps.publisher_mongo_id)
--       WHERE (p.object_payload->>'releasedAt') LIKE '201%'
--       GROUP BY n.object_id, (p.object_payload->>'itemId')::int
), t2 AS (
    SELECT *, rank() OVER (PARTITION BY sns_name, sns_id ORDER BY released_at) AS foo
      FROM t1
      WHERE released_at IS NOT NULL
)  -- SELECT COUNT(*) FROM t2;
UPDATE public.sns_account a SET
    released_at = COALESCE(t2.released_at, created_at)
  FROM t2
  WHERE t2.sns_name = a.sns_name
    AND t2.sns_id = a.sns_id
    AND t2.foo = 1;

UPDATE public.sns_account SET released_at = created_at WHERE released_at IS NULL;


-- Use `NamedEntity`'s `name`, `avatar_url` as `SnsAccount`'s `display_name` and `avatar_url`
WITH _t AS (
    SELECT
        named_entity_id,
        name,
        avatar_url
      FROM public.named_entity
)
UPDATE public.sns_account sa SET
    display_name = _t.name,
    avatar_url = _t.avatar_url,
    updated_at = now()
  FROM _t
  WHERE sa.display_name IS NULL
    AND sa.avatar_url IS NULL
    AND sa.sns_name = 'news'
    AND sa.named_entity_id = _t.named_entity_id;
