/*
ALTER TABLE public.named_entity ADD COLUMN ticker text;
ALTER TABLE public.named_entity ADD COLUMN ceo text;
ALTER TABLE public.named_entity ADD COLUMN headquarter text;
ALTER TABLE public.named_entity ADD COLUMN company_size text;
ALTER TABLE public.named_entity ADD COLUMN alert smallint;
ALTER TABLE public.named_entity ADD COLUMN force_alert_expired bigint;


UPDATE public.named_entity ne SET
    ceo = (
        SELECT NULLIF(TRIM(BOTH FROM i1.ceo), '')
          FROM public.item_audit i1
          WHERE i1.id = ne.named_entity_id
    ),
    updated_at = now() -- SELECT COUNT(*) FROM named_entity ne
  WHERE EXISTS (
    SELECT NULL
      FROM public.item_audit i2
      WHERE i2.id = ne.named_entity_id
        AND NULLIF(TRIM(BOTH FROM i2.ceo), '') IS NOT NULL
  );

UPDATE public.named_entity ne SET
    headquarter = (
        SELECT NULLIF(TRIM(BOTH FROM i1.headquarter), '')
          FROM public.item_audit i1
          WHERE i1.id = ne.named_entity_id
    ),
    updated_at = now() -- SELECT COUNT(*) FROM named_entity ne
  WHERE EXISTS (
    SELECT NULL
      FROM public.item_audit i2
      WHERE i2.id = ne.named_entity_id
        AND NULLIF(TRIM(BOTH FROM i2.headquarter), '') IS NOT NULL
  );

UPDATE public.named_entity ne SET
    company_size = (
        SELECT NULLIF(TRIM(BOTH FROM i1.companysize), '')
          FROM public.item_audit i1
          WHERE i1.id = ne.named_entity_id
    ),
    updated_at = now() -- SELECT COUNT(*) FROM named_entity ne
  WHERE EXISTS (
    SELECT NULL
      FROM public.item_audit i2
      WHERE i2.id = ne.named_entity_id
        AND NULLIF(TRIM(BOTH FROM i2.companysize), '') IS NOT NULL
  );

UPDATE public.named_entity ne SET
    alert = (
        SELECT i1.alert_enable
          FROM public.item_audit i1
          WHERE i1.id = ne.named_entity_id
    ),
    updated_at = now() -- SELECT COUNT(*) FROM named_entity ne
  WHERE EXISTS (
    SELECT NULL
      FROM public.item_audit i2
      WHERE i2.id = ne.named_entity_id
        AND i2.alert_enable IS NOT NULL
  );

UPDATE public.named_entity ne SET
    force_alert_expired = (
        SELECT i1.forcealertexpired
          FROM public.item_audit i1
          WHERE i1.id = ne.named_entity_id
    ),
    updated_at = now() -- SELECT COUNT(*) FROM named_entity ne
  WHERE EXISTS (
    SELECT NULL
      FROM public.item_audit i2
      WHERE i2.id = ne.named_entity_id
        AND i2.forcealertexpired IS NOT NULL
  );

UPDATE public.sns_account sa SET
    avatar_url = (
        SELECT NULLIF(TRIM(BOTH FROM object_payload->>'image'), '')
          FROM mongo.mg_publisher p
          INNER JOIN mongo.mg_publisher_sns ps ON (ps.publisher_mongo_id = p.object_id)
          WHERE ps.sns_name = sa.sns_name
            AND ps.sns_id = sa.sns_id
          LIMIT 1
    ),
    updated_at = now()
  WHERE sns_name = 'news';
*/


-- Preparation
CREATE TABLE public.id_map (
    named_entity_id serial  PRIMARY KEY,
    id              int,
    object_id       text
);
CREATE INDEX idx_id_map_id ON public.id_map USING btree(id);
CREATE INDEX idx_id_map_object_id ON public.id_map USING btree(object_id);

INSERT INTO public.id_map(named_entity_id, id)
    SELECT id, id
      FROM public.item
      INNER JOIN item_audit USING (id);

SELECT MAX(id) + 1 FROM public.id_map;
ALTER SEQUENCE id_map_named_entity_id_seq RESTART 746565;

INSERT INTO public.id_map(object_id)
    SELECT object_id
      FROM mongo.mg_publisher
      WHERE (object_payload->'itemId') IS NULL;

CREATE VIEW public.search_item AS
    SELECT *
      FROM public.id_map
      INNER JOIN public.item USING (id)
      INNER JOIN public.item_audit USING (id);

CREATE VIEW public.publisher AS
    SELECT im.named_entity_id, p.*
      FROM public.id_map im
      INNER JOIN mongo.mg_publisher p ON (p.object_id = im.object_id OR (p.object_payload->>'itemId')::int = im.id);


-- Update SNS Account
UPDATE public.sns_account a SET
    geo_level = (
        SELECT lower(sns_geo_level)
          FROM public.item_sns s
          WHERE s.sns_name = a.sns_name
            AND s.sns_id = a.sns_id
            AND lower(sns_geo_level) IN ('filing', 'general', 'googlefin', 'regional')
    ),
    updated_at = now()
  WHERE geo_level IS NULL;

UPDATE public.sns_account a SET
    named_entity_id_excluded = (
        SELECT ARRAY(
            SELECT DISTINCT (json_array_elements(object_payload->'priorityExclusive'))::text::int AS foo
              FROM mongo.mg_publisher p
              INNER JOIN mongo.mg_publisher_sns c ON (c.publisher_mongo_id = p.object_id)
              WHERE c.sns_name = a.sns_name
                AND c.sns_id = a.sns_id
              ORDER BY (json_array_elements(object_payload->'priorityExclusive'))::text::int
        )
    ),
    updated_at = now();

UPDATE public.sns_account SET
    named_entity_id_excluded = NULL,
    updated_at = now()
  WHERE named_entity_id_excluded = '{}';


-- Item To NamedEntity
INSERT INTO public.named_entity (
    named_entity_id,
    status,
    created_at,
    updated_at,
    streaming_released_at,
    product_released_at,
    analyst_id,
    category_id,
    name,
    name_de,
    shortname,
    legalname,
    legalname_de,
    detail,
    detail_de,
    history,
    history_de,
    about,
    about_de,
    profile_url,
    avatar_url,
    country_code,
    website_url,
    is_listed_company,
    released_to,
    isin,
    valor,
    ticker,
    ceo,
    headquarter,
    company_size,
    alert,
    force_alert_expired
)
    SELECT
        id,
        CASE
            WHEN ready = 1 THEN 1
            WHEN profileready = 0 THEN 4
            WHEN profileready = 1 THEN 3
            WHEN profileready = 2 THEN 2
        END,
        created,
        updated,
        CASE
            WHEN ready = 1 THEN lastready
            WHEN profileready = 0 THEN '2015-05-28'
        END,
        lastready,
        analyst_id,
        (
            COALESCE (
                (SELECT category_id
                  FROM public.category
                  WHERE category_mongo_id = cat6),
                (SELECT category_id
                  FROM public.category
                  WHERE category_mongo_id = cat5),
                (SELECT category_id
                  FROM public.category
                  WHERE category_mongo_id = cat4),
                (SELECT category_id
                  FROM public.category
                  WHERE category_mongo_id = cat3),
                (SELECT category_id
                  FROM public.category
                  WHERE category_mongo_id = cat2),
                (SELECT category_id
                  FROM public.category
                  WHERE category_mongo_id = cat1),
                (SELECT category_id
                  FROM public.category
                  WHERE category_mongo_id = itemgroup),
                (SELECT category_id
                  FROM public.category
                  WHERE category_mongo_id = itemtype)
            )
        ),
        name,
        NULLIF(TRIM(BOTH FROM name_de), ''),
        NULLIF(TRIM(BOTH FROM shortname), ''),
        NULLIF(TRIM(BOTH FROM legalname), ''),
        NULLIF(TRIM(BOTH FROM legalname_de), ''),
        NULLIF(TRIM(BOTH FROM detail), ''),
        NULLIF(TRIM(BOTH FROM detail_de), ''),
        NULLIF(TRIM(BOTH FROM history), ''),
        NULLIF(TRIM(BOTH FROM history_de), ''),
        NULLIF(TRIM(BOTH FROM about_us), ''),
        NULLIF(TRIM(BOTH FROM about_us_de), ''),
        NULLIF(TRIM(BOTH FROM profilelink), ''),
        NULLIF(TRIM(BOTH FROM photo), ''),
        UPPER(TRIM(BOTH FROM isocode)),
        NULLIF(TRIM(BOTH FROM www), ''),
        CASE profile_cat
            WHEN 'Listed Companies' THEN true
            ELSE NULL
        END,
        regexp_split_to_array(TRIM(BOTH FROM released), ',[ ]*'),
        regexp_split_to_array(TRIM(BOTH FROM isin), ',[ ]*'),
        regexp_split_to_array(TRIM(BOTH FROM valor), ',[ ]*'),
        NULLIF(TRIM(BOTH FROM ticker), ''),
        NULLIF(TRIM(BOTH FROM ceo), ''),
        NULLIF(TRIM(BOTH FROM headquarter), ''),
        NULLIF(TRIM(BOTH FROM companysize), ''),
        alert_enable,
        forcealertexpired
      FROM public.search_item;


-- Publisher To NamedEntity
INSERT INTO public.named_entity (
    named_entity_id,
    status,
    created_at,
    updated_at,
    category_id,
    partner_id,
    name,
    detail,
    avatar_url,
    country_code
)
    SELECT
        named_entity_id,
        CASE
            WHEN p1.object_payload->>'active' = 'true' THEN 2
            ELSE 0
        END,
        created_at,
        updated_at,
        (
            SELECT (foo->>'_id')::int
              FROM (
                SELECT json_array_elements(object_payload->'categories') AS foo
                  FROM mongo.mg_publisher p2
                  WHERE p2.object_id = p1.object_id
                    AND (object_payload->>'categories') NOT IN (NULL, '{}')
              ) bar
              ORDER BY (foo->>'level')::smallint DESC
              LIMIT 1
        ),
        partner_id,
        trim(both from object_payload->>'name'),
        trim(both from object_payload->>'description'),
        trim(both from object_payload->>'image'),
        upper(trim(both from object_payload->>'countryCode')) -- SELECT COUNT(*)
      FROM public.publisher p1
      WHERE p1.object_payload->>'active' = 'true'
        AND (p1.object_payload->'itemId') IS NULL
        AND (p1.object_payload->>'status') IN ('1', '2', '2.0')
        AND EXISTS (
            SELECT NULL
              FROM mongo.mg_publisher_sns ps
              WHERE ps.publisher_mongo_id = p1.object_id
        );


-- Migrate Links Item-SNS, Publisher-SNS To NamedEntity-SNS
CREATE TABLE public._t AS (
    SELECT named_entity_id, sns_name, sns_id
      FROM public.item_sns s
      INNER JOIN public.id_map m ON (s.item_id = m.id)
      WHERE s.status = 1
        AND EXISTS (
            SELECT NULL
              FROM public.named_entity n
              WHERE n.named_entity_id = m.named_entity_id
        )
);

CREATE INDEX _idx_t ON public._t USING btree(sns_name, sns_id);

UPDATE public.sns_account sa SET
    named_entity_id = (
        SELECT named_entity_id
          FROM public._t
          WHERE _t.sns_name = sa.sns_name
            AND _t.sns_id = sa.sns_id
    ),
    updated_at = now();

DROP TABLE public._t;

CREATE TABLE public._t AS (
    SELECT named_entity_id, sns_name, sns_id
      FROM mongo.mg_publisher_sns s
      INNER JOIN public.id_map m ON (s.publisher_mongo_id = m.object_id)
      WHERE EXISTS (
            SELECT NULL
              FROM public.named_entity n
              WHERE n.named_entity_id = m.named_entity_id
        )
);

CREATE INDEX _idx_t ON public._t USING btree(sns_name, sns_id);

UPDATE public.sns_account sa SET
    named_entity_id = (
        SELECT named_entity_id
          FROM public._t
          WHERE _t.sns_name = sa.sns_name
            AND _t.sns_id = sa.sns_id
            LIMIT 1 -- Hack due to new created duplicates
    ),
    updated_at = now()
  WHERE named_entity_id IS NULL;

DROP TABLE public._t;


-- Migrate Publisher's tags to sns_account.sns_tag
-- priority
WITH _t AS (
    SELECT sns_name, sns_id
      FROM mongo.mg_publisher_sns ps
      WHERE EXISTS (
        SELECT NULL
          FROM mongo.mg_publisher p
          WHERE p.object_id = ps.publisher_mongo_id
            AND (p.object_payload->>'priority') = '1'
      )
) -- SELECT COUNT(*) FROM public.sns_account sa
UPDATE public.sns_account sa SET
    sns_tag = (SELECT array_agg(distinct foo) FROM unnest(sns_tag || ARRAY['priority']::text[]) as foo)
  WHERE EXISTS (
    SELECT NULL
      FROM _t
      WHERE _t.sns_name = sa.sns_name
        AND _t.sns_id = sa.sns_id
  ) AND 'priority' != ALL(sns_tag);

-- newsAggregator
WITH _t AS (
    SELECT sns_name, sns_id
      FROM mongo.mg_publisher_sns ps
      WHERE EXISTS (
        SELECT NULL
          FROM mongo.mg_publisher p
          WHERE p.object_id = ps.publisher_mongo_id
            AND (p.object_payload->>'newsAggregator') = '1'
      )
) -- SELECT COUNT(*) FROM public.sns_account sa
UPDATE public.sns_account sa SET
    sns_tag = (SELECT array_agg(distinct foo) FROM unnest(sns_tag || ARRAY['news_aggregator']::text[]) as foo)
  WHERE EXISTS (
    SELECT NULL
      FROM _t
      WHERE _t.sns_name = sa.sns_name
        AND _t.sns_id = sa.sns_id
  ) AND 'news_aggregator' != ALL(sns_tag);

-- celeb
WITH _t AS (
    SELECT sns_name, sns_id
      FROM mongo.mg_publisher_sns ps
      WHERE EXISTS (
        SELECT NULL
          FROM public.publisher_tag p
          WHERE p.publisher_mongo_id = ps.publisher_mongo_id
            AND 'celeb' = ANY(tag_list)
      )
) -- SELECT COUNT(*) FROM public.sns_account sa
UPDATE public.sns_account sa SET
    sns_tag = (SELECT array_agg(distinct foo) FROM unnest(sns_tag || ARRAY['celeb']::text[]) as foo)
  WHERE EXISTS (
    SELECT NULL
      FROM _t
      WHERE _t.sns_name = sa.sns_name
        AND _t.sns_id = sa.sns_id
  ) AND 'celeb' != ALL(sns_tag);

-- paywall
WITH _t AS (
    SELECT sns_name, sns_id
      FROM mongo.mg_publisher_sns ps
      WHERE EXISTS (
        SELECT NULL
          FROM mongo.mg_publisher p
          WHERE p.object_id = ps.publisher_mongo_id
            AND (p.object_payload->>'paywall') = '1'
      )
) -- SELECT COUNT(*) FROM public.sns_account sa
UPDATE public.sns_account sa SET
    sns_tag = (SELECT array_agg(distinct foo) FROM unnest(sns_tag || ARRAY['paywall']::text[]) as foo)
  WHERE EXISTS (
    SELECT NULL
      FROM _t
      WHERE _t.sns_name = sa.sns_name
        AND _t.sns_id = sa.sns_id
  ) AND 'paywall' != ALL(sns_tag);

-- verified
WITH _t AS (
    SELECT sns_name, sns_id
      FROM mongo.mg_publisher_sns ps
      WHERE EXISTS (
        SELECT NULL
          FROM mongo.mg_publisher p
          WHERE p.object_id = ps.publisher_mongo_id
            AND (p.object_payload->'extras'->>'verified') = 'true'
      )
) -- SELECT COUNT(*) FROM public.sns_account sa
UPDATE public.sns_account sa SET
    sns_tag = (SELECT array_agg(distinct foo) FROM unnest(sns_tag || ARRAY['verified']::text[]) as foo)
  WHERE EXISTS (
    SELECT NULL
      FROM _t
      WHERE _t.sns_name = sa.sns_name
        AND _t.sns_id = sa.sns_id
  ) AND 'verified' != ALL(sns_tag);


-- News Country Code
UPDATE public.sns_account sa SET
    country_code = (
        SELECT UPPER(NULLIF(TRIM(BOTH FROM p.object_payload->>'countryCode'), ''))
          FROM mongo.mg_publisher_sns ps
          INNER JOIN mongo.mg_publisher p ON (ps.publisher_mongo_id = p.object_id)
          WHERE ps.sns_name = sa.sns_name
            AND ps.sns_id = sa.sns_id
          LIMIT 1
    ),
    updated_at = now()
  WHERE sns_name = 'news';


-- mg_publisher.partner_id To named_entity.partner_id
CREATE INDEX idx_named_entity_partner_id ON public.named_entity USING btree(partner_id) WHERE partner_id IS NOT NULL;
UPDATE public.named_entity ne SET
    partner_id = (
        SELECT partner_id
          FROM public.publisher p
          WHERE p.named_entity_id = ne.named_entity_id
    ),
    updated_at = now()
  WHERE EXISTS (
    SELECT NULL
      FROM publisher p1
      WHERE p1.partner_id IS NOT NULL
        AND p1.named_entity_id = ne.named_entity_id
  );


-- Hack for Thong
ALTER TABLE public.sns_account ADD COLUMN screen_name text;
UPDATE public.sns_account SET screen_name = payload->>'screen_name' WHERE sns_name = 'tw';
