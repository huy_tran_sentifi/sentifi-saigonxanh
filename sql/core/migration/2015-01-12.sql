CREATE TABLE public._bw_category_map (
    industry_subsector_local    text,
    category_id                 int,
    cat3                        text,
    cat2                        text,
    cat1                        text,
    itemgroup                   text
);

COPY public._bw_category_map FROM '/tmp/businessweek_category_map.csv' WITH CSV HEADER;


-- Verify the map provided by Dannie
-- Verify ``category_id``
SELECT *
  FROM public._bw_category_map bwc
  WHERE NOT EXISTS (
    SELECT NULL
      FROM public.category c
      WHERE c.category_id = bwc.category_id
  );
-- Verify category's names
SELECT
    category_id,
    position(TRIM(BOTH '~' FROM (COALESCE(itemgroup, '') || '~' || COALESCE(cat1, '') || '~' || COALESCE(cat2, '') || '~' || COALESCE(cat3, ''))) IN get_category_name_path(category_id, '~')) > 0 AS is_ok,
    TRIM(BOTH '~' FROM (COALESCE(itemgroup, '') || '~' || COALESCE(cat1, '') || '~' || COALESCE(cat2, '') || '~' || COALESCE(cat3, ''))) AS dannie_category_name_path,
    get_category_name_path(category_id, '~') AS system_name_path
  FROM _bw_category_map
  WHERE (position(TRIM(BOTH '~' FROM (COALESCE(itemgroup, '') || '~' || COALESCE(cat1, '') || '~' || COALESCE(cat2, '') || '~' || COALESCE(cat3, ''))) IN get_category_name_path(category_id, '~')) > 0) IS NOT true;
-- Verify BusinessWeek sectors
SELECT
    lower(industry_subsector_bw),
    COUNT(*)
  FROM
    public.item i
    INNER JOIN public.item_audit ia USING (id)
  WHERE profileready != 0
    AND NOT EXISTS (
      SELECT NULL
        FROM public._bw_category_map bcm
        WHERE lower(bcm.industry_subsector_local) = lower(ia.industry_subsector_bw)
    )
  GROUP BY lower(industry_subsector_bw)
  ORDER BY COUNT DESC;

SELECT
    lower(industry_subsector_local)
  FROM public._bw_category_map bcm
  WHERE NOT EXISTS (
      SELECT NULL
        FROM
          item i
          INNER JOIN item_audit ia USING (id)
        WHERE profileready != 0
          AND lower(ia.industry_subsector_bw) = lower(bcm.industry_subsector_local)
    );


-- Create the mapping
CREATE OR REPLACE FUNCTION public.get_parent_category_mongo_id(IN category_id int, IN category_level int) RETURNS text AS $$
WITH RECURSIVE t(category_id, parent_category_id, category_mongo_id, category_level) AS (
    SELECT category_id, parent_category_id, category_mongo_id, category_level
      FROM category
      WHERE category_id = $1
    UNION
    SELECT c2.category_id, c2.parent_category_id, c2.category_mongo_id, c2.category_level
      FROM t c1, category c2
      WHERE c1.parent_category_id = c2.category_id
)
SELECT category_mongo_id
  FROM t
  WHERE category_level = $2
$$ LANGUAGE SQL;

CREATE TABLE public._40k_category AS (
    SELECT
        id as item_id,
        (
            SELECT category_id
              FROM public._bw_category_map bcm
              WHERE lower(bcm.industry_subsector_local) = lower(ia.industry_subsector_bw)
              LIMIT 1
        ),
        NULL::text AS itemtype,
        NULL::text AS itemgroup,
        NULL::text AS cat1,
        NULL::text AS cat2,
        NULL::text AS cat3
      FROM item_audit ia
      WHERE EXISTS (
        SELECT NULL
          FROM item i
          WHERE i.id = ia.id
            AND profileready != 0
      )
);

CREATE INDEX _idx_40k_category_item_id ON public._40k_category USING btree(item_id);

UPDATE public._40k_category SET
    itemtype = public.get_parent_category_mongo_id(category_id, 1),
    itemgroup = public.get_parent_category_mongo_id(category_id, 2),
    cat1 = public.get_parent_category_mongo_id(category_id, 3),
    cat2 = public.get_parent_category_mongo_id(category_id, 4),
    cat3 = public.get_parent_category_mongo_id(category_id, 5);

/* Double-check
SELECT
    c.item_id,
    i.name,
    get_category_name_path(category_id, '~'),
    (SELECT category_name FROM category WHERE category_mongo_id = c.itemtype),
    (SELECT category_name FROM category WHERE category_mongo_id = c.itemgroup),
    (SELECT category_name FROM category WHERE category_mongo_id = c.cat1),
    (SELECT category_name FROM category WHERE category_mongo_id = c.cat2),
    (SELECT category_name FROM category WHERE category_mongo_id = c.cat3)
  FROM
    public._40k_category c
    INNER JOIN item i ON (id = item_id);
*/

CREATE TABLE _item_category_backup_20140113 AS
    SELECT * FROM item;

UPDATE item i SET
    itemtype = COALESCE((SELECT itemtype FROM public._40k_category c WHERE c.item_id = i.id LIMIT 1), ''),
    itemgroup = COALESCE((SELECT itemgroup FROM public._40k_category c WHERE c.item_id = i.id LIMIT 1), ''),
    cat1 = COALESCE((SELECT cat1 FROM public._40k_category c WHERE c.item_id = i.id LIMIT 1), ''),
    cat2 = COALESCE((SELECT cat2 FROM public._40k_category c WHERE c.item_id = i.id LIMIT 1), ''),
    cat3 = COALESCE((SELECT cat3 FROM public._40k_category c WHERE c.item_id = i.id LIMIT 1), ''),
    updated = now() -- SELECT COUNT(*) FROM item
  WHERE profileready != 0;
