ALTER TABLE public.indice ADD COLUMN country_code text;

UPDATE public.indice SET
    country_code = 'CH',
    updated = now()
  WHERE id IN (4, 5, 6, 29);

UPDATE public.indice SET
    country_code = 'CN',
    updated = now()
  WHERE id IN (25);

UPDATE public.indice SET
    country_code = 'DE',
    updated = now()
  WHERE id IN (1, 2, 3, 14, 15, 16);

UPDATE public.indice SET
    country_code = 'ES',
    updated = now()
  WHERE id IN (27);

UPDATE public.indice SET
    country_code = 'HK',
    updated = now()
  WHERE id IN (17, 26, 28);

UPDATE public.indice SET
    country_code = 'IN',
    updated = now()
  WHERE id IN (18, 30);

UPDATE public.indice SET
    country_code = 'JP',
    updated = now()
  WHERE id IN (20);

UPDATE public.indice SET
    country_code = 'SG',
    updated = now()
  WHERE id IN (19);

UPDATE public.indice SET
    country_code = 'UK',
    updated = now()
  WHERE id IN (8, 9, 10, 22, 23, 24);

UPDATE public.indice SET
    country_code = 'US',
    updated = now()
  WHERE id IN (11, 12, 13, 31);
