ALTER TABLE public.sentifi_user ADD COLUMN product_id smallint;
ALTER TABLE public.sentifi_user ADD COLUMN platform_id smallint;
COMMENT ON COLUMN public.sentifi_user.product_id IS 'ID of the product from which User registered his/her account
1: hybrid iOs, 2: hybrid Android, 3: iOs, 4: Android, 5: Web';
COMMENT ON COLUMN public.sentifi_user.platform_id IS 'ID of the client platform from which User registered his/her account
1: hybrid iOs, 2: hybrid Android, 3: iOs, 4: Android, 5: Web';


COMMENT ON COLUMN public.user_setting.product_id IS '1: Sentifi One, 2: myMarkets, 3: myCompany, 4: myScore';

CREATE TABLE public._user_setting AS (
    SELECT
        user_id,
        min(status),
        min(created_at) AS created_at,
        max(updated_at) AS updated_at,
        product_id,
        min(email_notification) AS email_notification
      FROM public.user_setting
      GROUP BY
        user_id,
        product_id
);
TRUNCATE TABLE public.user_setting;
INSERT INTO public.user_setting SELECT * FROM public._user_setting;
CREATE UNIQUE INDEX idx_user_setting_user_id_product_id ON public.user_setting USING btree(user_id, product_id);
DROP INDEX idx_user_setting_user_id;
DROP TABLE public._user_setting;


ALTER TABLE public.exchange ADD COLUMN moreover_name text;
ALTER TABLE public.exchange ADD COLUMN moreover_exchange text[];
ALTER TABLE public.ticker1 ADD COLUMN moreover_exchange text[];
ALTER TABLE public.ticker1 ADD COLUMN moreover_ticker text[];

ALTER TABLE public.named_entity ADD COLUMN cover_image_url text;
