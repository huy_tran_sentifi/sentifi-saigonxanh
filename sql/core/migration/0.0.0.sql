-- PREPARATION
CREATE TABLE public.id_map (
    named_entity_id uuid    PRIMARY KEY DEFAULT uuid_generate_v1(),
    object_id       uuid    NOT NULL DEFAULT uuid_generate_v1(),
    id              int
);

INSERT INTO public.id_map(id)
    SELECT id
      FROM public.item
      INNER JOIN item_audit USING (id);

CREATE VIEW public.search_item AS
    SELECT *
      FROM public.id_map
      INNER JOIN public.item USING (id)
      INNER JOIN public.item_audit USING (id);


-- PRIVILLEGE
GRANT USAGE ON SCHEMA mongo, public, sns TO dbo, dbw, dbr, root;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA mongo, public, sns TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA mongo, public, sns TO dbo, dbw, root;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA mongo, public, sns TO dbo, dbw, root;

GRANT SELECT ON ALL TABLES IN SCHEMA mongo, public, sns TO dbr;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA mongo, public, sns TO dbr;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA mongo, public, sns TO dbr;


-- TRANSFORMATION
-- PERSON
INSERT INTO public.person (
    person_id,
    created_at,
    updated_at,
    person_name,
    title,
    detail,
    country_code,
    person_url,
    photo_url,
    guru
) SELECT
    object_id,
    created,
    updated,
    NULLIF(name, ''),
    NULLIF(title, ''),
    NULLIF(detail, ''),
    COALESCE(NULLIF(isocode, ''), NULLIF(country, '')),
    NULLIF(www, ''),
    NULLIF(photo, ''),
    guru
  FROM public.search_item
  WHERE itemtype = '52d4bd98e4b08a17cb0aa9bf';


-- ORGANIZATION
INSERT INTO public.organization (
    organization_id,
    created_at,
    updated_at,
    organization_name,
    legal_name,
    short_name,
    detail,
    address,
    country_code,
    headquarter,
    chairman,
    ceo,
    cfo,
    industry,
    founded,
    organization_size,
    isin,
    ticker,
    valor,
    organization_url,
    photo_url
) SELECT
    object_id,
    created,
    updated,
    NULLIF(name, ''),
    NULLIF(legalname, ''),
    NULLIF(shortname, ''),
    NULLIF(detail, ''),
    NULLIF(contact, ''),
    NULLIF(isocode, ''),
    NULLIF(headquarter, ''),
    NULLIF(chairman, ''),
    NULLIF(ceo, ''),
    NULLIF(cfo, ''),
    NULLIF(industry, ''),
    NULLIF(founded, ''),
    NULLIF(companysize, ''),
    regexp_split_to_array(NULLIF(isin, ''), ','),
    regexp_split_to_array(NULLIF(ticker, ''), ','),
    regexp_split_to_array(NULLIF(valor, ''), ',')::int[],
    NULLIF(www, ''),
    NULLIF(photo, '')
  FROM public.search_item
  WHERE itemtype = '52d4bd98e4b08a17cb0aa9c0';


-- INDEX
INSERT INTO public.stock_index (
    index_id,
    created_at,
    updated_at,
    index_name,
    legal_name,
    short_name,
    isin,
    ticker,
    valor,
    detail,
    country_code
) SELECT
    object_id,
    created,
    updated,
    NULLIF(name, ''),
    NULLIF(legalname, ''),
    NULLIF(shortname, ''),
    NULLIF(isin, ''),
    NULLIF(ticker, ''),
    regexp_split_to_array(NULLIF(valor, ''), ',')::int[],
    NULLIF(detail, ''),
    COALESCE(NULLIF(isocode, ''), NULLIF(country, ''))
  FROM public.search_item
  WHERE itemtype = '53196c43ac360137518b4569';


-- COMMODITY
INSERT INTO public.commodity (
    commodity_id,
    created_at,
    updated_at,
    commodity_name,
    legal_name,
    short_name,
    isin,
    ticker,
    valor,
    detail
) SELECT
    object_id,
    created,
    updated,
    NULLIF(name, ''),
    NULLIF(legalname, ''),
    NULLIF(shortname, ''),
    regexp_split_to_array(NULLIF(isin, ''), ','),
    NULLIF(ticker, ''),
    regexp_split_to_array(NULLIF(valor, ''), ',')::int[],
    NULLIF(detail, '')
  FROM public.search_item
  WHERE itemtype = '53196c43ac360137518b456b';


-- CURRENCY
INSERT INTO public.currency (
    currency_id,
    created_at,
    updated_at,
    currency_name,
    legal_name,
    short_name,
    isin,
    ticker,
    valor,
    country_code
) SELECT
    object_id,
    created,
    updated,
    NULLIF(name, ''),
    NULLIF(legalname, ''),
    NULLIF(shortname, ''),
    NULLIF(isin, ''),
    NULLIF(ticker, ''),
    array_remove(regexp_split_to_array(NULLIF(valor, ''), ','), ' ')::int[],
    COALESCE(NULLIF(isocode, ''), NULLIF(country, ''))
  FROM public.search_item
  WHERE itemtype = '53196c43ac360137518b456a';


-- EVENT
INSERT INTO public.event (
    event_id,
    created_at,
    updated_at,
    event_name,
    legal_name,
    short_name,
    valor,
    detail,
    event_url
) SELECT
    object_id,
    created,
    updated,
    NULLIF(name, ''),
    NULLIF(legalname, ''),
    NULLIF(shortname, ''),
    array_remove(regexp_split_to_array(NULLIF(valor, ''), ','), ' ')::int[],
    NULLIF(detail, ''),
    NULLIF(www, '')
  FROM public.search_item
  WHERE itemtype = '52d4bd9ae4b08a17cb0aaa80';


-- SECURITY
INSERT INTO public.security (
    security_id,
    created_at,
    updated_at,
    security_name,
    short_name,
    isin,
    ticker,
    valor,
    detail,
    country_code
) SELECT
    object_id,
    created,
    updated,
    NULLIF(name, ''),
    NULLIF(shortname, ''),
    regexp_split_to_array(NULLIF(isin, ''), ','),
    NULLIF(ticker, ''),
    regexp_split_to_array(NULLIF(valor, ''), ',')::int[],
    NULLIF(detail, ''),
    COALESCE(NULLIF(isocode, ''), NULLIF(country, ''))
  FROM public.search_item
  WHERE itemtype = '53196c43ac360137518b456c';


-- NAMED ENTITY
INSERT INTO public.named_entity (
    named_entity_id,
    created_at,
    updated_at,
    ready_at,
    old_id,
    object_id,
    category_id,
    updated_by_analyst_id,
    named_entity_name,
    named_entity_name_de,
    named_entity_type,
    detail,
    detail_de,
    named_entity_url,
    is_topic,
    audited_field,
    synonym,
    released,
    sentifi_o,
    config,
    blacklist,
    keyword,
    profile_cat,
    rss,
    businessweek_url,
    sector,
    company,
    issue_curation,
    ready,
    alert_enabled,
    slug,
    indice,
    legal_misspelled,
    sector_local,
    sector_bw,
    industry_subsector_local,
    industry_subsector_bw,
    stock_exchange,
    ticker_local,
    ticker_bloomberg,
    ir,
    se_mainstock,
    se_sourceurl,
    se_document,
    se_documenturl,
    cashtag,
    cashtag_local,
    market
) SELECT
    named_entity_id,
    created,
    updated,
    lastready,
    id,
    object_id,
    (
        COALESCE (
            (SELECT category_id
              FROM public.category
              WHERE category_mongo_id = cat3),
            (SELECT category_id
              FROM public.category
              WHERE category_mongo_id = cat2),
            (SELECT category_id
              FROM public.category
              WHERE category_mongo_id = cat1),
            (SELECT category_id
              FROM public.category
              WHERE category_mongo_id = itemgroup),
            (SELECT category_id
              FROM public.category
              WHERE category_mongo_id = itemtype)
        )
    ),
    analyst_id,
    NULLIF(name, ''),
    NULLIF(name_de, ''),
    type,
    NULLIF(detail, ''),
    NULLIF(detail_de, ''),
    NULLIF(profilelink, ''),
    NOT profileready::int::boolean,
    regexp_split_to_array(NULLIF(audited, ''), ','),
    regexp_split_to_array(NULLIF(synonym, ''), ','),
    regexp_split_to_array(NULLIF(released, ''), ','),
    sentifi_o,
    NULLIF(config, ''),
    NULLIF(blacklists, ''),
    NULLIF(keywords, ''),
    NULLIF(profile_cat, ''),
    NULLIF(rss, ''),
    NULLIF(link, ''),
    NULLIF(sector, ''),
    NULLIF(company, ''),
    issue_curation,
    ready,
    alert_enable,
    NULLIF(slug, ''),
    indice,
    NULLIF(legal_misspelled, ''),
    NULLIF(sector_local, ''),
    NULLIF(sector_bw, ''),
    NULLIF(industry_subsector_local, ''),
    NULLIF(industry_subsector_bw, ''),
    NULLIF(stock_exchange, ''),
    NULLIF(ticker_local, ''),
    NULLIF(ticker_bloomberg, ''),
    NULLIF(ir, ''),
    NULLIF(se_mainstock, ''),
    NULLIF(se_sourceurl, ''),
    NULLIF(se_document, ''),
    NULLIF(se_documenturl, ''),
    NULLIF(cashtag, ''),
    NULLIF(cashtag_local, ''),
    NULLIF(market, '')
  FROM public.search_item;


-- NAMED ENTITY AUDIT HISTORY
INSERT INTO public.named_entity_audit_history (
    named_entity_id,
    updated_at,
    ready_at,
    old_id,
    object_id,
    category_id,
    updated_by_analyst_id,
    named_entity_name,
    named_entity_name_de,
    named_entity_type,
    detail,
    detail_de,
    named_entity_url,
    is_topic,
    audited_field,
    synonym,
    released,
    sentifi_o,
    config,
    blacklist,
    keyword,
    profile_cat,
    rss,
    businessweek_url,
    sector,
    company,
    issue_curation,
    ready,
    alert_enabled,
    slug,
    indice,
    legal_misspelled,
    sector_local,
    sector_bw,
    industry_subsector_local,
    industry_subsector_bw,
    stock_exchange,
    ticker_local,
    ticker_bloomberg,
    ir,
    se_mainstock,
    se_sourceurl,
    se_document,
    se_documenturl,
    cashtag,
    cashtag_local,
    market
) SELECT
    named_entity_id,
    updated,
    lastready,
    id,
    object_id,
    (
        COALESCE (
            (SELECT category_id
              FROM public.category
              WHERE category_mongo_id = cat3),
            (SELECT category_id
              FROM public.category
              WHERE category_mongo_id = cat2),
            (SELECT category_id
              FROM public.category
              WHERE category_mongo_id = cat1),
            (SELECT category_id
              FROM public.category
              WHERE category_mongo_id = itemgroup),
            (SELECT category_id
              FROM public.category
              WHERE category_mongo_id = itemtype)
        )
    ),
    analyst_id,
    NULLIF(name, ''),
    NULLIF(name_de, ''),
    type,
    NULLIF(detail, ''),
    NULLIF(detail_de, ''),
    NULLIF(profilelink, ''),
    NOT profileready::int::boolean,
    regexp_split_to_array(NULLIF(audited, ''), ','),
    regexp_split_to_array(NULLIF(synonym, ''), ','),
    regexp_split_to_array(NULLIF(released, ''), ','),
    sentifi_o,
    NULLIF(config, ''),
    NULLIF(blacklists, ''),
    NULLIF(keywords, ''),
    NULLIF(profile_cat, ''),
    NULLIF(rss, ''),
    NULLIF(link, ''),
    NULLIF(sector, ''),
    NULLIF(company, ''),
    issue_curation,
    ready,
    alert_enable,
    NULLIF(slug, ''),
    indice,
    NULLIF(legal_misspelled, ''),
    NULLIF(sector_local, ''),
    NULLIF(sector_bw, ''),
    NULLIF(industry_subsector_local, ''),
    NULLIF(industry_subsector_bw, ''),
    NULLIF(stock_exchange, ''),
    NULLIF(ticker_local, ''),
    NULLIF(ticker_bloomberg, ''),
    NULLIF(ir, ''),
    NULLIF(se_mainstock, ''),
    NULLIF(se_sourceurl, ''),
    NULLIF(se_document, ''),
    NULLIF(se_documenturl, ''),
    NULLIF(cashtag, ''),
    NULLIF(cashtag_local, ''),
    NULLIF(market, '')
  FROM public.search_item;


-- NAMED ENTITY AUDIT INFORMATION
/*
INSERT INTO public.named_entity_audit_information (
    named_entity_id,
    created_at,
    updated_at,
    analyst_id,
    audited_field,
    synonym,
    released,
    sentifi_o,
    config,
    blacklist,
    keyword,
    profile_cat,
    rss,
    businessweek_url,
    sector,
    company,
    issue_curation,
    ready,
    alert_enabled,
    named_entity_type,
    last_ready,
    slug,
    indice,
    legal_misspelled,
    sector_local,
    sector_bw,
    industry_subsector_local,
    industry_subsector_bw,
    stock_exchange,
    ticker_local,
    ticker_bloomberg,
    ir,
    se_mainstock,
    se_sourceurl,
    se_document,
    se_documenturl,
    cashtag,
    cashtag_local,
    market
) SELECT
    named_entity_id,
    created,
    updated,
    analyst_id,
    regexp_split_to_array(NULLIF(audited, ''), ','),
    regexp_split_to_array(NULLIF(synonym, ''), ','),
    regexp_split_to_array(NULLIF(released, ''), ','),
    sentifi_o,
    NULLIF(config, ''),
    NULLIF(blacklists, ''),
    NULLIF(keywords, ''),
    NULLIF(profile_cat, ''),
    NULLIF(rss, ''),
    NULLIF(link, ''),
    NULLIF(sector, ''),
    NULLIF(company, ''),
    issue_curation,
    ready,
    alert_enable,
    type,
    lastready,
    NULLIF(slug, ''),
    indice,
    NULLIF(legal_misspelled, ''),
    NULLIF(sector_local, ''),
    NULLIF(sector_bw, ''),
    NULLIF(industry_subsector_local, ''),
    NULLIF(industry_subsector_bw, ''),
    NULLIF(stock_exchange, ''),
    NULLIF(ticker_local, ''),
    NULLIF(ticker_bloomberg, ''),
    NULLIF(ir, ''),
    NULLIF(se_mainstock, ''),
    NULLIF(se_sourceurl, ''),
    NULLIF(se_document, ''),
    NULLIF(se_documenturl, ''),
    NULLIF(cashtag, ''),
    NULLIF(cashtag_local, ''),
    NULLIF(market, '')
  FROM public.search_item;
*/

-- SNS LINK
INSERT INTO public.named_entity_sns_link (
    named_entity_id,
    sns_name,
    sns_url
) (
    (
        SELECT
            named_entity_id,
            'fb',
            NULLIF(facebook, '')
          FROM public.search_item
          WHERE NULLIF(facebook, '') IS NOT NULL
    ) UNION (
        SELECT
            named_entity_id,
            'gp',
            NULLIF(gplus, '')
          FROM public.search_item
          WHERE NULLIF(gplus, '') IS NOT NULL
    ) UNION (
        SELECT
            named_entity_id,
            'li',
            NULLIF(linkedin, '')
          FROM public.search_item
          WHERE NULLIF(linkedin, '') IS NOT NULL
    ) UNION (
        SELECT
            named_entity_id,
            'tw',
            NULLIF(twitter, '')
          FROM public.search_item
          WHERE NULLIF(twitter, '') IS NOT NULL
    )
);
