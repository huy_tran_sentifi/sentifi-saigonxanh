* ``2016-06-07.sql`` [[CORE-25] Add column `keyword_verified` to table `namd_entity` and populate it with data](https://sentifi.atlassian.net/browse/CORE-25)
* ``2015-12-22.sql`` Add column `product_id` to table `sentifi_user` and add Moreover echange name to table `exchange` and `ticker1`
* ``2015-10-22.sql`` Fix after Publisher ID Migration
* ``2015-10-19.sql`` Add column `widgets` for table `partner`
* ``2015-09-24.sql`` Add column `country_code` for table `indice`
* ``2015-09-07.sql`` Add column `category_id` to replace `category_mongo_id` for table `item_portfolio`
* ``2015-08-21.sql`` Migrate Item and Publisher to NamedEntity - Part 2
* ``2015-07-21.sql`` Migrate Item and Publisher to NamedEntity - Part 1
* ``2015-07-08.sql`` Clean up table item_sns and add unique constrains (item_id, sns_name, sns_id)
* ``2015-05-11.sql`` Separate ``publisherTag`` from ``mg_publisher``
* ``2015-03-12.sql`` Migrate Ticker information from table ``public.item_audit`` to ``public.ticker``
* ``2015-03-09.sql`` Break table ``public.pattern`` to ``public.pattern``, ``public.impact`` and ``public.impact_event``
* ``2015-03-04.sql`` Add ``cat4``, ``cat5`` to table ``public.item`` and add more indexes to table ``public.category``
* ``2015-02-03.sql`` Import Cashtag and Finance Terms dictionary
* ``2015-01-30.sql`` [Updated existed ``twitter_id`` for Twitter profile by their screen_name] (https://podio.com/sentificom/3-big-data-analytics/apps/deliverables/items/19)
* ``2015-01-23.sql`` Unmute 7217 Twitter IDs by importing from Nhat's file ``list_twitter_validated_all_level.csv`` and release associated Publishers
* ``2015-01-12.sql`` [40K Reclassification based on Businessweek subsector] (https://podio.com/sentificom/3-big-data-analytics/apps/deliverables/items/10)
* ``2015-01-06.sql`` [108k RSS Feeds, Twitter of 40k] (https://podio.com/sentificom/1-frontend-team/apps/deliverables/items/92)
* ``2015-01-02.sql`` [Migrate SNS Information (Facebook, GooglePlus, LinedIn, Twitter) and RSS from table ``item_audit`` to ``item_sns``] (https://podio.com/tasks/27389821)
