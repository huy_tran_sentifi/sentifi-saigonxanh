-- Cashtag dictionary
INSERT INTO public.dictionary (status, purpose, dictionary_name, lang) VALUES (1, 'm', 'Cashtag', 'en');

CREATE TABLE public._tmp_vocab (word text);
COPY public._tmp_vocab (word) FROM '/tmp/cash_tag.csv';

INSERT INTO public.vocabulary (status, dictionary_id, word, score)
    SELECT
        1,
        1,
        trim(both word),
        1
      FROM public._tmp_vocab
      GROUP BY trim(both from word);


-- Finance Terms dictionary
INSERT INTO public.dictionary (status, purpose, dictionary_name, lang) VALUES (1, 'm', 'Finance Terms', 'en');

TRUNCATE TABLE public._tmp_vocab;
COPY public._tmp_vocab (word) FROM '/tmp/finterm.csv';

INSERT INTO public.vocabulary (status, dictionary_id, word, score)
    SELECT
        1,
        2,
        trim(both word),
        1
      FROM public._tmp_vocab
      GROUP BY trim(both from word);
