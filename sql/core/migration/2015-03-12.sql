-- Update schema
ALTER TABLE public.exchange ALTER COLUMN status SET NOT NULL;
ALTER TABLE public.exchange ALTER COLUMN status SET DEFAULT 1;

ALTER TABLE public.ticker ALTER COLUMN status SET NOT NULL;
ALTER TABLE public.ticker ALTER COLUMN status SET DEFAULT 1;

UPDATE public.ticker SET
    created_at = now() - '1 day'::interval,
    updated_at = now() - '1 day'::interval;
ALTER TABLE public.ticker ALTER COLUMN created_at SET NOT NULL;
ALTER TABLE public.ticker ALTER COLUMN created_at SET DEFAULT now();

ALTER TABLE public.ticker ALTER COLUMN updated_at SET NOT NULL;
ALTER TABLE public.ticker ALTER COLUMN updated_at SET DEFAULT now();

ALTER TABLE public.ticker DROP CONSTRAINT ticker_isin_security_name_exchange_id_key;
DROP INDEX idx_ticker_stock_index_id;
CREATE INDEX idx_ticker_stock_index_id ON public.ticker USING gin(stock_index_id);


-- Migrate ticker
INSERT INTO public.ticker (
    status,
    item_id,
    isin,
    bloomberg_ticker,
    local_ticker,
    stock_index_id
)
    SELECT
        2,
        id,
        NULLIF(isin, ''),
        NULLIF(ticker_bloomberg, ''),
        NULLIF(ticker_local, ''),
        NULLIF((
            SELECT ARRAY(
                SELECT DISTINCT id
                  FROM item_indice ii
                  WHERE ii.item_id = ia.id
                  ORDER BY id
            )
        ), '{}')
      FROM item
      INNER JOIN item_audit ia USING (id)
      WHERE NULLIF(ticker, '') IS NOT NULL;
