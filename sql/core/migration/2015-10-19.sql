ALTER TABLE public.partner ADD COLUMN widgets text[];
ALTER TABLE public.partner ADD COLUMN updated timestamp NOT NULL DEFAULT now();
