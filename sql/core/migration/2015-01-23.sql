-- Data sourcing (from Nhat)
CREATE TABLE public._item_twitter (
    item_id         int,
    screen_name     text,
    validated_by    text,
    twitter_id      text
);

COPY public._item_twitter (item_id, screen_name, validated_by, twitter_id) FROM '/tmp/40k_twitters_full_information_trimmed.csv' WITH CSV HEADER;
-- Remove data which are not validated (by machine)
DELETE FROM public._item_twitter WHERE validated_by IS NULL;

-- Table that tracks affected ``item_id``
CREATE TABLE public._affected_id (id int);
-- Fix inconsistent data caused by frontend's bug
INSERT INTO public._affected_id
    SELECT item_id -- SELECT COUNT(DISTINCT item_id)
      FROM public.item_sns
      WHERE sns_name = 'twitter';
UPDATE public.item_sns SET
    sns_name = 'tw',
    updated_at = now()
  WHERE sns_name = 'twitter';


-- Move Twitter's user ``screen_name`` from column ``sns_id`` to column ``detail``
UPDATE public.item_sns SET
    detail = sns_id,
    updated_at = now() -- SELECT COUNT(*) FROM public.item_sns
  WHERE NULLIF(detail, '') IS NULL
    AND sns_name = 'tw'
    AND status = 1;
-- Update ``sns_id`` by using ``twitter_id``
--INSERT INTO public._affected_id
--    SELECT item_id FROM public.item_sns
--      WHERE sns_name = 'tw'
--        AND status = 1;
UPDATE public.item_sns i1 SET
    sns_id = (
        SELECT twitter_id
          FROM public._item_twitter i2
          WHERE i2.item_id = i1.item_id
            AND i2.validated_by = 'crawler'
            AND replace(lower(i2.screen_name), '@', '') = replace(lower(i1.detail), '@', '')
          LIMIT 1
    ),
    updated_at = now() -- SELECT COUNT(*) FROM public.item_sns
  WHERE sns_name = 'tw'
    AND status = 1;
-- Add new ``twitter_id`` for SearchItems
INSERT INTO public.item_sns (
    id,
    item_id,
    sns_name,
    sns_geo_level,
    sns_id,
    detail
)
    SELECT
        nextval('item_sns_seq'::regclass),
        item_id,
        'tw',
        'general',
        twitter_id,
        screen_name -- SELECT COUNT(*)
      FROM public._item_twitter i1
      WHERE EXISTS (
            SELECT NULL
              FROM item i2
              WHERE i2.id = i1.item_id
        ) AND NOT EXISTS (
            SELECT NULL
              FROM item_sns i3
              WHERE i3.item_id = i1.item_id
                AND i3.sns_name = 'tw'
                AND i3.status = 1
                AND replace(lower(i3.detail), '@', '') = replace(lower(i1.screen_name), '@', '')
        );
