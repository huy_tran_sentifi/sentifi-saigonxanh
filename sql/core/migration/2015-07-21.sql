-- Update existed table
ALTER TABLE public.category
  ADD COLUMN category_id_path int[];

CREATE INDEX idx_category_category_id_path
  ON public.category
  USING gin(category_id_path);

CREATE OR REPLACE FUNCTION public.get_category_id_path_by_id(IN category_id int)
  RETURNS int[]
AS $$
WITH RECURSIVE t(category_id, parent_category_id, category_level) AS (
    SELECT category_id, parent_category_id, category_level
      FROM public.category
      WHERE category_id = $1
    UNION
    SELECT c2.category_id, c2.parent_category_id, c2.category_level
      FROM t c1, public.category c2
      WHERE c1.parent_category_id = c2.category_id
)
SELECT array_agg(DISTINCT category_id)
  FROM (
    SELECT category_id
      FROM t
      ORDER BY category_level
  ) AS foo;
$$ LANGUAGE SQL;

UPDATE public.category SET
    category_id_path = get_category_id_path_by_id(category_id),
    updated_at = now()
  WHERE status = 1;

CREATE OR REPLACE FUNCTION public.category_insert_update_trigger()
  RETURNS TRIGGER
AS $$
BEGIN
    IF (TG_OP = 'INSERT') THEN
        NEW.category_id_path = COALESCE(get_category_id_path_by_id(NEW.parent_category_id), ARRAY[]::int[]) || NEW.category_id;
    ELSIF (TG_OP = 'UPDATE') THEN
        NEW.category_id_path = get_category_id_path_by_id(NEW.category_id);
        NEW.updated_at = current_timestamp;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trigger_category
    BEFORE INSERT OR UPDATE ON public.category
    FOR EACH ROW EXECUTE PROCEDURE public.category_insert_update_trigger();


CREATE INDEX idx_role_role_id_path
  ON public.role
  USING gin(role_id_path);


-- Analyze NamedEntity network
select p1.object_payload->>'name', p2.object_payload->>'name' from mg_publisher p1 inner join mg_publisher p2 on (p1.object_id = (p2.object_payload->>'parentId')) where (p2.object_payload->>'parentId') IS NOT NULL limit 10;


-- Twitter -> SNS Account
INSERT INTO public.sns_account (
    sns_name,
    sns_id,
    created_at,
    updated_at,
    payload,
    display_name,
    avatar_url
)
    SELECT
        'tw',
        object_payload->>'id_str',
        created_at,
        updated_at,
        object_payload,
        object_payload->>'name',
        object_payload->>'profile_image_url' -- SELECT COUNT(*)
      FROM mongo.mg_twitter t
      WHERE NOT EXISTS (
        SELECT NULL
          FROM sns_account sa
          WHERE sa.sns_name = 'tw'
            AND sa.sns_id = t.object_payload->>'id_str'
      );

UPDATE public.sns_account a SET
    country_code = (SELECT location FROM public.sns_location l WHERE l.sns_name = 'tw' AND l.sns_id = a.sns_id),
    updated_at = now()
  WHERE sns_name = 'tw';


-- News -> SNS Account
INSERT INTO public.sns_account (
    sns_name,
    sns_id,
    created_at,
    updated_at,
    payload,
    display_name
)
    SELECT
        'news',
        object_id,
        created_at,
        updated_at,
        object_payload,
        object_payload->>'name'
      FROM mongo.mg_news;


UPDATE sns_account sa SET
    display_name = (
      SELECT NULLIF(TRIM(BOTH FROM object_payload->>'name'), '')
        FROM mongo.mg_publisher p
        INNER JOIN mongo.mg_publisher_sns ps ON (p.object_id = ps.publisher_mongo_id)
        WHERE ps.sns_name = sa.sns_name
          AND ps.sns_id = sa.sns_id
        LIMIT 1
    ),
    updated_at = now()
  WHERE sns_name = 'news'
    AND NULLIF(TRIM(BOTH FROM sa.display_name), '') IS NULL;

UPDATE sns_account sa SET
    avatar_url = (
      SELECT NULLIF(TRIM(BOTH FROM object_payload->>'image'), '')
        FROM mongo.mg_publisher p
        INNER JOIN mongo.mg_publisher_sns ps ON (p.object_id = ps.publisher_mongo_id)
        WHERE ps.sns_name = sa.sns_name
          AND ps.sns_id = sa.sns_id
        LIMIT 1
    ),
    updated_at = now()
  WHERE sns_name = 'news'
    AND NULLIF(TRIM(BOTH FROM sa.avatar_url), '') IS NULL;
