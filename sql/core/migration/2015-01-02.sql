-- Fix table ``item_sns``
UPDATE public.item_sns SET created_at = now()
  WHERE created_at IS NULL;

UPDATE public.item_sns SET updated_at = now()
  WHERE updated_at IS NULL;

UPDATE item_sns SET sns_geo_level = LOWER(sns_geo_level);

ALTER TABLE public.item_sns ALTER COLUMN created_at SET NOT NULL;
ALTER TABLE public.item_sns ALTER COLUMN created_at SET DEFAULT now();

ALTER TABLE public.item_sns ALTER COLUMN updated_at SET NOT NULL;
ALTER TABLE public.item_sns ALTER COLUMN updated_at SET DEFAULT now();


-- Migrate SNS information away from table ``item_audit`` to table ``item_sns``
CREATE TABLE public._affected_id (id int);

-- Twitter
CREATE TABLE _item_twitter AS
    SELECT
        id,
        TRIM(BOTH FROM regexp_split_to_table(twitter, ',')) as screen_name
      FROM public.item_audit i_a
      WHERE NULLIF(twitter, '') IS NOT NULL
        AND EXISTS (
            SELECT NULL
              FROM item i
              WHERE i.id = i_a.id
        );

INSERT INTO public._affected_id (id)
    SELECT DISTINCT id -- SELECT COUNT(*)
      FROM _item_twitter i_t
      WHERE NOT EXISTS (
            SELECT NULL
              FROM item_sns i_s
              WHERE i_s.item_id = i_t.id
                AND i_s.sns_name = 'tw'
                AND lower(i_s.sns_id) = lower(i_t.screen_name)
        );

INSERT INTO public.item_sns (
    id,
    item_id,
    sns_name,
    sns_geo_level,
    sns_id
)
    SELECT
        nextval('item_sns_seq'::regclass),
        id,
        'tw',
        'general',
        screen_name -- SELECT COUNT(*)
      FROM _item_twitter i_t
      WHERE NOT EXISTS (
            SELECT NULL
              FROM item_sns i_s
              WHERE i_s.item_id = i_t.id
                AND i_s.sns_name = 'tw'
                AND lower(i_s.sns_id) = lower(i_t.screen_name)
        );

-- RSS
CREATE TABLE _item_rss AS
    SELECT
        id,
        TRIM(BOTH FROM regexp_split_to_table(rss, ',')) as rss
      FROM public.item_audit i_a
      WHERE NULLIF(rss, '') IS NOT NULL
        AND EXISTS (
            SELECT NULL
              FROM item i
              WHERE i.id = i_a.id
        );

INSERT INTO public._affected_id (id)
    SELECT DISTINCT id -- SELECT COUNT(*)
      FROM _item_rss i_r
      WHERE NOT EXISTS (
            SELECT NULL
              FROM item_sns i_s
              WHERE i_s.item_id = i_r.id
                AND i_s.sns_name = 'rss'
                AND lower(i_s.sns_id) = lower(i_r.rss)
        );

INSERT INTO public.item_sns (
    id,
    item_id,
    sns_name,
    sns_geo_level,
    sns_id
)
    SELECT
        nextval('item_sns_seq'::regclass),
        id,
        'rss',
        'general',
        rss -- SELECT COUNT(*)
      FROM _item_rss i_r
      WHERE NOT EXISTS (
            SELECT NULL
              FROM item_sns i_s
              WHERE i_s.item_id = i_r.id
                AND i_s.sns_name = 'rss'
                AND lower(i_s.sns_id) = lower(i_r.rss)
        );

-- Facebook
CREATE TABLE _item_facebook AS
    SELECT
        id,
        TRIM(BOTH FROM regexp_split_to_table(facebook, ',')) as facebook
      FROM public.item_audit i_a
      WHERE NULLIF(facebook, '') IS NOT NULL
        AND EXISTS (
            SELECT NULL
              FROM item i
              WHERE i.id = i_a.id
        );

INSERT INTO public.item_sns (
    id,
    item_id,
    sns_name,
    sns_geo_level,
    sns_id
)
    SELECT
        nextval('item_sns_seq'::regclass),
        id,
        'fb',
        'general',
        facebook -- SELECT COUNT(*)
      FROM _item_facebook i_f
      WHERE NOT EXISTS (
            SELECT NULL
              FROM item_sns i_s
              WHERE i_s.item_id = i_f.id
                AND i_s.sns_name = 'fb'
                AND lower(i_s.sns_id) = lower(i_f.facebook)
        );

-- GooglePlus
CREATE TABLE _item_gplus AS
    SELECT
        id,
        TRIM(BOTH FROM regexp_split_to_table(gplus, ',')) as gplus
      FROM public.item_audit i_a
      WHERE NULLIF(gplus, '') IS NOT NULL
        AND EXISTS (
            SELECT NULL
              FROM item i
              WHERE i.id = i_a.id
        );

INSERT INTO public.item_sns (
    id,
    item_id,
    sns_name,
    sns_geo_level,
    sns_id
)
    SELECT
        nextval('item_sns_seq'::regclass),
        id,
        'gp',
        'general',
        gplus -- SELECT COUNT(*)
      FROM _item_gplus i_g
      WHERE NOT EXISTS (
            SELECT NULL
              FROM item_sns i_s
              WHERE i_s.item_id = i_g.id
                AND i_s.sns_name = 'gp'
                AND lower(i_s.sns_id) = lower(i_g.gplus)
        );

-- LinkedIn
CREATE TABLE _item_linkedin AS
    SELECT
        id,
        TRIM(BOTH FROM regexp_split_to_table(linkedin, ',')) as linkedin
      FROM public.item_audit i_a
      WHERE NULLIF(linkedin, '') IS NOT NULL
        AND EXISTS (
            SELECT NULL
              FROM item i
              WHERE i.id = i_a.id
        );

INSERT INTO public.item_sns (
    id,
    item_id,
    sns_name,
    sns_geo_level,
    sns_id
)
    SELECT
        nextval('item_sns_seq'::regclass),
        id,
        'li',
        'general',
        linkedin -- SELECT COUNT(*)
      FROM _item_linkedin i_l
      WHERE NOT EXISTS (
            SELECT NULL
              FROM item_sns i_s
              WHERE i_s.item_id = i_l.id
                AND i_s.sns_name = 'li'
                AND lower(i_s.sns_id) = lower(i_l.linkedin)
        );
