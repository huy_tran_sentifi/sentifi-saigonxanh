CREATE SCHEMA finance_base;

CREATE TABLE finance_base.fb_stock_price_history (
    isin            text,
    created_at      timestamp   NOT NULL DEFAULT now(),
    status_code     smallint,
    content_length  int,
    payload         text        NOT NULL
);

CREATE TABLE finance_base.fb_stock_price_daily (
    isin            text,
    created_at      timestamp   NOT NULL DEFAULT now(),
    status_code     smallint,
    content_length  int,
    payload         text
);


GRANT USAGE ON SCHEMA public, finance_base, twitter TO dbo, dbw;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, finance_base, twitter TO dbo, dbw;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, finance_base, twitter TO dbo, dbw;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, finance_base, twitter TO dbo, dbw;

ALTER ROLE dbo SET search_path = public, finance_base, twitter;
ALTER ROLE dbw SET search_path = public, finance_base, twitter;
