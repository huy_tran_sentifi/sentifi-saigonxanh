CREATE EXTENSION IF NOT EXISTS "uuid-ossp";


CREATE OR REPLACE FUNCTION public.to_timestamptz_immutable(date_str text)
  RETURNS timestamptz
  IMMUTABLE
AS $$
    SELECT date_str::timestamptz;
$$ LANGUAGE SQL;


CREATE TABLE public.twitter_tweet (
    object_id       uuid        PRIMARY KEY DEFAULT uuid_generate_v1(),
    object_mongo_id text,
    source          text,
    imported_at     timestamp   NOT NULL DEFAULT now(),
    object_payload  json
);

CREATE TABLE public.twitter_tweet_y2014m12 (
    CHECK (imported_at >= DATE '2014-12-01' AND imported_at < DATE '2015-01-01')
) INHERITS (public.twitter_tweet);
CREATE INDEX idx_twitter_tweet_y2014m12_imported_at ON public.twitter_tweet_y2014m12(imported_at);
CREATE INDEX idx_twitter_tweet_y2014m12_tweet_id ON public.twitter_tweet_y2014m12(((object_payload->>'id')::bigint));
CREATE INDEX idx_twitter_tweet_y2014m12_published_at ON public.twitter_tweet_y2014m12(to_timestamptz_immutable(object_payload->>'created_at'));
CREATE INDEX idx_twitter_tweet_y2014m12_user_id ON public.twitter_tweet_y2014m12(((object_payload->'user'->>'id')::bigint));

CREATE TABLE public.twitter_tweet_y2014m11 (
    CHECK (imported_at >= DATE '2014-11-01' AND imported_at < DATE '2014-12-01')
) INHERITS (public.twitter_tweet);
CREATE INDEX idx_twitter_tweet_y2014m11_imported_at ON public.twitter_tweet_y2014m11(imported_at);
CREATE INDEX idx_twitter_tweet_y2014m11_tweet_id ON public.twitter_tweet_y2014m11(((object_payload->>'id')::bigint));
CREATE INDEX idx_twitter_tweet_y2014m11_published_at ON public.twitter_tweet_y2014m11(to_timestamptz_immutable(object_payload->>'created_at'));
CREATE INDEX idx_twitter_tweet_y2014m11_user_id ON public.twitter_tweet_y2014m11(((object_payload->'user'->>'id')::bigint));

CREATE TABLE public.twitter_tweet_y2014m10 (
    CHECK (imported_at >= DATE '2014-10-01' AND imported_at < DATE '2014-11-01')
) INHERITS (public.twitter_tweet);
CREATE INDEX idx_twitter_tweet_y2014m10_imported_at ON public.twitter_tweet_y2014m10(imported_at);
CREATE INDEX idx_twitter_tweet_y2014m10_tweet_id ON public.twitter_tweet_y2014m10(((object_payload->>'id')::bigint));
CREATE INDEX idx_twitter_tweet_y2014m10_published_at ON public.twitter_tweet_y2014m10(to_timestamptz_immutable(object_payload->>'created_at'));
CREATE INDEX idx_twitter_tweet_y2014m10_user_id ON public.twitter_tweet_y2014m10(((object_payload->'user'->>'id')::bigint));

CREATE TABLE public.twitter_tweet_y2014m09 (
    CHECK (imported_at >= DATE '2014-09-01' AND imported_at < DATE '2014-10-01')
) INHERITS (public.twitter_tweet);
CREATE INDEX idx_twitter_tweet_y2014m09_imported_at ON public.twitter_tweet_y2014m09(imported_at);
CREATE INDEX idx_twitter_tweet_y2014m09_tweet_id ON public.twitter_tweet_y2014m09(((object_payload->>'id')::bigint));
CREATE INDEX idx_twitter_tweet_y2014m09_published_at ON public.twitter_tweet_y2014m09(to_timestamptz_immutable(object_payload->>'created_at'));
CREATE INDEX idx_twitter_tweet_y2014m09_user_id ON public.twitter_tweet_y2014m09(((object_payload->'user'->>'id')::bigint));

CREATE TABLE public.twitter_tweet_y2014m08 (
    CHECK (imported_at >= DATE '2014-08-01' AND imported_at < DATE '2014-09-01')
) INHERITS (public.twitter_tweet);
CREATE INDEX idx_twitter_tweet_y2014m08_imported_at ON public.twitter_tweet_y2014m08(imported_at);
CREATE INDEX idx_twitter_tweet_y2014m08_tweet_id ON public.twitter_tweet_y2014m08(((object_payload->>'id')::bigint));
CREATE INDEX idx_twitter_tweet_y2014m08_published_at ON public.twitter_tweet_y2014m08(to_timestamptz_immutable(object_payload->>'created_at'));
CREATE INDEX idx_twitter_tweet_y2014m08_user_id ON public.twitter_tweet_y2014m08(((object_payload->'user'->>'id')::bigint));

CREATE TABLE public.twitter_tweet_y2014m07 (
    CHECK (imported_at >= DATE '2014-07-01' AND imported_at < DATE '2014-08-01')
) INHERITS (public.twitter_tweet);
CREATE INDEX idx_twitter_tweet_y2014m07_imported_at ON public.twitter_tweet_y2014m07(imported_at);
CREATE INDEX idx_twitter_tweet_y2014m07_tweet_id ON public.twitter_tweet_y2014m07(((object_payload->>'id')::bigint));
CREATE INDEX idx_twitter_tweet_y2014m07_published_at ON public.twitter_tweet_y2014m07(to_timestamptz_immutable(object_payload->>'created_at'));
CREATE INDEX idx_twitter_tweet_y2014m07_user_id ON public.twitter_tweet_y2014m07(((object_payload->'user'->>'id')::bigint));

CREATE TABLE public.twitter_tweet_y2014m06 (
    CHECK (imported_at >= DATE '2014-06-01' AND imported_at < DATE '2014-07-01')
) INHERITS (public.twitter_tweet);
CREATE INDEX idx_twitter_tweet_y2014m06_imported_at ON public.twitter_tweet_y2014m06(imported_at);
CREATE INDEX idx_twitter_tweet_y2014m06_tweet_id ON public.twitter_tweet_y2014m06(((object_payload->>'id')::bigint));
CREATE INDEX idx_twitter_tweet_y2014m06_published_at ON public.twitter_tweet_y2014m06(to_timestamptz_immutable(object_payload->>'created_at'));
CREATE INDEX idx_twitter_tweet_y2014m06_user_id ON public.twitter_tweet_y2014m06(((object_payload->'user'->>'id')::bigint));

CREATE TABLE public.twitter_tweet_y2014m05 (
    CHECK (imported_at >= DATE '2014-05-01' AND imported_at < DATE '2014-06-01')
) INHERITS (public.twitter_tweet);
CREATE INDEX idx_twitter_tweet_y2014m05_imported_at ON public.twitter_tweet_y2014m05(imported_at);
CREATE INDEX idx_twitter_tweet_y2014m05_tweet_id ON public.twitter_tweet_y2014m05(((object_payload->>'id')::bigint));
CREATE INDEX idx_twitter_tweet_y2014m05_published_at ON public.twitter_tweet_y2014m05(to_timestamptz_immutable(object_payload->>'created_at'));
CREATE INDEX idx_twitter_tweet_y2014m05_user_id ON public.twitter_tweet_y2014m05(((object_payload->'user'->>'id')::bigint));

CREATE TABLE public.twitter_tweet_y2014m04 (
    CHECK (imported_at >= DATE '2014-04-01' AND imported_at < DATE '2014-05-01')
) INHERITS (public.twitter_tweet);
CREATE INDEX idx_twitter_tweet_y2014m04_imported_at ON public.twitter_tweet_y2014m04(imported_at);
CREATE INDEX idx_twitter_tweet_y2014m04_tweet_id ON public.twitter_tweet_y2014m04(((object_payload->>'id')::bigint));
CREATE INDEX idx_twitter_tweet_y2014m04_published_at ON public.twitter_tweet_y2014m04(to_timestamptz_immutable(object_payload->>'created_at'));
CREATE INDEX idx_twitter_tweet_y2014m04_user_id ON public.twitter_tweet_y2014m04(((object_payload->'user'->>'id')::bigint));

CREATE TABLE public.twitter_tweet_y2014m03 (
    CHECK (imported_at >= DATE '2014-03-01' AND imported_at < DATE '2014-04-01')
) INHERITS (public.twitter_tweet);
CREATE INDEX idx_twitter_tweet_y2014m03_imported_at ON public.twitter_tweet_y2014m03(imported_at);
CREATE INDEX idx_twitter_tweet_y2014m03_tweet_id ON public.twitter_tweet_y2014m03(((object_payload->>'id')::bigint));
CREATE INDEX idx_twitter_tweet_y2014m03_published_at ON public.twitter_tweet_y2014m03(to_timestamptz_immutable(object_payload->>'created_at'));
CREATE INDEX idx_twitter_tweet_y2014m03_user_id ON public.twitter_tweet_y2014m03(((object_payload->'user'->>'id')::bigint));

CREATE TABLE public.twitter_tweet_y2014m02 (
    CHECK (imported_at >= DATE '2014-02-01' AND imported_at < DATE '2014-03-01')
) INHERITS (public.twitter_tweet);
CREATE INDEX idx_twitter_tweet_y2014m02_imported_at ON public.twitter_tweet_y2014m02(imported_at);
CREATE INDEX idx_twitter_tweet_y2014m02_tweet_id ON public.twitter_tweet_y2014m02(((object_payload->>'id')::bigint));
CREATE INDEX idx_twitter_tweet_y2014m02_published_at ON public.twitter_tweet_y2014m02(to_timestamptz_immutable(object_payload->>'created_at'));
CREATE INDEX idx_twitter_tweet_y2014m02_user_id ON public.twitter_tweet_y2014m02(((object_payload->'user'->>'id')::bigint));

CREATE TABLE public.twitter_tweet_y2014m01 (
    CHECK (imported_at >= DATE '2014-01-01' AND imported_at < DATE '2014-02-01')
) INHERITS (public.twitter_tweet);
CREATE INDEX idx_twitter_tweet_y2014m01_imported_at ON public.twitter_tweet_y2014m01(imported_at);
CREATE INDEX idx_twitter_tweet_y2014m01_tweet_id ON public.twitter_tweet_y2014m01(((object_payload->>'id')::bigint));
CREATE INDEX idx_twitter_tweet_y2014m01_published_at ON public.twitter_tweet_y2014m01(to_timestamptz_immutable(object_payload->>'created_at'));
CREATE INDEX idx_twitter_tweet_y2014m01_user_id ON public.twitter_tweet_y2014m01(((object_payload->'user'->>'id')::bigint));

CREATE TABLE public.twitter_tweet_y2013m12 (
    CHECK (imported_at >= DATE '2013-12-01' AND imported_at < DATE '2014-01-01')
) INHERITS (public.twitter_tweet);
CREATE INDEX idx_twitter_tweet_y2013m12_imported_at ON public.twitter_tweet_y2013m12(imported_at);
CREATE INDEX idx_twitter_tweet_y2013m12_tweet_id ON public.twitter_tweet_y2013m12(((object_payload->>'id')::bigint));
CREATE INDEX idx_twitter_tweet_y2013m12_published_at ON public.twitter_tweet_y2013m12(to_timestamptz_immutable(object_payload->>'created_at'));
CREATE INDEX idx_twitter_tweet_y2013m12_user_id ON public.twitter_tweet_y2013m12(((object_payload->'user'->>'id')::bigint));

CREATE TABLE public.twitter_tweet_y2013m11 (
    CHECK (imported_at >= DATE '2013-11-01' AND imported_at < DATE '2013-12-01')
) INHERITS (public.twitter_tweet);
CREATE INDEX idx_twitter_tweet_y2013m11_imported_at ON public.twitter_tweet_y2013m11(imported_at);
CREATE INDEX idx_twitter_tweet_y2013m11_tweet_id ON public.twitter_tweet_y2013m11(((object_payload->>'id')::bigint));
CREATE INDEX idx_twitter_tweet_y2013m11_published_at ON public.twitter_tweet_y2013m11(to_timestamptz_immutable(object_payload->>'created_at'));
CREATE INDEX idx_twitter_tweet_y2013m11_user_id ON public.twitter_tweet_y2013m11(((object_payload->'user'->>'id')::bigint));

CREATE OR REPLACE FUNCTION public.twitter_tweet_insert_trigger()
  RETURNS TRIGGER
AS $$
BEGIN
    IF (NEW.imported_at >= DATE '2014-12-01' AND NEW.imported_at < DATE '2015-01-01' ) THEN
        INSERT INTO public.twitter_tweet_y2014m12 VALUES (NEW.*);
    ELSIF (NEW.imported_at >= DATE '2014-11-01' AND NEW.imported_at < DATE '2014-12-01' ) THEN
        INSERT INTO public.twitter_tweet_y2014m11 VALUES (NEW.*);
    ELSIF (NEW.imported_at >= DATE '2014-10-01' AND NEW.imported_at < DATE '2014-11-01' ) THEN
        INSERT INTO public.twitter_tweet_y2014m10 VALUES (NEW.*);
    ELSIF (NEW.imported_at >= DATE '2014-09-01' AND NEW.imported_at < DATE '2014-10-01' ) THEN
        INSERT INTO public.twitter_tweet_y2014m09 VALUES (NEW.*);
    ELSIF (NEW.imported_at >= DATE '2014-08-01' AND NEW.imported_at < DATE '2014-09-01' ) THEN
        INSERT INTO public.twitter_tweet_y2014m08 VALUES (NEW.*);
    ELSIF (NEW.imported_at >= DATE '2014-07-01' AND NEW.imported_at < DATE '2014-08-01' ) THEN
        INSERT INTO public.twitter_tweet_y2014m07 VALUES (NEW.*);
    ELSIF (NEW.imported_at >= DATE '2014-06-01' AND NEW.imported_at < DATE '2014-07-01' ) THEN
        INSERT INTO public.twitter_tweet_y2014m06 VALUES (NEW.*);
    ELSIF (NEW.imported_at >= DATE '2014-05-01' AND NEW.imported_at < DATE '2014-06-01' ) THEN
        INSERT INTO public.twitter_tweet_y2014m05 VALUES (NEW.*);
    ELSIF (NEW.imported_at >= DATE '2014-04-01' AND NEW.imported_at < DATE '2014-05-01' ) THEN
        INSERT INTO public.twitter_tweet_y2014m04 VALUES (NEW.*);
    ELSIF (NEW.imported_at >= DATE '2014-03-01' AND NEW.imported_at < DATE '2014-04-01' ) THEN
        INSERT INTO public.twitter_tweet_y2014m03 VALUES (NEW.*);
    ELSIF (NEW.imported_at >= DATE '2014-02-01' AND NEW.imported_at < DATE '2014-03-01' ) THEN
        INSERT INTO public.twitter_tweet_y2014m02 VALUES (NEW.*);
    ELSIF (NEW.imported_at >= DATE '2014-01-01' AND NEW.imported_at < DATE '2014-02-01' ) THEN
        INSERT INTO public.twitter_tweet_y2014m01 VALUES (NEW.*);
    ELSIF (NEW.imported_at >= DATE '2013-12-01' AND NEW.imported_at < DATE '2014-01-01' ) THEN
        INSERT INTO public.twitter_tweet_y2013m12 VALUES (NEW.*);
    ELSIF (NEW.imported_at >= DATE '2013-11-01' AND NEW.imported_at < DATE '2013-12-01' ) THEN
        INSERT INTO public.twitter_tweet_y2013m11 VALUES (NEW.*);
    ELSE
        RAISE EXCEPTION '``imported_at`` out of range.  Fix the public.twitter_tweet_insert_trigger() function!';
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER insert_twitter_tweet_trigger
    BEFORE INSERT ON public.twitter_tweet
    FOR EACH ROW EXECUTE PROCEDURE public.twitter_tweet_insert_trigger();


CREATE TABLE public.twitter_user (
    object_id       text        PRIMARY KEY,
    created_at      timestamp   NOT NULL DEFAULT now(),
    updated_at      timestamp   NOT NULL DEFAULT now(),
    object_payload  json
);

CREATE INDEX idx_twitter_user_screen_name ON public.twitter_user((object_payload->>'screen_name'));

GRANT USAGE ON SCHEMA public TO dbo, dbw;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO dbo, dbw;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO dbo, dbw;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO dbo, dbw;
