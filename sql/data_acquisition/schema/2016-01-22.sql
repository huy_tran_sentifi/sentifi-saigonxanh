CREATE SCHEMA gnip;

CREATE TABLE gnip.g_rule (
    rule_id         serial    PRIMARY KEY,
    status          smallint  NOT NULL DEFAULT 2,
    created_at      timestamp NOT NULL DEFAULT now(),
    updated_at      timestamp NOT NULL DEFAULT now(),
    on_at           timestamp,
    off_at          timestamp,
    created_by      text,
    on_by           text,
    off_by          text,
    team            text,
    rule_name       text,
    rule_use_case   text,
    rule            text,
    priority        smallint
);
COMMENT ON TABLE gnip.g_rule IS 'GNIP rule''s definition';
COMMENT ON COLUMN gnip.g_rule.status IS '0: Removed, 1: ON, 2: New, 3: OFF By Human, 4: OFF By Machine';
COMMENT ON COLUMN gnip.g_rule.created_by IS 'Email in lowercase of the creator';
COMMENT ON COLUMN gnip.g_rule.on_by IS 'Email in lowercase of the last person who turns it ON';
COMMENT ON COLUMN gnip.g_rule.off_by IS 'Email in lowercase of the last person who turns it OFF';
COMMENT ON COLUMN gnip.g_rule.team IS 'Lowercase, this can be either ''profile'', ''topic'' or ''event''';
COMMENT ON COLUMN gnip.g_rule.rule_use_case IS 'Group of rules';
COMMENT ON COLUMN gnip.g_rule.priority IS 'Priority of rule. 1: High, 2: Medium, 3: Low';
ALTER TABLE gnip.g_rule ADD CONSTRAINT g_rule_team_chk CHECK (team IN ('event', 'profile', 'topic'));

CREATE TABLE twitter.user_by_gnip_rule (
    user_id         bigint,
    created_at      timestamp   NOT NULL DEFAULT now(),
    rule_id         int         NOT NULL REFERENCES gnip.g_rule ON DELETE CASCADE,
    current_state   smallint,
    UNIQUE (user_id, rule_id)
);
COMMENT ON COLUMN twitter.user_by_gnip_rule.current_state IS 'Current State when an user is found by a rule. 1: New, 2: Candidate, 3: Publisher';
CREATE INDEX idx_user_by_gnip_rule_created_at ON twitter.user_by_gnip_rule USING btree(created_at);


GRANT USAGE ON SCHEMA public, finance_base, gnip, twitter TO dbo, dbw;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, finance_base, gnip, twitter TO dbo, dbw;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, finance_base, gnip, twitter TO dbo, dbw;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, finance_base, gnip, twitter TO dbo, dbw;

ALTER ROLE dbo SET search_path = public, finance_base, gnip, twitter;
ALTER ROLE dbw SET search_path = public, finance_base, gnip, twitter;
