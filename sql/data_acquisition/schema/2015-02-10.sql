CREATE SCHEMA twitter;

CREATE TABLE twitter.source (
    source_id   smallserial PRIMARY KEY,
    source_name text
);

CREATE TABLE twitter.tw_user (
    user_id         bigint      PRIMARY KEY,
    created_at      timestamp   NOT NULL DEFAULT now(),
    updated_at      timestamp   NOT NULL DEFAULT now(),
    payload         json,
    language_code   text
);
COMMENT ON COLUMN twitter.tw_user.language_code IS '2 characters, lowercase language code conforming to ISO 639-1';
CREATE INDEX idx_tw_user_created_at ON twitter.tw_user USING btree(created_at);
CREATE INDEX idx_tw_user_updated_at ON twitter.tw_user USING btree(updated_at);

CREATE TABLE twitter.list (
    list_id     bigint      PRIMARY KEY,
    created_at  timestamp   NOT NULL DEFAULT now(),
    updated_at  timestamp   NOT NULL DEFAULT now(),
    payload     json
);

CREATE TABLE twitter.user_list (
    user_id     bigint      NOT NULL,
    created_at  timestamp   NOT NULL DEFAULT now(),
    list_id     bigint      REFERENCES twitter.list ON DELETE CASCADE,
    label       text        NOT NULL,
    UNIQUE (user_id, list_id, label)
);
COMMENT ON COLUMN twitter.user_list.label IS 'represents the relationship between Twitter User and List, accepted values are:
"c": create,
"m": member of,
"s": subscribe to';

CREATE TABLE twitter.user_isocode (
    user_id     bigint      NOT NULL REFERENCES twitter.tw_user ON DELETE CASCADE,
    created_at  timestamp   NOT NULL DEFAULT now(),
    updated_at  timestamp   NOT NULL DEFAULT now(),
    isocode     text,
    UNIQUE (user_id)
);
COMMENT ON COLUMN twitter.user_isocode.isocode IS '2 characters, uppercase country code conforming to ISO 3166-1';

CREATE TABLE twitter.user_klout_score (
    user_id     bigint      NOT NULL REFERENCES twitter.tw_user ON DELETE CASCADE,
    created_at  timestamp   NOT NULL DEFAULT now(),
    updated_at  timestamp   NOT NULL DEFAULT now(),
    payload     json
);
CREATE INDEX idx_user_klout_score_user_id ON twitter.user_klout_score USING btree(user_id);

CREATE TABLE twitter.user_source (
    user_id         bigint      NOT NULL,
    created_at      timestamp   NOT NULL DEFAULT now(),
    source_id       smallint    NOT NULL REFERENCES twitter.source ON DELETE SET NULL,
    UNIQUE (user_id, source_id)
);
CREATE INDEX idx_user_source_created_at ON twitter.user_source USING btree(created_at);
CREATE INDEX idx_user_source_source_id ON twitter.user_source USING btree(source_id);

CREATE TABLE twitter.user_tracking (
    user_id                 bigint      NOT NULL REFERENCES twitter.tw_user ON DELETE CASCADE,
    status                  integer     NOT NULL DEFAULT 1,
    created_at              timestamp   NOT NULL DEFAULT now(),
    updated_at              timestamp   NOT NULL DEFAULT now(),
    is_classified           boolean     NULL,
    is_qualified            boolean     NULL,
    is_exported             boolean     NULL,
    crawled_tweet_count     smallint,
    last_crawled_at         timestamp,
    num_score_cr            integer,
    avg_score_cr            numeric,
    is_qualified_by_cashtag boolean
);
COMMENT ON COLUMN twitter.user_tracking.status IS '
0: 200 messages were processed and available in ES
1: is publisher
2: disqualified with no description
3: disqualified
4: lack messages
5: to be classified
6: to be qualified
7: unable to qualify
8: qualified by classes
9: qualified by content relevance
10: qualified by named entities
11: qualified by guru following
12: qualified by seed following
13: qualified by following seed
14: qualified by cashtags
15: qualified by guru mention
16: qualified by sentifi mention
17: 200 messages were crawled and waiting to process
18: invalid twitter id
19: protected twitter id (only approved follower could see this user timeline). Sometimes, invalid twitter id also has this status
20: profile is crawled
21: profile to be crawled (once crawled, this will become 20)
';
COMMENT ON COLUMN twitter.user_tracking.is_qualified IS 'this candidate is qualified to import';
COMMENT ON COLUMN twitter.user_tracking.is_classified IS 'this candidate is classified by machine';
COMMENT ON COLUMN twitter.user_tracking.is_exported IS 'this candidate is exported to be audited by analysts';
CREATE UNIQUE INDEX idx_user_tracking_user_id ON twitter.user_tracking USING btree(user_id);
CREATE INDEX idx_user_tracking_created_at ON twitter.user_tracking USING btree(created_at);
CREATE INDEX idx_user_tracking_updated_at ON twitter.user_tracking USING btree(updated_at);

CREATE TABLE twitter.user_classified_data (
    user_id         bigint      PRIMARY KEY,
    name            text        NULL,
    screen_name     text        NOT NULL,
    created_at      timestamp   NOT NULL DEFAULT now(),
    location        text        NULL,
    description     text        NULL,
    occupations     text[]      NULL,
    func_roles      text[]      NULL,
    prediction      text        NOT NULL,
    confidence      real        NOT NULL,
    entities        json        NOT NULL,
    other_meta_data json        NOT NULL
);

CREATE TABLE twitter.publishers_reclassified_data (
    user_id         bigint      PRIMARY KEY,
    name            text        NULL,
    screen_name     text        NOT NULL,
    inserted_at     timestamp   NOT NULL DEFAULT now(),
    created_at      timestamp   NOT NULL DEFAULT now(),
    location        text        NULL,
    description     text        NULL,
    occupations     text[]      NULL,
    func_roles      text[]      NULL,
    prediction      text        NOT NULL,
    confidence      real        NOT NULL,
    entities        json        NOT NULL,
    other_meta_data json        NOT NULL
);
CREATE INDEX idx_publishers_reclassified_data_created_at ON twitter.publishers_reclassified_data USING btree(created_at);

CREATE SCHEMA report;
CREATE TABLE report.log_change_publisher (
    sns_id          bigint      NOT NULL,
    status          integer     NOT NULL,
    updated_at      timestamp   NOT NULL DEFAULT now(),
    new             text        NULL,
    old             text        NULL,
    PRIMARY KEY(sns_id, status)
);
COMMENT ON COLUMN report.log_change_publisher.status IS '0: description; 1: location; 3: name; 4: url; 5: profile_image';

CREATE TABLE twitter.follower (
    user_id     bigint,
    follower_id bigint,
    status      smallint,
    created_at  timestamp   NOT NULL DEFAULT now(),
    updated_at  timestamp   NOT NULL DEFAULT now(),
    UNIQUE (user_id, follower_id)
);


GRANT USAGE ON SCHEMA public, twitter TO dbo, dbw;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, twitter TO dbo, dbw;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public, twitter TO dbo, dbw;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public, twitter TO dbo, dbw;

ALTER ROLE dbo SET search_path = public, twitter;
ALTER ROLE dbw SET search_path = public, twitter;
