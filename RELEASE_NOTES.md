2.2.0
=====
May 24, 2016

Changes
-------
* Change to new version convention
* Add table `user_subscribed_product`
* [[BAC-65] Update README for sentifi-saigonxanh](https://sentifi.atlassian.net/browse/BAC-65)
* [[BAC-63] Add `cover_image_url` for table `named_entity`](https://sentifi.atlassian.net/browse/BAC-63)
* Add Moreover's columns for table `exchange`, `ticker1`
* [Update data schema to track shared contents & activity](https://podio.com/sentificom/1-sentifi-score/apps/dev-tasks/items/63)


0.2.1
=====
February 23, 2016

Changes
-------
* `kpi_statistics.py` can now generate report and then upload to Podio
* Add data schema to store GNIP rules and track Twitter's users captured by GNIP
rule
* Add more indexes to `da0` for monitor/report queries

Fixes
-----
* Fix some schema/data after Publisher ID Migration project


0.2.0
=====
December 09, 2015

Changes
-------
* Code changes to adapt the new schema after Publisher ID Migration project
* Update tool that archives MongoDB data to S3


0.1.1
=====
September 09, 2015

Bug Fixes
---------
* S3 path is badly generated from the backup scripts due to refactor


0.1.0
=====
September 08, 2015

Features
--------
* Archive tool for backup MongoDB data to S3
* Deployment tool using Fabric